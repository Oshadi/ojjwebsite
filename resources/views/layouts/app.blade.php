<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <link href="{{ asset('template_assets/assets/images/favicon/favicon.png') }} " rel="icon">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:400,500,700%7cPoppins:400,600,700&display=swap">

    <link rel="stylesheet" href="{{ asset('template_assets/assets/css/libraries.css') }}">
    <link rel="stylesheet" href="{{ asset('template_assets/assets/css/style.css') }}">

    {{--  Choices CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom_assets/libraries/choices/choises.min.css') }}">

    {{--  Custom CSS  --}}
    <link rel="stylesheet" href="{{ asset('custom_assets/css/custom-style.css') }}">
    <link rel="stylesheet" href="{{ asset('custom_assets/css/custom-responsive.css') }}">

    {{-- Laravel Mix - CSS File --}}
    {{-- <link rel="stylesheet" href="{{ mix('css/web.css') }}"> --}}

</head>
<body>
@yield('content')

{{-- Laravel Mix - JS File --}}
{{-- <script src="{{ mix('js/web.js') }}"></script> --}}

<script src="{{ asset('template_assets/assets/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('template_assets/assets/js/plugins.js') }}"></script>
<script src="{{ asset('template_assets/assets/js/main.js') }}"></script>

{{--  Choices JS  --}}
<script src="{{ asset('custom_assets/libraries/choices/choices.min.js') }}"></script>

{{-- Custom JS --}}
<script src="{{ asset('custom_assets/js/custom-script.js') }}"></script>

</body>
</html>
