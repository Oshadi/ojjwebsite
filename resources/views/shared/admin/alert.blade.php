@if (session('status'))
    {{--    <div class="alert bg-success alert-styled-left">--}}
    {{--        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span--}}
    {{--                class="sr-only">Close</span></button>--}}
    {{--        {{session('status')}}--}}
    {{--    </div>--}}
    <script>
        swal("success","{{session('status')}}");
    </script>
@endif

@if (session('error'))
    <script>
        swal("Notify!", "{{session('error')}}", "warning");
    </script>
    {{--    <div class="alert bg-danger alert-styled-left">--}}
    {{--        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span--}}
    {{--                class="sr-only">Close</span></button>--}}
    {{--        {{session('error')}}--}}
    {{--    </div>--}}
@endif

@if ($errors->any())
    <div class="alert alert-danger alert-styled-left alert-bordered">
        <ul>
            @foreach ($errors->all() as $error)
                <li>
                    <button type="button" class="close" data-dismiss="alert">
                        <span>&times;</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold"></span> {{ $error }}.
                </li>
            @endforeach
        </ul>
    </div>
@endif

{{--<script>--}}
{{--    $(function () {--}}
{{--        setTimeout(function () {--}}
{{--            $(".alert").fadeOut(1500);--}}
{{--        }, 5000)--}}
{{--    })--}}
{{--</script>--}}
