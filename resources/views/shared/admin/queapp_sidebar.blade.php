<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Main</div>
                    <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item" {{ Request::route()->getName() == 'dashboard' ? 'class=active' : '' }}>
                    <a href="{{ route('dashboard') }}" class="nav-link ">
                        <i class="icon-home4"></i>
                        <span>Home</span>
                    </a>
                </li>


            {{--                <li class="nav-item nav-item-submenu">--}}
            {{--                    <a href="#" class="nav-link"><i class="icon-user"></i> <span>Employee Management</span></a>--}}

            {{--                    <ul class="nav nav-group-sub" data-submenu-title="Employee Management">--}}
            {{--                        <li class="nav-item nav-item-submenu">--}}
            {{--                            <a href="#" class="nav-link">Configuration</a>--}}
            {{--                            <ul class="nav nav-group-sub" data-submenu-title="Configuration">--}}
            {{--                                <li class="nav-item"><a href="#" class="nav-link">Custom Fields</a></li>--}}
            {{--                                <li class="nav-item"><a href="#" class="nav-link">Manage Employee Master</a></li>--}}
            {{--                            </ul>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item"><a href="#" class="nav-link">Employee List</a></li>--}}
            {{--                        <li class="nav-item"><a href="#" class="nav-link">Add New Employee</a></li>--}}
            {{--                    </ul>--}}
            {{--                </li>--}}

            {{--                <li class="nav-item nav-item-submenu">--}}
            {{--                    <a href="#" class="nav-link"><i class="icon-sort-time-asc"></i> <span>Time Management</span></a>--}}
            {{--                    <ul class="nav nav-group-sub" data-submenu-title="Employee Management">--}}
            {{--                        <li class="nav-item {{ (Request::route()->getName() == 'hr.daytype.index' || Request::route()->getName() == 'hr.daytype.create' || Request::route()->getName() == 'hr.daytype.edit') ? 'active' : '' }}">--}}
            {{--                            <a href="{{ route('hr.daytype.index') }}" class="nav-link">Day Type</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item  {{ (Request::route()->getName() == 'hr.shift.index' || Request::route()->getName() == 'hr.shift.create' || Request::route()->getName() == 'hr.shift.edit') ? 'active' : '' }}">--}}
            {{--                            <a href="{{ route('hr.shift.index') }}" class="nav-link">Manage Shift</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item {{ (Request::route()->getName() == 'hr.roster.index' || Request::route()->getName() == 'hr.roster.create' || Request::route()->getName() == 'hr.roster.edit') ? 'active' : '' }}">--}}
            {{--                            <a href="{{ route('hr.roster.index') }}" class="nav-link">Manage Roster</a>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}
            {{--                </li>--}}
            {{--                <li class="nav-item nav-item-submenu">--}}
            {{--                    <a href="#" class="nav-link"><i class="icon-hour-glass2"></i> <span>Leave Management</span></a>--}}
            {{--                    <ul class="nav nav-group-sub" data-submenu-title="Employee Management">--}}
            {{--                        <li class="nav-item {{ (Request::route()->getName() == 'hr.leavetype.index' || Request::route()->getName() == 'hr.leavetype.create' || Request::route()->getName() == 'hr.leavetype.edit') ? 'active' : '' }}">--}}
            {{--                            <a href="{{ route('hr.leavetype.index') }}" class="nav-link">Leave Type</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item {{ (Request::route()->getName() == 'hr.leaveperiod.index' || Request::route()->getName() == 'hr.leaveperiod.create' || Request::route()->getName() == 'hr.leaveperiod.edit') ? 'active' : '' }}">--}}
            {{--                            <a href="{{ route('hr.leaveperiod.index') }}" class="nav-link">Leave Period</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item {{ (Request::route()->getName() == 'hr.leavegroup.index' || Request::route()->getName() == 'hr.leavegroup.create' || Request::route()->getName() == 'hr.leavegroup.edit') ? 'active' : '' }}">--}}
            {{--                            <a href="{{ route('hr.leavegroup.index') }}" class="nav-link">Leave Group</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item {{ (Request::route()->getName() == 'hr.leaverule.index' || Request::route()->getName() == 'hr.leaverule.create' || Request::route()->getName() == 'hr.leaverule.edit') ? 'active' : '' }}">--}}
            {{--                            <a href="{{ route('hr.leaverule.index') }}" class="nav-link">Leave Rules</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item nav-item-submenu">--}}
            {{--                            <a href="#" class="nav-link">Leave Request</a>--}}
            {{--                            <ul class="nav nav-group-sub" data-submenu-title="Configuration">--}}
            {{--                                <li class="nav-item {{ (Request::route()->getName() == 'hr.leave-request.index' || Request::route()->getName() == 'hr.leave-request.create' || Request::route()->getName() == 'hr.leave-request.edit') ? 'active' : '' }}">--}}
            {{--                                    <a href="{{ route('hr.leave-request.index') }}" class="nav-link">Calendar View</a>--}}
            {{--                                </li>--}}
            {{--                                <li class="nav-item {{ (Request::route()->getName() == 'hr.leave-request.list-view' || Request::route()->getName() == 'hr.leave-request.create-leave' || Request::route()->getName() == 'hr.leave-request.edit') ? 'active' : '' }}">--}}
            {{--                                    <a href="{{ route('hr.leave-request.list-view') }}" class="nav-link">Manage Leave--}}
            {{--                                        Request</a>--}}
            {{--                                </li>--}}
            {{--                            </ul>--}}
            {{--                        </li>--}}

            {{--                    </ul>--}}
            {{--                </li>--}}

            {{--                <li class="nav-item nav-item-submenu">--}}
            {{--                    <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Authorization</span></a>--}}

            {{--                    <ul class="nav nav-group-sub" data-submenu-title="Authorization">--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/auth/role-type' ? 'class=active' : '' }}>--}}
            {{--                            <a href="{{ route('auth.roletype.index') }}" class="nav-link">Role Types</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/auth/role' ? 'class=active' : '' }}>--}}
            {{--                            <a href="{{ route('auth.role.index') }}" class="nav-link">Roles</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/auth/permission' ? 'class=active' : '' }}>--}}
            {{--                            <a href="{{ route('auth.permission.index') }}" class="nav-link">User Permissions</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/auth/user' ? 'class=active' : '' }}>--}}
            {{--                            <a href="{{ route('auth.user.index') }}" class="nav-link">Users</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/auth/module' ? 'class=active' : '' }}>--}}
            {{--                            <a href="{{ route('auth.module.index') }}" class="nav-link">Modules</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/auth/sub-module' ? 'class=active' : '' }}>--}}
            {{--                            <a href="{{ route('auth.submodule.index') }}" class="nav-link">Submodule</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/auth/sub-module-item' ? 'class=active' : '' }}>--}}
            {{--                            <a href="{{ route('auth.submoduleitem.index') }}" class="nav-link">Submodule Item</a>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}
            {{--                </li>--}}

            {{--                <li class="nav-item nav-item-submenu">--}}
            {{--                    <a href="#" class="nav-link"><i class="icon-accessibility"></i> <span>System</span></a>--}}

            {{--                    <ul class="nav nav-group-sub" data-submenu-title="System">--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/system/country' ? 'class=active' : '' }}><a--}}
            {{--                                href="{{ route('system.country.index') }}" class="nav-link">Country</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/system/province' ? 'class=active' : '' }}><a--}}
            {{--                                href="{{ route('system.province.index') }}" class="nav-link">Province</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/system/nationality' ? 'class=active' : '' }}><a--}}
            {{--                                href="{{ route('system.nationality.index') }}" class="nav-link">Nationality</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/system/ethnicity' ? 'class=active' : '' }}><a--}}
            {{--                                href="{{ route('system.ethnicity.index') }}" class="nav-link">Ethnicity</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-item" {{ Request::path() == '/system/immigration' ? 'class=active' : '' }}><a--}}
            {{--                                href="{{ route('system.immigration.index') }}" class="nav-link">Immigration</a>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}
            {{--                </li>--}}
            <!-- /main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Visitors Management</div>
                    <i class="icon-switch" title="Logout"></i>
                </li>
                <li class="nav-item">
                    <a href="{{route('queapp.visitor.dashboard')}}"
                       class="nav-link {{ (Request::route()->getName() == 'queapp.visitor.dashboard') ? ' active' : '' }}">
                        <i class="icon-meter-slow"></i><span>Dashboard</span>
                    </a>
                    <a href="{{route('queapp.visitor.index')}}"
                       class="nav-link {{ (Request::route()->getName() == 'queapp.visitor.index') ? ' active' : '' }}">
                        <i class="icon-users"></i><span>Manage Visitor</span>
                    </a>
                    <a href="{{route('queapp.visitor.create')}}"
                       class="nav-link {{ (Request::route()->getName() == 'queapp.visitor.create') ? ' active' : '' }}">
                        <i class="icon-user-plus"></i><span>Register Visitor</span>
                    </a>
                </li>

                <!-- Forms -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Logout</div>
                    <i class="icon-switch" title="Logout"></i></li>
                <li class="nav-item">
                    <a href="#" class="nav-link" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                        <i class="icon-switch2"></i> <span>Logout</span>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->
