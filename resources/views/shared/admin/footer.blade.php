<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 d-flex justify-content-center border-0 custom-footer">
    <div class="text-muted text-center py-1 border-0">
        <span>&copy; <?php echo date('Y') ; ?>. All rights reserved. <?php echo env('APP_NAME', 'H2bis') ?> (<?php echo 'v '. env('APP_VERSION', '2.0.0') ?>)</span>
    </div>
</div>


