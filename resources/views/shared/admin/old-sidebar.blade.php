<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Main</div>
                    <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item" {{ Request::route()->getName() == 'dashboard' ? 'class=active' : '' }}>
                    <a href="{{ route('dashboard') }}" class="nav-link ">
                        <i class="icon-home4"></i>
                        <span>Home</span>
                    </a>
                </li>

                @foreach(\Modules\Auth\Entities\Module::where('status',1)->get() as $module)
                    @php
                        $module_sub_module_route_name[]=array();
                         foreach ($module->submodules as $sM) {
                                 foreach ($sM->submoduleItems as $sMItem) {
                                      array_push($module_sub_module_route_name,$sMItem->route);
                                 }
                         }
                    @endphp
                    <li class="nav-item nav-item-submenu {{  in_array(Request::route()->getName(), $module_sub_module_route_name )   ? 'nav-item-expanded nav-item-open' : '' }}">

                        <a href="#" class="nav-link"><i class="{{$module->icon}}"></i>
                            <span>{{$module->name}} </span></a>
                        <ul class="nav nav-group-sub" data-submenu-title="{{ $module->name }}">
                            @php
                                $module_sub_module_route_name=[];
                            @endphp
                            @foreach($module->submodules->where('status',1)  as $subModule)
                                @php
                                    $sub_module_route_name[]=array();
                                   foreach ($subModule->submoduleItems as $sMItem) {
                                       array_push( $sub_module_route_name,$sMItem->route);
                                   }
                                @endphp

                                <li class="nav-item nav-item-submenu  {{ in_array(Request::route()->getName(),$sub_module_route_name) ? 'nav-item-expanded nav-item-open' : '' }} ">
                                    <a href="#"
                                       class="nav-link">
                                        <i class="{{$subModule->icon}}"></i>
                                        <span>{{$subModule->name}}</span></a>

                                    <ul class="nav nav-group-sub" data-submenu-title="{{$subModule->name}}">
                                        @php
                                            $sub_module_route_name=[]
                                        @endphp
                                        @foreach($subModule->submoduleItems->where('status',1)  as $subModuleItem)
                                            @php
                                                if(checkHasPermission($subModuleItem->name." VIEW")){
                                            @endphp
                                            <li class="nav-item ">
                                                <a href="{{ route($subModuleItem->route) }}"
                                                   class="nav-link {{ Request::route()->getName() == $subModuleItem->route? ' active' : '' }}">{{$subModuleItem->name}}</a>
                                            </li>
                                            @php
                                                 }
                                            @endphp
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                    </li>

                @endforeach


                {{--                <li class="nav-item nav-item-submenu  {{ (  Request::route()->getName() == 'auth.role.index' || Request::route()->getName() == 'auth.user.index') ? 'nav-item-expanded nav-item-open' : '' }} ">--}}
                {{--                    <a href="#"--}}
                {{--                       class="nav-link {{ ( Request::route()->getName() == 'auth.roletype.create' || Request::route()->getName() == 'auth.roletype.edit') ? 'active' : '' }} "><i--}}
                {{--                            class="icon-user"></i> <span>User Management</span></a>--}}
                {{--                    <ul class="nav nav-group-sub" data-submenu-title="User Management">--}}
                {{--                        --}}{{--                        <li class="nav-item">--}}
                {{--                        --}}{{--                            <a href="{{ route('auth.roletype.index') }}"--}}
                {{--                        --}}{{--                               class="nav-link {{ (Request::route()->getName() == 'auth.roletype.index' || Request::route()->getName() == 'auth.roletype.create' || Request::route()->getName() == 'auth.roletype.edit') ? 'active' : '' }}">Role--}}
                {{--                        --}}{{--                                Types</a>--}}
                {{--                        --}}{{--                        </li>--}}
                {{--                        <li class="nav-item">--}}
                {{--                            <a href="{{ route('auth.role.index') }}"--}}
                {{--                               class="nav-link {{ (Request::route()->getName() == 'auth.role.index') ? 'active' : '' }}">User--}}
                {{--                                Roles</a>--}}
                {{--                        </li>--}}
                {{--                        <li class="nav-item">--}}
                {{--                            <a href="{{ route('auth.user.index') }}"--}}
                {{--                               class="nav-link {{ (Request::route()->getName() == 'auth.user.index' ) ? 'active' : '' }}">Users</a>--}}
                {{--                        </li>--}}
                {{--                        <li class="nav-item" {{ (Request::route()->getName() == 'auth.permission.index' || Request::route()->getName() == 'auth.permission.create' || Request::route()->getName() == 'auth.permission.edit') ? 'class=active' : '' }}>--}}
                {{--                            <a href="{{ route('auth.permission.index') }}" class="nav-link">User Permissions</a>--}}
                {{--                        </li>--}}
                {{--                    </ul>--}}
                {{--                </li>--}}
                {{--                <li class="nav-item nav-item-submenu {{ (Request::route()->getName() == 'system.location.index') || (Request::route()->getName() == 'system.company.index') ||  (Request::route()->getName() == 'system.salarycomponent.index') ||(Request::route()->getName() == 'system.branch.index' ) ||(Request::route()->getName() == 'system.bank.index' ) ||  (Request::route()->getName() == 'system.paytype.index')||(Request::route()->getName() == 'system.religion.index')||  (Request::route()->getName() == 'system.section.index')||(Request::route()->getName() == 'system.membership.index') ||(Request::route()->getName() == 'system.language.index') ||(Request::route()->getName() == 'system.certification.index') ||(Request::route()->getName() == 'system.education.index') ||(Request::route()->getName() == 'system.skill.index')|| (Request::route()->getName() == 'system.employmenttype.index')||(Request::route()->getName() == 'system.paygrade.index')||(Request::route()->getName() == 'system.jobcategory.index')||(Request::route()->getName() == 'system.division.index')|| (Request::route()->getName() == 'system.designation.index')||(Request::route()->getName() == 'system.department.index' ) ||(Request::route()->getName() == 'system.country.index' ) || (Request::route()->getName() == 'system.province.index')  || (Request::route()->getName() == 'system.nationality.index')|| (Request::route()->getName() == 'system.ethnicity.index') || (Request::route()->getName() == 'system.immigration.index') ? 'nav-item-expanded nav-item-open' : '' }}">--}}
                {{--                    <a href="#" class="nav-link"><i class="icon-user-tie"></i> <span>Master Data</span></a>--}}

                {{--                    <ul class="nav nav-group-sub" data-submenu-title="Master Data">--}}

                {{--                        <li class="nav-item nav-item-submenu {{ (Request::route()->getName() == 'system.religion.index' )  || (Request::route()->getName() == 'system.country.index' ) || (Request::route()->getName() == 'system.province.index')|| (Request::route()->getName() == 'system.nationality.index')|| (Request::route()->getName() == 'system.ethnicity.index')|| (Request::route()->getName() == 'system.immigration.index')? 'nav-item-expanded nav-item-open' : '' }}">--}}
                {{--                            <a href="#" class="nav-link"><span>Metadata Management</span></a>--}}
                {{--                            <ul class="nav nav-group-sub" data-submenu-title="Metadata Management">--}}
                {{--                                <li class="nav-item ">--}}
                {{--                                    <a href="{{ route('system.country.index') }}"--}}
                {{--                                       class="nav-link  {{ (Request::route()->getName() == 'system.country.index') ? 'active' : '' }}">Country</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item ">--}}
                {{--                                    <a href="{{ route('system.province.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.province.index') ? 'active' : '' }}">Province</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.nationality.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.nationality.index') ? 'active' : '' }}">Nationality</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.ethnicity.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.ethnicity.index') ? 'active' : '' }}">Ethnicity</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.religion.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.religion.index') ? 'active' : '' }}">Religion</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.immigration.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.immigration.index') ? 'active' : '' }}">Immigration--}}
                {{--                                        Status</a>--}}
                {{--                                </li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}
                {{--                        <li class="nav-item nav-item-submenu {{(Request::route()->getName() == 'system.branch.index' ) ||(Request::route()->getName() == 'system.bank.index' ) || (Request::route()->getName() == 'system.paytype.index' )  ? 'nav-item-expanded nav-item-open' : '' }}">--}}
                {{--                            <a href="#" class="nav-link"><span>Account Detail</span></a>--}}
                {{--                            <ul class="nav nav-group-sub" data-submenu-title="Metadata Management">--}}

                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.paytype.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.paytype.index') ? 'active' : '' }}">Payment--}}
                {{--                                        Method</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.bank.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.bank.index') ? 'active' : '' }}">Bank--}}
                {{--                                    </a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.branch.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.branch.index') ? 'active' : '' }}">Bank--}}
                {{--                                        Branch</a>--}}
                {{--                                </li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}


                {{--                        <li class="nav-item nav-item-submenu {{(Request::route()->getName() == 'system.membership.index')||(Request::route()->getName() == 'system.language.index')|| (Request::route()->getName() == 'system.certification.index')|| (Request::route()->getName() == 'system.education.index') ||(Request::route()->getName() == 'system.skill.index')?'nav-item-expanded nav-item-open' : ''}}">--}}
                {{--                            <a href="#" class="nav-link"><span>Qualifications Setup</span></a>--}}

                {{--                            <ul class="nav nav-group-sub" data-submenu-title="Qualifications Setup">--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.skill.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.skill.index') ? 'active' : '' }}">Skill</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.education.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.education.index') ? 'active' : '' }}">Education</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.certification.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.certification.index') ? 'active' : '' }}">Certification</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.language.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.language.index') ? 'active' : '' }}">Languages</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.membership.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.membership.index') ? 'active' : '' }}">Memberships</a>--}}
                {{--                                </li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}

                {{--                        <li class="nav-item nav-item-submenu {{(Request::route()->getName() == 'system.salarycomponent.index')  ||(Request::route()->getName() == 'system.section.index')||(Request::route()->getName() == 'system.employmenttype.index'||Request::route()->getName() == 'system.paygrade.index'||Request::route()->getName() == 'system.jobcategory.index')||(Request::route()->getName() == 'system.division.index')||(Request::route()->getName() == 'system.division.index')||(Request::route()->getName() == 'system.department.index')||(Request::route()->getName() == 'system.designation.index')? 'nav-item-expanded nav-item-open' : ''}}">--}}
                {{--                            <a href="#" class="nav-link"><span>Job Details Setup</span></a>--}}

                {{--                            <ul class="nav nav-group-sub" data-submenu-title="Job Details Setup">--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.department.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.department.index') ? 'active' : '' }}">Department</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.designation.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.designation.index') ? 'active' : '' }}">Designation</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.division.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.division.index') ? 'active' : '' }}">Division</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.employmenttype.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.employmenttype.index') ? 'active' : '' }}">Employment--}}
                {{--                                        Type</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.jobcategory.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.jobcategory.index') ? 'active' : '' }}">Job--}}
                {{--                                        Categories</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.paygrade.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.paygrade.index') ? 'active' : '' }}">Pay--}}
                {{--                                        Grade</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.section.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.section.index') ? 'active' : '' }}">Section</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.salarycomponent.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.salarycomponent.index') ? 'active' : '' }}">Salary--}}
                {{--                                        Components</a>--}}
                {{--                                </li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}

                {{--                        <li class="nav-item nav-item-submenu  {{(Request::route()->getName() == 'system.location.index')|| (Request::route()->getName() == 'system.company.index') ? 'nav-item-expanded nav-item-open' : ''}}">--}}
                {{--                            <a href="#" class="nav-link"><span>Company Setup</span></a>--}}

                {{--                            <ul class="nav nav-group-sub" data-submenu-title="Company">--}}
                {{--                                --}}{{--                                <li class="nav-item">--}}
                {{--                                --}}{{--                                    <a href="{{ route('system.company.index') }}"--}}
                {{--                                --}}{{--                                       class="nav-link {{ (Request::route()->getName() == 'system.company.index') ? 'active' : '' }}">Company</a>--}}
                {{--                                --}}{{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.location.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.location.index') ? 'active' : '' }}">Location</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item"><a href="#" class="nav-link">Company Hierarchy</a></li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}


                {{--                    </ul>--}}
                {{--                </li>--}}

                {{--                <li class="nav-item nav-item-submenu {{  (Request::route()->getName() == 'system.personaldetail.index' )|| (Request::route()->getName() == 'system.customfield.index' )||  (Request::route()->getName() == 'system.commissiontype.index' )||(Request::route()->getName() == 'system.currency.index' )||(Request::route()->getName() == 'system.unitconversion.index') ||(Request::route()->getName() == 'system.unittype.index') ||(Request::route()->getName() == 'system.unitcategory.index') ||(Request::route()->getName() == 'system.transportationtype.index') ||(Request::route()->getName() == 'system.documenttype.index') ||(Request::route()->getName() == 'system.salarycomponent.index') ||(Request::route()->getName() == 'system.branch.index' ) ||(Request::route()->getName() == 'system.bank.index' ) ||(Request::route()->getName() == 'system.license.index')||(Request::route()->getName() == 'system.expensescategory.index') ? 'nav-item-expanded nav-item-open' : '' }}">--}}
                {{--                    <a href="#" class="nav-link"><i class="icon-accessibility"></i> <span>System</span></a>--}}

                {{--                    <ul class="nav nav-group-sub" data-submenu-title="System">--}}

                {{--                        <li class="nav-item nav-item-submenu {{ (Request::route()->getName() == 'system.personaldetail.index' )|| (Request::route()->getName() == 'system.customfield.index' )||(Request::route()->getName() == 'system.commissiontype.index' )|| (Request::route()->getName() == 'system.currency.index' )||(Request::route()->getName() == 'system.unitconversion.index' ) || (Request::route()->getName() == 'system.unittype.index' ) ||(Request::route()->getName() == 'system.unitcategory.index' ) ||(Request::route()->getName() == 'system.transportationtype.index' ) ||(Request::route()->getName() == 'system.documenttype.index' ) ||(Request::route()->getName() == 'system.license.index' ) ||(Request::route()->getName() == 'system.expensescategory.index' ) ? 'nav-item-expanded nav-item-open' : '' }}">--}}
                {{--                            <a href="#" class="nav-link"><span>Setting</span></a>--}}
                {{--                            <ul class="nav nav-group-sub" data-submenu-title="Setting">--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.currency.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.currency.index') ? 'active' : '' }}">Currency</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.expensescategory.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.expensescategory.index') ? 'active' : '' }}">Expenses--}}
                {{--                                        Category</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.license.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.license.index') ? 'active' : '' }}">License</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.documenttype.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.documenttype.index') ? 'active' : '' }}">Document--}}
                {{--                                        Type</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.transportationtype.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.transportationtype.index') ? 'active' : '' }}">Transportation--}}
                {{--                                        Type</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.unitcategory.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.unitcategory.index') ? 'active' : '' }}">Unit--}}
                {{--                                        Category</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.unittype.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.unittype.index') ? 'active' : '' }}">Unit--}}
                {{--                                        Type</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.unitconversion.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.unitconversion.index') ? 'active' : '' }}">Unit--}}
                {{--                                        Conversion</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.commissiontype.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.commissiontype.index') ? 'active' : '' }}">Commission--}}
                {{--                                        Types</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.personaldetail.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.personaldetail.index') ? 'active' : '' }}">Employee--}}
                {{--                                        Personal Detail</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('system.customfield.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'system.customfield.index') ? 'active' : '' }}">Employee--}}
                {{--                                        Custom Fields</a>--}}
                {{--                                </li>--}}


                {{--                            </ul>--}}
                {{--                        </li>--}}

                {{--                    </ul>--}}
                {{--                </li>--}}
                {{--                --}}{{-- <li class="nav-item nav-item-submenu {{  (Request::route()->getName() == 'loan.loan-types.index' ) || ( Request::route()->getName() == 'hr.personaldetails.index'  || Request::route()->getName() == 'hr.otsetup.index' ) ||   (Request::route()->getName() == 'hr.overtimecategory.index' ) ||  (Request::route()->getName() == 'hr.leavetype.index' ) ? 'nav-item-expanded nav-item-open' : '' }}">--}}
                {{--                </li> --}}

                {{--                <li class="nav-item nav-item-submenu {{  (Request::route()->getName() == 'loan.loan-types.index' ) || (Request::route()->getName() == 'hr.otsetup.index' ) || (Request::route()->getName() == 'hr.overtimecategory.index' ) || (Request::route()->getName() == 'hr.leavetype.index' ) ? 'nav-item-expanded nav-item-open' : '' }}">--}}

                {{--                    <a href="#" class="nav-link"><i class="icon-users"></i> <span>HR</span></a>--}}

                {{--                    <ul class="nav nav-group-sub" data-submenu-title="System">--}}


                {{--                        --}}{{--                <li class="nav-item nav-item-submenu">--}}
                {{--                        --}}{{--                    <a href="#" class="nav-link"><i class="icon-user"></i> <span>Employee Management</span></a>--}}

                {{--                        --}}{{--                    <ul class="nav nav-group-sub" data-submenu-title="Employee Management">--}}
                {{--                        --}}{{--                        <li class="nav-item nav-item-submenu">--}}
                {{--                        --}}{{--                            <a href="#" class="nav-link">Configuration</a>--}}
                {{--                        --}}{{--                            <ul class="nav nav-group-sub" data-submenu-title="Configuration">--}}
                {{--                        --}}{{--                                <li class="nav-item"><a href="#" class="nav-link">Custom Fields</a></li>--}}
                {{--                        --}}{{--                                <li class="nav-item"><a href="#" class="nav-link">Manage Employee Master</a></li>--}}
                {{--                        --}}{{--                            </ul>--}}
                {{--                        --}}{{--                        </li>--}}
                {{--                        --}}{{--                        <li class="nav-item"><a href="#" class="nav-link">Employee List</a></li>--}}
                {{--                        --}}{{--                        <li class="nav-item"><a href="#" class="nav-link">Add New Employee</a></li>--}}
                {{--                        --}}{{--                    </ul>--}}
                {{--                        --}}{{--                </li>--}}

                {{--                        --}}{{--                <li class="nav-item nav-item-submenu">--}}
                {{--                        --}}{{--                    <a href="#" class="nav-link"><i class="icon-sort-time-asc"></i> <span>Time Management</span></a>--}}
                {{--                        --}}{{--                    <ul class="nav nav-group-sub" data-submenu-title="Employee Management">--}}
                {{--                        --}}{{--                        <li class="nav-item {{ (Request::route()->getName() == 'hr.daytype.index' || Request::route()->getName() == 'hr.daytype.create' || Request::route()->getName() == 'hr.daytype.edit') ? 'active' : '' }}">--}}
                {{--                        --}}{{--                            <a href="{{ route('hr.daytype.index') }}" class="nav-link">Day Type</a>--}}
                {{--                        --}}{{--                        </li>--}}
                {{--                        --}}{{--                        <li class="nav-item  {{ (Request::route()->getName() == 'hr.shift.index' || Request::route()->getName() == 'hr.shift.create' || Request::route()->getName() == 'hr.shift.edit') ? 'active' : '' }}">--}}
                {{--                        --}}{{--                            <a href="{{ route('hr.shift.index') }}" class="nav-link">Manage Shift</a>--}}
                {{--                        --}}{{--                        </li>--}}
                {{--                        --}}{{--                        <li class="nav-item {{ (Request::route()->getName() == 'hr.roster.index' || Request::route()->getName() == 'hr.roster.create' || Request::route()->getName() == 'hr.roster.edit') ? 'active' : '' }}">--}}
                {{--                        --}}{{--                            <a href="{{ route('hr.roster.index') }}" class="nav-link">Manage Roster</a>--}}
                {{--                        --}}{{--                        </li>--}}
                {{--                        --}}{{--                    </ul>--}}
                {{--                        --}}{{--                </li>--}}
                {{--                        <li class="nav-item nav-item-submenu {{  (Request::route()->getName() == 'hr.leaverule.index' ) || (Request::route()->getName() == 'hr.leavetype.index' )  ? 'nav-item-expanded nav-item-open' : '' }}">--}}
                {{--                            <a href="#" class="nav-link "><i class="icon-hour-glass2"></i> <span>Leave Management</span></a>--}}
                {{--                            <ul class="nav nav-group-sub " data-submenu-title="Employee Management">--}}
                {{--                                <li class="nav-item ">--}}
                {{--                                    <a href="{{ route('hr.leavetype.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'hr.leavetype.index' ) ? 'active' : '' }}">Leave--}}
                {{--                                        Type</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('hr.leaveperiod.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'hr.leaveperiod.index' ) ? 'active' : '' }}">Leave--}}
                {{--                                        Period</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item  ">--}}
                {{--                                    <a href="{{ route('hr.leavegroup.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'hr.leavegroup.index' ) ? 'active' : '' }}">Leave--}}
                {{--                                        Group</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item ">--}}
                {{--                                    <a href="{{ route('hr.leaverule.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'hr.leaverule.index' ) ? 'active' : '' }}">Leave--}}
                {{--                                        Rules</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item nav-item-submenu">--}}
                {{--                                    <a href="#" class="nav-link">Leave Request</a>--}}
                {{--                                    <ul class="nav nav-group-sub" data-submenu-title="Configuration">--}}
                {{--                                        <li class="nav-item {{ (Request::route()->getName() == 'hr.leave-request.index' || Request::route()->getName() == 'hr.leave-request.create' || Request::route()->getName() == 'hr.leave-request.edit') ? 'active' : '' }}">--}}
                {{--                                            <a href="{{ route('hr.leave-request.index') }}" class="nav-link">Calendar--}}
                {{--                                                View</a>--}}
                {{--                                        </li>--}}
                {{--                                        <li class="nav-item {{ (Request::route()->getName() == 'hr.leave-request.list-view' || Request::route()->getName() == 'hr.leave-request.create-leave' || Request::route()->getName() == 'hr.leave-request.edit') ? 'active' : '' }}">--}}
                {{--                                            <a href="{{ route('hr.leave-request.list-view') }}"--}}
                {{--                                               class="nav-link">Manage--}}
                {{--                                                Leave--}}
                {{--                                                Request</a>--}}
                {{--                                        </li>--}}
                {{--                                    </ul>--}}
                {{--                                </li>--}}

                {{--                            </ul>--}}
                {{--                        </li>--}}
                {{--                        <li class="nav-item nav-item-submenu {{  (Request::route()->getName() == 'loan.loan-types.index' ) || (Request::route()->getName() == 'loan.loan-requests.index' ) ||  Request::route()->getName() == 'loan.loan-inspections.index' ? 'nav-item-expanded nav-item-open' : '' }}">--}}
                {{--                            <a href="#" class="nav-link "><i class="icon-calculator"></i>--}}
                {{--                                <span>Loan Management</span></a>--}}
                {{--                            <ul class="nav nav-group-sub " data-submenu-title="Employee Management">--}}
                {{--                                <li class="nav-item ">--}}
                {{--                                    <a href="{{ route('loan.loan-types.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'loan.loan-types.index' ) ? 'active' : '' }}">Loan--}}
                {{--                                        Type</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('loan.loan-requests.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'loan.loan-requests.index' ) ? 'active' : '' }}">Loan--}}
                {{--                                        Requests</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('loan.loan-inspections.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'loan.loan-inspections.index' ) ? 'active' : '' }}">Loan--}}
                {{--                                        Inspection</a>--}}
                {{--                                </li>--}}

                {{--                            </ul>--}}
                {{--                        </li>--}}

                {{--                        <li class="nav-item nav-item-submenu {{  (Request::route()->getName() == 'hr.overtimecategory.index' ) || (Request::route()->getName() == 'hr.otsetup.index' )  ? 'nav-item-expanded nav-item-open' : '' }}">--}}
                {{--                            <a href="#" class="nav-link "><i class="icon-hour-glass2"></i>--}}
                {{--                                <span>Over Time Management</span></a>--}}
                {{--                            <ul class="nav nav-group-sub " data-submenu-title="Employee Management">--}}
                {{--                                <li class="nav-item ">--}}
                {{--                                    <a href="{{ route('hr.overtimecategory.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'hr.overtimecategory.index' ) ? 'active' : '' }}">OT--}}
                {{--                                        Category</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('hr.otsetup.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'hr.otsetup.index' ) ? 'active' : '' }}">--}}
                {{--                                        OT Setup</a>--}}
                {{--                                </li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}

                {{--                        <li class="nav-item nav-item-submenu {{ (Request::route()->getName() == 'hr.personaldetails.index' ) ? 'nav-item-expanded nav-item-open' : '' }}">--}}
                {{--                            <a href="#" class="nav-link "><i class="icon-hour-glass2"></i> <span>Personal Details Management</span></a>--}}
                {{--                            <ul class="nav nav-group-sub " data-submenu-title="Employee Management">--}}
                {{--                                <li class="nav-item ">--}}
                {{--                                    <a href="{{ route('hr.personaldetails.index') }}"--}}
                {{--                                       class="nav-link {{ (Request::route()->getName() == 'hr.personaldetails.index' ) ? 'active' : '' }}">Create--}}
                {{--                                        Personal Record</a>--}}
                {{--                                </li>--}}
                {{--                            </ul>--}}
                {{--                        </li>--}}

                {{--                    --}}{{--                <li class="nav-item nav-item-submenu">--}}
                {{--                    --}}{{--                    <a href="#" class="nav-link"><i class="icon-copy"></i> <span>Authorization</span></a>--}}

                {{--                    --}}{{--                    <ul class="nav nav-group-sub" data-submenu-title="Authorization">--}}
                {{--                    --}}{{--                        <li class="nav-item" {{ Request::path() == '/auth/role' ? 'class=active' : '' }}>--}}
                {{--                    --}}{{--                            <a href="{{ route('auth.role.index') }}" class="nav-link">Roles</a>--}}
                {{--                    --}}{{--                        </li>--}}
                {{--                    --}}{{--                        <li class="nav-item" {{ Request::path() == '/auth/permission' ? 'class=active' : '' }}>--}}
                {{--                    --}}{{--                            <a href="{{ route('auth.permission.index') }}" class="nav-link">User Permissions</a>--}}
                {{--                    --}}{{--                        </li>--}}
                {{--                    --}}{{--                        <li class="nav-item" {{ Request::path() == '/auth/user' ? 'class=active' : '' }}>--}}
                {{--                    --}}{{--                            <a href="{{ route('auth.user.index') }}" class="nav-link">Users</a>--}}
                {{--                    --}}{{--                        </li>--}}
                {{--                    --}}{{--                        <li class="nav-item" {{ Request::path() == '/auth/module' ? 'class=active' : '' }}>--}}
                {{--                    --}}{{--                            <a href="{{ route('auth.module.index') }}" class="nav-link">Modules</a>--}}
                {{--                    --}}{{--                        </li>--}}
                {{--                    --}}{{--                        <li class="nav-item" {{ Request::path() == '/auth/sub-module' ? 'class=active' : '' }}>--}}
                {{--                    --}}{{--                            <a href="{{ route('auth.submodule.index') }}" class="nav-link">Submodule</a>--}}
                {{--                    --}}{{--                        </li>--}}
                {{--                    --}}{{--                        <li class="nav-item" {{ Request::path() == '/auth/sub-module-item' ? 'class=active' : '' }}>--}}
                {{--                    --}}{{--                            <a href="{{ route('auth.submoduleitem.index') }}" class="nav-link">Submodule Item</a>--}}
                {{--                    --}}{{--                        </li>--}}
                {{--                    --}}{{--                    </ul>--}}
                {{--                    --}}{{--                </li>--}}

                {{--                    --}}{{--                            <li class="nav-item nav-item-submenu">--}}
                {{--                    --}}{{--                                <a href="#" class="nav-link"><i class="icon-accessibility"></i> <span>System</span></a>--}}

                {{--                    --}}{{--                                <ul class="nav nav-group-sub" data-submenu-title="System">--}}
                {{--                    --}}{{--                                    <li class="nav-item" {{ Request::path() == '/system/country' ? 'class=active' : '' }}><a--}}
                {{--                    --}}{{--                                            href="{{ route('system.country.index') }}" class="nav-link">Country</a>--}}
                {{--                    --}}{{--                                    </li>--}}
                {{--                    --}}{{--                                    <li class="nav-item" {{ Request::path() == '/system/province' ? 'class=active' : '' }}><a--}}
                {{--                    --}}{{--                                            href="{{ route('system.province.index') }}" class="nav-link">Province</a>--}}
                {{--                    --}}{{--                                    </li>--}}
                {{--                    --}}{{--                                    <li class="nav-item" {{ Request::path() == '/system/nationality' ? 'class=active' : '' }}><a--}}
                {{--                    --}}{{--                                            href="{{ route('system.nationality.index') }}" class="nav-link">Nationality</a>--}}
                {{--                    --}}{{--                                    </li>--}}
                {{--                    --}}{{--                                    <li class="nav-item" {{ Request::path() == '/system/ethnicity' ? 'class=active' : '' }}><a--}}
                {{--                    --}}{{--                                            href="{{ route('system.ethnicity.index') }}" class="nav-link">Ethnicity</a>--}}
                {{--                    --}}{{--                                    </li>--}}
                {{--                    --}}{{--                                    <li class="nav-item" {{ Request::path() == '/system/immigration' ? 'class=active' : '' }}><a--}}
                {{--                    --}}{{--                                            href="{{ route('system.immigration.index') }}" class="nav-link">Immigration</a>--}}
                {{--                    --}}{{--                                    </li>--}}
                {{--                    --}}{{--                                </ul>--}}
                {{--                    --}}{{--                            </li>--}}
                {{--                    <!-- /main -->--}}

                {{--                    </ul>--}}
                {{--                </li>--}}
                {{--                <li class="nav-item-header">--}}
                {{--                    <div class="text-uppercase font-size-xs line-height-xs">Visitors Management</div>--}}
                {{--                    <i class="icon-switch" title="Logout"></i>--}}
                {{--                </li>--}}

                {{--                --}}{{-- @can('submodule 1 item 1 DRAFT') --}}

                {{--                <li class="nav-item">--}}
                {{--                    <a href="{{route('queapp.visitor.dashboard')}}"--}}
                {{--                       class="nav-link {{ (Request::route()->getName() == 'queapp.visitor.dashboard') ? ' active' : '' }}">--}}
                {{--                        <i class="icon-meter-slow"></i><span>Dashboard</span>--}}
                {{--                    </a>--}}
                {{--                    <a href="{{route('queapp.visitor.index')}}"--}}
                {{--                       class="nav-link {{ (Request::route()->getName() == 'queapp.visitor.index') ? ' active' : '' }}">--}}
                {{--                        <i class="icon-users"></i><span>Manage Visitor</span>--}}
                {{--                    </a>--}}
                {{--                    <a href="{{route('queapp.visitor.create')}}"--}}
                {{--                       class="nav-link {{ (Request::route()->getName() == 'queapp.visitor.create') ? ' active' : '' }}">--}}
                {{--                        <i class="icon-user-plus"></i><span>Register Visitor</span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}

                {{--            --}}{{-- @endcan --}}

                {{--            --}}{{--<li class="nav-item-header">--}}
                {{--            --}}{{--<div class="text-uppercase font-size-xs line-height-xs">CRM</div>--}}
                {{--            --}}{{--<i class="icon-switch" title="Logout"></i>--}}
                {{--            --}}{{--</li>--}}
                {{--            --}}{{--<li class="nav-item">--}}
                {{--            --}}{{--<a href="{{route('crm.dashboard')}}"--}}
                {{--            --}}{{--class="nav-link {{ (Request::route()->getName() == 'crm.dashboard') ? ' active' : '' }}">--}}
                {{--            --}}{{--<i class="icon-meter-slow"></i><span>CRM Dashboard</span>--}}
                {{--            --}}{{--</a>--}}
                {{--            --}}{{--<a href="{{route('crm.customer.index')}}"--}}
                {{--            --}}{{--class="nav-link {{ (Request::route()->getName() == 'crm.customer.index') ? ' active' : '' }}">--}}
                {{--            --}}{{--<i class="icon-users"></i><span>Manage Customer</span>--}}
                {{--            --}}{{--</a>--}}
                {{--            --}}{{--<a href="{{route('crm.customer.create')}}"--}}
                {{--            --}}{{--class="nav-link {{ (Request::route()->getName() == 'crm.customer.create') ? ' active' : '' }}">--}}
                {{--            --}}{{--<i class="icon-user-plus"></i><span>Register Customer</span>--}}
                {{--            --}}{{--</a>--}}
                {{--            --}}{{--</li>--}}

                {{--            <!-- Forms -->--}}
                {{--                <li class="nav-item-header">--}}
                {{--                    <div class="text-uppercase font-size-xs line-height-xs">Logout</div>--}}
                {{--                    <i class="icon-switch" title="Logout"></i></li>--}}
                {{--                <li class="nav-item">--}}
                {{--                    <a href="#" class="nav-link" onclick="event.preventDefault();--}}
                {{--                                document.getElementById('logout-form').submit();">--}}
                {{--                        <i class="icon-switch2"></i> <span>Logout</span>--}}
                {{--                    </a>--}}

                {{--                    <form id="logout-form" action="{{ route('logout') }}" method="POST"--}}
                {{--                          style="display: none;">--}}
                {{--                        {{ csrf_field() }}--}}
                {{--                    </form>--}}
                {{--                </li>--}}
                {{--            --}}
            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->
