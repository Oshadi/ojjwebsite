<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

    <!-- Sidebar mobile toggler -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-arrow-left8"></i>
        </a>
        Navigation
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-screen-full"></i>
            <i class="icon-screen-normal"></i>
        </a>
    </div>
    <!-- /sidebar mobile toggler -->


    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- Main navigation -->
        <div class="card card-sidebar-mobile">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <div id="sidebarMainText" class="text-uppercase font-size-xs line-height-xs">Main</div>
                    <i id="sidebarMainIcon" class="icon-menu" title="Main"></i></li>
                <li class="nav-item" {{ Request::route()->getName() == 'dashboard' ? 'class=active' : '' }}>
                    <a href="{{ route('dashboard') }}" class="nav-link ">
                        <i class="icon-home4"></i>
                        <span>Home</span>
                    </a>
                </li>

                @php
                    $modules =   \Modules\Auth\Entities\Module::where('status',1)->get();
                @endphp
                @foreach($modules as $module)
                    @if(count($module->submodules->where('status',1))!=0)
                        @php
                            $module_sub_module_route_name[]=array();
                             foreach ($module->submodules as $sM) {
                                     foreach ($sM->submoduleItems as $sMItem) {
                                          array_push($module_sub_module_route_name,$sMItem->route);
                                     }
                             }
                        @endphp
                        <li class="nav-item nav-item-submenu {{  in_array(Request::route()->getName(), $module_sub_module_route_name )   ? 'nav-item-expanded nav-item-open' : '' }}">

                            <a href="#" class="nav-link"><i class="{{$module->icon}}"></i>
                                <span>{{$module->name}} </span></a>
                            <ul class="nav nav-group-sub" data-submenu-title="{{ $module->name }}">
                                @php
                                    $module_sub_module_route_name=[];
                                @endphp
                                @foreach($module->submodules->where('status',1)  as $subModule)
                                    @php
                                        $sub_module_route_name[]=array();
                                       foreach ($subModule->submoduleItems as $sMItem) {
                                           array_push( $sub_module_route_name,$sMItem->route);
                                       }
                                    @endphp

                                    {{-- CHECK AND MENU SHOW HIDE --}}
                                    @php $flag = false;  @endphp
                                    @foreach($subModule->submoduleItems->where('status',1)  as $subModuleItem)
                                        @if(checkHasPermission($subModuleItem->name." VIEW"))
                                            @php
                                                $flag = true;
                                            @endphp
                                        @endif
                                    @endforeach
                                    {{-- END CHECK AND MENU SHOW HIDE --}}

                                    @if(count($subModule->submoduleItems)!=0 && $flag)
                                        <li class="nav-item nav-item-submenu  {{ in_array(Request::route()->getName(),$sub_module_route_name) ? 'nav-item-expanded nav-item-open' : '' }} ">
                                            <a href="#"
                                               class="nav-link">
                                                <i class="{{$subModule->icon}}"></i>
                                                <span> {{$subModule->name}} </span></a>


                                            <ul class="nav nav-group-sub" data-submenu-title="{{$subModule->name}}">
                                                @php
                                                    $sub_module_route_name=[]
                                                @endphp
                                                @foreach($subModule->submoduleItems->where('status',1)  as $subModuleItem)
                                                    @php
                                                        if(checkHasPermission($subModuleItem->name." VIEW")){
                                                    @endphp
                                                    <li class="nav-item ">
                                                        <a href="{{ route($subModuleItem->route) }}"
                                                           class="nav-link {{ Request::route()->getName() == $subModuleItem->route? ' active' : '' }}">{{$subModuleItem->name}}</a>
                                                    </li>
                                                    @php
                                                        }
                                                    @endphp
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        <li class="nav-item  {{ in_array(Request::route()->getName(),$sub_module_route_name) ? 'nav-item-expanded nav-item-open' : '' }} ">
                                            <a href="#"
                                               class="nav-link">
                                                <i class="{{$subModule->icon}}"></i>
                                                <span> {{$subModule->name}} </span></a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </li>

                    @else
                        <li class="nav-item {{  in_array(Request::route()->getName(), $module_sub_module_route_name )   ? 'nav-item-expanded nav-item-open' : '' }}">
                            <a href="#" class="nav-link"><i class="{{$module->icon}}"></i>
                                <span> {{$module->name}} </span></a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->

<script>

    $(window).on('load', function () {

        var width = $(window).width();
        if (width < 768) {
            $('.sidebar-main').css("width", "initial");
            $('#sidebarMainText').removeClass('d-none');
            $('#sidebarMainIcon').removeClass('sidebar-main-icon');
        }
        else if (width < 1200) {
            $('.sidebar-main').css("width", "3.5rem");
            $('#sidebarMainText').addClass('d-none');
            $('#sidebarMainIcon').addClass('sidebar-main-icon');
        }
    });

</script>
<script>

    $(document).ready(function () {

        // console.log($('.main-content-section').height());

        $('.who-is-out-content').each(function () {

            if ($(this).height() > 142) {
                $(this).find('.who-is-out-collapse-btn').removeClass('d-none');
                $(this).addClass('who-is-out-content-height');
            } else {


                $(this).find('.who-is-out-collapse-btn').addClass('d-none');
                $(this).removeClass('who-is-out-content-height');
            }

            $(this).find('.who-is-out-collapse-btn').click(function () {
                // alert("ghfkdfkm");
                $(this).parent('.who-is-out-content').toggleClass("who-is-out-content--show-all");


                var text = $(this).text();
                $(this).text(
                    text == "View More" ? "View Less" : "View More");
            });
        });

        $('.welcome-zincat-content').each(function () {

            if ($(this).height() > 207) {
                $(this).find('.welcome-zincat-view-btn').removeClass('d-none');
                $(this).addClass('welcome-zincat-content-height');
            } else {


                $(this).find('.welcome-zincat-view-btn').addClass('d-none');
                $(this).removeClass('welcome-zincat-content-height');
            }

            $(this).find('.welcome-zincat-view-btn').click(function () {
                // alert("ghfkdfkm");
                $(this).parent('.welcome-zincat-content').toggleClass("welcome-zincat-content--show-all");


                // var text = $(this).text();
                // $(this).text(
                //     text == "View More" ? "More About" : "More About");
            });
        });


        $('.side-notification-toggle-btn').click(function () {
            $('.side-notifications').toggleClass('toggle-side-notifications');
            $('.side-notification-toggle-svg').toggleClass('rotate-svg');
        });

        $('.custom-sidebar-mobile-toggle-btn').click(function () {
            $('.custom-sidebar-content').toggleClass('toggle-custom-sidebar');
            $('.custom-sidebar-mobile-toggle-svg').toggleClass('rotate-svg');
        });
        // if ($(window).width() < 1200) {
        //     $('.sidebar-custom-toggle').click(function () {
        //         $('.sidebar-content').toggleClass('toggle-sidebar');
        //     });
        // }

        $('.sidebar-custom-toggle').click(function () {
            $('.custom-main-sidebar .sidebar-content').toggleClass('toggle-dash-sidebar');
            $('.sidebar-content').toggleClass('toggle-sidebar');
            $('.sidebar-main').toggleClass('sidebar-main-toggle');
        });
    });

</script>
