<div class="table-responsive">
    <table class="table datatable-basic {{$classes = ''}} table-striped table-bordered  table-hover " id="data-tables" width="100%">
        <thead>
        {{$head}}
        </thead>
        <tbody>
        {{$body}}
        </tbody>
    </table>
</div>

