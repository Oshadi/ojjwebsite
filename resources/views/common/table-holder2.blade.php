@extends('layouts.admin')

@section('top_nav_bar')
    @include('shared.admin.nav_bar_top.nav_bar_top_fixed')
@endsection

@section('theme_js')
    <script type="text/javascript"
            src="{{ asset('js/plugins/notifications/sweet_alert.min.js') }}"></script>
@endsection

@section('content')

    <div class="page-container">
        <!-- Main sidebar -->
        @include('shared.admin.sidebar')
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex py-2">
                        <h4><i class="icon-quill4"></i><span class="ml-2 font-weight-semibold">{{$title}}</span></h4>
                    </div>

                    <div class="header-elements py-0 py-sm-0 py-md-2 py-lg-2 py-xl-2">
                        <div class="d-flex">
                            <div class="breadcrumb">
                                <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a>
                                <a class="breadcrumb-item active">{{$title}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Main content -->
            <div class="content-wrapper">
                <div class="content">
                    @include('shared.admin.alert')
                    <div class="panel panel-flat border-top-danger-800 border-bottom-primary-800">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4>
                                    <a>
                                        <span class="text-semibold">{{$title}}</span>
                                    </a>
                                </h4>
                            </div>
                            @if(isset($addPermission) && $addPermission)
                                <div class="heading-elements">
                                    <div class="heading-btn-group">
                                        <a href="{{ route($addRoute) }}"
                                           class="btn btn-sm has-text legitRipple add-btn-common">&nbsp;Add New <i
                                                class="icon-plus2 position-right"></i></a>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                @component('shared.admin.components.datatable')
                                    @slot('head')@endslot
                                    @slot('body') @endslot
                                @endcomponent
                            </div>
                            <!-- /basic datatable -->
                        </div>
                    </div>

                    <!-- /2 columns form -->


                    @include('shared.admin.footer')
                </div>
                <!-- /content area -->

            </div>
            <!-- /main content -->

        </div>
        <!-- /page content -->

    </div>
    <!-- /page container -->

@endsection

@section('custom_js')
    <script src="{{ url('js/datatable-test.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var columns = <?php echo $columns;?>;
            initDatatable("{{route($dataTableRoute)}}", columns)
        });
    </script>
@endsection
