@extends('admin::layouts.master')

<style>
    td {
        word-break: break-word;
        font-size: 13px;
    }
</style>
@section('top_nav_bar')
@endsection
@section('content')
    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content py-1 px-2">
            {{--@include('shared.admin.alert')--}}


            <!-- Content -->
                <div class="row">
                    <div class="col-xl-12">

                        @if(!empty($table_before_section))
                            <div class="card">
                                <div class="filter-form-holder mt-3">
                                    {!! $table_before_section !!}
                                </div>
                            </div>
                        @endif

                        <div class="card">
                            <!-- Action toolbar -->
                            <div class="navbar navbar-light bg-light navbar-expand-lg border-bottom-0 py-lg-2">
                                <div class="text-center d-lg-none w-100">
                                    <button type="button" class="navbar-toggler w-100" data-toggle="collapse"
                                            data-target="#inbox-toolbar-toggle-single">
                                        <i class="icon-circle-down2"></i>
                                    </button>
                                </div>

                                <div class="navbar-collapse text-center text-lg-left flex-wrap collapse"
                                     id="inbox-toolbar-toggle-single">
                                    <div class="mt-3 mt-lg-0 mb-2 mb-lg-0">
                                        @if(isset($addPermission) && $addPermission)
                                            <div class="btn-group">
                                                @if(isset($form_type)&& $form_type=="modal")
                                                    <button type="button"
                                                            data-backdrop="static"
                                                            id="create-modal-open"
                                                            class="btn green-btn has-text legitRipple add-btn-common open-modal"
                                                        {{--                                                            data-toggle="modal"--}}
                                                        {{--                                                            data-target="#create-modal-success-color"--}}
                                                    >Add <i
                                                            class="icon-plus2 ml-2"></i>
                                                    </button>
                                                @else
                                                    <a href="{{ route($addRoute) }}"
                                                       class="btn green-btn has-text legitRipple add-btn-common">&nbsp;Add
                                                        New<i class="icon-plus2 ml-2"></i></a>
                                                @endif


                                            </div>
                                        @endif

                                        <div class="btn-group ml-2">
                                            {{--                                            <a onclick="exportData('CSV')" target="_blank"--}}
                                            {{--                                               class="btn btn-gray buttons-html5" tabindex="0"--}}
                                            {{--                                               aria-controls="data-tables"><span>--}}
                                            {{--                                                            <i class="icon-file-spreadsheet position-left"></i> CSV</span>--}}
                                            {{--                                            </a>--}}
                                            @if(isset($export)&& $export==true)
                                                <a onclick="exportData('EXCEL')" target="_blank"
                                                   class="btn btn-gray buttons-html5" tabindex="0"
                                                   aria-controls="data-tables"><span>
                                                            <i class="icon-file-excel position-left"></i> Excel</span>
                                                </a>
                                                <a onclick="exportData('PDF')" target="_blank"
                                                   class="btn btn-gray buttons-html5" tabindex="0"
                                                   aria-controls="data-tables"><span>
                                                            <i class="icon-file-pdf position-left"></i> PDF</span>
                                                </a>
                                                <a onclick="exportData('PRINT')" target="_blank"
                                                   class="btn btn-gray buttons-html5" tabindex="0"
                                                   aria-controls="data-tables"><span>
                                                            <i class="icon-printer position-left"></i> Print</span>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    {{--                                    <div class="ml-lg-3  ml-lg-auto">--}}
                                    {{--                                        <div class="btn-group ml-3">--}}
                                    {{--                                            <button type="button" class="btn btn-light dropdown-toggle"--}}
                                    {{--                                                    data-toggle="dropdown" aria-expanded="false"><i--}}
                                    {{--                                                    class="icon-cog3"></i></button>--}}
                                    {{--                                            <div class="dropdown-menu dropdown-menu-right">--}}
                                    {{--                                                <a href="#" class="dropdown-item">Action</a>--}}
                                    {{--                                                <a href="#" class="dropdown-item">Another action</a>--}}
                                    {{--                                                <a href="#" class="dropdown-item">Something else here</a>--}}
                                    {{--                                                <a href="#" class="dropdown-item">One more line</a>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                </div>
                            </div>
                            <!-- /action toolbar -->

                            @include('common.generalmodel')

                            <input type="hidden" name="page_name" value="{{isset($page_name)?$page_name:''}}">
                            <!-- Table -->
                            <div class="table-responsive custom-table-action-overlap">
                            @component('shared.admin.components.datatable')
                                @slot('head')  @endslot
                                @slot('body') @endslot
                            @endcomponent
                            <!-- /basic datatable -->
                            </div>
                        </div>
                    </div>
                </div>

                {{--                @include('shared.admin.footer')--}}
            </div>
            <!-- /content area -->
        </div>
        <!-- /page content -->
    </div>

@endsection


@section('theme_js')
    <script type="text/javascript"
            src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/extensions/contextmenu.js') }}"></script>
    <script src="{{ asset('global_assets/js/sweetalert.min.js') }}"></script>

    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js')}}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js')}}"></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>

@endsection



@section('custom_js')
    <script src="{{ url('js/datatable-test.js') }}"></script>

    @include('common.generalmodel-script')

    <script type="text/javascript">

        $(document).ready(function () {
            localStorage.clear();

        });
        var columns = <?php echo $columns;?>;
        initDatatable("{{route($dataTableRoute )}}", columns)

        /**
         * created by : Dulan
         * created at : 23-09-2020
         * summary : open edit modal
         * */
        function editModal(id, className) {
            var route_name = $(className).attr("data-name");
            $.ajax({
                url: route_name,
                type: 'get',
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                // data: {id: id},
                success: function (response) {
                    $.getScript("{{ asset('js/backend-validation.js') }}", function (js) {
                        $('.modal-content').html(response.data);
                        $('#open-modal-success-color').modal('show');
                        $('.form-control-select2').select2();
                        $("#onlyForDuration").change(function () {
                            if (this.checked) {
                                $('.duration').show();
                            } else {
                                $('.duration').hide();
                            }
                        });

                        $('input[name="duration"]').daterangepicker({
                            opens: 'left'
                        }, function (start, end, label) {
                            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                        });

                        // TIME PICKER INITIAL
                        $('.clockpicker').clockpicker({
                            autoclose: true,
                            twelvehour: false,
                            placement: 'bottom',
                            align: 'left',
                            //autoclose: true,
                            donetext: 'Done',
                        });
                    })

                },
                error: function () {

                }
            });
        }

        /**
         * created by : Dulan
         * created at : 23-12-2020
         * summary : open view modal
         * */
        function viewModal(id, className) {

            var route_name = $(className).attr("data-name");
            $.ajax({
                url: route_name,
                type: 'get',
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                // data: {id: id},
                success: function (response) {
                    $.getScript("{{ asset('js/backend-validation.js') }}", function (js) {
                        $('.modal-content').html(response.data);
                        $('#open-modal-success-color').modal('show');
                        $('.form-control-select2').select2();
                        $("#onlyForDuration").change(function () {
                            if (this.checked) {
                                $('.duration').show();
                            } else {
                                $('.duration').hide();
                            }
                        });

                        $('input[name="duration"]').daterangepicker({
                            opens: 'left'
                        }, function (start, end, label) {
                            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                        });

                    })

                },
                error: function () {

                }
            });
        }

        /**
         * created by : Dulan
         * created at : 23-09-2020
         * summary : active and inactive records
         * */
        function active_inactive_record(route, id) {

            swal({
                text: "Are you sure to change the status of this record ?",
                buttons: true,
                dangerMode: true,
            })
                .then((confirm) => {
                    if (confirm) {
                        $.ajax({
                            url: route,
                            type: 'post',
                            dataType: 'json',
                            data: {"_token": "{{ csrf_token() }}", id: id},
                            success: function (response) {
                                if (response.success == true) {
                                    swal("Success", response.message);
                                    $('#data-tables').DataTable().ajax.reload();
                                }
                            },
                            error: function () {

                            }
                        });
                    } else {
                        $('#data-tables').DataTable().ajax.reload();
                    }

                });
        }

        $('#create-modal-success-color').on('hidden.bs.modal', function (e) {
            $(".input-form").trigger("reset");
        })

        // $('.access-multiple-clear').on('click', function () {
        //     $('select').val(null).trigger('change');
        // });

        $(document).on('click', ".access-multiple-clear", function () {
            $('.form-control-select2').val(null).trigger('change');

        })

        /**
         * created by : Dulan
         * created at : 23-09-2020
         * summary : Data table export using backend call
         * @param type
         */
        function exportData(type) {
            var searchValue = $('.dataTables_filter input').val();
            var exportWindow = window.open("export", "_blank")
            exportWindow.location.href = "{{$export_route}}/" + type + "/" + searchValue;
        }

        /**
         * created by : Dulan
         * created at : 23-09-2020
         * summary : column customization
         * */
        $('#data-tables').on('column-visibility.dt', function (e, settings, column, state) {

            let ar = [];
            for (var i = 0; i < settings.aoColumns.length; i++) {
                ar.push({
                    "name": settings.aoColumns[i].name,
                    "visible": settings.aoColumns[i].bVisible,
                    "title": settings.aoColumns[i].sTitle,
                    "searchable": settings.aoColumns[i].searchable,
                    "width": settings.aoColumns[i].width,
                    "sortable": settings.aoColumns[i].sortable,
                    "className": settings.aoColumns[i].className ? settings.aoColumns[i].className : "",


                });
            }
            $.ajax({
                {{--url: '{{route("column-customizer.store")}}',--}}
                {{--type: 'post',--}}
                {{--dataType: 'json',--}}
                {{--data: {--}}
                {{--"_token": "{{ csrf_token() }}",--}}
                {{--columns: ar,--}}
                {{--table: $("input[name=page_name]").val()--}}
                {{--},--}}
                {{--success: function (response) {--}}
                {{--},--}}
                {{--error: function () {--}}

                {{--}--}}
            });

            console.log('column-customizer.store');
        });

        $(function () {
            $(document).on("click", ".filter-form .filter-form-submit", function (e) {
                e.preventDefault();

                var table = $('.datatable-basic').DataTable();
                table.clear().draw();

                // window.customTable = $('.datatable-basic').DataTable({
                //     "processing": true,
                //     "order": [],
                //     "scrollX": true,
                //     "serverSide": true,
                //     dom: "<'row'<'col-sm-4'l><'col-sm-4'><'col-sm-4'fB>>" +
                //         "<'row'<'col-sm-12'tr>>" +
                //         "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                //     "aoColumnDefs": [
                //         {bSortable: false, aTargets: ['no-sort']} // Disable sorting on columns marked as so
                //     ],
                //     // "ajax": $('.get-data-holder').data('url'),
                //
                //     //"buttons": getDataTableButtons()
                // });

                //window.customTable.draw();
            });
        })

    </script>



@endsection

