<script>

    $(document).ready(function () {


        /**
         * created by : Dulan
         * created at : 23-09-2020
         * summary : open create modal
         * */
        $("#create-modal-open, .create-modal-open").click(function () {

            var select = $(this).attr('data-select');
            var routeName = '{{((@$addRoute) ? route($addRoute) : "")}}';
            routeName = (!routeName) ? $(this).attr('data-route') : routeName;
            dataresource = $(this).attr('data-resource');
            $.ajax({
                url: routeName,
                type: 'get',
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    $.getScript("{{ asset('js/backend-validation.js') }}", function (js) {
                        $('.modal-content').html(response.data);
                        $('#open-modal-success-color').modal('show');
                        $('.form-control-select2').select2();

                        if ($('.selectelement').length > 0 && select) {
                            $('.selectelement').val(select);
                        }

                        $("#onlyForDuration").change(function () {
                            if (this.checked) {
                                $('.duration').show();
                            } else {
                                $('.duration').hide();
                            }
                        });

                        $('input[name="duration"]').daterangepicker({
                            opens: 'left'
                        }, function (start, end, label) {
                            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                        });
                        $('.form-control-select2').select2();

                        // TIME PICKER INITIAL
                        $('.clockpicker').clockpicker({
                            autoclose: true,
                            twelvehour: false
                        });

                    })
                    $.getScript("{{ asset('js/custom.js') }}", function (js) {


                    })


                },
                error: function () {

                }
            });

        });

    });


</script>
