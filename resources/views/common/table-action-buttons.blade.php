{{-- View Button --}}
@if (Route::has("$route.show") && !empty($permissions['view']) && $permissions['view'])
    {!! Form::open(['method' => 'GET', 'class'=> 'd-inline view-form', 'route' => ["$route.show", $id]]) !!}
    <button class="btn btn-info view-button btn-sm" type="submit" title="View record"><i class="fa fa-eye"></i></button>
    {!! Form::close() !!}
@endif

@if (Route::has("$route.edit") && !empty($permissions['edit']) && Auth::user()->hasPermissionTo($permissions['edit']))
    @if($route == 'call-center')
        {{-- Take Call Button --}}
        {!! Form::open(['class'=> 'd-inline edit-form', 'route' => ["$route.take-call", $id]]) !!}
        <button class="btn btn-info edit-button btn-sm" type="submit" title="Edit record"><i class="fa fa-phone"></i> Take Call</button>
        {!! Form::close() !!}
    @else
        {{-- Edit Button --}}
        {!! Form::open(['class'=> 'd-inline edit-form', 'route' => ["$route.edit", $id]]) !!}
        <button class="btn btn-info edit-button btn-sm" type="submit" title="Edit record"><i class="fa fa-pencil"></i></button>
        {!! Form::close() !!}
    @endif
@endif

@if (Route::has("$route.destroy") && !empty($permissions['delete']) && Auth::user()->hasPermissionTo($permissions['delete']))
{{-- Delete Button --}}
{!! Form::open(['method' => 'DELETE', 'class'=> 'd-inline delete-form', 'route' => ["$route.destroy", $id]]) !!}
<button class="btn btn-danger delete-button btn-sm" type="submit" title="Delete record"><i class="fa fa-trash"></i></button>
{!! Form::close() !!}
@endif