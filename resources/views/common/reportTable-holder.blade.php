<meta name="csrf-token" content="{{ csrf_token() }}" />
@extends('layouts.admin')
<style>
    td {
        word-break: break-word;
        font-size: 13px;
    }
</style>
@section('top_nav_bar')
    @include('shared.admin.nav_bar_top.nav_bar_top_fixed')
@endsection
@section('content')
    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
    @include('shared.admin.sidebar')



    <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex py-2">
                        <h4><i class="{{isset($icon)?$icon:"icon-quill4"}}"></i><span
                                class="ml-2 font-weight-semibold">{{$title}}</span></h4>
                    </div>

                    <div class="header-elements py-0 py-sm-0 py-md-2 py-lg-2 py-xl-2">
                        <div class="d-flex">
                            <div class="breadcrumb">
                                <a href="{{route('dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                                    Home</a>
                                <a class="breadcrumb-item active">{{$title}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content py-1 px-2">
            @include('shared.admin.alert')

            <!-- Content -->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card">
                            <!-- Action toolbar -->
                            <div class="navbar navbar-light bg-light navbar-expand-lg border-bottom-0 py-lg-2">
                                <div class="text-center d-lg-none w-100">
                                    <button type="button" class="navbar-toggler w-100" data-toggle="collapse"
                                            data-target="#inbox-toolbar-toggle-single">
                                        <i class="icon-circle-down2"></i>
                                    </button>
                                </div>

                                <div class="navbar-collapse text-center text-lg-left flex-wrap collapse"
                                     id="inbox-toolbar-toggle-single">
                                    <div class="mt-3 mt-lg-0">

                                        @if(isset($addPermission) && $addPermission)
                                            <div class="btn-group">
                                                @if(isset($form_type)&& $form_type=="modal")
                                                    <button type="button"
                                                            data-backdrop="static"
                                                            id="create-modal-open"
                                                            class="btn btn-sm btn-outline-primary has-text legitRipple add-btn-common open-modal"
                                                    >Add <i
                                                            class="icon-plus2 ml-2"></i>
                                                    </button>
                                                @else
                                                    <a href="{{ route($addRoute) }}"
                                                       class="btn btn-sm btn-outline-primary has-text legitRipple add-btn-common">&nbsp;Add
                                                        New<i class="icon-plus2 ml-2"></i></a>
                                                @endif


                                            </div>
                                        @endif

                                        <div class="btn-group mr-lg-3">
                                            @if(isset($export)&& $export==true)
                                                <a onclick="exportData('EXCEL')" target="_blank"
                                                   class="btn btn-mini btn-light buttons-html5" tabindex="0"
                                                   aria-controls="data-tables"><span>
                                                            <i class="icon-file-excel position-left"></i> Excel</span>
                                                </a>
                                                <a onclick="exportData('PDF')" target="_blank"
                                                   class="btn btn-mini btn-light buttons-html5" tabindex="0"
                                                   aria-controls="data-tables"><span>
                                                            <i class="icon-file-pdf position-left"></i> PDF</span>
                                                </a>
                                                <a onclick="exportData('PRINT')" target="_blank"
                                                   class="btn btn-mini btn-light buttons-html5" tabindex="0"
                                                   aria-controls="data-tables"><span>
                                                            <i class="icon-printer position-left"></i> Print</span>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /action toolbar -->

                            @include('common.generalmodel')

                            <input type="hidden" name="page_name" value="{{isset($page_name)?$page_name:''}}">
                            <!-- Table -->
                            <div class="table-responsive">
                                <form class="wizard-form steps-basic wizard clearfix input-form"
                                      novalidate="novalidate" enctype="multipart/form-data">
                                    @csrf
                                <div class="row col-sm-12 mt-2 mb-2">

                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">Start Date</label>
                                        <input class="form-control" type="date" name="start_date" id="start_date" placeholder="Select date">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">End Date</label>
                                        <input class="form-control" type="date" name="end_date" id="end_date" placeholder="Select date">
                                    </div>

                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">Select Employee Code</label>
                                        <select type="text"
                                                data-placeholder="Select Employee Code" required
                                                class="form-control form-control-select2 select-clear employee_id"
                                                name="emp_code_id" id="emp_code_id">
                                            <option disabled selected>Select Employee Code</option>
                                            @foreach($employees as $data)
                                                @if(property_exists($data, "id"))
                                                    <option value="{{ $data->profile_registries_id }}"
                                                    >{{$data->code }}</option>
                                                @else
                                                    <option value="{{ $data->profile_registries_id }}"
                                                    >{{ $data->code }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <label for="exampleInputEmail1">Select Employee Name</label>
                                        <select type="text"
                                                data-placeholder="Select Employee Name" required
                                                class="form-control form-control-select2 select-clear employee_id"
                                                name="emp_id" id="emp_id">
                                            <option disabled selected>Select Employee Name</option>
                                            @foreach($employees as $data)
                                                @if(property_exists($data, "id"))
                                                    <option value="{{ $data->profile_registries_id }}"
                                                    >{{$data->name }}</option>
                                                @else
                                                    <option value="{{ $data->profile_registries_id }}"
                                                    >{{ $data->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>



                                    <div class="col-sm-3 mt-2">
                                        <label for="exampleInputEmail1">Select Location</label>
                                        <select type="text"
                                                data-placeholder="Select Location" required
                                                class="form-control form-control-select2 select-clear"
                                                name="location" id="location">
                                            <option disabled selected>Select Location</option>
                                            @foreach($locations as $data)
                                                @if(property_exists($data, "id"))
                                                    <option value="{{ $data->id }}"
                                                    >{{$data->name }}</option>
                                                @else
                                                    <option value="{{ $data->id }}"
                                                    >{{ $data->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-3 mt-2">
                                        <label for="exampleInputEmail1">Select Department</label>
                                        <select type="text"
                                                data-placeholder="Select Department" required
                                                class="form-control form-control-select2 select-clear"
                                                name="department" id="department">
                                            <option disabled selected>Select Department</option>
                                            @foreach($departments as $data)
                                                @if(property_exists($data, "id"))
                                                    <option value="{{ $data->id }}"
                                                    >{{$data->name }}</option>
                                                @else
                                                    <option value="{{ $data->id }}"
                                                    >{{ $data->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-3 mt-2">
                                        <label for="exampleInputEmail1">Select Designation</label>
                                        <select type="text"
                                                data-placeholder="Select Designation" required
                                                class="form-control form-control-select2 select-clear"
                                                name="designation" id="designation">
                                            <option disabled selected>Select Designation</option>
                                            @foreach($designations as $data)
                                                @if(property_exists($data, "id"))
                                                    <option value="{{ $data->id }}"
                                                    >{{$data->name }}</option>
                                                @else
                                                    <option value="{{ $data->id }}"
                                                    >{{ $data->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-sm-1 mt-2">
                                        <label for="exampleInputEmail1">#</label>
                                        <input type="button" name="calculate" class="btn btn-primary searchBtn" value="Search">
                                    </div>
                                    <div class="col-sm-2 mt-2">
                                        <label for="exampleInputEmail1">#</label> <br>
                                        <button class="btn btn-success btn-flat" onclick='generatePDF()'>Print All Report</button>
                                    </div>
                                </div>
                                </form>
                            @component('shared.admin.components.datatable')
                                @slot('head')  @endslot
                                @slot('body') @endslot
                            @endcomponent
                            <!-- /basic datatable -->
                            </div>
                        </div>
                    </div>
                </div>

                @include('shared.admin.footer')
            </div>

            <!-- /content area -->
        </div>

        <!-- /page content -->
    </div>

@endsection


@section('theme_js')
    <script type="text/javascript" src="{{ asset('js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/extensions/contextmenu.js') }}"></script>
    <script src="{{ asset('js/sweetalert.min.js') }}"></script>

    <script src="{{ asset('js/plugins/tables/datatables/extensions/jszip/jszip.min.js')}}"></script>
    <script src="{{ asset('js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{ asset('js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js')}}"></script>
    <script src="{{ asset('js/plugins/tables/datatables/extensions/buttons.min.js')}}"></script>

@endsection

@section('custom_js')
    {{--<script src="{{ url('js/datatable-test.js') }}"></script>--}}
    <script type="text/javascript">

        function generatePDF() {
            window.open(
                "{{url('hr/attendance-report/generateAllAttendancePDF/')}}",'_blank'
            );
            return;
        }



        $(".dataTables_scrollHeadInner").css({"width":"100%"});
        $(".table ").css({"width":"100%"});


        let table = $('.datatable-basic').DataTable(
            {
                "processing": true,
                "stateSave": true,
                "serverSide": true,
                "dom": 'r<"datatable-header"fBl>t<"datatable-footer"ip>',
                "buttons": {
                    dom: {
                        button: {
                            className: 'btn btn-mini btn-default'
                        },
                    },
                    buttons: [
                        {
                            extend: 'colvis',
                            text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                            className: 'btn bg-blue btn-icon custom-column',
                            columnText: function (dt, idx, title) {
                                return title;
                            },
                        }
                    ]
                },
                orderCellsTop: true,
                fixedHeader: true,

                // ajax: {
                //     "url": route,
                // },

                "ajax": {
                    dataType: "JSON",
                    type: "GET",
                    url: "{{route($dataTableRoute)}}",
                    data:  function (d) {
                        // d.reference = $('.searchRef').val(),
                            d.emp_id = $('#emp_id').val(),
                            d.start_date = $('#start_date').val(),
                            d.end_date = $('#end_date').val(),
                                d.location = $('#location').val(),
                                d.department = $('#department').val(),
                                d.designation = $('#designation').val()

                    },
                    // data: $('.input-form').serialize(),
                    async: true,
                    "dataSrc": function (json) {
                        $(".invalid-feedback").text("");
                        if (json.status == "warning") {
                            swal('warning', json.message);
                            return [];
                        } else if (json.status == "error") {
                            swal('warning', json.message);
                            return [];
                        } else {
                            return json.data;
                        }
                    }
                },
                "columns": <?php echo $columns;?>,

                "drawCallback": function (settings) {
                    function formSubmit(e, form) {
                        swal({
                            title: "Are you sure?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#FF7043",
                            confirmButtonText: "Yes"
                        }, function (isConfirm) {
                            if (isConfirm) {
                                form.submit();
                            }
                        });
                    }
                }
            }
        );

        $(".searchBtn").click(function(){
            table.draw();
        });


        let name = $('#name');
        $('#filter').on('click', function (e) {
            e.preventDefault();
            table.column(1).search(name.val()).draw();

        });

        $('#clear').on('click', function (e) {
            e.preventDefault();
            name.val(null).change();

            table.column(1).search(name.val()).draw();
        });

        function formSubmit(e, form) {
            swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff7043",
                confirmButtonText: "Yes"
            }, function (isConfirm) {
                if (isConfirm) {
                    form.submit();
                }
            });
        }


        $("#view-result").click(function(){
            // alert('dd')
            table.draw();
        });



        $('#create-modal-success-color').on('hidden.bs.modal', function (e) {
            $(".input-form").trigger("reset");
        })


        function exportData(type) {
            var searchValue = $('.dataTables_filter input').val();
            var exportWindow = window.open("export", "_blank")
            exportWindow.location.href = "{{$export_route}}/" + type + "/" + searchValue;
        }

        /**
         * created by : Dulan
         * created at : 23-09-2020
         * summary : column customization
         * */
        $('#data-tables').on('column-visibility.dt', function (e, settings, column, state) {
            let ar = [];
            for (var i = 0; i < settings.aoColumns.length; i++) {
                ar.push({
                    "name": settings.aoColumns[i].name,
                    "visible": settings.aoColumns[i].bVisible,
                    "title": settings.aoColumns[i].sTitle,
                    "searchable": settings.aoColumns[i].searchable,
                    "width": settings.aoColumns[i].width,
                    "sortable": settings.aoColumns[i].sortable,
                    "className": settings.aoColumns[i].className ? settings.aoColumns[i].className : "",
                });
            }
            $.ajax({
                url: '{{route("column-customizer.store")}}',
                type: 'post',
                dataType: 'json',
                data: {
                    "_token": "{{ csrf_token() }}",
                    columns: ar,
                    table: $("input[name=page_name]").val()
                },
                success: function (response) {
                },
                error: function () {

                }
            });
        });



    </script>

    <script>
        $(".employee_id").change(function(){
            var employee_id = $(this).val();
            $('.employee_id').val(employee_id).trigger('change.select2');
        });
    </script>
@endsection

