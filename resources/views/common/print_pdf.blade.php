<html>
<head>
    <title>{{ $title }}</title>
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">

</head>
<body class="bg-white">
<h3 class="text-center">{{ $heading}}</h3>
@if(isset($filter_text)|| !empty($filter_text))
    <h5>Search Text : {{$filter_text}}</h5>
@endif
<hr>
<div>

    @php
        $columnHeader = array();
               foreach ($columns as $co) {
                   $replace = ucwords(str_replace("_", " ", $co));
                   array_push($columnHeader, $replace);
               }
    @endphp
    <table class="table table-bordered">
        <thead>
        <tr>
            @foreach($columnHeader as $column)
                <th scope="col">{{$column}}</th>
            @endforeach
        </tr>
        </thead>
        <tbody>
        @php
            foreach ($data as $value) {
        @endphp
        <tr>
            @php
                $ar = array();
                foreach ($columns as $c) {
            @endphp
            <td>{{$value->$c}}</td>
            @php
                }
            @endphp
        </tr>
        @php

            }
        @endphp
        </tbody>
    </table>
</div>
</body>
</html>
