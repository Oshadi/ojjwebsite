<span class="badge badge-{{ (($obj->status  == 1) ? 'success' : 'danger') }} position-right">
    {{(($obj->status == 1) ? 'Active' : 'Blocked')}}
</span>
