@if ($paginator->hasPages())
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12">
            <!-- Pagination -->
            <div class="pagination-container margin-top-60 margin-bottom-60">
                <nav class="pagination">
                    <ul>
                        {{-- Previous Page Link --}}
                        @if ($paginator->onFirstPage())
                            <li class="pagination-arrow disabled" aria-disabled="true"
                                aria-label="@lang('pagination.previous')"><a href="#"><i
                                        class="icon-material-outline-keyboard-arrow-left"></i></a></li>
                        @else
                            <li class="pagination-arrow"><a href="{{ $paginator->previousPageUrl() }}" rel="prev"
                                                            aria-label="@lang('pagination.previous')"><i
                                        class="icon-material-outline-keyboard-arrow-left"></i></a></li>
                        @endif

                        {{-- Pagination Elements --}}
                        @foreach ($elements as $element)
                            {{-- "Three Dots" Separator --}}
                            @if (is_string($element))
                                <li><a href="#">{{ $element }}</a></li>
                            @endif

                            {{-- Array Of Links --}}
                            @if (is_array($element))
                                @foreach ($element as $page => $url)
                                    @if ($page == $paginator->currentPage())
                                        <li><a href="#" class="current-page">{{ $page }}</a></li>
                                    @else
                                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach

                        {{-- Next Page Link --}}
                        @if ($paginator->hasMorePages())
                            <li class="pagination-arrow"><a href="{{ $paginator->nextPageUrl() }}" rel="next"
                                                            aria-label="@lang('pagination.next')"><i
                                        class="icon-material-outline-keyboard-arrow-right"></i></a></li>
                        @else
                            <li class="pagination-arrow disabled" aria-disabled="true"
                                aria-label="@lang('pagination.next')"><a href="#"><i
                                        class="icon-material-outline-keyboard-arrow-right"></i></a></li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
@endif
