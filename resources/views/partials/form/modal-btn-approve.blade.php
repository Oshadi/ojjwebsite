<div class="btn-group">
    <button type="button" class="btn btn-success" data-route="{{$route}}" onclick="approveTransaction({{$id}}, this, 1)">Approve</button> &nbsp;&nbsp;&nbsp;
    <button type="button" class="btn btn-danger" data-route="{{$route}}" onclick="approveTransaction({{$id}}, this, 2)">Reject</button>&nbsp;&nbsp;&nbsp;
</div>

<button type="button" tabindex="-1"
        data-dismiss="modal"
        class="btn btn-light legitRipple access-multiple-clear">
    <span class="ladda-label">
        Cancel
    </span>
</button>

<script>
    $(".btn").button();
</script>
