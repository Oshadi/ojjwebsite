<div class="form-group has-feedback">
    @php $dTime = date('mdYHis') . uniqid() @endphp
    <label class="control-label">
        @if($required==true)
            <b>{{ $title }}</b>
            <small class="text-danger">*</small>
        @else
            <b>{{ $title }}</b>
        @endif
    </label>
    <div class="input-group clockpicker clockpicker-24h pull-center" data-placement="bottom" data-align="top"
         data-autoclose="true">
        <input autocomplete="off" placeholder="{{ $placeholder }}"
               @if($required) required @endif
               type="text" name="{{ $name }}"
               id="t{{$dTime}}"
               class="form-control {{ $errors->has($name) ? ' is-invalid' : '' }}"
               value="{{ $value }}">
        <span class="input-group-addon">
					<span class="glyphicon glyphicon-time"></span>
				</span>
    </div>
    @if ($errors->has($name))
        <span class="{{$name}}-error invalid-feedback invalid-feedback-span" role="alert"></span>
    @endif
</div>
<span class="validation-invalid-label  {{$name}}-error" role="alert"></span>

<script type="text/javascript">
    var input = $('.clockpicker');
    input.clockpicker({
        /*autoclose: true,
        twelvehour: false*/
        autoclose: true,
        twelvehour: false,
        placement: 'bottom',
        align: 'left',
        donetext: 'Done',
    });

</script>
