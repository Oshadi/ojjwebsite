<div class="form-group has-feedback">
    <label>
        @if(isset($required) && $required==true)
            <b>{{ $title }}</b>
            <small class="text-danger">*</small>
        @else
            <b>{{ $title }}</b>
        @endif
    </label>
    <div class="input-group" style="display: flex;flex-wrap: nowrap;">
        <select
            name="{{$name}}"
            data-placeholder="{{$placeholder}}"
            @if(isset($required) && $required==true) required @endif
            class="form-control form-control-select2 multiselect-select-all-filtering  {{@$class}}"
            multiple="multiple" data-fouc>

            @foreach($values as $data)
                <option
                    @if(isset($value)&& count($value)!=0)
                    @foreach($value as $ar)
                    @if($data->id==$ar) selected @endif

                    @if(isset($value_id))
                    @if($data->$value_id==$ar) selected @endif
                    @else
                    @if($data->id==$ar) selected @endif
                    @endif

                    @endforeach
                    @endif

                    @isset($disabled)
                    @if($disabled==true) disabled @endif
                    @endisset

                    @if(isset($value_id))
                    value="{{ $data->$value_id }}"
                    @else
                    value="{{ $data->id }}"
                    @endif
                >{{ isset($display_name)?$data->$display_name: $data->name }} </option>
            @endforeach

        </select>

        @if(isset($expand))
            <div class="input-group-append">
                <button type="button"
                        {{ (@$exroute)? 'data-route ='. route($exroute) : '' }}
                        data-resource="{{$name}}"
                        data-select="{{@$select}}"
                        data-backdrop="static"
                        id="create-modal-open"
                        class="btn btn-sm btn-outline-primary has-text legitRipple add-btn-common open-modal create-modal-open">
                    <i class="icon-plus2 "></i>
                </button>
            </div>
        @endif

    </div>
    <span class="{{str_replace("[]","",$name)}}-error validation-invalid-label " role="alert"></span>
</div>


