<div class="input-group has-feedback w-100">
<label for="{{$name}}">
    @if($required==true)
    <b>{{ $title }}</b>
    <small class="text-danger">*</small>
    @else
        <b>
    {{ $title }}</b>
    @endif
</label>


{{--    {{dd($required)}}--}}
    <input
        @isset($max_length)
        maxlength="{{$max_length}}"
        @endisset
        @isset($min_length)
        min="{{$min_length}}"
        @endisset
        @isset($disabled)
        @if($disabled==true) disabled @endif
        @endisset
        @isset($readonly) readonly  @endisset


        type="{{ $type }}" value="{{ $value }}" name="{{ $name }}" id="{{ $name }}"

        class="form-control px-2 {{$name}}-is-invalid {{@$class}}"
        placeholder="{{ $placeholder ?? 'Enter ' . $title }}"
        @if($required== true) required @endif
        @if(isset($onkeyup))
        onkeyup="{{$onkeyup}}"
        @endif
        @isset($oninput)
        oninput="{{$oninput}}"
            @endisset
    >
    <div class="input-group-append">
        <span class="input-group-text" id="basic-addon2">{{$sideMark}}</span>
    </div>
    <span class="validation-invalid-label  {{isset($error_class) ? $error_class : $name}}-error" role="alert"></span>

</div>


{{--<script>--}}
{{--    $('input[name={{ $name }}]').keyup(function () {--}}
{{--        if (this.value=="")--}}
{{--            $(".{{$name}}-error").show();--}}
{{--        else $(".{{$name}}-error").hide();--}}
{{--    }); --}}
{{--</script>--}}
