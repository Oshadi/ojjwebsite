    <input
        @isset($max_length)
        maxlength="{{$max_length}}"
        @endisset
        @isset($min_length)
        min="{{$min_length}}"
        @endisset
        @isset($disabled)
        @if($disabled==true) disabled @endif
        @endisset

        type="{{ $type }}" value="{{ $value }}" name="{{ $name }}"
        class="{{$name}}-is-invalid"

        @if($required==true) required @endif>