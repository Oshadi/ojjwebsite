<div class="form-group has-feedback">
    <label class="control-label">{{ $title }} </label>
    <input type="color" class="form-control colorpicker-show-input" data-preferred-format="name" name="{{ $name }}"
           value="{{ $value }}">
</div>
