<div class="form-group has-feedback">
    <label class="control-label">{{$title}}</label>
    <input type="file" name="{{$name}}"
           class="form-control-uniform" @if(isset($multiple)){{ $multiple==true ?"multiple":""}} @endif>
    <span class="validation-invalid-label  {{str_replace("[]","",$name)}}-error" role="alert"></span>
</div>
