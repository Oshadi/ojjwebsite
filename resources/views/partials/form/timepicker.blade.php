<div class="form-group has-feedback">
    @php $dTime = date('mdYHis') . uniqid() @endphp

    @if(isset($label) && $label || !isset($label))
    <label class="control-label">
        @if($required==true)
            <b>{{ $title }}</b>
            <small class="text-danger">*</small>
        @else
            <b>{{ $title }}</b>
        @endif
    </label>
    @endif
    <div class="input-group clockpicker pull-center" data-placement="bottom" data-align="top"
         data-autoclose="true">
        <input autocomplete="off" placeholder="{{ $placeholder }}"
               @if($required) required @endif
               @if(isset($disabled) && $disabled) disabled @endif
               type="text" name="{{ $name }}"
               id="t{{$dTime}}"
               class="form-control {{ $errors->has($name) ? ' is-invalid' : '' }} {{ isset($class) ? $class : '' }}"
               @isset($data_row_id)
               data-row-id="{{$data_row_id}}"
               @endisset
               value="{{ $value }}">
        <span class="input-group-addon">
					<span class="glyphicon glyphicon-time"></span>
				</span>
    </div>
    @if ($errors->has($name))
        <span class="{{$name}}-error invalid-feedback invalid-feedback-span" role="alert"></span>
    @endif
</div>

<script type="text/javascript">
    var input = $('.clockpicker');
    input.clockpicker({
        autoclose: true,
        twelvehour: true
    });
</script>
