{{--@include('partials.form.checkbox', ['name' => 'sample name',  'title' => 'sample title', 'required'=>false,'checked'=>false,'value'=>'1'])--}}

<div class="custom-control custom-checkbox custom-control-inline">
    <label class="form-check-label">
        <input type="checkbox" @if(isset($onchange)) onchange="{{$onchange}}" @endif class="form-check-inputx @if(isset($class)) {{$class}} @endif"
               value="{{$value}}"
               name="{{$name}}"
               @if(isset($data_row_id)) data-row-id="{{$data_row_id}}" @endif

               id="{{$id ?? $name}}"
               @if($checked==true | $checked=="1") checked @endif @if(isset($readonly) && $readonly ==true) readonly @endif>
        {{$title}}
    </label>
    <span class="{{$name}}-error invalid-feedback invalid-feedback-span" role="alert"></span>
</div>
