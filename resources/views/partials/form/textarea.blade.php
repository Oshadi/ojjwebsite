{{--
@include('partials.form.textarea', ['name' => 'description', 'title' => 'Description', 'placeholder' => 'enter description', 'icon' => '','required'=>'true','value'=>old('description')])
--}}

<div class="form-group has-feedback w-100">
    <label class="control-label">
        @if(isset($required) && $required==true)
            <b>{{ $title }}</b>
            <small class="text-danger">*</small>
        @else
            <b>{{ $title }}</b>
            @if(isset($showRedStar) && $showRedStar==true)
                <small class="text-danger">*</small>
            @endif
        @endif
    </label>
    <textarea @if(isset($rows))  rows="{{$rows}}" @endif id="{{ $name }}" name="{{ $name }}"
              class="form-control {{$name}}-is-invalid {{@$class}}"
              placeholder="{{ $placeholder ?? 'Enter ' . $title  }}"
              @if(isset($required) && $required==true) required @endif
              @isset($disabled)
              @if($disabled==true) disabled @endif
              @endisset
              @isset($readonly) readonly  @endisset
    >{{ $value }}</textarea>


    <span class="validation-invalid-label {{@$class}} {{ $id??$name}}-error" role="alert"></span>

</div>
