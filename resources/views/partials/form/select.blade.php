{{--@include('partials.form.select', ['name' => 'unit_type_id',  'title' => 'Base Unit Type', 'placeholder' => 'select unit type',  'required'=>'true','values'=>[],'value'=>old('unit_type_id'),'plusBtn'=>'true'])--}}

<div class="form-group has-feedback">
    <?php
    $displayproperty = (isset($displayproperty)) ? $displayproperty : "name";
    ?>
    @if(!isset($label) || $label==true)
        <label>
            @if(isset($required) && $required==true)
                <b> {{$title}}</b>
                <small class="text-danger">*</small>
            @else
                <b> {{$title}}</b>
                @if(isset($showRedStar) && $showRedStar==true)
                    <small class="text-danger">*</small>
                @endif
            @endif
        </label>
    @endif
    <div class="input-group" style="display: flex;flex-wrap: nowrap;">
        <select type="text"
                @isset($disabled)
                @if($disabled==true) disabled @endif
                @endisset
                @isset($onchange)
                onchange="{{$onchange}}"
                @endisset

                data-placeholder="{{$placeholder ?? 'Select ' . $title}}"
                @if(isset($required) && $required==true) required @endif
                {{-- data-select2-id="239" form-control-select2 --}}
                class="form-control form-control-select2 {{$name}}-is-invalid select-clear {{ @$class }}"
                name="{{$name}}"
                id="{{$id ?? $name}}"
        >
            <option disabled selected value="">{{$placeholder ?? 'Select ' . $title}}</option>
            @foreach($values as $data)
                @if(property_exists($data, "code"))
                    <option
                            @if($data->id==($value ?? old($name))) selected @endif

                    @if(isset($data_attribute)) data-{{$data_attribute}}="{{$data->$data_attribute}}" @endif
                            value="{{ $data->id }}"
                    >{{ $data->code." - ".$data->$displayproperty }}</option>
                @else
                    <option
                            @if($data->id==($value ?? old($name)))  selected
                            @endif @if(isset($data_attribute))  data-{{$data_attribute}}="{{$data->$data_attribute}}"
                            @endif
                            value="{{ $data->id }}"
                    >{{ $data->$displayproperty }}</option>
                @endif
            @endforeach
        </select>
        @if(isset($expand))
            <div class="input-group-append">
                <button type="button"
                        {{ (@$exroute)? 'data-route ='. route($exroute) : '' }}
                        data-resource="{{$name}}"
                        data-select="{{@$select}}"
                        data-backdrop="static"
                        id="create-modal-open"
                        class="btn btn-sm btn-outline-primary has-text legitRipple add-btn-common open-modal create-modal-open">
                    <i class="icon-plus2 "></i>
                </button>
            </div>
        @endif
    </div>
    <span class="validation-invalid-label  {{$name}}-error" role="alert"></span>
</div>

