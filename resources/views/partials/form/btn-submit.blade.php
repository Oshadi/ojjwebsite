<div class="actions clearfix">
    <ul role="menu" aria-label="Pagination">
        <li aria-hidden="false" aria-disabled="false">
            <button type="reset" tabindex="-1" class="btn btn-outline-warning legitRipple access-multiple-clear" id="reset">
                <span class="ladda-label">Clear</span>
            </button>
        </li>
        <li aria-hidden="false" aria-disabled="false">
            <a href="#" type="submit" class="btn btn-success legitRipple btn-ladda form-submit-button "
               id="submit-button" role="menuitem">
                <span class="ladda-label">{{$title}}</span>
                <span class="ladda-spinner"></span>
            </a>
        </li>
        <li aria-hidden="false" aria-disabled="false">
            <a href="#" type="submit" class="btn btn-primary legitRipple btn-ladda form-submit-button-create-and-new"
               id="submit-button" role="menuitem">
                <span class="ladda-label">{{$title}} and New </span>
                <span class="ladda-spinner"></span>
            </a>
            @if(isset($addRoute))
                <a href="{{$addRoute}}" class="create-new-button"></a>
            @endif
        </li>

    </ul>
</div>

{{--<div class="text-right">--}}

{{--    <button type="submit"--}}
{{--            class=" form-submit-button btn btn-success btn-ladda btn-ladda-spinner ladda-button legitRipple"--}}
{{--            id="submit-button" data-spinner-color="#fff" data-style="zoom-out">--}}
{{--        <span class="ladda-label"></span>--}}
{{--        <span class="ladda-spinner"></span></button>--}}

{{--    <button type="button"--}}
{{--            class="form-submit-button-create-and-new btn btn-primary btn-ladda btn-ladda-spinner ladda-button legitRipple"--}}
{{--            id="submit-button" data-spinner-color="#fff" data-style="zoom-out">--}}
{{--        <span class="ladda-label"> and New</span>--}}
{{--        <span class="ladda-spinner"></span></button>--}}

{{--</div>--}}
<script>
    $('#reset').click(function (e) {
        $(".select").each(function (index) {
            $(this).val('').change();
        });
    });
</script>
