 <div class="form-check form-check-inline">
    <label class="form-check-label">
        <input type="radio" class="form-check-input"  
               value="{{$value}}"
               name="{{$name}}"
               @if(isset($required))
               required="{{$required}}"
               @endif
               @if(isset($disabled))
               disabled="{{$disabled}}"
               @endif
               @if($checked==true | $checked=="1") checked @endif>
        {{$title}}
    </label>
    <span class="{{$name}}-error invalid-feedback invalid-feedback-span" role="alert"></span>
</div>
