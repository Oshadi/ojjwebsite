{{--
@include('partials.form.input', ['name' => 'name', 'type' => 'text', 'title' => 'Skill Name', 'placeholder' => 'enter skill...', 'icon' => '','required'=>'true','value'=>old('name'), 'readonly'=>'readonly'])
--}}
<div class="form-group has-feedback w-100">
    @if(!isset($label) || $label==true)
        <label class="control-label">
            @if(isset($required) && $required==true)
                <b>{{ $title }}</b>
                <small class="text-danger">*</small>
            @else
                <b>{{ $title }}</b>
                @if(isset($showRedStar) && $showRedStar==true)
                    <small class="text-danger">*</small>
                @endif
            @endif
        </label>
    @endif

    <input
        @isset($max_length)
        maxlength="{{$max_length}}"
        @endisset
        @isset($onchange)
        onchange="{{$onchange}}"
        @endisset

        @isset($multiple)
        multiple
        @endisset

        @isset($oninput)
        oninput="{{$oninput}}"
        @endisset

        @isset($pattern)
        pattern="{{$pattern}}"
        @endisset

        @isset($data_row_id)
        data-row-id="{{$data_row_id}}"
        @endisset

        @isset($min_length)
        min="{{$min_length}}"
        minlength="{{$min_length}}"
        @endisset
        @isset($disabled)
        @if($disabled==true) disabled @endif
        @endisset
        @if( isset($readonly) && $readonly == true) readonly @endif


        type="{{ $type }}" value="{{ $value }}" name="{{ $name }}" id="{{ $id ?? $name }}"

        class="form-control-file {{$name}}-is-invalid {{@$class}}"
        placeholder="{{ $placeholder }}"
        @if(isset($required) && $required==true) required @endif
    >

    <span class="validation-invalid-label  {{$name}}-error" role="alert"></span>
</div>


{{--<script>--}}
{{--    $('input[name={{ $name }}]').keyup(function () {--}}
{{--        if (this.value=="")--}}
{{--            $(".{{$name}}-error").show();--}}
{{--        else $(".{{$name}}-error").hide();--}}
{{--    }); --}}
{{--</script>--}}
