<button type="button" tabindex="-1"
        data-dismiss="modal"
        class="btn btn-light legitRipple access-multiple-clear mr-1"
>
    <span class="ladda-label">
        Cancel
    </span>
</button>
{{--<button type="reset" tabindex="-1" class="btn btn-outline-info legitRipple access-multiple-clear">--}}
    {{--<span class="ladda-label">Reset</span>--}}
{{--</button>--}}
<div class="btn-group m-1">
    <button type="button" class="btn btn-primary form-submit-button">{{$title}}</button>
    {{--<button type="button" class="btn btn-primary dropdown-toggle " data-toggle="dropdown"></button>
    <div class="dropdown-menu dropdown-menu-right">
        <a href="#" class="dropdown-item create-new-form-submit-button">{{$title}} and New</a>
    </div>--}}
</div>
@if(isset($addRoute))
    <a href="{{$addRoute}}" class="create-new-button"></a>
@endif

<script>
    $(".btn").button();
</script>
