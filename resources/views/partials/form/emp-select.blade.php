<div class="form-group has-feedback">
    <label class="control-label">{{$title}}
        @if($required)
            <small class="text-danger">*</small>
        @endif
    </label>
    <select
        data-placeholder="{{$placeholder}}"
        @if($required) required @endif
        class="select select-size-lg {{$name}}-is-invalid" name="{{$name}}"
    >
        <option></option>
        @foreach($values as $data)
            @if(property_exists($data, "code"))
                <option
                    @if($data->id==$value) selected @endif
                value="{{ $data->id }}"
                >{{ $data->code." - ".$data->name }}</option>
            @else
                <option
                    @if($data->id==$value) selected @endif
                value="{{ $data->id }}"
                >{{ $data->display_name }}</option>
            @endif
        @endforeach
    </select>
    <span class="invalid-feedback invalid-feedback-span {{$name}}-error" role="alert"></span>
</div>



