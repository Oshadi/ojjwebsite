{{--@include('partials.form.select-custom-display-value', [ 'name' => 'role_type_id',  'title' => 'Role Type', 'placeholder' => 'select role type',  'required'=>'true','values'=>$roleTypes,'value'=>old('role_type_id'),'value_id'=>'id','display_name'=>'role_type',])--}}

<div class="form-group has-feedback">
    <label class="control-label">

        @if($required==true)
            <b>{{$title}}</b>
            <small class="text-danger">*</small>
        @else
            {{$title}}
        @endif
    </label>
    <select
        @isset($disabled)
        @if($disabled==true) disabled @endif
        @endisset
        @isset($onchange)
        onchange="{{$onchange}}"
        @endisset
        data-placeholder="{{$placeholder ?? 'Select ' . $title}}"
        @if($required==true) required @endif
        class="form-control form-control-select2 {{$name}}-is-invalid select-clear @if(isset($className)) {{$className}} @endif"
        name="{{$name}}" id="{{$name}}"
    >
        <option disabled selected>{{$placeholder ?? 'Select ' . $title}}</option>
        @foreach($values as $data)
            <option
                @if($data->$value_id==$value) selected @endif
                value="{{ $data->$value_id }}"
            >{{ $data->$display_name }}</option>
        @endforeach
    </select>
    <span class="validation-invalid-label  {{$name}}-error" role="alert"></span>

</div>
<script>
    $('.select-size-sm').select2();
</script>
