<div class="form-group form-group-material has-feedback has-feedback-left">
    <label class="control-label">{{ $title }}</label>
    <input id="email" type="{{ $type }}"
           class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}"
           name="{{ $name }}" value="{{ old($name) }}"
           placeholder="{{ $placeholder }}" required>
    @if ($errors->has($name))
        <span class="invalid-feedback" role="alert"></span>
    @endif
    <div class="form-control-feedback">
        <i class="{{ $icon }} text-muted"></i>
    </div>
    <span class="{{$name}}-error invalid-feedback invalid-feedback-span" role="alert"></span>

</div>
