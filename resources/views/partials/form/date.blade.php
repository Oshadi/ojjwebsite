<div class="form-group has-feedback">
    <label class="control-label">
        <b>{{ $title }}
        @if(isset($required) && $required==true)
            <small class="text-danger">*</small>
        @endif
        </b>
    </label>
    <input type="date" class="form-control pickadate-year {{@$class ? $class : ''}}" @if(isset($required) && $required==true) required
           @endif placeholder="{{ $placeholder }}"
           value="{{ $value }}" name="{{ $name }}">
    {{--    @if ($errors->has($name))--}}
{{--    <span class="{{isset($error_class) ? $error_class : $name}}-error invalid-feedback invalid-feedback-span" role="alert"></span>--}}
    <span class="validation-invalid-label  {{$name}}-error" role="alert"></span>
    {{--    @endif--}}
</div>
