@if(count($customFields) > 0)
<hr>
<h3><b>Custom Fields</b></h3>
<div class="row">
    @foreach($customFields as $customField)
    <div class="col-md-{{ $colsize }}">
        <div class="form-group">
            <?php 
            $fieldName = str_replace(' ', '-', (str_replace('_', '-', $customField->field_name)));
            $fieldName = 'customfield_' . $fieldName . '_' . $customField->display_section . '_' . $customField->id;
            switch($customField->type){
                case 1: // Text 
                ?>
                      @include('partials.form.input', ['name' => $fieldName, 'type' => 'text', 'title' => $customField->field_label, 'placeholder' => 'Type '. $customField->field_label, 'icon' => '','required'=>  (($customField->validation == 'true')?  true : false ),'value'=>old($fieldName)])
                <?php 
                    break;
                case 2: // Text area
                ?>
                      @include('partials.form.textarea', ['name' => $fieldName, 'type' => '', 'title' => $customField->field_label, 'placeholder' => 'Type '. $customField->field_label, 'icon' => '','required'=>(($customField->validation == 'true')?  true : false ),'value'=>old($fieldName)])                
                <?php 
                    break;
                case 3: // Single Select
                ?>
                    @include('partials.form.select', ['name' => $fieldName, 'select' => 'text', 'title' => $customField->field_label, 'placeholder' => 'Select '. $customField->field_label, 'icon' => '','required'=> (($customField->validation == 'true')?  true : false ),'values'=> $customField->fieldOptions, 'value' => '', 'displayproperty' => 'option'] )
                <?php 
                    break;
                case 4: // Multi Select
                ?>
                       @include('partials.form.select-multiple', ['name' => $fieldName.'[]', 'title' =>  $customField->field_label, 'placeholder' => 'Select '. $customField->field_label, 'required'=> (($customField->validation == 'true')?  true : false ),'values'=> $customField->fieldOptions,'value'=>[], 'display_name' => 'option' ])
                <?php 
                    break;
                case 5: // Single Select Checkbox
                ?>
                    @foreach($customField->fieldOptions as $option)
                        @include('partials.form.checkbox', ['name' => $fieldName.'[]', 'title' => $option->option, 'placeholder' => '', 'checked' => false ,'icon' => '','required'=>true,'value'=>$option->option])
                    @endforeach                  
                <?php 
                    break;
                case 6: // Checkbox multi select
                ?>
                    @foreach($customField->fieldOptions as $option)
                        @include('partials.form.checkbox', ['name' => $fieldName.'[]', 'title' => $option->option, 'placeholder' => '', 'checked' => false ,'icon' => '','required'=>true,'value'=>$option->option])
                    @endforeach   
                <?php 
                    break;
                case 7: // Radio buttom 
                ?>
                    @foreach($customField->fieldOptions as $option)
                        @include('partials.form.radio', ['name' => $fieldName, 'checked' => false ,'title' => $option->option ,'value'=> $option->option])
                    @endforeach                     
                <?php 
                    break;
                case 8: // Number
                ?>
                    @include('partials.form.input', ['name' => $fieldName, 'type' => 'number', 'title' => $customField->field_label, 'placeholder' => 'Type '. $customField->field_label, 'icon' => '','required'=>  (($customField->validation == 'true')?  true : false ),'value'=>old($fieldName)])
                <?php 
                    break;
            }
            ?>
        </div>
    </div> 
    @endforeach
</div>
@endif