
<div class="col-sm-12 d-flex justify-content-end">
    @if($permissions['status']==true)
        <label class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="status{{$data['row']->id}}"
                   @if($data['row']->status==1)
                   onchange="active_inactive_record('{{route($data["module"].".".$data["route"].".inactive", $data['row']->id)}}','{{ $data['row']->id }}')   "
                   checked
                   @else
                   onchange="active_inactive_record('{{route($data["module"].".".$data["route"].".active", $data['row']->id)}}','{{ $data['row']->id }}')"
                   @endif
                   data-fouc>&nbsp;
            <span class="custom-control-label"></span>
        </label>
    @endif


    @if($permissions['view']==true)
        @if(isset($permissions['modal']) && $permissions['modal']==false)
            <a href="{{route($data["module"].".".$data["route"].".show",$data['row']->id)}}"><i
                    class="icon-list ml-2" title="View"></i></a>
        @endif
        @if(!isset($permissions['modal']) || $permissions['modal']==true)
            <a style="display: none" href="javascript:void(0)" id="view-route-url{{$data['row']->id}}"
               data-name="{{route($data["module"].".".$data["route"].".show",$data['row']->id)}}"></a>

            <a style="cursor: pointer;"
               data-backdrop="static"
               class="  open-modal"
               data-toggle="modal" title="View"
               onclick="viewModal({{$data['row']->id}},'#view-route-url{{$data['row']->id}}')">
                <i class="icon-eye ml-2"></i>
            </a>
        @endif
    @endif

    @if($permissions['edit']==true)
        @if(isset($permissions['modal']) && $permissions['modal']==false)
            <a href="{{route($data["module"].".".$data["route"].".edit",$data['row']->id)}}"><i
                    class="icon-pencil7 ml-2" title="Edit"></i></a>
        @endif
        @if(!isset($permissions['modal']) || $permissions['modal']==true)
            <a style="display: none" href="javascript:void(0)"
               id="edit{{$data["route"] ? '-'.$data["route"].'-' : '-'}}route-url{{$data['row']->id}}"
               data-name="{{route($data["module"].".".$data["route"].".edit",$data['row']->id)}}"></a>

            <a style="cursor: pointer;"
               data-backdrop="static"
               class="  open-modal"
               data-toggle="modal" title="Edit"
               onclick="editModal({{$data['row']->id}},'#edit{{$data["route"] ? '-'.$data["route"].'-' : '-'}}route-url{{$data['row']->id}}')"><i
                    class="icon-pencil7 ml-2"></i>
            </a>
        @endif
        {{--@else--}}
        {{--    <p>-</p>--}}
    @endif

    @if($permissions['delete']==true)
        @if(!isset($permissions['modal']) || $permissions['modal']==true)
            <a style="display: none" href="javascript:void(0)" id="edit-route-url{{$data['row']->id}}" title="Delete"
               data-name="{{route($data["module"].".".$data["route"].".edit",$data['row']->id)}}"></a>

            <a style="cursor: pointer;"
               data-backdrop="static"
               class="mr-1 ml-1  open-modal"
               data-toggle="modal" title="Delete"
               onclick="deleteModal({{$data['row']->id}},this)"
               data-name="{{route($data["module"].".".$data["route"].'.destroy',[$data['row']->id])}}"
               data-page-refresh="{{$data["page_refresh"] ?? false}}"><i class="fas fa-trash-alt"></i></i>
            </a>
        @endif
    @endif
</div>


@if($permissions['status']==true)
    <script>
        var elems = Array.prototype.slice.call(document.querySelectorAll('.form-input-switchery'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html);
        });
    </script>
@endif


