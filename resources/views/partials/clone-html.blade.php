<div class="sections-outer">
    <div class="section">
        <fieldset>
            <legend>User</legend>
            <p>
                <label for="firstName">First Name:</label>
                <input name="firstName[]" id="firstName" value="" type="text" />
            </p>

            <p>
                <label for="lastName">Last Name:</label>
                <input name="lastName[]" id="lastName" value="" type="text" />
            </p>

            <p><a href="#" class='remove'>Remove Section</a></p>

        </fieldset>
    </div>
</div>

<p><a href="#" class='addsection'>Add Section</a></p>
