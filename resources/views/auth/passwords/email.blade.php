@extends('layouts.app')


@section('content')
    <div class="wrapper auth-page-wrapper">
        <!-- =========================
            Header
        =========================== -->

    @include('web::shared.header')

    <!-- /.Header -->

        <section class="login-section p-0">
            <div class="container-fluid h-100">
                <div class="row h-100">
                    <div class="col-sm-6 p-0">
                        <img class="w-100 h-100 object-fit-cover" src="https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg" alt="login-img">
                    </div>
                    <div class="col-sm-6">
                        <div class="row h-100 d-flex justify-content-center align-items-center">
                            <div class="col-sm-12 col-md-10 col-lg-8">
                                <div class="row">
                                    <!-- Login Form -->
                                    <div class="col-sm-12">
                                        <form>
                                            <div class="row">
                                                <div class="col-sm-12 mb-4">
                                                    <h4 class="f-18 m-0">Forget Password</h4>
                                                </div>

                                                <div class="col-sm-12 form-group mt-2 mb-2">
                                                    <label for="exampleInputEmail1">Enter your email</label>
                                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                                </div>
                                                <div class="col-sm-12 mt-3 mb-2 d-flex justify-content-center">
                                                    <button type="submit" class="btn btn-primary">Continue</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!--/ Login Form -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="module__search-container">
            <i class="fa fa-times close-search"></i>
            <form class="module__search-form">
                <input type="text" class="search__input" placeholder="Type Words Then Enter">
                <button class="module__search-btn"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.module-search-container -->

        <button id="scrollTopBtn"><i class="fa fa-long-arrow-up"></i></button>
    </div><!-- /.wrapper -->
@endsection
