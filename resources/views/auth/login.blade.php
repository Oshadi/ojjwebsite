@extends('layouts.app')

@section('content')
    <div class="wrapper auth-page-wrapper">
        <!-- =========================
            Header
        =========================== -->

    @include('web::shared.header')

    <!-- /.Header -->

        <section class="login-section p-0">
            <div class="container-fluid h-100">
                <div class="row h-100">
                    <div class="col-sm-6 p-0">
                        <img class="w-100 h-100 object-fit-cover" src="{{ asset('custom_assets/images/login-image.jpg') }}" alt="login-img">
                    </div>

                    <div class="col-sm-6">

                        @if (session('success'))
                            <div class="alert alert-success" style="background-color: limegreen">
                                {!! session('success') !!}
                            </div>
                        @endif

                        <div class="row h-100 d-flex justify-content-center align-items-center">
                            <div class="col-sm-12 col-md-10 col-lg-8">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="nav nav-tabs login-tabs d-flex justify-content-center" id="loginTab" role="tablist">

                                            <li class="nav-item">
                                                <a class="nav-link active text-center f-14" id="retailer-login-tab" data-toggle="tab" href="#retailer" role="tab" aria-controls="retailer" aria-selected="false">
                                                    <div class="f-26">
                                                        <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                                    </div>
                                                    User Login
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-sm-12 mt-3">
                                        <div class="tab-content" id="loginTabContent">

                                            <div class="tab-pane fade show active" id="retailer" role="tabpanel" aria-labelledby="retailer-login-tab">
                                                <div class="row">
                                                    <!-- Login Form -->
                                                    <div class="col-sm-12">
                                                        <form method="POST" action="{{ route('login') }}">
                                                            @csrf
                                                            <div class="row">
                                                                <div class="col-sm-12 form-group mb-2">
                                                                    <label for="email"
                                                                           class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                                                    <input id="email" type="email"
                                                                           class="form-control @error('email') is-invalid @enderror"
                                                                           name="email" value="{{ old('email') }}"
                                                                           required autocomplete="email" autofocus>

                                                                    @error('email')
                                                                    <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                    @enderror
                                                                </div>
                                                                <div class="col-sm-12 form-group mb-2">

                                                                    <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>

                                                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                                                        @error('password')
                                                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror

                                                                </div>
                                                                <div class="col-sm-12 d-flex justify-content-between align-items-center">
                                                                    <div class="form-check">
                                                                        <input type="checkbox" class="form-check-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                                        <label class="form-check-label" for="remember">Remember me</label>
                                                                    </div>
                                                                    <a href="{{ route('password.request') }}" class="f-14 m-0">Forget Password?</a>
                                                                </div>
                                                                <div class="col-sm-12 mt-3 mb-2 d-flex justify-content-center">
                                                                    <div class="text-center">
                                                                        <button type="submit" class="btn btn-primary">Login</button>
                                                                        <h4 class="f-14 mt-2 mb-2">Or</h4>
                                                                        <button type="button" onclick="location.href='{{route("openAnAccount")}}'" class="btn btn-secondary">Register</button>
                                                                    </div>
                                                                </div>

<!--                                                                <div class="col-sm-12 mt-4 d-flex align-items-center">
                                                                    <h4 class="f-14 m-0">Sign up with</h4>
                                                                    <div class="ml-3 d-flex align-items-center">
                                                                        <a href="#" class="f-20 d-flex justify-content-center align-items-center mr-2 border-circle w-36-px h-36 bg-google">
                                                                            <i class="fa fa-google text-white" aria-hidden="true"></i>
                                                                        </a>

                                                                        <a href="#" class="f-20 d-flex justify-content-center align-items-center mr-2 border-circle w-36-px h-36 bg-facebook">
                                                                            <i class="fa fa-facebook text-white" aria-hidden="true"></i>
                                                                        </a>

                                                                        <a href="#" class="f-20 d-flex justify-content-center align-items-center mr-2 border-circle w-36-px h-36 bg-insta">
                                                                            <i class="fa fa-instagram text-white" aria-hidden="true"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>-->
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <!--/ Login Form -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="module__search-container">
            <i class="fa fa-times close-search"></i>
            <form class="module__search-form">
                <input type="text" class="search__input" placeholder="Type Words Then Enter">
                <button class="module__search-btn"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.module-search-container -->

        <button id="scrollTopBtn"><i class="fa fa-long-arrow-up"></i></button>
    </div><!-- /.wrapper -->
@endsection
