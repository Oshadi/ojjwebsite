<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@index')->name('index');

Route::get('/rescue-boat', 'WebController@viewRescueBoat')->name('rescue_boat');

// Landing page news related routes
Route::get('/news', 'WebController@viewNews')->name('news');
Route::get('/news/{category}', 'WebController@viewNews')->name('load-news-category');
Route::get('/news/{category}/{news}', 'WebController@viewNews')->name('load-news');
Route::get('/news/{category}/{news}/{page_no}', 'WebController@viewNews')->name('load-news-pagination');

Route::get('/product', 'WebController@viewProductInformation')->name('product');
Route::get('/product-details', 'WebController@getDetailsProduct')->name('get-product-details');
Route::get('/maintenance', 'WebController@viewMaintenance')->name('maintenance');
Route::get('/about-us', 'WebController@viewAboutUs')->name('about_us');
Route::post('/store', 'WebController@store')->name('store');
Route::get('/contact-us', 'WebController@viewContactUs')->name('contact_us');
Route::get('/privacy-policy', 'WebController@viewPrivacyPolicy')->name('privacy_policy');
Route::get('/site-map', 'WebController@viewSiteMap')->name('site_map');
Route::get('/popup-video', 'WebController@viewPopupVideo')->name('popup_video');
Route::get('/popup-video2', 'WebController@viewPopupVideo2')->name('popup_video_2');
Route::get('/popup-video3', 'WebController@viewPopupVideo3')->name('popup_video_3');


