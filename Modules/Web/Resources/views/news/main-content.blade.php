<!--News Main Content-->
<div class="col-sm-12 col-md-12 col-lg-8 news-main-content">
    <!--News Title-->
    <div class="news-title mb-4">
        <!--News Title Text-->
        <h2 class="mt-0 mb-2 f-36 yu-gothic-reg fw-500 black-color">
            {{ $mainNews->title ?? '' }}
        </h2>
        <!--News Title Text-->
        <!--News Date-->
        <p class="m-0 f-18 yu-gothic-reg fw-500 black-color">
            {{ $mainNews->added_date ?? '' }}
        </p>
        <!--News Date-->
    </div>
    <!--News Title-->

    <!--News Button-->
    <div class="d-flex">
        @isset($mainNews->newsCategory)
        <a href="{{ route('load-news-category', $mainNews->newsCategory->slug) }}" class="c-btn yellow-btn"> {{ $mainNews->newsCategory->name }}</a>
        @endisset
    </div>
    <!--News Button-->

    <!--News Content Inner-->
    <div class="news-content-inner mt-5">

        <!--News Content Inner Image-->
        @isset($mainNews->main_image)
            <div class="w-100 news-content-img mb-4">
                <img class="br-2 w-100 h-auto" src="{{ $mainNews->image_path }}" alt="news-content-img">
            </div>
        @endisset
        <!--News Content Inner Image-->

        <!--News Content Inner Text-->
        <p class="mt-0 mb-5 f-18 yu-gothic-reg fw-400 black-color">
            {{ $mainNews->description ?? '' }}
        </p>
        <!--News Content Inner Text-->

        @isset($mainNews->newsImages)
            @foreach($mainNews->newsImages as $subDetails)
                <!--News Content Inner Image-->
                    @isset($subDetails->image)
                        <div class="w-100 news-content-img mb-4">
                            <img class="br-2 w-100 h-auto" src="{{ $subDetails->image_path }}" alt="news-content-img">
                        </div>
                    @endisset
                    <!--News Content Inner Image-->

                    <!--News Content Inner Text-->
                    <p class="mt-0 mb-5 f-18 yu-gothic-reg fw-400 black-color">
                        {{ $subDetails->description }}
                    </p>
                    <!--News Content Inner Text-->
            @endforeach
        @endisset

    </div>
    <!--News Content Inner-->

</div>
<!--News Main Content-->
