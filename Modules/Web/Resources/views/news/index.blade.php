@extends('web::layouts.master')
@section('content')

    @include('web::shared.secondary_header')

    <!--News-->
    <section class="news-section home-section">
        <div class="container">
            <div class="row">
                <!--News Title-->
                <div class="col-sm-12 col-md-12 pb-3 section-header">
                    <h1 class="m-0">
                        Information
                        <span>お知らせ</span>
                    </h1>
                </div>
                <!--News Title-->

                <!--News Content-->
                <div class="col-sm-12 col-md-12 mt-3 mt-md-5 news-content">
                    <div class="row">

                    <!--News Main Content-->
                    @include('web::news.main-content')
                    <!--News Main Content-->

                        <!--News List Content-->
                        <div class="col-sm-12 col-md-12 col-lg-4 new-list-content">
                            <div class="row">

                            <!--News Category List-->
                            @include('web::news.category-list')
                            <!--News Category List-->

                            <!--News Card List-->
                            @include('web::news.card-list')
                            <!--News Card List-->

                            </div>
                        </div>
                        <!--News List Content-->
                    </div>
                </div>
                <!--News Content-->
            </div>
        </div>
    </section>
    <!--News-->

    @include('web::shared.footer')

@endsection
