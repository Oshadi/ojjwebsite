<!--News Card List-->
<div class="col-sm-12 col-md-12 news-card-list">
    <!--News Card List Title-->
    <h6 class="mt-0 mb-3 f-18 yu-gothic-reg fw-400 black-color">
        {{ $subNewsName }}
    </h6>
    <!--News Card List Title-->

    <div class="row">
        <!--News Card-->

        @isset($allNews)
            @foreach($allNews as $news)
                <div class="col-sm-12 col-md-6 col-lg-12 news-list-card mb-4">
                    <a href="{{ route('load-news',['category'=>$news->newsCategory->slug,'news'=>$news->slug] ) }}">
                        <div class="row">
                            <!--News Card Image-->
                            <div class="col-4 col-sm-4 col-md-4">
                                <img class="object-fit-cover br-2" src="{{ $news->image_path }}" alt="news-list-card-img">
                            </div>
                            <!--News Card Image-->

                            <!--News Card Content-->
                            <div class="col-8 col-sm-8 col-md-8 pl-0">
                                <!--News Card Content Title-->
                                <h6 class="mt-0 mb-2 f-14 yu-gothic-reg fw-600 black-color lh-24 h-48px txt-overflow txt-overflow-2">
                                    {{ $news->title }}
                                </h6>
                                <!--News Card Content Title-->

                                <!--News Card Content Button-->
                                <div class="d-flex">
                                    <div class="c-btn yellow-btn"> {{ $news->newsCategory->name }}</div>
                                </div>
                                <!--News Card Content Button-->
                            </div>
                            <!--News Card Content-->
                        </div>
                    </a>
                </div>
        @endforeach
    @endisset
    <!--News Card-->
    </div>
</div>

<div class="col-sm-12 col-md-12 mt-5 d-flex justify-content-end">
    <nav class="custom-pagination" aria-label="...">
        <ul class="pagination">
            <li class="page-item"><a class="page-link" href="{{ route('load-news-pagination',['category'=>$newsCardPagination['categorySlug'],'news'=>$newsCardPagination['newsSlug'], 'page_no'=>$newsCardPagination['currentPage']-1] ) }}">Previous</a></li>

            @isset($newsCardPagination)
                @for( $i = 1; $i <= $newsCardPagination['noOfPage']; $i++)
                    <li class="page-item @if($newsCardPagination['currentPage'] == $i) active @endif"
                        aria-current="page">
                        <a class="page-link"
                           href="{{ route('load-news-pagination',['category'=>$newsCardPagination['categorySlug'],'news'=>$newsCardPagination['newsSlug'], 'page_no'=>$i] ) }}"> {{ $i }}</a>
                    </li>
                @endfor
            @endisset

            <li class="page-item"><a class="page-link" href="{{ route('load-news-pagination',['category'=>$newsCardPagination['categorySlug'],'news'=>$newsCardPagination['newsSlug'], 'page_no'=>$newsCardPagination['currentPage']+1] ) }}">Next</a></li>
        </ul>
    </nav>
</div>
<!--News Card List-->
