<!--News Category List-->
<div class="col-sm-12 col-md-12 news-category-list mb-5">
    <!--News Category List Title-->
    <h6 class="mb-2 f-18 yu-gothic-reg fw-400 black-color">
        カテゴリー
    </h6>
    <!--News Category List Title-->

    <!--News Category List Content-->
    <ul class="mb-0">

        @isset($newsCategories)
            @foreach($newsCategories as $newsCategory)
                <li>
                    <!--News Category List Item-->
                    <a href="{{ route('load-news-category', $newsCategory->slug) }}" class="mb-3 f-14 yu-gothic-reg fw-600 black-color">
                        {{ $newsCategory->name }}
                        <span class="ml-2">({{ $newsCategory->news->where('status', 1)->count() }})</span>
                    </a>
                    <!--News Category List Item-->
                </li>
            @endforeach
        @endisset
    </ul>
    <!--News Category List Content-->
</div>
<!--News Category List-->
