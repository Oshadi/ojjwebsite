@extends('web::layouts.master')
{{--@section('meta-keyword', 'Online Games')--}}
{{--@section('title', 'Online Games for Free | Game PVT Games | Best Fun Games to Play')--}}
{{--@section('meta-description', 'Online Games are more fun with the Game PVT Games Website. Play your Favourite Games Arcade, Puzzle, Funny, Sports, Card games and more.')--}}
@section('content')

    @include('web::shared.secondary_header')

    <!--OJJ Rescue Boat-->
    <section class="ojj-rescue-boat px-0-section">
        <div class="container-fluid">
            <div class="row">
                <!--OJJ Rescue Boat Title-->
                <div class="col-sm-12 col-md-12 section-header pb-3 px-section-header mxw-container">
                    <h1 class="m-0">
                        OJJ-Rescue Boat
                        <span><em>OJJ</em>RESCUE ボート</span>
                    </h1>
                </div>
                <!--OJJ Rescue Boat Title-->

                <!--OJJ Rescue Boat Content-->
                <div class="col-sm-12 col-md-12 mt-3 mt-md-5 rescue-boat-content">
                    <div class="row d-flex justify-content-center align-items-center">
                        <!--OJJ Rescue Boat Text Content-->
                        <div class="col-sm-6 col-md-6 col-lg-5 col-xl-4 rescue-boat-text">
                            <!--OJJ Rescue Boat Title-->
                            <h4 class="mt-0 mb-5 f-40 text-white font-weight-bold">西日本豪雨による被害</h4>
                            <!--OJJ Rescue Boat Title-->
                            <!--OJJ Rescue Boat Description-->
                            <h6 class="m-0 f-18 text-white yu-gothic-reg fw-600">
                                平成30年7月、西日本を中心に上陸した台風7号及び 線状降水帯の影響により集中豪雨に見舞われ電柱が 倒れ、多くの人が家屋に取り残され多くの方が被害 に遭われました。<br> 一般的なインフレータブルボートでは救助活動を行え るような状況ではありませんでした。
                            </h6>
                            <!--OJJ Rescue Boat Description-->
                        </div>
                        <!--OJJ Rescue Boat Text Content-->
                    </div>
                </div>
                <!--OJJ Rescue Boat Content-->
            </div>
        </div>
    </section>
    <!--OJJ Rescue Boat-->

    <!--OJJ Boat Point-->
    <section class="ojj-boat-point px-0-section pt-0">
        <div class="container-fluid">
            <div class="row">
                <!--OJJ Boat Point Title-->
                <div class="col-sm-12 col-md-12 section-header pb-4 px-section-header mxw-container">
                    <h1 class="m-0">
                        OJJ-Boat Point
                        <span><em>OJJ</em>ボートの特徴</span>
                    </h1>
                </div>
                <!--OJJ Boat Point Title-->

                <!--OJJ Boat Point Wide Content-->
                <div class="col-sm-12 col-md-12 mt-5 boat-point-wide-img-content">
                    <div class="row mxw-container">
                        <div class="col-sm-6 col-md-5">
                            <h1 class="mt-0 mb-5 text-white font-weight-bold f-76">常識を変える</h1>
                            <h6 class="m-0 text-white fw-600 f-18 lh-32">
                                どんな過酷な状況でも安全に救助活動を行うため OJJボートは構造・船外機・表面素材にこだわった 「救助」に特化したボートを採用しています。
                            </h6>
                        </div>
                    </div>
                </div>
                <!--OJJ Boat Point Wide Content-->

                <!--OJJ Boat Point Double Side Content-->
                <div class="col-sm-12 col-md-12 boat-point-double-side-content">
                    <div class="row">
                        <!--OJJ Boat Point Double Side Content Image-->
                        <div class="col-sm-12 col-md-6 boat-point-double-side-img p-0 order-2 order-md-1">
                            <img src="{{ asset('template_assets/assets/images/rescue-boat/OJJBoat-Point_01.webp') }}" alt="rescue-boat">
                        </div>
                        <!--OJJ Boat Point Double Side Content Image-->

                        <!--OJJ Boat Point Double Side Text Content-->
                        <div class="col-sm-12 col-md-6 boat-point-double-side-bg-gradient order-1 order-md-2 double-left-padding">
                            <div class="w-100 position-relative d-flex justify-content-center">
                                <h1 class="m-0 fade-count">01</h1>
                                <h6 class="m-0 f-32 text-white fw-600 lh-43">
                                    トンネルハル構造による <br> ジェット船外機とのマッチングが <br> 安定性の高い走行を実現
                                </h6>
                            </div>
                        </div>
                        <!--OJJ Boat Point Double Side Text Content-->
                    </div>
                </div>
                <!--OJJ Boat Point Double Side Content-->

                <!--OJJ Boat Point Double Side Content-->
                <div class="col-sm-12 col-md-12 boat-point-double-side-content">
                    <div class="row">
                        <!--OJJ Boat Point Double Side Text Content-->
                        <div class="col-sm-12 col-md-6 boat-point-double-side-bg-gradient double-right-padding">
                            <div class="w-100 position-relative d-flex justify-content-center">
                                <h1 class="m-0 fade-count">02</h1>
                                <h6 class="m-0 f-32 text-white fw-600 lh-43">
                                    世界最軽量の実用的な <br> ジェット走行を可能にする <br> SUZUKI9.9hpを採用
                                </h6>
                            </div>
                        </div>
                        <!--OJJ Boat Point Double Side Text Content-->

                        <!--OJJ Boat Point Double Side Content Image-->
                        <div class="col-sm-12 col-md-6 boat-point-double-side-img p-0">
                            <img src="{{ asset('template_assets/assets/images/rescue-boat/OJJBoat%20Point_02.webp') }}" alt="rescue-boat">
                        </div>
                        <!--OJJ Boat Point Double Side Content Image-->
                    </div>
                </div>
                <!--OJJ Boat Point Double Side Content-->

                <!--OJJ Boat Point Double Side Content-->
                <div class="col-sm-12 col-md-12 boat-point-double-side-content">
                    <div class="row">
                        <!--OJJ Boat Point Double Side Content Image-->
                        <div class="col-sm-12 col-md-6 boat-point-double-side-img p-0 order-2 order-md-1">
                            <img src="{{ asset('template_assets/assets/images/rescue-boat/OJJBoat-Point_03.webp') }}" alt="rescue-boat">
                        </div>
                        <!--OJJ Boat Point Double Side Content Image-->

                        <!--OJJ Boat Point Double Side Text Content-->
                        <div class="col-sm-12 col-md-6 boat-point-double-side-bg-gradient order-1 order-md-2">
                            <div class="w-100 position-relative d-flex justify-content-center">
                                <h1 class="m-0 fade-count">03</h1>
                                <h6 class="m-0 f-32 text-white fw-600 lh-43">
                                    ボートハル側には <br> TPUの擦れに強い <br> 特殊強化素材を装着
                                </h6>
                            </div>
                        </div>
                        <!--OJJ Boat Point Double Side Text Content-->
                    </div>
                </div>
                <!--OJJ Boat Point Double Side Content-->
            </div>
        </div>
    </section>
    <!--OJJ Boat Point-->

    <!--OJJ Durability Test Section-->
    <section class="durability-test-section px-0-section pt-0">
        <div class="container">
            <div class="row">
                <!--OJJ Durability Test Section Title-->
                <div class="col-sm-12 col-md-12 mb-5">
                    <h4 class="m-0 f-32 black-color yu-gothic-reg fw-600 text-center">
                        特殊強化素材の耐久試験
                    </h4>
                </div>
                <!--OJJ Durability Test Section Title-->

                <!--OJJ Durability Test Content-->
                <div class="col-sm-12 col-md-12 durability-test-content mt-3 mb-5">
                    <div class="row">
                        <!--OJJ Durability Test Content Image-->
                        <div class="col-sm-12 col-md-6 mb-3 mb-md-0 durability-test-content-img">
                            <img src="{{ asset('template_assets/assets/images/rescue-boat/durability-test/endurance%20test_01.webp') }}" alt="durability-test-img">
                        </div>
                        <!--OJJ Durability Test Content Image-->

                        <!--OJJ Durability Test Text Content-->
                        <div class="col-sm-12 col-md-6 durability-test-text-content content-1">
                            <h5 class="mt-0 mb-3 black-color durability-test-text durability-test-text-content-title fw-600 yu-gothic-reg f-24">
                                優れた耐擦過性能を誇る特殊強化素材
                            </h5>

                            <div class="w-100 mb-4 first-border">
                                <span class="w-100 b-b-1-s"></span>
                            </div>

                            <p class="mt-0 mb-3 black-color fw-500 durability-test-text yu-gothic-reg f-18 lh-32">
                                ボート底面に採用されている特殊強化素材は浅瀬走行 時や陸乗り上げ時の岩や貝殻また粗砂などにより大変 な摩擦不可がかかります。 <br> 一般的インフレータブルボートに使用される船体素材 （PVC）とOJJボートに使用されている特殊強化素材と を一定の同じ負荷摩擦をかけて両素材の耐久性比較テ ストをしてみました。
                            </p>

                            <p class="mt-0 mb-5 black-color fw-500 durability-test-text yu-gothic-reg f-18 lh-32">
                                PVCは摩擦をかけ460回目に素材(写真A)が破れてしまっ たのに対し、特殊強化素材(写真B)は1060回の摩擦をか けても破れることはありませんでした。
                            </p>

                            <!--OJJ Durability Test Box Image Content-->
                            <div class="row durability-test-content-img-box-content">
                                <!--OJJ Durability Test Box Image-->
                                <div class="col-6 durability-test-content-img-box">
                                    <img src="{{ asset('template_assets/assets/images/rescue-boat/durability-test/endurance%20test_pvc.webp') }}" alt="durability-test-content-img-box">

                                    <h6 class="mt-3 mb-0 f-18 fw-500 yu-gothic-reg black-color text-center">A:PVC負荷摩擦結果</h6>
                                </div>
                                <!--OJJ Durability Test Box Image-->

                                <!--OJJ Durability Test Box Image-->
                                <div class="col-6 durability-test-content-img-box">
                                    <img src="{{ asset('template_assets/assets/images/rescue-boat/durability-test/endurance%20test_tpu.webp') }}" alt="durability-test-content-img-box">

                                    <h6 class="mt-3 mb-0 f-18 fw-500 yu-gothic-reg black-color text-center">B:特殊強化素材の 負荷摩擦結果</h6>
                                </div>
                                <!--OJJ Durability Test Box Image-->
                            </div>
                            <!--OJJ Durability Test Box Image Content-->
                        </div>
                        <!--OJJ Durability Test Text Content-->
                    </div>
                </div>
                <!--OJJ Durability Test Content-->

                <!--OJJ Durability Test Content-->
                <div class="col-sm-12 col-md-12 durability-test-content mt-3 mt-md-5">
                    <div class="row">
                        <!--OJJ Durability Test Text Content-->
                        <div class="col-sm-12 col-md-6 durability-test-text-content content-2">
                            <h5 class="mt-0 mb-3 black-color durability-test-text durability-test-text-content-title fw-600 yu-gothic-reg f-24">
                                優れた耐擦過性能を誇る特殊強化素材
                            </h5>

                            <div class="w-100 mb-4 second-border">
                                <span class="w-100 b-b-1-s"></span>
                            </div>

                            <p class="mt-0 mb-3 black-color fw-500 durability-test-text yu-gothic-reg f-18 lh-32">
                                PVCと特殊強化素材それぞれの素材の上から垂直真下 方向に、十字ドライバー先端をセットし一定の力を加 えて突き刺し、素材の破断突き抜け耐久度をテストし ました。 <br> PVCに比べ特殊強化素材は突き破るまでの素材弾力性 は13.6／17.1と25％増でした。
                            </p>

                            <!--OJJ Durability Test Content Table-->
                            <div class="row">
                                <!--OJJ Durability Test Content Table-->
                                <div class="col-sm-12 col-md-12 durability-test-content-table table-responsive mt-4 mb-2">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col">試験モード</th>
                                            <th scope="col">シングル</th>
                                            <th scope="col">試験種類</th>
                                            <th scope="col">圧縮</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>速度</td>
                                            <td>4mm/min</td>
                                            <td>試験片形状</td>
                                            <td>平板</td>
                                        </tr>
                                        <tr>
                                            <td>バッチ数</td>
                                            <td>1</td>
                                            <td>サブバッチ数</td>
                                            <td>4</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--OJJ Durability Test Content Table-->

                                <!--OJJ Durability Test Content Table-->
                                <div class="col-sm-12 col-md-12 durability-test-content-table table-responsive mt-4 mb-4">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th scope="col">名前</th>
                                            <th scope="col">最大点_試験力</th>
                                            <th scope="col">最大点_ストローク</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>パラメータ</td>
                                            <td>全エリアで計算</td>
                                            <td>全エリアで計算</td>
                                        </tr>
                                        <tr>
                                            <td>単位</td>
                                            <td>N</td>
                                            <td>mm</td>
                                        </tr>
                                        <tr>
                                            <td>PVC</td>
                                            <td>473.746</td>
                                            <td>13.6018</td>
                                        </tr>
                                        <tr>
                                            <td>特殊強化素材</td>
                                            <td>293.446</td>
                                            <td>17.1411</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!--OJJ Durability Test Content Table-->
                            </div>
                            <!--OJJ Durability Test Content Table-->
                        </div>
                        <!--OJJ Durability Test Text Content-->

                        <!--OJJ Durability Test Content Image-->
                        <div class="col-sm-12 col-md-6 mb-3 mb-md-0 durability-test-content-img">
                            <img src="{{ asset('template_assets/assets/images/rescue-boat/durability-test/endurance%20test_02.webp') }}" alt="durability-test-img">

                            <div class="row pt-5 m-0">
                                <div class="col-6 second-content-small-img-box pl-md-0">
                                    <img src="{{ asset('template_assets/assets/images/rescue-boat/durability-test/test%20site.webp') }}" alt="durability-test-img">
                                </div>
                                <div class="col-6 d-flex align-items-end pl-0">
                                    <div class="w-100">
                                        <h6 class="mt-0 mb-2 f-18 fw-500 black-color yu-gothic-reg">
                                            施設協力
                                        </h6>
                                        <div class="w-100 b-b-1-s mb-3"></div>

                                        <h6 class="m-0 f-18 fw-500 black-color yu-gothic-reg">
                                            浜松工業技術支援センター
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--OJJ Durability Test Content Image-->
                    </div>
                </div>
                <!--OJJ Durability Test Content-->
            </div>
        </div>
    </section>
    <!--OJJ Durability Test Section-->

    <!--OJJ Rescue Video Section-->
    <section class="ojj-rescue-video-section home-section">
        <div class="container">
            <div class="row">
                <!--OJJ Rescue Video Section Title-->
                <div class="col-sm-12 col-md-12 mb-3 mb-md-5">
                    <h4 class="m-0 f-32 yu-gothic-reg text-white fw-600 text-center">
                        OJJ-RESCUE380 + JET船外機だからできる走行性能
                    </h4>
                </div>
                <!--OJJ Rescue Video Section Title-->

                <!--OJJ Rescue Video Content-->
                <div class="col-sm-6 col-md-6 col-lg-3 ojj-rescue-video-content">
                    <a href="{{route('popup_video')}}" target="_blank">
                        <div class="ojj-rescue-video-content-box mt-5">
                            <img src="{{ asset('template_assets/assets/images/rescue-boat/video-content/landing.webp') }}" alt="ojj-rescue-video-content-img">

                            <div class="ojj-rescue-video-play-button">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 411 356">
                                    <path id="Polygon_1" data-name="Polygon 1" d="M205.5,0,411,356H0Z" transform="translate(411 356) rotate(180)" fill="#fff"/>
                                </svg>
                            </div>
                        </div>
                    </a>


                    <div class="w-100 ojj-rescue-video-content-text pt-5 pb-3">
                        <h6 class="mt-0 mb-3 f-18 text-white fw-600">
                            陸上への乗り上げ
                        </h6>

                        <p class="m-0 f-18 fw-400 yu-gothic-reg text-white">
                            JET噴出口が船底より飛び出 ない設計になっているため、 緊急時の陸上への乗り上げ が可能です。
                        </p>
                    </div>
                </div>
                <!--OJJ Rescue Video Content-->

                <!--OJJ Rescue Video Content-->
                <div class="col-sm-6 col-md-6 col-lg-3 ojj-rescue-video-content pr-5px">
                    <a href="{{route('popup_video_2')}}" target="_blank">
                        <div class="ojj-rescue-video-content-box">
                            <img src="{{ asset('template_assets/assets/images/rescue-boat/video-content/riffle%20running.webp') }}" alt="ojj-rescue-video-content-img">

                            <div class="ojj-rescue-video-play-button">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 411 356">
                                    <path id="Polygon_1" data-name="Polygon 1" d="M205.5,0,411,356H0Z" transform="translate(411 356) rotate(180)" fill="#fff"/>
                                </svg>
                            </div>
                        </div>
                    </a>

                    <div class="w-100 ojj-rescue-video-content-text minus-top-content">
                        <h6 class="mt-0 mb-3 f-18 text-white fw-600">
                            浅瀬・岩場走行
                        </h6>

                        <p class="m-0 f-18 fw-400 yu-gothic-reg text-white">
                            プロペラを使用していないた め、水深25cmの浅瀬でも走 行が可能です。
                        </p>
                    </div>
                </div>
                <!--OJJ Rescue Video Content-->

                <!--OJJ Rescue Video Content-->
                <div class="col-sm-6 col-md-6 col-lg-3 ojj-rescue-video-content pl-0">
                    <a href="{{route('popup_video_3')}}" target="_blank">
                        <div class="ojj-rescue-video-content-box mt-5">
                            <img src="{{ asset('template_assets/assets/images/rescue-boat/video-content/running%20on%20floating%20objects.webp') }}" alt="ojj-rescue-video-content-img">

                            <div class="ojj-rescue-video-play-button">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 411 356">
                                    <path id="Polygon_1" data-name="Polygon 1" d="M205.5,0,411,356H0Z" transform="translate(411 356) rotate(180)" fill="#fff"/>
                                </svg>
                            </div>
                        </div>
                    </a>

                    <div class="w-100 ojj-rescue-video-content-text pt-5 pb-3">
                        <h6 class="mt-0 mb-3 f-18 text-white fw-600">
                            網・フェンス走行
                        </h6>

                        <p class="m-0 f-18 fw-400 yu-gothic-reg text-white">
                            網などの浮遊物上の走行でも ものを巻き込む心配はありま せん。
                        </p>
                    </div>
                </div>
                <!--OJJ Rescue Video Content-->

                <!--OJJ Rescue Video Content-->
                <div class="col-sm-6 col-md-6 col-lg-3 ojj-rescue-video-content">
                    <a>
                        <div class="ojj-rescue-video-content-box">
                            <img src="{{ asset('template_assets/assets/images/rescue-boat/video-content/Entering%20water.webp') }}" alt="ojj-rescue-video-content-img">
                        </div>
                    </a>

                    <div class="w-100 ojj-rescue-video-content-text minus-top-content">
                        <h6 class="mt-0 mb-3 f-18 text-white fw-600">
                            上陸時の移動
                        </h6>

                        <p class="m-0 f-18 fw-400 yu-gothic-reg text-white">
                            スロープがある場合は 台車による上陸が可能。
                        </p>
                    </div>
                </div>
                <!--OJJ Rescue Video Content-->
            </div>
        </div>
    </section>
    <!--OJJ Rescue Video Section-->

    @include('web::shared.footer')

@endsection
