@extends('web::layouts.master')
{{--@section('meta-keyword', 'Online Games')--}}
{{--@section('title', 'Online Games for Free | Game PVT Games | Best Fun Games to Play')--}}
{{--@section('meta-description', 'Online Games are more fun with the Game PVT Games Website. Play your Favourite Games Arcade, Puzzle, Funny, Sports, Card games and more.')--}}
@section('content')

    @include('web::shared.secondary_header')

    <!--OJJ Mission-->
    <section class="ojj-mission home-section">
        <div class="container">
            <div class="row">
                <!--OJJ Mission Title-->
                <div class="col-sm-12 col-md-12 section-header">
                    <h1 class="m-0">
                        OJJ Mission
                        <span><em>OJJ</em>の使命</span>
                    </h1>
                </div>
                <!--OJJ Mission Title-->

                <div class="col-sm-12 col-md-12 my-5">
                    <h4 class="m-0 yu-gothic-reg f-28 fw-500 black-color">
                        より安全な水難救助のためのボート&ジェット船外機を提供する
                    </h4>
                </div>

                <!-- OJJ Mission Content -->
                <div class="col-sm-12 col-md-12">
                    <h4 class="mt-0 mb-5 pb-3 f-40 font-weight-bold"><em>OJJ</em>お問い合わせ</h4>

                    <div class="row mt-3 mb-4">
                        <div class="col-sm-4 col-md-3 col-lg-2 mb-3 mb-sm-0">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                名称
                            </h6>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                {{@$contactUs->name}}
                            </h6>
                        </div>

                        <div class="col-sm-12 mt-4">
                            <div class="w-100 b-b-1-s"></div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-sm-4 col-md-3 col-lg-2 mb-3 mb-sm-0">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                所在地
                            </h6>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                {{@$contactUs->address_line_1 . @$contactUs->address_line_2}}
                            </h6>
                        </div>

                        <div class="col-sm-12 mt-4">
                            <div class="w-100 b-b-1-s"></div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-sm-4 col-md-3 col-lg-2 mb-3 mb-sm-0">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                電話
                            </h6>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                {{@$contactUs->contact}}
                            </h6>
                        </div>

                        <div class="col-sm-12 mt-4">
                            <div class="w-100 b-b-1-s"></div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-sm-4 col-md-3 col-lg-2 mb-3 mb-sm-0">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                ファックス
                            </h6>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                {{@$contactUs->fax}}
                            </h6>
                        </div>

                        <div class="col-sm-12 mt-4">
                            <div class="w-100 b-b-1-s"></div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-sm-4 col-md-3 col-lg-2 mb-3 mb-sm-0">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                Eメール
                            </h6>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                {{@$contactUs->email}}
                            </h6>
                        </div>

                        <div class="col-sm-12 mt-4">
                            <div class="w-100 b-b-1-s"></div>
                        </div>
                    </div>

                    <div class="row mb-4">
                        <div class="col-sm-4 col-md-3 col-lg-2 mb-3 mb-sm-0 col-lg-2">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                事業内容
                            </h6>
                        </div>
                        <div class="col-sm-8 col-md-8">
                            <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500">
                                {{@$contactUs->business_content}}
                            </h6>
                        </div>

                        <div class="col-sm-12 mt-4">
                            <div class="w-100 b-b-1-s"></div>
                        </div>
                    </div>
                </div>
                <!-- OJJ Mission Content -->
            </div>
        </div>
    </section>
    <!--OJJ Mission-->

    <!--OJJ Composition-->
    <section class="ojj-mission home-section pt-0">
        <div class="container">
            <div class="row">
                <!--OJJ Composition Title-->
                <div class="col-sm-12 col-md-12 section-header">
                    <h1 class="m-0">
                        Composition
                        <span><em>OJJ</em>構成企業</span>
                    </h1>
                </div>
                <!--OJJ Composition Title-->

                <!--OJJ Composition Content-->
                <div class="col-sm-12 col-md-12 mt-5">
                    <div class="row">

                        <!--OJJ Composition Content Image-->
                        <div class="col-sm-12 col-md-6 d-flex justify-content-center mb-5">
                            <img class="" src="{{ asset('template_assets/assets/images/overview-organization/organization%20chart.webp') }}" alt="composition-image">
                        </div>
                        <!--OJJ Composition Content Image-->

                        <!--OJJ Composition Content Text-->
                        <div class="col-sm-12 col-md-6 d-flex align-items-center mb-4">
                            <div class="row">
                                <div class="col-sm-12 col-md-12 mb-4 composition-content-text">
                                    <h5 class="mt-0 mb-3 f-18 yu-gothic-reg font-weight-bold">
                                        <span class="composition-content-text-dash"></span>
                                        日本船外機
                                    </h5>
                                    <h5 class="mt-0 mb-3 f-18 d-flex yu-gothic-reg fw-500 composition-content-text-2nd">
{{--                                        <span>金田一 正秀</span>--}}
                                        <span class="mr-2">
                                            <img src="{{ asset('template_assets/assets/images/about-us/nameimg_1.jpg') }}" alt="about-us-image">
                                        </span>
                                        神奈川県横須賀市長井1-25-31
                                    </h5>
                                </div>

                                <div class="col-sm-12 col-md-12 mb-4 composition-content-text">
                                    <h5 class="mt-0 mb-3 f-18 yu-gothic-reg font-weight-bold">
                                        <span class="composition-content-text-dash"></span>
                                        有限会社 SHIPMAN
                                    </h5>
                                    <h5 class="mt-0 mb-3 f-18 yu-gothic-reg fw-500 composition-content-text-2nd">
{{--                                        <span>城田 守</span>--}}
                                        <span class="mr-2">
                                            <img src="{{ asset('template_assets/assets/images/about-us/nameimg_2.jpg') }}" alt="about-us-image">
                                        </span>
                                        浜松市中区高林3-7-15 E-1
                                    </h5>
                                </div>

                                <div class="col-sm-12 col-md-12 mb-5 composition-content-text">
                                    <h5 class="mt-0 mb-3 f-18 yu-gothic-reg font-weight-bold">
                                        <span class="composition-content-text-dash"></span>
                                        アドックスマリーン
                                    </h5>
                                    <h5 class="mt-0 mb-3 f-18 yu-gothic-reg fw-500 composition-content-text-2nd">
                                        <span class="mr-2">林 克易</span>
                                        浜松市西区神ケ谷町2877-3
                                    </h5>
                                </div>

                                <div class="col-sm-12 col-md-12 pt-3">
                                    <h5 class="mt-0 mb-3 f-18 yu-gothic-reg fw-500">
                                        協力団体／浜松ライフセービングクラブ
                                    </h5>
                                </div>
                            </div>
                        </div>
                        <!--OJJ Composition Content Text-->
                    </div>
                </div>
                <!--OJJ Composition Content-->
            </div>
        </div>
    </section>
    <!--OJJ Composition-->

    @include('web::shared.footer')

@endsection
