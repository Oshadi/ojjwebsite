@extends('web::layouts.master')
{{--@section('meta-keyword', 'Online Games')--}}
{{--@section('title', 'Online Games for Free | Game PVT Games | Best Fun Games to Play')--}}
{{--@section('meta-description', 'Online Games are more fun with the Game PVT Games Website. Play your Favourite Games Arcade, Puzzle, Funny, Sports, Card games and more.')--}}
@section('content')

    @include('web::shared.secondary_header')

    <!--Site Map-->
    <section class="site-map home-section">
        <div class="container">
            <div class="row">
                <!--Site Map Title-->
                <div class="col-sm-12 col-md-12 pb-3 section-header">
                    <h1 class="m-0">
                        Site map
                        <span>サイトマップ</span>
                    </h1>
                </div>
                <!--Site Map Title-->

                <!--Site Content-->
                <div class="col-sm-12 col-md-12 mt-3 mt-md-5 site-map-content">
                    <a href="{{route('index')}}" class="mt-0 mb-3 f-24 fw-500 yu-gothic-reg black-color site-map-content-title">
          <span class="d-block mr-1">
            <svg viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"><path d="M416 352c-8.188 0-16.38-3.125-22.62-9.375L224 173.3l-169.4 169.4c-12.5 12.5-32.75 12.5-45.25 0s-12.5-32.75 0-45.25l192-192c12.5-12.5 32.75-12.5 45.25 0l192 192c12.5 12.5 12.5 32.75 0 45.25C432.4 348.9 424.2 352 416 352z"/></svg>
          </span>
                        TOP
                    </a>
                    <div class="w-100 b-b-1-s mb-5"></div>

                    <div class="row mb-5">
                        <div class="col-sm-6 col-md-3">
                            <a href="{{route('product')}}" class="mt-0 d-block mb-4 f-18 black-color yu-gothic-reg fw-500">
                                製品案内
                            </a>

                            <a href="{{route('rescue_boat')}}" class="mt-0 d-block mb-4 f-18 black-color yu-gothic-reg fw-500">
                                OJJ-RESCUEボート
                            </a>

                            <a href="{{route('maintenance')}}" class="mt-0 d-block mb-4 f-18 black-color yu-gothic-reg fw-500">
                                メンテナンス
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <a href="{{route('news')}}" class="mt-0 d-block mb-4 f-18 black-color yu-gothic-reg fw-500">
                                お知らせ
                            </a>

                            <a href="{{route('about_us')}}" class="mt-0 d-block mb-4 f-18 black-color yu-gothic-reg fw-500">
                                組織概要
                            </a>

                            <a href="{{route('contact_us')}}" class="mt-0 d-block mb-4 f-18 black-color yu-gothic-reg fw-500">
                                お問い合わせ
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <a href="{{route('privacy_policy')}}" class="mt-0 d-block mb-4 f-18 black-color yu-gothic-reg fw-500">
                                個人情報保護基本方針
                            </a>

                            <a href="{{route('site_map')}}" class="mt-0 d-block mb-4 f-18 black-color yu-gothic-reg fw-500">
                                サイトマップ
                            </a>
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <a href="" class="mt-0 d-block mb-4 f-18 black-color yu-gothic-reg fw-500" target="_blank">
                                Facebook
                            </a>

                            <a href="" class="mt-0 d-block mb-4 f-18 black-color yu-gothic-reg fw-500" target="_blank">
                                Instagram
                            </a>
                        </div>
                    </div>

                    <div class="w-100 b-b-1-s"></div>
                </div>
                <!--Site Content-->
            </div>
        </div>
    </section>
    <!--Site Map-->

    @include('web::shared.footer')

@endsection
