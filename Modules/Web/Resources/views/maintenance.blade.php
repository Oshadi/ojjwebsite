@extends('web::layouts.master')
@section('content')

    @include('web::shared.secondary_header')

    <!--Maintenance Top Banner-->
    <section class="maintenance-top-section px-0-section">
        <div class="container-fluid">
            <div class="row">
                <!--Maintenance Top Title-->
                <div class="col-sm-12 col-md-12 pb-3 section-header px-section-header mxw-container">
                    <h1 class="m-0">
                        Maintenance
                        <span>メンテナンス</span>
                    </h1>
                </div>
                <!--Maintenance Top Title-->

                <div class="col-sm-12 col-md-12 maintenance-banner mt-3 mt-md-5 p-0">
                    <img class="object-fit-cover"
                         src="{{ asset('template_assets/assets/images/maintenance/maintenance-banner.webp') }}"
                         alt="maintenance-banner-image">
                </div>
            </div>
        </div>
    </section>
    <!--Maintenance Top Banner-->

    <!--Maintenance Content-->
    <section class="maintenance-content home-section pt-0">
        <div class="container">
            <div class="row">
                <!--Maintenance Content Title-->
                <div class="col-sm-12 col-md-12 mb-5 pb-3">
                    <h6 class="m-0 f-18 fw-500 yu-gothic-reg black-color text-center">
                        メンテナンスのポイントや点検方法、OJJがお勧めするサポートプログラムについてご紹介いたします。
                    </h6>
                </div>
                <!--Maintenance Content Title-->

                <!--Maintenance Card-->
                <div class="col-sm-12 col-md-12 maintenance-card">
                    <img src="{{ asset('template_assets/assets/images/maintenance/maintenance_01.webp') }}"
                         alt="maintenance-card-img">
                    <div class="maintenance-card-content">
                        <h2 class="mt-0 mb-1 f-32 font-weight-bold yu-gothic-reg black-color maintenance-card-text">
                            通常管理の注意点
                        </h2>
                        <div class="b-b-1-s mt-2 mb-4"></div>

                        <p class="m-0 f-18 fw-500 yu-gothic-reg black-color lh-32 maintenance-card-text">
                            ボートを使用する予定がなくても、定期的に試運転を励 行することによって、こんな効果があります。週に一回、 5分の走行が望ましいです。 試運転含め、ボートの使用後は必ずポンプ部にグリスの
                            注入をおこなってください。
                        </p>
                    </div>
                </div>
                <!--Maintenance Card-->

                <!--Maintenance Card-->
                <div class="col-sm-12 col-md-12 maintenance-card">
                    <img src="{{ asset('template_assets/assets/images/maintenance/maintenance_02.webp') }}"
                         alt="maintenance-card-img">
                    <div class="maintenance-card-content">
                        <h2 class="mt-0 mb-1 f-32 font-weight-bold yu-gothic-reg black-color maintenance-card-text">
                            定期点検について
                        </h2>
                        <div class="b-b-1-s mt-2 mb-4"></div>

                        <p class="m-0 f-18 fw-500 yu-gothic-reg black-color lh-32 maintenance-card-text">
                            定期点検は基本的に1年に1回を基準として行ってくだ さい。また、海水、汽水域で使用された場合は特に注 意が必要です。（点検交換箇所については、整備明細 書を例とする）
                            定期点検において老朽化や消耗部品などにつきまして は、緊急時に万全な状態で使えることを維持するた めに点検時に部品交換させていただきます。 経年劣化、消耗部品などは耐久限度を待たずに
                            新品に交換することをおすすめします。
                        </p>
                    </div>
                </div>
                <!--Maintenance Card-->

                <!--Maintenance Card-->
                <div class="col-sm-12 col-md-12 maintenance-card">
                    <img src="{{ asset('template_assets/assets/images/maintenance/maintenance_03.webp') }}"
                         alt="maintenance-card-img">
                    <div class="maintenance-card-content">
                        <h2 class="mt-0 mb-1 f-32 font-weight-bold yu-gothic-reg black-color maintenance-card-text">
                            故障・修理について
                        </h2>
                        <div class="b-b-1-s mt-2 mb-4"></div>

                        <p class="m-0 f-18 fw-500 yu-gothic-reg black-color lh-32 maintenance-card-text">
                            ボートの一部が故障した場合には、電話にて症状の 確認をさせていただきます。お客様自身で解決可能 な内容につきましては、その場で回答致します。
                            修理が必要な場合は、最善・最短な修理方法を提案 いたします。時間の猶予がない場合は、代替え機の 貸し出しもおこなっております。
                        </p>
                    </div>
                </div>
                <!--Maintenance Card-->

                <!--Maintenance Card-->
                <div class="col-sm-12 col-md-12 maintenance-card">
                    <img src="{{ asset('template_assets/assets/images/maintenance/maintenance_04.webp') }}"
                         alt="maintenance-card-img">
                    <div class="maintenance-card-content">
                        <h2 class="mt-0 mb-1 f-32 font-weight-bold yu-gothic-reg black-color maintenance-card-text">
                            Jet船外機への仕様変更
                        </h2>
                        <div class="b-b-1-s mt-2 mb-4"></div>

                        <p class="m-0 f-18 fw-500 yu-gothic-reg black-color lh-32 maintenance-card-text">
                            お客様が使用しているプロペラの船外機をジェット 船外機に変更するサービスも承っております。 Jet船外機への仕様変更につきましては、お客様の
                            船外機のメーカーやサイズにより対応できない場合 があります。メーカー機種でお調べいたしますので お問い合わせください。
                        </p>
                    </div>
                </div>
                <!--Maintenance Card-->
            </div>
        </div>
    </section>
    <!--Maintenance Content-->

    <!--Q and A-->
    <section class="q-n-a-section home-section pt-0">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 mb-5">
                    <h2 class="mt-0 mb-1 f-40 font-weight-bold black-color font-italic text-center">
                        Q&A
                    </h2>
                    <p class="m-0 f-18 fw-600 yu-gothic-reg black-color text-center">
                        よくある質問
                    </p>
                </div>

                <div class="col-sm-12 col-md-12 q-n-a-content">
                    <h2 class="mt-0 mb-5 f-40 text-white fw-500 px-4 bg-black py-3 br-2">
                        <em>Jet</em>船外機について
                    </h2>

                    <!--Q and A Accordion-->
                    <div class="row">
                        <div class="col-sm-12 col-md-12 accordion" id="accordionExample">
                            <!--Q and A Accordion Card-->
                            @isset($faqMain)
                                @foreach($faqMain as $k => $question)
                                    <div class="card q-n-a-card">
                                        <!--Q and A Accordion Card Header-->
                                        <div class="card-header" id="heading-{{$k}}">
                                            <a class="f-24 fw-600 black-color q-btn collapsed" type="button"
                                               data-toggle="collapse"
                                               data-target="#collapse-{{$k}}"
                                               aria-expanded="@if($k == 0 ) true @else false @endif"
                                               aria-controls="collapse-{{$k}}">
                                                <span class="q-block">Q</span>
                                                {{ $question->question }}
                                                <span class="q-btn-arrow">
                                        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                             viewBox="0 0 55.751 55.751"
                                             style="enable-background:new 0 0 55.751 55.751;" xml:space="preserve"><g><path
                                                    d="M31.836,43.006c0.282-0.281,0.518-0.59,0.725-0.912L54.17,20.485c2.107-2.109,2.109-5.528,0-7.638 c-2.109-2.107-5.527-2.109-7.638,0l-18.608,18.61L9.217,12.753c-2.109-2.108-5.527-2.109-7.637,0 C0.527,13.809-0.002,15.19,0,16.571c-0.002,1.382,0.527,2.764,1.582,3.816l21.703,21.706c0.207,0.323,0.445,0.631,0.729,0.913 c1.078,1.078,2.496,1.597,3.91,1.572C29.336,44.604,30.758,44.084,31.836,43.006z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>
                                        </span>
                                            </a>
                                        </div>
                                        <!--Q and A Accordion Card Header-->

                                        <div id="collapse-{{$k}}" class="collapse @if($k == 0 ) show @endif "
                                             aria-labelledby="heading-{{$k}}"
                                             data-parent="#accordionExample">
                                            <div class="card-body">
                                                <span class="a-block">A</span>
                                                <p class="m-0 f-18 fw-500 yu-gothic-reg black-color">
                                                    {{ $question->answer }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                        @endisset

                        <!--Q and A Accordion Card-->

                        </div>
                    </div>
                    <!--Q and A Accordion-->
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-sm-12 col-md-12 mb-5">
                    <h4 class="m-0 f-18 fw-500 yu-gothic-reg black-color">
                        その他下記のような質問をお問い合わせいただいております。詳細につきましては、お気軽にお問い合わせください。
                    </h4>
                </div>

                @isset($faqSub)
                    @foreach($faqSub as $k => $question)
                        <div class="col-sm-12 col-md-6 pre-questions mb-4">
                            <h6 class="m-0 f-18 fw-500 yu-gothic-reg black-color p-3 b-1-s br-2">
                                {{ $question->question }}
                            </h6>
                        </div>
                    @endforeach
                @endisset
            </div>
        </div>
    </section>
    <!--Q and A-->

    <!--Contact Info-->
    <section class="contact-info-section home-section pt-0">
        <div class="container">
            <div class="row">
                <!--Contact Info Content-->
                <div class="col-sm-12 col-md-12 contact-info-content">
                    <h6 class="mt-0 mb-3 mb-md-4 f-18 fw-500 yu-gothic-reg black-color text-center">
                        お見積もり・お問い合わせはこちら
                    </h6>

                    <div class="d-flex justify-content-center mb-3 mb-md-4">
                        <a href="{{route('contact_us')}}" type="button" class="cus-white-btn">お問い合わせフォーム</a>
                    </div>

                    <h6 class="mt-0 mb-3 mb-md-4 f-18 fw-500 yu-gothic-reg black-color text-center">
                        お電話でのお問い合わせの場合
                    </h6>

                    <div class="d-flex justify-content-center align-items-center mb-2">
                        <span class="d-block mr-2 font-italic red-text f-22 fw-600">TEL</span>
                        <a href="tel:{{@$contactUs->contact}}" class="font-italic b-b-2-s f-22 fw-600 black-color">{{@$contactUs->contact}}</a>
                    </div>

                    <h6 class="m-0 f-18 fw-500 yu-gothic-reg black-color text-center">
                        アドックス-OJJ事業部まで
                    </h6>
                </div>
                <!--Contact Info Content-->
            </div>
        </div>
    </section>
    <!--Contact Info-->

    @include('web::shared.footer')

@endsection
