<!--Footer-->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 mb-4">
                <!--Footer Logo-->
                <a href="{{route('index')}}" class="footer-logo">
                    <img src="{{ asset('template_assets/assets/images/logo/logo_white.webp') }}" alt="logo">
                </a>
                <!--Footer Logo-->

                <!--Footer Text Content-->
                <div class="footer-text-content mt-4">
                    <h6 class="mt-0 mb-1">{{@$contactUs->address_line_1}}</h6>
                    <h6 class="mt-0 mb-1">{{@$contactUs->address_line_2}}</h6>
                    <h6 class="m-0">TEL {{@$contactUs->contact}}（アドックスマリーン内）</h6>
                </div>
                <!--Footer Text Content-->
            </div>
            <div class="col-sm-12 col-md-3 mb-4">
                <!--Footer Link List-->
                <ul class="list-unstyled m-0 footer-link-list">
                    <!--Footer Link-->
                    <li>
                        <a href="{{route('product')}}">製品案内</a>
                    </li>
                    <!--Footer Link-->
                    <li>
                        <a href="{{route('rescue_boat')}}">OJJ-RESCUEボート</a>
                    </li>
                    <!--Footer Link-->
                    <li>
                        <a href="{{route('maintenance')}}">メンテナンス</a>
                    </li>
                    <!--Footer Link-->
                    <li>
                        <a href="{{route('news')}}">お知らせ</a>
                    </li>
                    <!--Footer Link-->
                    <li>
                        <a href="{{route('about_us')}}">組織/概要</a>
                    </li>
                    <!--Footer Link-->
                </ul>
                <!--Footer Link List-->
            </div>
            <div class="col-sm-12 col-md-3 mb-4">
                <!--Footer Link List-->
                <ul class="list-unstyled m-0 footer-link-list">
                    <!--Footer Link-->
                    <li>
                        <a href="{{route('contact_us')}}">お問い合わせ</a>
                    </li>
                    <!--Footer Link-->
                    <li>
                        <a href="{{route('privacy_policy')}}">個人情報保護基本方針</a>
                    </li>
                    <!--Footer Link-->
                    <li>
                        <a href="{{route('site_map')}}">サイトマップ</a>
                    </li>
                    <!--Footer Link-->
                    <li>
                        <a href="{{@$contactUs->facebook}}" target="_blank" class="social-link facebook">
                            <span class="d-block mr-2">
                                <svg enable-background="new 0 0 56.693 56.693" id="Layer_1" version="1.1" viewBox="0 0 56.693 56.693" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M28.347,5.157c-13.6,0-24.625,11.027-24.625,24.625c0,13.6,11.025,24.623,24.625,24.623c13.6,0,24.625-11.023,24.625-24.623  C52.972,16.184,41.946,5.157,28.347,5.157z M34.864,29.679h-4.264c0,6.814,0,15.207,0,15.207h-6.32c0,0,0-8.307,0-15.207h-3.006  V24.31h3.006v-3.479c0-2.49,1.182-6.377,6.379-6.377l4.68,0.018v5.215c0,0-2.846,0-3.398,0c-0.555,0-1.34,0.277-1.34,1.461v3.163  h4.818L34.864,29.679z"/></svg>
                            </span>
                            Facebook
                        </a>
                    </li>
                    <!--Footer Link-->
                    <li>
                        <a href="{{@$contactUs->instagram}} " target="_blank" class="social-link instagram">
                            <span class="d-block mr-2">
                                <svg style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;" version="1.1" viewBox="0 0 512 512" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:serif="http://www.serif.com/" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M256,0c141.29,0 256,114.71 256,256c0,141.29 -114.71,256 -256,256c-141.29,0 -256,-114.71 -256,-256c0,-141.29 114.71,-256 256,-256Zm0,96c-43.453,0 -48.902,0.184 -65.968,0.963c-17.03,0.777 -28.661,3.482 -38.839,7.437c-10.521,4.089 -19.444,9.56 -28.339,18.455c-8.895,8.895 -14.366,17.818 -18.455,28.339c-3.955,10.177 -6.659,21.808 -7.437,38.838c-0.778,17.066 -0.962,22.515 -0.962,65.968c0,43.453 0.184,48.902 0.962,65.968c0.778,17.03 3.482,28.661 7.437,38.838c4.089,10.521 9.56,19.444 18.455,28.34c8.895,8.895 17.818,14.366 28.339,18.455c10.178,3.954 21.809,6.659 38.839,7.436c17.066,0.779 22.515,0.963 65.968,0.963c43.453,0 48.902,-0.184 65.968,-0.963c17.03,-0.777 28.661,-3.482 38.838,-7.436c10.521,-4.089 19.444,-9.56 28.34,-18.455c8.895,-8.896 14.366,-17.819 18.455,-28.34c3.954,-10.177 6.659,-21.808 7.436,-38.838c0.779,-17.066 0.963,-22.515 0.963,-65.968c0,-43.453 -0.184,-48.902 -0.963,-65.968c-0.777,-17.03 -3.482,-28.661 -7.436,-38.838c-4.089,-10.521 -9.56,-19.444 -18.455,-28.339c-8.896,-8.895 -17.819,-14.366 -28.34,-18.455c-10.177,-3.955 -21.808,-6.66 -38.838,-7.437c-17.066,-0.779 -22.515,-0.963 -65.968,-0.963Zm0,28.829c42.722,0 47.782,0.163 64.654,0.933c15.6,0.712 24.071,3.318 29.709,5.509c7.469,2.902 12.799,6.37 18.397,11.969c5.6,5.598 9.067,10.929 11.969,18.397c2.191,5.638 4.798,14.109 5.509,29.709c0.77,16.872 0.933,21.932 0.933,64.654c0,42.722 -0.163,47.782 -0.933,64.654c-0.711,15.6 -3.318,24.071 -5.509,29.709c-2.902,7.469 -6.369,12.799 -11.969,18.397c-5.598,5.6 -10.928,9.067 -18.397,11.969c-5.638,2.191 -14.109,4.798 -29.709,5.509c-16.869,0.77 -21.929,0.933 -64.654,0.933c-42.725,0 -47.784,-0.163 -64.654,-0.933c-15.6,-0.711 -24.071,-3.318 -29.709,-5.509c-7.469,-2.902 -12.799,-6.369 -18.398,-11.969c-5.599,-5.598 -9.066,-10.928 -11.968,-18.397c-2.191,-5.638 -4.798,-14.109 -5.51,-29.709c-0.77,-16.872 -0.932,-21.932 -0.932,-64.654c0,-42.722 0.162,-47.782 0.932,-64.654c0.712,-15.6 3.319,-24.071 5.51,-29.709c2.902,-7.468 6.369,-12.799 11.968,-18.397c5.599,-5.599 10.929,-9.067 18.398,-11.969c5.638,-2.191 14.109,-4.797 29.709,-5.509c16.872,-0.77 21.932,-0.933 64.654,-0.933Zm0,49.009c-45.377,0 -82.162,36.785 -82.162,82.162c0,45.377 36.785,82.162 82.162,82.162c45.377,0 82.162,-36.785 82.162,-82.162c0,-45.377 -36.785,-82.162 -82.162,-82.162Zm0,135.495c-29.455,0 -53.333,-23.878 -53.333,-53.333c0,-29.455 23.878,-53.333 53.333,-53.333c29.455,0 53.333,23.878 53.333,53.333c0,29.455 -23.878,53.333 -53.333,53.333Zm104.609,-138.741c0,10.604 -8.597,19.199 -19.201,19.199c-10.603,0 -19.199,-8.595 -19.199,-19.199c0,-10.604 8.596,-19.2 19.199,-19.2c10.604,0 19.201,8.596 19.201,19.2Z"/></svg>
                            </span>
                            Instagram
                        </a>
                    </li>
                    <!--Footer Link-->
                </ul>
                <!--Footer Link List-->
            </div>
        </div>

        <div class="row mt-5">
            <!--Footer Bottom Content-->
            <div class="col-sm-12 col-md-12 text-center">
                <!--Footer Copyright Text-->
                <p class="m-0 copyright-text">copyright © Outboard Jets Japan All Rights Reserved.</p>
                <!--Footer Copyright Text-->
            </div>
            <!--Footer Bottom Content-->
        </div>
    </div>
</footer>
<!--Footer-->
