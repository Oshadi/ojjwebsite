<!--Top Content-->
<div class="top-content other-top-content">
    <!--Header-->
    <header class="header other-page-header">
        <div class="container">
            <!--Navbar-->
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="{{route('index')}}">
                    <img class="logo white-logo" src="{{ asset('template_assets/assets/images/logo/logo_white.webp') }}" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 92.833 92.833" style="enable-background:new 0 0 92.833 92.833;" xml:space="preserve"><g><g><path d="M89.834,1.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V4.75 C92.834,3.096,91.488,1.75,89.834,1.75z"/><path d="M89.834,36.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V39.75 C92.834,38.096,91.488,36.75,89.834,36.75z"/><path d="M89.834,71.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V74.75 C92.834,73.095,91.488,71.75,89.834,71.75z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>

                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{route('rescue_boat')}}">OJJ-RESCUEボート <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('news')}}">お知らせ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('product')}}">製品案内</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('maintenance')}}">メンテナンス</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('about_us')}}">概要/組織</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('contact_us')}}">お問い合わせ</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!--Navbar-->
        </div>
    </header>
    <!--Header-->
</div>
