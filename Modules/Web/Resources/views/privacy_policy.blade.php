@extends('web::layouts.master')
{{--@section('meta-keyword', 'Online Games')--}}
{{--@section('title', 'Online Games for Free | Game PVT Games | Best Fun Games to Play')--}}
{{--@section('meta-description', 'Online Games are more fun with the Game PVT Games Website. Play your Favourite Games Arcade, Puzzle, Funny, Sports, Card games and more.')--}}
@section('content')

    @include('web::shared.secondary_header')

    <!--Privacy Policy-->
    <section class="privacy-policy home-section">
        <div class="container">
            <div class="row">
                <!--Privacy Policy Title-->
                <div class="col-sm-12 col-md-12 pb-3 section-header">
                    <h1 class="m-0">
                        Privacy Policy
                        <span>個人情報保護基本方針</span>
                    </h1>
                </div>
                <!--Privacy Policy Title-->

                <div class="col-sm-12 col-md-12 mt-3 mt-md-5 privacy-policy-content">

                    <div class="w-100">
                        <h6 class="mt-0 mb-5 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                            個人情報の利用目的 <br> ご提供いただいた個人情報は、アウトボードジェット ジャパン（以下、当組織といいます）が、以下の目的で利用するものと します。
                        </h6>
                    </div>

                    <div class="w-100">
                        <h6 class="mt-0 mb-5 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                            1．お客様の個人情報とは <br> 当組織がお客様から提供いただく氏名、電話番号、団体名、メールアドレスなどの個人を識別できる情報（他の情報と容易に 照合でき、個人を識別できる情報を含む）を個人情報といいます。
                        </h6>
                    </div>

                    <div class="w-100">
                        <h6 class="mt-0 mb-5 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                            2．法令・規則の遵守 <br> 当組織は、個人情報保護に関する法律、国が定める指針等を遵守し、当社が保有する個人情報の保護に努めます。
                        </h6>
                    </div>

                    <div class="w-100 mb-5">
                        <h6 class="mt-0 mb-2 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                            3．個人情報の管理
                        </h6>
                        <ul class="list-unstyled m-0">
                            <li class="d-flex align-items-center">
                                <span class="d-block mr-2">（1）</span>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    当組織は、個人情報の正確性を保ち、これを安全に管理します。
                                </h6>
                            </li>

                            <li class="d-flex align-items-center">
                                <span class="d-block mr-2">（2）</span>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    当組織は、個人情報管理者を任命し、個人情報の適切な管理を行います。
                                </h6>
                            </li>

                            <li class="d-flex align-items-center">
                                <span class="d-block mr-2">（3）</span>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    当組織は、個人情報の紛失、破壊、改ざん及び漏えいなどを防止するため、不正アクセス、コンピュータウィルス等に対  する適切なセキュリティ対策を講じます。
                                </h6>
                            </li>
                        </ul>
                    </div>

                    <div class="w-100 mb-5">
                        <h6 class="mt-0 mb-2 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                            4．個人情報の取得について、その目的と利用の範囲 <br> 当組織が取得する個人情報は、以下の目的のために利用し、お客様の同意を得ずに、利用目的の範囲を超えて使用いたしません。

                        </h6>

                        <ul class="list-unstyled m-0 pl-3">
                            <li>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    商品の配送
                                </h6>
                            </li>
                            <li>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    顧客管理
                                </h6>
                            </li>
                            <li>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    お問い合わせ、ご相談、ご意見への対応
                                </h6>
                            </li>
                            <li>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    当該ウェブサイトの統計による利用改善
                                </h6>
                            </li>
                            <li>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    カタログや資料、情報誌の送付
                                </h6>
                            </li>
                            <li>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    商品やサービス、イベントのご案内
                                </h6>
                            </li>
                        </ul>

                    </div>

                    <div class="w-100 mb-5">
                        <h6 class="mt-0 mb-2 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                            6．個人情報の開示について <br> 個人情報の利用は、利用目的の範囲内で、業務の遂行上必要な限りにおいて行うものといたします。 但し、次の各号に該当する場合は、お客様等情報主体の事前の同意を得ることなく、当該情報主体の個人情報を第三者に提供し たり、利用目的の範囲を超えて利用することがあります。
                        </h6>

                        <ul class="list-unstyled m-0">
                            <li class="d-flex align-items-center">
                                <span class="d-block mr-2">（1）</span>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    法令に基づく場合。
                                </h6>
                            </li>

                            <li class="d-flex align-items-center">
                                <span class="d-block mr-2">（2）</span>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    人の生命、身体又は財産の保護の為に必要がある場合であって、お客様等情報主体の同意を得ることが困難である場合。
                                </h6>
                            </li>

                            <li class="d-flex align-items-center">
                                <span class="d-block mr-2">（3）</span>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    公衆衛生の向上又は児童の健全な育成の推進の為に特に必要がある場合であって、お客様等情報主体の同意を得ることが困  難である場合。
                                </h6>
                            </li>

                            <li class="d-flex align-items-center">
                                <span class="d-block mr-2">（4）</span>
                                <h6 class="m-0 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                                    国の機関もしくは地方公共団体又はその委託を受けた者が法令の定める事務を遂行することに対して協力する必要がある場  合であって、お客様等情報主体の同意を得ることにより当該事務の遂行に支障をおよぼすおそれがある場合。
                                </h6>
                            </li>
                        </ul>
                    </div>

                    <div class="w-100">
                        <h6 class="mt-0 mb-5 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                            7．個人情報の外部委託 <br> お客様から提供いただいた個人情報の取り扱いを、個人情報の保護に関する契約を締結している業務委託会社に委託することがあ ります。また、お客様に明示した利用目的の達成に必要な範囲で個人情報の取り扱いを委託する場合を除き委託することはありま せん。
                        </h6>
                    </div>

                    <div class="w-100">
                        <h6 class="mt-0 mb-5 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                            9．情報内容の照会、修正または削除 <br> 個人情報に関するお問合せ、確認、ならびに修正または削除のお申し出等につきましては、下記窓口までお願いいたします。
                        </h6>
                    </div>

                    <div class="w-100">
                        <h6 class="mt-0 mb-5 f-18 yu-gothic-reg fw-500 lh-32 black-color">
                            10．お問い合わせ等の窓口 <br> アウトボードジェットジャパン　お問い合わせ窓口 <br> 〒430-0907 静岡県浜松市中区高林3-7-15 E-1 <br> TEL：053-482-0385（アドックスマリーン内）　　ファクス：053-482-0325　　電話受付時間 10：00～18：00
                            <br> 当社定休日（原則として、土日祝日、正月休み、盆休み）を除きます。 <br> 当サイトからメールでのお問い合わせは、「お問い合わせ」のページのお問い合わせフォームをご利用ください。
                        </h6>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 mt-5 privacy-policy-content d-flex justify-content-end">
                    <h6 class="m-0 f-18 yu-gothic-reg black-color fw-500 lh-32 text-right">
                        2022年 7月 <br> アウトボードジェット ジャパン
                    </h6>
                </div>
            </div>
        </div>
    </section>
    <!--Privacy Policy-->

    @include('web::shared.footer')

@endsection
