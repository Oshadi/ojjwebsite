<!--Product Information Content-->
<link rel="stylesheet" href="{{ asset('template_assets/assets/css/bootstrap.min.css') }}">

{{--  Slick CSS  --}}
<link rel="stylesheet" href="{{ asset('template_assets/assets/plugins/slick/slick.css') }}">
<link rel="stylesheet" href="{{ asset('template_assets/assets/plugins/slick/slick-theme.css') }}">

{{--  Custom CSS  --}}
<link rel="stylesheet" href="{{ asset('template_assets/assets/css/custom/custom-style.css') }}">
<link rel="stylesheet" href="{{ asset('template_assets/assets/css/custom/custom-responsive.css') }}">
<div  id="product_details">
<section class="product-info-content-section px-0-section pt-0" id="productInfo">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 mb-5 pb-3 h-max">
                <div class="row">
                    <div class="col-sm-12 col-md-12 px-80 mb-4">
                        <h4 class="m-0 font-weight-bold f-40 black-color"><em>{{$product->name}}</em></h4>
                    </div>

                    <div class="col-sm-12 col-md-12 main-product-view-slider px-80" id="mainProductSlider">



                            @isset($product->main_image)

                            <div class="image-container" style="width: 1189px;position: relative;left: 0px;top: 0px;z-index: 999;opacity: 1;">
                                <img rel="preload" src="{{$product->image_path}}" alt="product-image">
                            </div>
                            @endisset
                            @isset($product->ProductImages)
                                @foreach($product->ProductImages as $product_subImages)

                                <div class="image-container" style="width: 123px;">
                                    <img rel="preload" src="{{$product_subImages->image_path}}" alt="product-image">
                                </div>
                                    @endforeach
                            @endisset

                    </div>

                    <div class="col-sm-12 col-md-12 product-list-slider mt-4 px-80">
                        @isset($product->main_image)
                        <div class="image-container"  style="width: 123px;">
                            <img rel="preload" src="{{$product->image_path}}" alt="product-image">
                        </div>
                        @endisset
                            @isset($product->ProductImages)
                                @foreach($product->ProductImages as $product_subImages)

                                    <div class="image-container" style="width: 123px;">
                                        <img rel="preload" src="{{$product_subImages->image_path}}" alt="product-image">
                                    </div>
                                @endforeach
                            @endisset

                    </div>

                </div>
            </div>

            <!--Product Information Content Inner-->
            <div class="col-sm-12 col-md-12 product-info-inner mt-md-5 mxw-container">
                <div class="row">
                    <!--Product Information Inner Table Content-->
                    <div class="col-sm-12 col-md-12 col-lg-6 mb-3">
                        <!--Product Information Inner Table Title-->
                        <h4 class="mt-0 mb-4 f-24 yu-gothic-reg fw-500 black-color">
                            {{$product->ProductMeasurement->table_1_name}}
                        </h4>
                        <!--Product Information Inner Table Title-->

                        <!--Product Information Inner Table-->
                        <div class="w-100 table-responsive">
                            <table class="table table-bordered product-info-table">
                                @isset($product->ProductMeasurement)
                                <tbody>
                                <tr>
                                    <td class="bg-table-gray">定員数</td>
                                    <td>人</td>
                                    <td>{{$product->ProductMeasurement->capacity}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">全長</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductMeasurement->full_length}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">全幅</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductMeasurement->full_width}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">内幅</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductMeasurement->inner_width}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">チューブ径</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductMeasurement->tube_diameter}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">重量</td>
                                    <td>kg</td>
                                    <td>{{$product->ProductMeasurement->weight}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">トランサム高</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductMeasurement->transom_high}}</td>
                                </tr>
                                </tbody>
                                @endisset
                            </table>
                        </div>
                        <!--Product Information Inner Table-->
                    </div>
                    <!--Product Information Inner Table Content-->

                    <!--Product Information Inner Image-->
                    <div class="col-sm-12 col-md-12 col-lg-6 d-flex justify-content-center mb-3">
                        <img class="w-75 h-auto" src="{{$product->ProductMeasurement->image_path}}" alt="product-info-image">
                    </div>
                    <!--Product Information Inner Image-->
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12 mt-5">
                        <!--Product Information Inner Table Title-->
                        <h4 class="mt-0 mb-4 f-24 yu-gothic-reg fw-500 black-color">
                            {{$product->ProductDetails->table_2_name}}
                        </h4>
                        <!--Product Information Inner Table Title-->

                        <!--Product Information Inner Table-->
                        <div class="w-100 table-responsive">
                            <table class="table table-bordered product-info-table">
                                @isset($product->ProductDetails)
                                <tbody>
                                <tr>
                                    <td class="bg-table-gray">全長</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductDetails->ful_length}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">全幅</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductDetails->full_width}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">全高</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductDetails->overall_height}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">重量</td>
                                    <td>kg</td>
                                    <td>{{$product->ProductDetails->weight}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">トランサム高</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductDetails->transom_high}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">最大出力</td>
                                    <td>kW(PS)/r/min</td>
                                    <td>{{$product->ProductDetails->maximum_output}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">全開時推奨回転</td>
                                    <td>r/min</td>
                                    <td>{{$product->ProductDetails->recommended_rotation_when_fully_open}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">トローリング回転数</td>
                                    <td>r/min</td>
                                    <td>{{$product->ProductDetails->trolling_speed}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">エンジンタイプ</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->engine_type}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">シリンダー配列</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->cylinder_array}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">ボア</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductDetails->boa}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">ストローク</td>
                                    <td>mm</td>
                                    <td>{{$product->ProductDetails->stroke}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">総排気量</td>
                                    <td>㎤</td>
                                    <td>{{$product->ProductDetails->total_displacement}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">圧縮比</td>
                                    <td>：1</td>
                                    <td>{{$product->ProductDetails->compression_ratio}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">スパークプラグ</td>
                                    <td>NGK</td>
                                    <td>{{$product->ProductDetails->spark_plug}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">イグニッションシステム</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->ignition_system}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">燃料供給方式</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->fuel_supply_method}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">排気方式</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->exhaust_system}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">潤滑方式</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->lubrication_method}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">始動裝置</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->starter}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">チョークシステム</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->chalk_system}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">スロットルコントロール</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->throttle_control}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">サーモスタット開弁温度</td>
                                    <td>℃</td>
                                    <td>{{$product->ProductDetails->thermostat_valve_opening_temperature}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">使用燃料</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->fuel_used}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">エンジンオイル</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->engine_oil}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">エンジンオイルグレード</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->engine_oil_grade}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">エンジンオイル粘度</td>
                                    <td></td>
                                    <td> {{$product->ProductDetails->engine_oil_viscosity}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">エンジンオイル規定量 <br> （オイル交換のみ）</td>
                                    <td>L</td>
                                    <td>{{$product->ProductDetails->engine_oil_specified_amount}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">エンジンオイル規定量 <br> （オイルフィルター交換時）</td>
                                    <td>L</td>
                                    <td>{{$product->ProductDetails->engine_oil_specified_amount}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">JETポンプグリス</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->jet_pump_grease}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">クラッチ形式</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->clutch_type}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">クラッチ操作方式</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->clutch_operation_method}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">ギヤ比</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->gear_ratio}}</td>
                                </tr>
                                <tr>
                                    <td class="bg-table-gray">JETインペラ回転方向</td>
                                    <td></td>
                                    <td>{{$product->ProductDetails->jet_impeller_rotation_direction}}</td>
                                </tr>
                                </tbody>
                                @endisset
                            </table>
                        </div>
                        <!--Product Information Inner Table-->
                    </div>
                </div>

                <div class="row mt-5">
                    <!--Product Information Inner Image-->
                    <div class="col-sm-12 col-md-12 col-lg-6 d-flex justify-content-center justify-content-md-start mb-3">
                        <img class="w-75 h-auto" src="{{$product->ProductDetails->image_path}}" alt="product-info-image">
                    </div>
                    <!--Product Information Inner Image-->
                    <div class="col-sm-12 col-md-12 col-lg-6 d-flex justify-content-center justify-content-md-start mb-3">
                        <img class="w-75 h-auto" src="{{$product->ProductDetails->table_Image}}" alt="product-info-image">
                    </div>
                    <!--Product Information Inner Table-->

                    <!--Product Information Inner Table-->
                </div>
            </div>
            <!--Product Information Content Inner-->
        </div>
    </div>
</section>
<!--Product Information Content-->

    <!--Contact Info-->
    <section class="contact-info-section home-section pt-0">
        <div class="container">
            <div class="row">
                <!--Contact Info Content-->
                <div class="col-sm-12 col-md-12 contact-info-content">
                    <h6 class="mt-0 mb-3 mb-md-4 f-18 fw-500 yu-gothic-reg black-color text-center">
                        お見積もり・お問い合わせはこちら
                    </h6>

                    <div class="d-flex justify-content-center mb-3 mb-md-4">
                        <a href="{{route('contact_us')}}" type="button" class="cus-white-btn">お問い合わせフォーム</a>
                    </div>

                    <h6 class="mt-0 mb-3 mb-md-4 f-18 fw-500 yu-gothic-reg black-color text-center">
                        お電話でのお問い合わせの場合
                    </h6>

                    <div class="d-flex justify-content-center align-items-center mb-2">
                        <span class="d-block mr-2 font-italic red-text f-22 fw-600">TEL</span>
                        <a href="tel:{{@$contactUs->contact}}" class="font-italic b-b-2-s f-22 fw-600 black-color">{{@$contactUs->contact}}</a>
                    </div>

                    <h6 class="m-0 f-18 fw-500 yu-gothic-reg black-color text-center">
                        アドックス-OJJ事業部まで
                    </h6>
                </div>
                <!--Contact Info Content-->
            </div>
        </div>
    </section>

</div>

<!--Top Scroll Button-->
<a href="#top" class="scroll-top-btn">
    <svg viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"><path d="M416 352c-8.188 0-16.38-3.125-22.62-9.375L224 173.3l-169.4 169.4c-12.5 12.5-32.75 12.5-45.25 0s-12.5-32.75 0-45.25l192-192c12.5-12.5 32.75-12.5 45.25 0l192 192c12.5 12.5 12.5 32.75 0 45.25C432.4 348.9 424.2 352 416 352z"/></svg>
</a>
<!--Top Scroll Button-->
{{-- Laravel Mix - JS File --}}

<script src="{{ asset('template_assets/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('template_assets/assets/js/bootstrap.bundle.min.js') }}"></script>

{{--  Slick JS  --}}
<script src="{{ asset('template_assets/assets/plugins/slick/slick.min.js') }}"></script>

{{-- Custom JS --}}
<script src="{{ asset('template_assets/assets/js/custom/custom.js') }}"></script>
