@extends('web::layouts.master')
{{--@section('meta-keyword', 'Online Games')--}}
{{--@section('title', 'Online Games for Free | Game PVT Games | Best Fun Games to Play')--}}
{{--@section('meta-description', 'Online Games are more fun with the Game PVT Games Website. Play your Favourite Games Arcade, Puzzle, Funny, Sports, Card games and more.')--}}
@section('content')

    @include('web::shared.secondary_header')

    <!--Product Information-->
    <section class="product-info-section px-0-section">
        <div class="container-fluid">
            <div class="row">
                <!--Product Information Title-->
                <div class="col-sm-12 col-md-12 pb-3 section-header px-section-header mxw-container">
                    <h1 class="m-0">
                        Product Information
                        <span>製品案内</span>
                    </h1>
                </div>
                <!--Product Information Title-->

                <!--Product Information Products-->
                <div class="col-sm-12 col-md-12 mt-3 mt-md-5 product-info-products mxw-container">

                    <div class="row">
                        <div class="col-sm-12 col-md-12 mb-4">
                            <!--Product Information Title-->
                            <h4 class="mt-0 mb-3 f-24 fw-600 black-color txt-overflow txt-overflow-1">
                                ボートラインアップ
                            </h4>
                            <!--Product Information Title-->
                        </div>
                    </div>

                    <div class="row justify-content-center">
                        <!-- Product Information Product Card-->

                        @foreach($details as $detail)
                        <div class="col-sm-6 col-md-4 product-info-product-card mb-5">
                            <a href="#" onclick="loardOtherDetails({{$detail->id}})" id="productLink">

                                <!--  Product Information Product Card Image;-->
                                <div class="w-100 mb-3 d-flex justify-content-center">
                                    <img class="w-100 h-auto"
                                         src="{{$detail->thumbnail_image}}"
                                         alt="product-img">
                                </div>
                                <!--  Product Information Product Card Image  -->

                                <!-- Product Information Product Card Code -->
                                <h4 class="m-0 f-18 font-weight-bold black-color text-center">
                                    <em>{{$detail->name}}</em>
                                </h4>
                                <!-- Product Information Product Card Code&ndash -->
                            </a>
                        </div>
                        @endforeach

                    </div>
                </div>
                <!--Product Information Products-->
            </div>
        </div>
    </section>
    <div  id="product_details">

    <!--Product Information Content-->
    <section class="product-info-content-section px-0-section pt-0" id="productInfo">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-md-12 mb-5 pb-3 h-max">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 px-80 mb-4">
                            <h4 class="m-0 font-weight-bold f-40 black-color">{{$product_details->name}}</h4>
                        </div>

                        <div class="col-sm-12 col-md-12 main-product-view-slider px-80" id="mainProductSlider">



                            @isset($product_details->main_image)

                                <div class="image-container" style="width: 1189px;position: relative;left: 0px;top: 0px;z-index: 999;opacity: 1;">
                                    <img rel="preload" src="{{$product_details->image_path}}" alt="product-image">
                                </div>
                            @endisset
                            @isset($product_details->ProductImages)
                                @foreach($product_details->ProductImages as $product_subImages)

                                    <div class="image-container" style="width: 123px;">
                                        <img rel="preload" src="{{$product_subImages->image_path}}" alt="product-image">
                                    </div>
                                @endforeach
                            @endisset

                        </div>

                        <div class="col-sm-12 col-md-12 product-list-slider mt-sm-4 px-80">
                            @isset($product_details->main_image)
                                <div class="image-container"  style="width: 123px;">
                                    <img rel="preload" src="{{$product_details->image_path}}" alt="product-image">
                                </div>
                            @endisset
                            @isset($product_details->ProductImages)
                                @foreach($product_details->ProductImages as $product_subImages)

                                    <div class="image-container" style="width: 123px;">
                                        <img rel="preload" src="{{$product_subImages->image_path}}" alt="product-image">
                                    </div>
                                @endforeach
                            @endisset

                        </div>

                    </div>
                </div>

                <!--Product Information Content Inner-->
                <div class="col-sm-12 col-md-12 product-info-inner mt-md-5 mxw-container">
                    <div class="row">
                        <!--Product Information Inner Table Content-->
                        <div class="col-sm-12 col-md-12 col-lg-6 mb-3">
                            <!--Product Information Inner Table Title-->
                            <h4 class="mt-0 mb-4 f-24 yu-gothic-reg fw-500 black-color">
                                {{$product_details->ProductMeasurement->table_1_name}}
                            </h4>
                            <!--Product Information Inner Table Title-->

                            <!--Product Information Inner Table-->
                            <div class="w-100 table-responsive">
                                <table class="table table-bordered product-info-table">
                                    @isset($product_details->ProductMeasurement)
                                        <tbody>
                                        <tr>
                                            <td class="bg-table-gray">定員数</td>
                                            <td>人</td>
                                            <td>{{$product_details->ProductMeasurement->capacity}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">全長</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductMeasurement->full_length}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">全幅</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductMeasurement->full_width}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">内幅</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductMeasurement->inner_width}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">チューブ径</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductMeasurement->tube_diameter}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">重量</td>
                                            <td>kg</td>
                                            <td>{{$product_details->ProductMeasurement->weight}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">トランサム高</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductMeasurement->transom_high}}</td>
                                        </tr>
                                        </tbody>
                                    @endisset
                                </table>
                            </div>
                            <!--Product Information Inner Table-->
                        </div>
                        <!--Product Information Inner Table Content-->

                        <!--Product Information Inner Image-->
                        <div class="col-sm-12 col-md-12 col-lg-6 d-flex justify-content-center mb-3">
                            <img class="w-75 h-auto" src="{{$product_details->ProductMeasurement->image_path}}" alt="product-info-image">
                        </div>
                        <!--Product Information Inner Image-->
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-md-12 mt-5">
                            <!--Product Information Inner Table Title-->
                            <h4 class="mt-0 mb-4 f-24 yu-gothic-reg fw-500 black-color">
                                {{$product_details->ProductDetails->table_2_name}}
                            </h4>
                            <!--Product Information Inner Table Title-->

                            <!--Product Information Inner Table-->
                            <div class="w-100 table-responsive">
                                <table class="table table-bordered product-info-table">
                                    @isset($product_details->ProductDetails)
                                        <tbody>
                                        <tr>
                                            <td class="bg-table-gray">全長</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductDetails->ful_length}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">全幅</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductDetails->full_width}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">全高</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductDetails->overall_height}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">重量</td>
                                            <td>kg</td>
                                            <td>{{$product_details->ProductDetails->weight}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">トランサム高</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductDetails->transom_high}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">最大出力</td>
                                            <td>kW(PS)/r/min</td>
                                            <td>{{$product_details->ProductDetails->maximum_output}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">全開時推奨回転</td>
                                            <td>r/min</td>
                                            <td>{{$product_details->ProductDetails->recommended_rotation_when_fully_open}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">トローリング回転数</td>
                                            <td>r/min</td>
                                            <td>{{$product_details->ProductDetails->trolling_speed}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">エンジンタイプ</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->engine_type}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">シリンダー配列</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->cylinder_array}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">ボア</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductDetails->boa}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">ストローク</td>
                                            <td>mm</td>
                                            <td>{{$product_details->ProductDetails->stroke}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">総排気量</td>
                                            <td>㎤</td>
                                            <td>{{$product_details->ProductDetails->total_displacement}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">圧縮比</td>
                                            <td>：1</td>
                                            <td>{{$product_details->ProductDetails->compression_ratio}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">スパークプラグ</td>
                                            <td>NGK</td>
                                            <td>{{$product_details->ProductDetails->spark_plug}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">イグニッションシステム</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->ignition_system}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">燃料供給方式</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->fuel_supply_method}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">排気方式</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->exhaust_system}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">潤滑方式</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->lubrication_method}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">始動裝置</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->starter}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">チョークシステム</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->chalk_system}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">スロットルコントロール</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->throttle_control}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">サーモスタット開弁温度</td>
                                            <td>℃</td>
                                            <td>{{$product_details->ProductDetails->thermostat_valve_opening_temperature}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">使用燃料</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->fuel_used}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">エンジンオイル</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->engine_oil}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">エンジンオイルグレード</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->engine_oil_grade}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">エンジンオイル粘度</td>
                                            <td></td>
                                            <td> {{$product_details->ProductDetails->engine_oil_viscosity}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">エンジンオイル規定量 <br> （オイル交換のみ）</td>
                                            <td>L</td>
                                            <td>{{$product_details->ProductDetails->engine_oil_specified_amount}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">エンジンオイル規定量 <br> （オイルフィルター交換時）</td>
                                            <td>L</td>
                                            <td>{{$product_details->ProductDetails->engine_oil_specified_amount}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">JETポンプグリス</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->jet_pump_grease}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">クラッチ形式</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->clutch_type}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">クラッチ操作方式</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->clutch_operation_method}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">ギヤ比</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->gear_ratio}}</td>
                                        </tr>
                                        <tr>
                                            <td class="bg-table-gray">JETインペラ回転方向</td>
                                            <td></td>
                                            <td>{{$product_details->ProductDetails->jet_impeller_rotation_direction}}</td>
                                        </tr>
                                        </tbody>
                                    @endisset
                                </table>
                            </div>
                            <!--Product Information Inner Table-->
                        </div>
                    </div>

                    <div class="row mt-5">
                        <!--Product Information Inner Image-->
                        <div class="col-sm-12 col-md-12 col-lg-6 d-flex justify-content-center justify-content-md-start mb-3">
                            <img class="w-75 h-auto" src="{{$product_details->ProductDetails->image_path}}" alt="product-info-image">
                        </div>
                        <!--Product Information Inner Image-->
                        <div class="col-sm-12 col-md-12 col-lg-6 d-flex justify-content-center justify-content-md-end mb-3">
                            <img class="w-75 h-auto" src="{{$product_details->ProductDetails->table_Image}}" alt="product-info-image">
                        </div>
                        <!--Product Information Inner Table-->

                        <!--Product Information Inner Table-->
                    </div>
                </div>
                <!--Product Information Content Inner-->
            </div>
        </div>
    </section>
    <!--Product Information Content-->

    <!--Contact Info-->
    <section class="contact-info-section home-section pt-0">
        <div class="container">
            <div class="row">
                <!--Contact Info Content-->
                <div class="col-sm-12 col-md-12 contact-info-content">
                    <h6 class="mt-0 mb-3 mb-md-4 f-18 fw-500 yu-gothic-reg black-color text-center">
                        お見積もり・お問い合わせはこちら
                    </h6>

                    <div class="d-flex justify-content-center mb-3 mb-md-4">
                        <a href="contact-us.html" type="button" class="cus-white-btn">お問い合わせフォーム</a>
                    </div>

                    <h6 class="mt-0 mb-3 mb-md-4 f-18 fw-500 yu-gothic-reg black-color text-center">
                        お電話でのお問い合わせの場合
                    </h6>

                    <div class="d-flex justify-content-center align-items-center mb-2">
                        <span class="d-block mr-2 font-italic red-text f-22 fw-600">TEL</span>
                        <a href="tel:053-482-0325" class="font-italic b-b-2-s f-22 fw-600 black-color">053 482 0325</a>
                    </div>

                    <h6 class="m-0 f-18 fw-500 yu-gothic-reg black-color text-center">
                        アドックス-OJJ事業部まで
                    </h6>
                </div>
                <!--Contact Info Content-->
            </div>
        </div>
    </section>
    <!--Contact Info-->


    </div>
    @include('web::shared.footer')

@endsection
<script>
    /**
     * Created by - Oshadhi Chamodya
     * Created at - 29-07-2022
     *
     * This function used to click product main image and loard other details
     * @param id
     */
    function loardOtherDetails(id){
        $.ajax({
            data: {id:id},
            url: "{{ route('get-product-details') }}",
            type: "get",
            dataType: 'json',

            success: function (result) {
                // document.getElementById("div_new").style.display = "none";

                $("#product_details").html(result.data);
                // console.log(data);
                // document.getElementById("producr_details").style.display = "block";
                // var image_path_main=data['product_details'].image_path
                //
                // document.getElementById("name_boat").innerHTML=data['product_details'].name;
                // document.getElementById("main_image").src=image_path_main;
                // document.getElementById("main_image_2").src=image_path_main;
                // let len = data['sub_images'].length;
                // console.log(len);
                // for (let i = 0; i < len; i++) {
                //     document.getElementById("sub_image_1").src=data['sub_images'][i].image_path;
                //     document.getElementById("sub_image_2").src=data['sub_images'][i].image_path;
                //
                // }
                //
                // console.log(data['product_measurement']);
                // document.getElementById("table_1_col_1").innerHTML=data['product_measurement'].capacity;
                // document.getElementById("table_1_col_2").innerHTML=data['product_measurement'].tube_diameter;
                // document.getElementById("table_1_col_3").innerHTML=data['product_measurement'].full_length;
                // document.getElementById("table_1_col_4").innerHTML=data['product_measurement'].weight;
                // document.getElementById("table_1_col_5").innerHTML=data['product_measurement'].full_width;
                // document.getElementById("table_1_col_6").innerHTML=data['product_measurement'].transom_high;
                // document.getElementById("table_1_col_7").innerHTML=data['product_measurement'].inner_width;
                //
                // document.getElementById("boat_image").src=data['product_measurement'].image_path;
                //
                // document.getElementById("table_2_col_1").innerHTML=data['product_other_details'].ful_length;
                // document.getElementById("table_2_col_2").innerHTML=data['product_other_details'].total_displacement;
                // document.getElementById("table_2_col_3").innerHTML=data['product_other_details'].fuel_used;
                // document.getElementById("table_2_col_4").innerHTML=data['product_other_details'].full_width;
                // document.getElementById("table_2_col_5").innerHTML=data['product_other_details'].compression_ratio;
                // document.getElementById("table_2_col_6").innerHTML=data['product_other_details'].engine_oil;
                // document.getElementById("table_2_col_7").innerHTML=data['product_other_details'].overall_height;
                //
                // document.getElementById("table_2_col_8").innerHTML=data['product_other_details'].overall_height;
                // document.getElementById("table_2_col_9").innerHTML=data['product_other_details'].spark_plug;
                // document.getElementById("table_2_col_10").innerHTML=data['product_other_details'].engine_oil_grade;
                // document.getElementById("table_2_col_11").innerHTML=data['product_other_details'].weight;
                // document.getElementById("table_2_col_12").innerHTML=data['product_other_details'].ignition_system;
                // document.getElementById("table_2_col_13").innerHTML=data['product_other_details'].transom_high;
                // document.getElementById("table_2_col_14").innerHTML=data['product_other_details'].fuel_supply_method;
                //
                // document.getElementById("table_2_col_15").innerHTML=data['product_other_details'].engine_oil_specified_amount;
                // document.getElementById("table_2_col_16").innerHTML=data['product_other_details'].maximum_output;
                // document.getElementById("table_2_col_17").innerHTML=data['product_other_details'].exhaust_system;
                // document.getElementById("table_2_col_18").innerHTML=data['product_other_details'].specified_amount_of_engine_oil;
                // document.getElementById("table_2_col_19").innerHTML=data['product_other_details'].recommended_rotation_when_fully_open;
                // document.getElementById("table_2_col_20").innerHTML=data['product_other_details'].lubrication_method;
                // document.getElementById("table_2_col_21").innerHTML=data['product_other_details'].jet_pump_grease;
                //
                // document.getElementById("table_2_col_22").innerHTML=data['product_other_details'].trolling_speed;
                // document.getElementById("table_2_col_23").innerHTML=data['product_other_details'].starter;
                // document.getElementById("table_2_col_24").innerHTML=data['product_other_details'].clutch_type;
                // document.getElementById("table_2_col_25").innerHTML=data['product_other_details'].engine_type;
                // document.getElementById("table_2_col_26").innerHTML=data['product_other_details'].chalk_system;
                // document.getElementById("table_2_col_27").innerHTML=data['product_other_details'].clutch_operation_method;
                // document.getElementById("table_2_col_28").innerHTML=data['product_other_details'].cylinder_array;
                //
                // document.getElementById("table_2_col_29").innerHTML=data['product_other_details'].throttle_control;
                // document.getElementById("table_2_col_30").innerHTML=data['product_other_details'].gear_ratio;
                // document.getElementById("table_2_col_31").innerHTML=data['product_other_details'].boa;
                // document.getElementById("table_2_col_32").innerHTML=data['product_other_details'].thermostat_valve_opening_temperature;
                // document.getElementById("table_2_col_33").innerHTML=data['product_other_details'].stroke;
                // document.getElementById("table_2_col_34").innerHTML=data['product_other_details'].stroke;
                //
                // document.getElementById("image_graph").src=data['product_other_details'].image_path;
                // document.getElementById("image_table").src=data['product_other_details'].image_path;
            },

        });

        $("html, body").animate({
            scrollTop: $("#productInfo").offset().top
        }, 1000);
        return false;
    }
</script>
