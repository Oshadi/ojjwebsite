@extends('web::layouts.master')
{{--@section('meta-keyword', 'Online Games')--}}
{{--@section('title', 'Online Games for Free | Game PVT Games | Best Fun Games to Play')--}}
{{--@section('meta-description', 'Online Games are more fun with the Game PVT Games Website. Play your Favourite Games Arcade, Puzzle, Funny, Sports, Card games and more.')--}}
@section('content')

    <!--Popup Video Content-->
    <div class="popup-video-content">
        <video class="popup-video" width="100%" height="100%" autoplay loop muted playsinline id="popupVideo" controls>
            <source src="https://ojj.h2mobi.com/template_assets/assets/videos/rescue-boat/riffle_running.mp4" type="video/mp4">
            <source src="https://ojj.h2mobi.com/template_assets/assets/videos/rescue-boat/riffle_running.mp4" type="video/ogg">
            <source src="image.webm" onerror="fallback(parentNode)">
            <img src="image.gif">
        </video>
    </div>
    <!--Popup Video Content-->

@endsection
