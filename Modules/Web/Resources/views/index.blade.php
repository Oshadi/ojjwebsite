@extends('web::layouts.master')
@section('content')

    <!--Top Content-->
    <div class="top-content">

    @include('web::shared.header')

        <!--Main Top Video-->
        <div class="main-top">
            <div class="video-wrapper">
                <video class="main-top-video" width="100%" height="100%" autoplay loop muted playsinline id="mainVideo">
                    <source src="https://ojj.h2mobi.com/template_assets/assets/videos/top.mp4" type="video/mp4">
                    <source src="https://ojj.h2mobi.com/template_assets/assets/videos/top.mp4" type="video/ogg">
                    <source src="image.webm" onerror="fallback(parentNode)">
                    <img src="image.gif">
                </video>
            </div>
        </div>

        <!--    <button class="btn btn-success" id="xxx">XXX</button>-->
        <!--Main Top Video-->

        <!--Scroll Indicators-->
        <div class="w-100 scroll-indicators d-flex justify-content-around">
            <div class="w-33"></div>
            <div class="w-33 d-flex justify-content-center scroll-indicate-triangle">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 411 356">
                    <path id="Polygon_1" data-name="Polygon 1" d="M205.5,0,411,356H0Z" transform="translate(411 356) rotate(180)" fill="#fff"/>
                </svg>
            </div>
            <div class="w-33 d-flex justify-content-end align-items-center scroll-indicate-arrow">
                <div class="scroll-rotate">
                    <span class="d-block mr-2 text-white meiryo f-14">Scroll</span>
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 813.158 78.882">
                        <path id="Path_1" data-name="Path 1" d="M7506.136-228.467h798.89l-87.583-69.975,87.583,69.975Z" transform="translate(-7506.136 302.348)" fill="none" stroke="#fff" stroke-width="10"/>
                    </svg>
                </div>
            </div>
        </div>
        <!--Scroll Indicators-->
    </div>
    <!--Top Content-->

    <!--News & Topics-->
    <section class="news-and-topics home-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 section-header">
                    <h1 class="m-0">
                        News & Topics
                        <span>新着情報</span>
                    </h1>
                </div>

                <div class="col-sm-12 col-md-12 mt-80px mt-xs-40px mt-sm-40px">
                    <div class="row">
                        <!--News Card-->

                        @if(isset($latestNews))

                            @foreach($latestNews as $news)
                                <div class="col-sm-12 col-md-4 italic-card news-card mb-3">
                                    <!--News Card Inner-->
                                    <div class="italic-card-inner news-card-inner">
                                        <!--News Card Image-->
                                        <img src="{{ $news->image_path }}" alt="news01">
                                        <!--News Card Image-->

                                        <!--News Card Content-->
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12 mb-4">
                                                <h4 class="m-0 news-card-text">
                                                    {{ $news->title }}
                                                </h4>
                                            </div>

                                            <!--News Card Button-->
                                            <div class="col-sm-12 col-md-12 d-flex justify-content-center">
                                                <a href="{{route('news')}}" class="yellow-btn">救助訓練</a>
                                            </div>
                                            <!--News Card Button-->
                                        </div>
                                        <!--News Card Content-->
                                    </div>
                                    <!--News Card Inner-->

                                    <!--News Card Date-->
                                    <div class="w-100 news-card-date mt-3">
                                        <h6 class="m-0 text-center f-14 yu-gothic-reg black-color">{{ $news->added_date }}</h6>
                                    </div>
                                    <!--News Card Date-->
                                </div>
                            @endforeach
                        @endif
                        <!--News Card-->

{{--                        <!--News Card-->--}}
{{--                        <div class="col-sm-12 col-md-4 italic-card news-card mb-3">--}}
{{--                            <!--News Card Inner-->--}}
{{--                            <div class="italic-card-inner news-card-inner">--}}
{{--                                <!--News Card Image-->--}}
{{--                                <img src="{{ asset('template_assets/assets/images/home/news_02.webp') }}" alt="news01">--}}
{{--                                <!--News Card Image-->--}}

{{--                                <!--News Card Content-->--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-sm-12 col-md-12 mb-4">--}}
{{--                                        <h4 class="m-0 news-card-text">--}}
{{--                                            浜名湖 <br> ライフセービングクラブ <br> OJJ380を使用した <br> 救助訓練を実施--}}
{{--                                        </h4>--}}
{{--                                    </div>--}}

{{--                                    <!--News Card Button-->--}}
{{--                                    <div class="col-sm-12 col-md-12 d-flex justify-content-center">--}}
{{--                                        <a href="{{route('news')}}" class="yellow-btn">救助訓練</a>--}}
{{--                                    </div>--}}
{{--                                    <!--News Card Button-->--}}
{{--                                </div>--}}
{{--                                <!--News Card Content-->--}}
{{--                            </div>--}}
{{--                            <!--News Card Inner-->--}}

{{--                            <!--News Card Date-->--}}
{{--                            <div class="w-100 news-card-date mt-3">--}}
{{--                                <h6 class="m-0 text-center f-14 yu-gothic-reg black-color">2022.00.00</h6>--}}
{{--                            </div>--}}
{{--                            <!--News Card Date-->--}}
{{--                        </div>--}}
{{--                        <!--News Card-->--}}

{{--                        <!--News Card-->--}}
{{--                        <div class="col-sm-12 col-md-4 italic-card news-card mb-3">--}}
{{--                            <!--News Card Inner-->--}}
{{--                            <div class="italic-card-inner news-card-inner">--}}
{{--                                <!--News Card Image-->--}}
{{--                                <img src="{{ asset('template_assets/assets/images/home/news_01.webp') }}" alt="news01">--}}
{{--                                <!--News Card Image-->--}}

{{--                                <!--News Card Content-->--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-sm-12 col-md-12 mb-4">--}}
{{--                                        <h4 class="m-0 news-card-text">--}}
{{--                                            浜名湖 <br> ライフセービングクラブ <br> OJJ380を使用した <br> 救助訓練を実施--}}
{{--                                        </h4>--}}
{{--                                    </div>--}}

{{--                                    <!--News Card Button-->--}}
{{--                                    <div class="col-sm-12 col-md-12 d-flex justify-content-center">--}}
{{--                                        <a href="{{route('news')}}" class="yellow-btn">救助訓練</a>--}}
{{--                                    </div>--}}
{{--                                    <!--News Card Button-->--}}
{{--                                </div>--}}
{{--                                <!--News Card Content-->--}}
{{--                            </div>--}}
{{--                            <!--News Card Inner-->--}}

{{--                            <!--News Card Date-->--}}
{{--                            <div class="w-100 news-card-date mt-3">--}}
{{--                                <h6 class="m-0 text-center f-14 yu-gothic-reg black-color">2022.00.00</h6>--}}
{{--                            </div>--}}
{{--                            <!--News Card Date-->--}}
{{--                        </div>--}}
{{--                        <!--News Card-->--}}
                    </div>
                </div>

                <div class="col-sm-12 col-md-12 mt-80px mt-xs-40px mt-sm-40px d-flex justify-content-center">
                    <a href="{{route('news')}}" type="button" class="c-btn black-btn">お知らせ一覧</a>
                </div>
            </div>
        </div>
    </section>
    <!--News & Topics-->


    <!--OJJ Rescue-->
    <section class="ojj-rescue home-section">
        <div class="container">
            <div class="row">
                <!--OJJ Rescue Empty-->
                <div class="col-sm-12 col-md-5 col-lg-5 col-xl-6"></div>
                <!--OJJ Rescue Empty-->

                <div class="col-sm-12 col-md-7 col-lg-7 col-xl-6">
                    <div class="row">
                        <!--OJJ Rescue Card-->
                        <div class="col-sm-12 col-md-12 ojj-rescue-card">
                            <div class="ojj-rescue-card-inner">
                                <h2 class="mt-0 mb-4 ojj-rescue-card-title">OJJ - RESCUEとは</h2>

                                <p class="mt-0 mb-3 ojj-rescue-card-text">
                                    一般的なインフレータブルボートでの 災害時の救助では瓦礫や岩場での接触亀裂による 空気漏れやパンク、またスクリュー船外機では 水中にいる人や物を巻き込んでしまう事故となる 危険性をはらんでいます。 OJJレスキューは、平成30年7月の西日本豪雨を教訓に 従来のPVC素材の上に破断に強い強化皮膜を重ねた 耐破断性能の高いOJJ-RESCUE 380ボートと スクリュー推進ではない OJJジェット船外機を採用しています。
                                </p>

                                <div class="d-flex justify-content-center mt-3 mt-md-5">
                                    <a href="{{route('rescue_boat')}}" type="button" class="c-btn black-btn">OJJ-RESCUEボート</a>
                                </div>
                            </div>
                        </div>
                        <!--OJJ Rescue Card-->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--OJJ Rescue-->

    <!--Product Information-->
    <section class="product-information">
        <div class="container-fluid">
            <div class="row">
                <!--Product Information Header-->
                <div class="col-sm-12 col-md-12 section-header mxw-container">
                    <h1 class="m-0">
                        Product Information
                        <span>製品案内</span>
                    </h1>
                </div>
                <!--Product Information Header-->

                <!--Product Information Content-->
                <div class="col-sm-12 col-md-12 product-info-content mt-80px mt-xs-40px mt-sm-40px">

                    <!--Product Information Video Content-->
                    <div class="product-info-video-content">
                        <!--Product Information Video-->
                        <video class="product-info-video" width="100%" height="100%" autoplay loop muted playsinline id="productInfoVideo">
                            <source src="https://ojj.h2mobi.com/template_assets/assets/videos/product_information.mp4" type="video/mp4">
                            <source src="https://ojj.h2mobi.com/template_assets/assets/videos/product_information.mp4" type="video/ogg">
                            <source src="image.webm" onerror="fallback(parentNode)">
                            <img src="image.gif">
                        </video>
                        <!--Product Information Video-->
                    </div>
                    <!--Product Information Video Content-->

                    <!--Product Information Content Inner-->
                    <div class="product-info-content-inner mxw-container">
                        <h2 class="mt-0 mb-2 mb-md-4 product-info-content-text">
                            OJJ-RESCUE <br> BOAT <br> SUZUKI-OJJ 9.9hpJet
                        </h2>
                        <h2 class="m-0 product-info-content-text-japan">
                            「安全」がテーマです
                        </h2>

                        <div class="mt-3 mt-md-4 d-flex justify-content-end">
                            <a href="{{route('product')}}" type="button" class="c-btn white-btn">詳細を見る</a>
                        </div>
                    </div>
                    <!--Product Information Content Inner-->
                </div>
                <!--Product Information Content-->
            </div>
        </div>
    </section>
    <!--Product Information-->


    <!--Maintenance-->
    <section class="maintenance mb-5">
        <!--Maintenance Image-->
        <img class="maintenance-img" src="{{ asset('template_assets/assets/images/home/maintenance_top.webp') }}" alt="maintenance-img">
        <!--Maintenance Image-->

        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 maintenance-content">
                    <div class="row">
                        <!--Maintenance Header-->
                        <div class="col-sm-12 col-md-12 section-header">
                            <h1 class="m-0">
                                Maintenance
                                <span>メンテナンス</span>
                            </h1>
                        </div>
                        <!--Maintenance Header-->

                        <!--Maintenance Content Inner-->
                        <div class="col-sm-12 col-md-12 maintenance-content-inner mt-4">
                            <h2 class="mt-0 mb-2 mb-md-4 maintenance-content-text">
                                OJJ Pro <br class="d-sm-none"> mechanic<span class="maintenance-japan-text">による</span>
                            </h2>
                            <h2 class="m-0 maintenance-content-text-japan">「安心」な <br> ジェット船外機サポート
                            </h2>

                            <div class="mt-3 mt-md-4 mb-lg-5">
                                <a href="{{route('maintenance')}}" type="button" class="c-btn black-btn">詳細を見る</a>
                            </div>
                        </div>
                        <!--Maintenance Content Inner-->
                    </div>
                </div>

                <!--            <div class="col-sm-12 col-md-12 d-sm-none">-->
                <!--                <img class="w-100" src="assets/images/home/maintenance_top.webp" alt="maintenance">-->
                <!--            </div>-->
            </div>
        </div>
    </section>
    <!--Maintenance-->

    <!--SNS-->
    <section class="sns home-section">
        <div class="container">
            <div class="row">
                <!--SNS Header-->
                <div class="col-sm-12 col-md-12 section-header">
                    <h1 class="m-0">
                        SNS
                        <span>ボートの情報を、もっと手軽に。</span>
                    </h1>
                </div>
                <!--SNS Header-->

                <!--SNS Facebook Content-->
                <div class="col-sm-12 col-md-12 mt-80px mt-xs-40px mt-sm-40px sns-content">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 mb-4 text-center">
                            <h4 class="m-0 sns-header-text"><a href="{{@$contactUs->facebook}}" target="_blank" class="social-link facebook">Facebook</a></h4>
                        </div>
                        <div class="col-sm-12 col-md-12 sns-image-content">
                            <a href="{{@$contactUs->facebook}}" target="_blank" class="social-link facebook"> <img src="{{ asset('template_assets/assets/images/home/facebook.webp') }}" alt="sns-image"></a>
                        </div>
                    </div>
                </div>
                <!--SNS Facebook Content-->

                <!--SNS Instagram Content-->
                <div class="col-sm-12 col-md-12 mt-80px mt-xs-40px mt-sm-40px sns-content">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 mb-4 text-center">
                            <h4 class="m-0 sns-header-text"><a href="{{@$contactUs->instagram}}" target="_blank" class="social-link facebook">Instagram</a></h4>
                        </div>
                        <div class="col-sm-12 col-md-12 sns-image-content">
                            <a href="{{@$contactUs->facebook}}" target="_blank" class="social-link facebook"> <a href="{{@$contactUs->instagram}}" target="_blank" class="social-link facebook">   <img src="{{ asset('template_assets/assets/images/home/instagram.webp') }}" alt="sns-image"></a>
                        </div>
                    </div>
                </div>
                <!--SNS Instagram Content-->
            </div>
        </div>
    </section>
    <!--SNS-->

    @include('web::shared.footer')
@endsection
