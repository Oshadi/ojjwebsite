<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @php
            header("Access-Control-Allow-Origin: *");
        @endphp
        <title>{{ config('app.name', 'Laravel') }} | OJJ Rescue</title>

        <link href="{{ asset('template_assets/assets/images/favicon/favicon.png') }} " rel="icon">

        <link rel="stylesheet" href="{{ asset('template_assets/assets/css/bootstrap.min.css') }}">

        {{--  Slick CSS  --}}
        <link rel="stylesheet" href="{{ asset('template_assets/assets/plugins/slick/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('template_assets/assets/plugins/slick/slick-theme.css') }}">

        {{--  Custom CSS  --}}
        <link rel="stylesheet" href="{{ asset('template_assets/assets/css/custom/custom-style.css') }}">
        <link rel="stylesheet" href="{{ asset('template_assets/assets/css/custom/custom-responsive.css') }}">

        {{--  Custom Script  --}}
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="{{ asset('global_assets/js/sweetalert.min.js') }}"></script>

    </head>
    <body class="body">

        <!--Preloader-->
        <div id="preloader">
            <div id="loader"></div>
        </div>
        <!--Preloader-->

        @yield('content')


        <!--Top Scroll Button-->
        <a href="#top" class="scroll-top-btn d-none">
            <svg viewBox="0 0 448 512" xmlns="http://www.w3.org/2000/svg"><path d="M416 352c-8.188 0-16.38-3.125-22.62-9.375L224 173.3l-169.4 169.4c-12.5 12.5-32.75 12.5-45.25 0s-12.5-32.75 0-45.25l192-192c12.5-12.5 32.75-12.5 45.25 0l192 192c12.5 12.5 12.5 32.75 0 45.25C432.4 348.9 424.2 352 416 352z"/></svg>
        </a>
        <!--Top Scroll Button-->
        {{-- Laravel Mix - JS File --}}

        <script src="{{ asset('template_assets/assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('template_assets/assets/js/bootstrap.bundle.min.js') }}"></script>

        {{--  Slick JS  --}}
        <script src="{{ asset('template_assets/assets/plugins/slick/slick.min.js') }}"></script>

        {{-- Custom JS --}}
        <script src="{{ asset('template_assets/assets/js/custom/custom.js') }}"></script>

    </body>
</html>
