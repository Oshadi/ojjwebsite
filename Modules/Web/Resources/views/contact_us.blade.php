@extends('web::layouts.master')
{{--@section('meta-keyword', 'Online Games')--}}
{{--@section('title', 'Online Games for Free | Game PVT Games | Best Fun Games to Play')--}}
{{--@section('meta-description', 'Online Games are more fun with the Game PVT Games Website. Play your Favourite Games Arcade, Puzzle, Funny, Sports, Card games and more.')--}}
@section('content')

    @include('web::shared.secondary_header')

    <!--Contact Form-->
    <section class="ojj-mission home-section">
        <div class="container">
            <div class="row">
                <!--Contact Form Title-->
                <div class="col-sm-12 col-md-12 section-header">
                    <h1 class="m-0">
                        Contact form
                        <span>お問い合わせフォーム</span>
                    </h1>
                </div>
                <!--Contact Form Title-->

                <div class="col-sm-12 col-md-12 mt-3 mt-md-5 pt-4">
                    <form class="contact-form steps-basic wizard clearfix form-submit" data-reload-on-success="true" data-fouc="" novalidate="novalidate" method="post" action="{{  route('store') }}" enctype="multipart/form-data">
                        {{ method_field('POST') }}
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-sm-4 col-md-3 col-form-label">
                                お名前 <span>必須</span>
                            </label>
                            <div class="col-sm-8 col-md-7">
                                <input type="text" class="form-control cus-input" id="name" name="name">
                                <span class="validation-invalid-label name-error" role="alert" style="color:red;"></span>
                            </div>

                            <div class="col-sm-12 col-md-10 mt-3 mb-2">
                                <div class="w-100 b-b-1-s"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="furigana" class="col-sm-4 col-md-3 col-form-label">
                                ふりがな <span>必須</span>
                            </label>
                            <div class="col-sm-8 col-md-7">
                                <input type="text" class="form-control cus-input" id="furigana" name="furigana">
                                <span class="validation-invalid-label furigana-error" role="alert" style="color:red;"></span>
                            </div>

                            <div class="col-sm-12 col-md-10 mt-3 mb-2">
                                <div class="w-100 b-b-1-s"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="companyOrganizationName" class="col-sm-4 col-md-3 col-form-label">
                                会社名 / 団体名 <span>必須</span>
                            </label>
                            <div class="col-sm-8 col-md-7">
                                <input type="text" class="form-control cus-input" id="company_organization_name" name="company_organization_name">
                                <span class="validation-invalid-label company_organization_name-error" role="alert" style="color:red;"></span>
                            </div>

                            <div class="col-sm-12 col-md-10 mt-3 mb-2">
                                <div class="w-100 b-b-1-s"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-md-3 col-form-label">
                                Eメール <span>必須</span>
                            </label>
                            <div class="col-sm-8 col-md-7">
                                <input type="email" class="form-control cus-input" id="email" name="email">
                                <span class="validation-invalid-label email-error" role="alert" style="color:red;"></span>
                            </div>

                            <div class="col-sm-12 col-md-10 mt-3 mb-2">
                                <div class="w-100 b-b-1-s"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="telephone" class="col-sm-4 col-md-3 col-form-label">
                                TEL（電話連絡をご希望の場合）
                            </label>
                            <div class="col-sm-8 col-md-7">
                                <input type="text" onkeypress="validateTelephone(event)" class="form-control cus-input" id="telephone" name="telephone">
                            </div>

                            <div class="col-sm-12 col-md-10 mt-3 mb-2">
                                <div class="w-100 b-b-1-s"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-4 col-md-3 col-form-label">
                                お問い合わせタイプ <span>必須</span>
                            </label>

                            <div class="col-sm-8 col-md-7 d-flex flex-wrap">
                            <select type="text"
                                    data-placeholder="Select a Inquiry Type"
                                    data-select2-id="inquiry_type_id" class="form-control form-control-select2 inquiry_type_id-is-invalid select-clear inquiry_type_id-class"
                                    name="inquiry_type_id" id="inquiry_type_id">
                                <option value="" disabled selected>お問い合わせタイプを選択してください</option>
                                @foreach($inquiryTypes as $inquiryType)
                                    @if(property_exists($inquiryType, "code"))
                                        <option @if($inquiryType->id == request()->inquiry_type_id) selected @endif value="{{ $inquiryType->id }}">{{ $inquiryType->name }}</option>
                                    @else
                                        <option @if($inquiryType->id == request()->inquiry_type_id) selected @endif value="{{ $inquiryType->id }}">{{ $inquiryType->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                                <span class="validation-invalid-label inquiry_type_id-error" role="alert" style="color:red;"></span>
                            </div>

                            <div class="col-sm-12 col-md-10 mt-3 mb-2">
                                <div class="w-100 b-b-1-s"></div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="contentsOfInquiry" class="col-sm-4 col-md-3 col-form-label">
                                お問い合わせ内容 <span>必須</span>
                            </label>
                            <div class="col-sm-8 col-md-7">
                                <textarea class="form-control cus-input" id="contents_of_inquiry" name="contents_of_inquiry" rows="10"></textarea>
                                <span class="validation-invalid-label contents_of_inquiry-error" role="alert" style="color:red;"></span>
                            </div>
                        </div>

                        <div class="row mt-5">
                            <div class="col-sm-12 col-md-12 d-flex justify-content-center my-4">
                                <label class="cus-checkbox">
                                    <a href="{{route('privacy_policy')}}" class="b-b-2-s pr-4">プライバシーポリシー</a>
                                    <span class="ml-2">に同意する</span>
                                    <input type="checkbox" name="check">
                                    <span class="cus-checkbox-checkmark"></span><br>
                                    <span class="validation-invalid-label check-error" role="alert" style="color:red;"></span>
                                </label>
                            </div>

                            <div class="col-sm-12 col-md-12 d-flex justify-content-center my-2">
                                @if(config('services.recaptcha.key'))
                                    <div class="g-recaptcha" name="g-recaptcha-response"
                                         data-sitekey="{{config('services.recaptcha.key')}}">
                                    </div>
                                @endif
                            </div>
                            <div class="col-sm-12 col-md-12 d-flex justify-content-center" style="padding-bottom: 60px;">
                                <span class="validation-invalid-label g-recaptcha-response-error" role="alert" style="color:red;"></span>
                            </div>

                            <div class="col-sm-12 col-md-12 d-flex justify-content-center">
                                <input id="submit-button" onclick="submitClicked();" class="cus-white-btn" type="submit" name="submit" value="送 信"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!--Contact Form-->
    <script type="text/javascript" src="{{ asset('js/backend-validation.js') }}" defer></script>

    <script>
        function submitClicked(){
            setTimeout(function(){
                $("#submit-button").prop("disabled", true).val("Wait...");
            }, 20);

            setTimeout(function(){
                $("#submit-button").prop("disabled", false).val("Send");
            }, 3000);
        }

        function validateTelephone(evt)
        {
            if(evt.keyCode!=8)
            {
                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
                var regex = /[0-9]|\+/;
                if (!regex.test(key))
                {
                    theEvent.returnValue = false;

                    if (theEvent.preventDefault)
                        theEvent.preventDefault();
                    }
                }
        }

    </script>

    @include('web::shared.footer')

@endsection
