<?php

namespace Modules\Web\Services;

use App\FeeType;
use App\RetailerHasFee;
use App\Services\MainService;
use App\SettingConvenienceFee;
use App\UserProfile;
use Exception;
use Modules\Web\Repositories\InquiryRepository;
use Illuminate\Support\Facades\Log;

class InquiryService extends MainService
{

    protected $inquiryRepository;

    public function __construct(InquiryRepository $inquiryRepository)
    {
    $this->inquiryRepository = $inquiryRepository;
    }

    /**
     *
     * Created at: oshadhi
     * Created by: 2022.06.16
     * Summary: Create new account category data & save
     *
     * @param $request
     * @return array|\Illuminate\Database\Eloquent\Model|void
     */

    public function store($request)
    {
        try {
            $this->inquiryRepository->store($request);
            return getFormattedResponce(true, "success", 'Your Inquiry Successfully Submitted!', [], "", "");
        }
        catch (Exception $ex) {
            //Log::error($ex);
            //dd($ex);
            return getFormattedResponce(false, "failed", __('An Error Occurred') . __(), [], "", "");
        }
    }

}

