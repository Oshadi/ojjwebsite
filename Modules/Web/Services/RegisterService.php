<?php

namespace Modules\Web\Services;

use App\FeeType;
use App\RetailerHasFee;
use App\Services\MainService;
use App\SettingConvenienceFee;
use App\User;
use App\UserProfile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Role;

class RegisterService extends MainService
{

    public function __construct()
    {
        $this->remember_token = base64_encode(uniqid(18));
    }


    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     *
     * Created By : oshadhi
     * Created At : 2022-03-30
     */
    public function storeUser(Request $request)
    {
        try {
            DB::beginTransaction();

            $user = new User();
            $user->first_name = $request->firstName;
            $user->last_name = $request->lastName;
            $user->user_name = $request->userName;
            $user->email = $request->email;
            $user->mobile = $request->mobile;
            $user->password = Hash::make($request->password);
            $user->remember_token = $this->remember_token;
            $user->save();

            $userProfile = new UserProfile();
            $userProfile->user_id = $user->id;
            $userProfile->user_mode = 1;
            $userProfile->address1 = $request->address1;
            $userProfile->address2 = $request->address2;
            $userProfile->address3 = $request->address3;
            $userProfile->district_id = $request->district;
            $userProfile->city_id = $request->city;
            $userProfile->save();

            // DEFAULT CONVENIENCE FREE SETUP
            $feeTypes = SettingConvenienceFee::where('user_mode', 'PERSONAL')->get();
            foreach ($feeTypes as $fee){
                $retailerHasFee = new RetailerHasFee();
                $retailerHasFee->user_id = $user->id;
                $retailerHasFee->fee_type_id = $fee->fee_type_id;
                $retailerHasFee->value = $fee->value;
                $retailerHasFee->value_mode = $fee->value_mode;
                $retailerHasFee->created_by = $user->id;
                $retailerHasFee->save();
            }

            //Assigning role to user
            $role_r = Role::where('id', '=', 3)->firstOrFail(); // Retail
            $user->assignRole($role_r);

            DB::commit();

            // SEND EMAIL
            $this->sendEmail($user, $request->email);

            return ['status' => 'success', 'user' => $user];
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('RegisterController | store() ' . $e);
            throw $e;
        }
    }

    /**
     * Business User Store
     * Created By : oshadhi
     * Created At : 2022-03-30
     *
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function storeBusinessUser($request)
    {
        try {
            DB::beginTransaction();

            $user = new User();
            $user->first_name = $request['business-firstName'];
            $user->last_name = $request['business-lastName'];
            $user->user_name = $request['business-userName'];
            $user->email = $request['business-email'];
            $user->mobile = $request['business-mobile'];
            $user->password = Hash::make($request['business-password']);
            $user->remember_token = $this->remember_token;
            $user->save();

            $filename = null;
            if ($request->hasFile('businessLogo')) {
                $uploads = $request->file('businessLogo');
                $filename = time() . '.' . $uploads->getClientOriginalExtension();

                $destinationPath = storage_path('app/public/business_logo');
                $uploads->move($destinationPath, $filename);

                /*$img = Image::make($image->getRealPath());
                $img->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->crop(200, 200);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('images/users' . '/' . $imageName, $img, 'public');*/
            }

            $userProfile = new UserProfile();
            $userProfile->user_id = $user->id;
            $userProfile->user_mode = 2;
            $userProfile->business_name = $request['businessName'];
            $userProfile->business_nature = $request['businessNature'];
            $userProfile->business_logo = $filename;
            $userProfile->address1 = $request['business-address1'];
            $userProfile->address2 = $request['business-address2'];
            $userProfile->address3 = $request['business-address3'];
            $userProfile->district_id = $request['business-district'];
            $userProfile->city_id = $request['business-city'];
            $userProfile->save();

            // DEFAULT CONVENIENCE FREE SETUP
            $feeTypes = SettingConvenienceFee::where('user_mode', 'BUSINESS')->get();
            foreach ($feeTypes as $fee){
                $retailerHasFee = new RetailerHasFee();
                $retailerHasFee->user_id = $user->id;
                $retailerHasFee->fee_type_id = $fee->fee_type_id;
                $retailerHasFee->value = $fee->value;
                $retailerHasFee->value_mode = $fee->value_mode;
                $retailerHasFee->created_by = $user->id;
                $retailerHasFee->save();
            }

            //Assigning role to user
            $role_r = Role::where('id', '=', 3)->firstOrFail(); // Retail
            $user->assignRole($role_r);

            DB::commit();

            // SEND EMAIL
            $this->sendEmail($user, $request['business-email']);

            return ['status' => 'success', 'user' => $user];
        } catch (\Exception $e) {
            DB::rollBack();
            Log::error('RegisterController | storeBusinessUser() ' . $e);
            throw $e;
        }
    }

    /*
     * USER REGISTER EMAIL VERIFY FUNCTION
     * Created At : 2022-06-03
     * Created By : oshadhi
     */
    function sendEmail ($user, $to)
    {
        $data['remember_token'] = $user->remember_token;
        $data['user_name'] = $user->first_name.' ' . $user->last_name;
        $subject= 'Verify Your Email Address';
        //$to = "gemunuudaya.zincat@gmail.com";

        Mail::send(['html' => 'web::email.email-verify'], $data, function ($message) use ($to, $subject) {
            $message->to($to)->subject($subject);
            $message->from('event@expogun.com', $subject);
        });
    }

}
