<?php

namespace Modules\Web\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 *
 * Created by : oshadhi
 * Created at : 2022.07.28
 * Summary:
 */

class InquiryType extends Model
{
    public function inquiries()
    {
        return $this->belongsTo(Inquiry::class,'inquiry_type_id','id');
    }
}
