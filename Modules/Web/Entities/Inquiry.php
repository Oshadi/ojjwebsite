<?php

namespace Modules\Web\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 *
 * Created by : oshadhi
 * Created at : 2022.07.28
 * Summary:
 */

class Inquiry extends Model
{
    public function inquiryTypes()
    {
        return $this->hasMany(InquiryType::class,'inquiry_type_id','id');
    }
}
