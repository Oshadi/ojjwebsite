<?php

namespace Modules\Web\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalRegisterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName' => 'required|alpha|max:255',
            'lastName' => 'required|alpha|max:255',
            'email' => 'required|email:rfc,dns|max:255|unique:users',
            'mobile' => 'required|string|min:10|max:10',
            'address1' => 'required|string|max:255',
            'address2' => 'required|string|max:255',

            'district' => 'required|int',
            'city' => 'required|int',

            'userName' => 'required|string|min:6|unique:users,user_name',
            'password' => 'required|string|min:6|same:passwordConfirm',
            'passwordConfirm' => 'required|string|min:6',
        ];
    }


    public function messages()
    {
        return [
            'city.required' => 'The city field is required.',
            'city.integer' => 'The city field is required.',
            'district.required' => 'The district field is required.',
            'district.integer' => 'The district field is required.',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
