<?php

namespace Modules\Web\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InquiryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'furigana' => 'required|max:255',
            'company_organization_name' => 'required|max:255',
            'email' => 'required|email:rfc,dns|max:255',
            'inquiry_type_id' => 'required',
            'contents_of_inquiry' => 'required|string',
            'g-recaptcha-response' => 'required|recaptcha',
            'check' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '* 名前フィールドは必須です.',
            'furigana.required' => '* ふりがなフィールドは必須です.',
            'company_organization_name.required' => '* 会社名フィールドは必須です.',
            'email.required' => '* メールアドレス必須.',
            'inquiry_type_id.required' => '* お問い合わせの種類必須.',
            'contents_of_inquiry.required' => '* お問い合わせ内容必須.',
            'g-recaptcha-response.required' => '* Google キャプチャを完成させてください',
            'check.required' => '* プライバシーポリシーに同意する必要があります.',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
