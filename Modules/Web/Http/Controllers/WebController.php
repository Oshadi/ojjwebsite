<?php

namespace Modules\Web\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\View;
use Modules\Admin\Entities\Product;
use Modules\Admin\Services\FaqService;
use Modules\Admin\Services\NewsCategoryService;
use Modules\Admin\Services\NewsService;
use Modules\Admin\Services\ProductService;
use Modules\Web\Entities\ContactUsDetail;

use Illuminate\Support\Facades\Redirect;
use Modules\Web\Entities\Inquiry;
use Modules\Web\Entities\InquiryType;
use Modules\Web\Services\InquiryService;
use Modules\Web\Http\Requests\InquiryRequest;

class WebController extends Controller
{
    protected $footer;
    protected $productService;
    protected $inquiryType;
    protected $inquiryService;

    public function __construct(
        NewsService $newsService,
        InquiryService $inquiryService,
        ProductService $productService,
        NewsCategoryService $newsCategoryService,
        FaqService $faqService,
        Inquiry $inquiry,
        InquiryType $inquiryType
    ) {
        $this->newsService = $newsService;
        $this->inquiryService = $inquiryService;
        $this->productService=$productService;
        $this->footer = ContactUsDetail::whereStatus(1)->first();
        $this->newsCategoryService = $newsCategoryService;
        $this->faqService = $faqService;
        $this->inquiry= $inquiry;
        $this->inquiryType= $inquiryType;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {

        $data['latestNews'] = $this->newsService->getActiveAll(3);
        $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();

        return view('web::index', $data);
    }

    public function store(InquiryRequest $request){
        try {
            return $this->inquiryService->store($request);
        }
        catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    /**
     * Show the Place Order.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function viewRescueBoat()
    {
        $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();
        return view('web::rescue_boat', $data);
    }

    /**
     * News Page
     */
    public function viewNews($categorySlug = null, $newsSlug = null, $currantPageNo = null)
    {

        $data['newsCategories'] = $this->newsCategoryService->getActiveAll();
        $data['mainNews'] = $this->newsService->getLatestNews( $categorySlug, $newsSlug)->first(); // ->where('slug', $newsSlug)

        $data['allNews'] = $this->newsService->getLatestNews($categorySlug, '', $currantPageNo);

        $data['newsCardPagination'] = $this->newsService->getCardFilterPageNews($categorySlug, $newsSlug, $currantPageNo);

        $data['subNewsName'] = ($categorySlug) ? $data['mainNews']->newsCategory->name ?? 'No News' : " All News ";

        $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();
        return view('web::news.index', $data);
    }

    public function viewProductInformation()
    {
        $data['details'] = $this->productService->getActiveAll();
        $data['product_details']=$this->productService->firstProduct();
        $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();
        return view('web::products.product', $data);
    }

    public function getDetailsProduct(Request $request){

        try {
            $data['product']= $this->productService->getProductDetails($request->id);

            $result = View::make('web::products.product_information', $data)->render();

            return getFormattedResponce(true, "success", 'get product details', $result, "", "");
        } catch (\Exception $ex) {

            $outPutArray = array(
                'status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '',
                'notifyType' => 'message'
            );
            return $outPutArray;
        }

    }

    public function viewMaintenance()
    {
        $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();
        $data['faqMain'] = $this->faqService->getAllQuestion("MAIN");
        $data['faqSub'] = $this->faqService->getAllQuestion("SUB");

        return view('web::maintenance', $data);
    }

    public function viewAboutUs()
    {
        $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();
        return view('web::about_us', $data);
    }

    public function viewContactUs()
    {
        $data['inquiryTypes'] = $this->inquiryType->all();
        $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();
        return view('web::contact_us', $data);
    }

    public function viewPrivacyPolicy()
    {
        $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();
        return view('web::privacy_policy', $data);
    }

    public function viewSiteMap() {
        $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();
        return view('web::site_map', $data);
    }

    public function viewPopupVideo()
    {
        return view('web::popup_video');
    }

    public function viewPopupVideo2()
    {
        return view('web::popup_video_2');
    }

    public function viewPopupVideo3()
    {
        return view('web::popup_video_3');
    }

    public function footerDetail()
    {
        $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();
        return view('web::shared.footer', $data);
    }

}
