<?php

namespace Modules\Web\Repositories;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\Repositories\Repository;
use Modules\Web\Entities\InquiryType;
use Modules\Web\Entities\Inquiry;

class InquiryRepository extends Repository
{
    protected $inquiry;
    protected $inquiryType;

    public function __construct(Inquiry $inquiry, InquiryType $inquiryType)
    {
        parent::__construct($inquiry);
        $this->inquiry = $inquiry;
        $this->inquiryType = $inquiryType;
    }

    public function store($request){
        try {
            $inquiryData = new $this->inquiry;
            $inquiryData->name = $request->name;
            $inquiryData->furigana = $request->furigana;
            $inquiryData->company_organization_name = $request->company_organization_name;
            $inquiryData->email = $request->email;
            $inquiryData->telephone = $request->telephone;
            $inquiryData->inquiry_type_id = $request->inquiry_type_id;
            $inquiryData->contents_of_inquiry = $request->contents_of_inquiry;
            $inquiryData->status = 1;
            //$inquiryData->created_by = auth()->user()->id;
            //$inquiryData->updated_by = auth()->user()->id;
            $inquiryData->save();

            $inquiryType = $this->inquiryType->where('id',$request->inquiry_type_id)->first();

            Mail::send('web::inquiry_email',
                array(
                    'name' => $request->name,
                    'furigana' => $request->furigana,
                    'company_organization_name' => $request->company_organization_name,
                    'email' => $request->email,
                    'telephone' => $request->telephone,
                    'inquiry_type_id' => $inquiryType->name,
                    'contents_of_inquiry' => $request->contents_of_inquiry
                ), function($message) use ($request)
                {
                    $message
                        ->subject("You have new inquiry from $request->name")
                        ->from($request->email)
                        ->to(config('mail.from.address'));
                });

            return $inquiryData;
        }
        catch (Exception $ex) {
            dd($ex);
        }
    }
}
