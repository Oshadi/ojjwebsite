<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;


Route::group(['middleware' => ['auth:web'], 'prefix' => 'admin'], function () {

    Route::get('system-update', function () {

        \Illuminate\Support\Facades\Artisan::call('cache:clear');
        \Illuminate\Support\Facades\Artisan::call('view:clear');
        \Illuminate\Support\Facades\Artisan::call('route:clear');

        \Illuminate\Support\Facades\Artisan::call('storage:link');
        \Illuminate\Support\Facades\Artisan::call('config:cache');
        dd("Process Done");
    });


    //Dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');

    // Role routes
    Route::get('roles/get-data', 'RoleController@getData')->name('admin.roles.get-data');
    Route::resource('roles', 'RoleController', resourceNames('admin.roles'));

    // Role routes
    Route::get('permissions/get-data', 'PermissionController@getData')->name('admin.permissions.get-data');
    Route::resource('permissions', 'PermissionController', resourceNames('admin.permissions'));

    // User routes
    Route::get('users/get-data', 'UserController@getData')->name('admin.users.get-data');
    Route::resource('users', 'UserController', resourceNames('admin.users'));


    // News Category
    Route::get('news-category/get-data', 'NewsCategoryController@getData')->name('admin.news-category.get-data');
    Route::post('news-category/{auid}/active', 'NewsCategoryController@active')->name('admin.news-category.active');
    Route::post('news-category/{auid}/inactive', 'NewsCategoryController@inactive')->name('admin.news-category.inactive');
    Route::resource('news-category', 'NewsCategoryController', resourceNames('admin.news-category'));

    Route::get('product/get-data', 'ProductController@getData')->name('admin.product.get-data');
    Route::post('product/{auid}/active', 'ProductController@active')->name('admin.product.active');
    Route::post('product/{auid}/inactive', 'ProductController@inactive')->name('admin.product.inactive');
    Route::post('delete-image', 'ProductController@deleteImage')->withoutMiddleware([\App\Http\Middleware\VerifyCsrfToken::class])->name('admin.product.delete_image');

    Route::resource('product', 'ProductController', resourceNames('admin.product'));
//    Route::get('product/create_product', 'ProductController@showCreate')->name('admin.product.create_product');

    // News
    Route::get('news/get-data', 'NewsController@getData')->name('admin.news.get-data');
    Route::post('news/{auid}/active', 'NewsController@active')->name('admin.news.active');
    Route::post('news/{auid}/inactive', 'NewsController@inactive')->name('admin.news.inactive');
    Route::resource('news', 'NewsController', resourceNames('admin.news'));

    // Q & A
    Route::post('faq/update-faq-main', 'FaqController@updateMainFaq')->name('admin.faq.update-faq-main');
    Route::post('faq/update-faq-sub', 'FaqController@updateSubFaq')->name('admin.faq.update-faq-sub');
    Route::resource('faq', 'FaqController', resourceNames('admin.faq'));

    //Inquiry Management Routes
    Route::get('inquiry-management/get-data', 'InquiryManagementController@getData')->name('admin.inquiry-management.get-data');
    Route::resource('inquiry-management', 'InquiryManagementController', resourceNames('admin.inquiry-management'));

    //Admin Contact Management
    Route::post('web-content/update-contactUs', 'ContactUsController@updateContactUs')->name('admin.web-content.update-contactUs');
    Route::resource('web-content', 'ContactUsController', resourceNames('admin.web-content'));
});
