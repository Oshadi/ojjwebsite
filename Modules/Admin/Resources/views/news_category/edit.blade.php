<div class="modal-header bg-">
    <h5 class="modal-title fw-600">Edit News Category</h5>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<form class="steps-basic form-submit"
      action="{{  route($baseRouteName.'.update', $getData->id) }}" data-fouc="" role="application"
      id="steps-uid-0"
      method="post"
      novalidate="novalidate" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
    <input type="hidden" value="{{$getData->id}}" name="id">
    <div class="modal-body">

        <div class="row mt-3">

            <div class="col-sm-12 collapse show in" id="companyDetails">
                <div class="row">
                    <div class="col-sm-12 form-group">
                        @include('partials.form.input', ['name' => 'name', 'type' => 'text', 'title' => 'Category Name', 'required'=>'true','value'=>$getData->name])
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        @include('partials.form.modal-btn-submit', [ 'title' => 'Update','addRoute'=>''])
    </div>
</form>

<script>

    jQuery(document).ready(function () {


    });

</script>
