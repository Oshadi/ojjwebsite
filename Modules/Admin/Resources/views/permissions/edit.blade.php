<div class="modal-header bg-">
    <h5 class="modal-title fw-600">Edit '{{$permission['name']}}' Permission</h5>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<form class="wizard-form steps-basic wizard clearfix update-form"
      action="{{  route($baseRouteName.'.update', $permission['id']) }}" data-fouc="" role="application"
      id="steps-uid-0"
      method="post"
      novalidate="novalidate" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
    <input type="hidden" value="{{$permission['id']}}" name="id">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                @include('partials.form.input', ['name' => 'name', 'type' => 'text', 'title' => 'Permission Name', 'placeholder' => '', 'icon' => '','required'=>'true','value'=>$permission['name'] ])

                @include('partials.form.select-multiple', ['name' => 'roles[]',  'title' => 'Roles', 'placeholder' => 'Select Roles',  'required'=>true,'values'=>$roles,'value'=>$permission['roles'], 'className'=>''])

                <!--                <div class="form-group has-feedback">
                    <label><b>Roles</b><small class="text-danger">*</small></label>
                    <select
                            style="width:221px;"
                            name="roles[]"
                            data-placeholder="Select Roles"
                            required
                            class="form-control form-control-select2 multiselect-select-all-filtering  {{@$class}}"
                            multiple="multiple" data-fouc placeholder="Select roles">

                        @foreach ($roles as $role)
                            <option value="{{$role['id']}}" @if(in_array($role['name'], $permission['roles'])) selected @endif>{{$role['name']}}</option>
                        @endforeach
                    </select>
                    <span class="permissions-error validation-invalid-label " role="alert"></span>
                </div>-->
            </div>
        </div>
    </div>
    <div class="modal-footer">
        @include('partials.form.modal-btn-submit', [ 'title' => 'Update','addRoute'=>''])
    </div>
</form>
