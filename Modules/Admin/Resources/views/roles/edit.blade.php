<div class="modal-header bg-">
    <h5 class="modal-title fw-600">Update User Role</h5>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<form class="wizard-form steps-basic wizard clearfix update-form"
      action="{{  route($baseRouteName.'.update', $role['id']) }}" data-fouc="" role="application"
      id="steps-uid-0"
      method="post"
      novalidate="novalidate" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
    <input type="hidden" value="{{$role['id']}}" name="id">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                @include('partials.form.input', ['name' => 'name', 'type' => 'text', 'title' => 'Role Name', 'placeholder' => 'ex: Admin, User, Editor...', 'icon' => '','required'=>'true','value'=>$role['name'] ])

                @include('partials.form.select-multiple', ['name' => 'permissions[]',  'title' => 'Permissions', 'placeholder' => 'Select Permissions',  'required'=>true,'values'=>$permissions,'value'=>$role['permissions'], 'className'=>''])

<!--                <div class="form-group has-feedback">
                    <label><b>Permissions</b><small class="text-danger">*</small></label>
                    <select
                            name="permissions[]"
                            data-placeholder="Select Permissions"
                            required
                            class="form-control form-control-select2 multiselect-select-all-filtering  {{@$class}}"
                            multiple="multiple"  placeholder="Select Permissions">

                        @foreach ($permissions as $permission)
                            <option value="{{$permission->id}}" @if(in_array($permission->name, $role['permissions'])) selected @endif>{{$permission->name}}</option>
                        @endforeach
                    </select>
                    <span class="permissions-error validation-invalid-label " role="alert"></span>
                </div>-->
            </div>
        </div>
    </div>
    <div class="modal-footer">
        @include('partials.form.modal-btn-submit', [ 'title' => 'Update','addRoute'=>''])
    </div>
</form>
