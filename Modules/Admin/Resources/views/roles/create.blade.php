<div class="modal-content">
    <div class="modal-header bg-">
        <h5 class="modal-title fw-600">Add New Role</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <form class="wizard-form steps-basic wizard clearfix input-form"
          action="{{  route($baseRouteName.'.store') }}" data-fouc="" role="application"
          id="steps-uid-0"
          method="post"
          novalidate="novalidate" enctype="multipart/form-data">
        {{ method_field('POST') }}
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    @include('partials.form.input', ['name' => 'name', 'type' => 'text', 'title' => 'Role Name', 'placeholder' => 'ex: Admin, User, Editor ...', 'icon' => '','required'=>'true','value'=>old('name')])
                </div>
                <div class="col-md-12">
                    @include('partials.form.select-multiple', ['name' => 'permissions[]',  'title' => 'Permissions', 'placeholder' => 'Select Permissions',  'required'=>true,'values'=>$permissions,'value'=>old('permissions'), 'className'=>''])
                </div>

            </div>
        </div>
        <div class="modal-footer">
            @include('partials.form.modal-btn-submit', [ 'title' => 'Create','addRoute'=>''])
        </div>
    </form>
</div>
