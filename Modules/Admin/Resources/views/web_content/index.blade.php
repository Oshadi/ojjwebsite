@extends('admin::layouts.master')

<style>
    .file-preview .file-drop-zone{
        height: 75%;
    }
    .recommended-color{
        color: red;
        font-weight: bolder;
    }
</style>
@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">

                        <div class="row">

                            <form class="steps-basic form-submit"
                                  action="{{  route($baseRoute.'.update-contactUs') }}"
                                  id="steps-uid-0"
                                  method="post"
                                  novalidate="novalidate" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                @csrf

                                <input name="id" type="hidden" value="{{@$footer->id}}"/>

                                <div class="col-sm-12 contact-details">
                                    <h4 class="f-18 fw-700 secondary-text mt-0 mb-3">Contact Details</h4>

                                    <div class="row">
                                        <div class="col-sm-10 d-flex ">
                                            @include('partials.form.input', ['name' => 'name', 'type' => 'name', 'title' => 'Name', 'required' => 'true', 'value'=>@$contactUs->name])
                                        </div>
                                        <div class="col-sm-10 d-flex align-items-end" >
                                            @include('partials.form.input', ['name' => 'address_line_1', 'type' => 'text', 'rows' => '5', 'required' => 'true', 'title' => 'Address Line 1', 'value'=>@$contactUs->address_line_1])
                                        </div>
                                        <div class="col-sm-10 d-flex align-items-end" >
                                            @include('partials.form.input', ['name' => 'address_line_2', 'type' => 'text', 'rows' => '5', 'required' => 'true', 'title' => 'Line 2', 'value'=>@$contactUs->address_line_2])
                                        </div>
                                        <div class="col-sm-10 d-flex ">
                                            @include('partials.form.input', ['name' => 'email', 'type' => 'email', 'title' => 'Email', 'required' => 'true', 'value'=>@$contactUs->email])
                                        </div>
                                        <div class="col-sm-10 d-flex ">
                                            @include('partials.form.input', ['name' => 'contact', 'type' => 'text', 'title' => 'Customer Service Number', 'required' => 'true', 'value'=>@$contactUs->contact])
                                        </div>
                                        <div class="col-sm-10 d-flex ">
                                            @include('partials.form.input', ['name' => 'fax', 'type' => 'text', 'title' => 'Fax', 'value'=>@$contactUs->fax])
                                        </div>
                                        <div class="col-sm-10 d-flex ">
                                            @include('partials.form.input', ['name' => 'business_content', 'type' => 'text', 'title' => 'Business content','required' => 'true', 'value'=>@$contactUs->business_content])
                                        </div>
                                        <div class="col-sm-10 d-flex ">
                                            @include('partials.form.input', ['name' => 'id', 'type' => 'hidden','title' => '', 'value'=>@$contactUs->id])
                                        </div>
                                    </div>

                                    <hr>
                                </div>

                                <div class="col-sm-12 social-media">
                                    <h4 class="f-18 fw-700 secondary-text mt-0 mb-3">Social Media</h4>

                                    <div class="row">


                                        <div class="col-sm-10 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'facebook', 'type' => 'url', 'title' => 'Facebook', 'value'=>@$contactUs->facebook])
                                        </div>

                                        <div class="col-sm-10 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'instagram', 'type' => 'url', 'title' => 'Instagram', 'value'=>@$contactUs->instagram])
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 d-flex justify-content-end align-items-center mt-4">
                                        <div class="btn-group m-1">
                                            <button type="button" class="btn btn-primary form-submit-button"> Update </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
            </div>
        </div>
    </div>

@endsection

<script src="{{ asset('global_assets/js/main/jquery.min.js')}}"></script>
<script src="{{ asset('global_assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('custom_assets/backend/js/custom-file-upload-script.js') }}" defer></script>


<script>
    window.shiftTypeCounter = 1;
    window.partnerDetailsCount = {{ (isset($ourPartner->partners)) ? $ourPartner->partners->count() : 1-1}};

    $(document).on('click', ".question-add", function () {
        duplicate();
    });

    function duplicate() {
        //var duplicable = $(this).parent().parent().parent();
        var duplicable = $('.original');
        var container = duplicable.parent();
        var clone = duplicable.clone().removeClass("original");

        var newId = window.shiftTypeCounter++;

        clone.find("span").remove();

        clone.find(".origin-remove").removeClass("d-none");
        clone.find(".question-add").addClass("d-none");

        clone.find(".text-area").val('');

        clone.find("input:text").each(function () {
            $(this).val('');
        });
        clone.find("textarea:text").each(function () {
            $(this).val('');
        });


        $(container).append(clone);
        /*$("select").select2();
        $(".form-control-select2").select2();

        var input = $('.clockpicker');
        input.clockpicker({
            autoclose: true,
            twelvehour: false,
            placement: 'top',
        });*/
    }


    // PARTNERS DUPLICATE
    $(document).on('click', ".partner-add", function () {
        duplicatePartner();
    });

    function duplicatePartner() {
        var newId = window.partnerDetailsCount+1;

        //var duplicable = $(this).parent().parent().parent();
        var duplicable = $('.original-partner');
        var container = duplicable.parent();
        var clone = duplicable.clone().removeClass("original-partner");

        clone.find("span").remove();

        clone.find(".origin-remove").removeClass("d-none");
        clone.find(".question-add").addClass("d-none");

        clone.find(".details-text-area").val('');
        clone.find(".details-text-area").attr("id", 'details.'+newId);

        clone.find("input:text").each(function () {
            $(this).val('');
        });
        clone.find("textarea:text").each(function () {
            $(this).val('');
        });

        clone.find("select").each(function () {
            //console.log(newId);
            $(this).attr("id", newId);
            $(this).val('');
        });

        $(container).append(clone);
        $("select").select2();
        $(".form-control-select2").select2();

        /*var input = $('.clockpicker');
         input.clockpicker({
         autoclose: true,
         twelvehour: false,
         placement: 'top',
         });*/
    }

    $(document).on('click', ".remove-faq", function () {
        //$(this).parent().parent().parent().parent().remove();
        $(this).parent().parent().remove();
    });

    // RETAILERS DUPLICATE
    $(document).on('click', ".retailer-add", function () {
        duplicateRetailer();
    });

    function duplicateRetailer() {
        var newId = window.partnerDetailsCount+1;

        //var duplicable = $(this).parent().parent().parent();
        var duplicable = $('.original-retailer');
        var container = duplicable.parent();
        var clone = duplicable.clone().removeClass("original-retailer");

        clone.find("span").remove();

        clone.find(".origin-remove").removeClass("d-none");
        clone.find(".question-add").addClass("d-none");

        clone.find(".details-text-area").val('');
        clone.find(".details-text-area").attr("id", 'details.'+newId);

        clone.find("input:text").each(function () {
            $(this).val('');
        });
        clone.find("textarea:text").each(function () {
            $(this).val('');
        });

        clone.find("select").each(function () {
            //console.log(newId);
            $(this).attr("id", newId);
            $(this).val('');
        });

        $(container).append(clone);
        $("select").select2();
        $(".form-control-select2").select2();

        /*var input = $('.clockpicker');
         input.clockpicker({
         autoclose: true,
         twelvehour: false,
         placement: 'top',
         });*/
    }

    $(document).on('click', ".remove-faq", function () {
        //$(this).parent().parent().parent().parent().remove();
        $(this).parent().parent().remove();
    });


</script>
