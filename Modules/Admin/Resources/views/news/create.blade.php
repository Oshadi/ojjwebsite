<div class="modal-content">
    <div class="modal-header bg-">
        <h5 class="modal-title fw-600">Add News</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <form class="steps-basic form-submit"
          action="{{  route($baseRouteName.'.store') }}" data-fouc=""
          id="steps-uid-0"
          method="post"
          novalidate="novalidate" enctype="multipart/form-data">
        {{ method_field('POST') }}
        @csrf
        <div class="modal-body">

            <div class="row mt-3">

                <div class="col-sm-12 collapse show in" id="companyDetails">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            @include('partials.form.select', ['name' => 'category_id',  'title' => 'News Category', 'placeholder' => 'select News Category', 'required'=>'true','values'=>$newsCategories,'value'=>old('category_id')])
                            @include('partials.form.input', ['name' => 'title', 'type' => 'text', 'title' => 'News Title', 'required'=>'true','value'=>old('title')])

                                <div class="form-group">
                                    <label for="logo">Main Image</label>
                                    <input type="file" class="form-control-file" id="main_image" name="main_image" accept="image/png, image/jpeg" >
                                    <span class="validation-invalid-label  main_image-error" role="alert"></span>
                                </div>

                        </div>

                        <div class="col-sm-6 form-group">
                            @include('partials.form.input', ['name' => 'added_date', 'type' => 'date', 'title' => 'Added Date', 'required'=>'true','value'=>date('Y-m-d')])
                            @include('partials.form.textarea', ['name' => 'description', 'type' => 'test', 'rows' => '5', 'title' => 'Description', 'required'=>'true','value'=>old('description')])

                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="custom-hr"></div>
                </div>

                <div class="col-sm-12">
                    <div class="row">
                    <div class="col-md-12 mt-3 duplicate-container original-partner">
                        <div class="row duplicater original-news-div">
                            <div class="col-sm-10 bg-color pt-2 border-4">
                                <div class="row">
                                    <div class="col-sm-8 d-flex align-items-end">
                                        @include('partials.form.textarea', ['name' => 'details[]', 'type' => 'text', 'title' => 'Description', 'class' => 'details-text-area', 'required' => '', 'rows' => '5', 'value'=>old('description')])
                                    </div>

                                    <div class="col-sm-4 align-items-end">
                                        <div class="form-group d-none image-area">
                                            <label for="logo">Add Image Here</label>
                                            <input type="file" class="form-control-file" id="images" name="images[]" accept="image/png, image/jpeg" >
                                            <span class="validation-invalid-label  images-error" role="alert"></span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <!--<div class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                                <button type="button" class="success-circle-btn partner-add"><i class="icon-plus3"></i></button>
                            </div>-->
                            <div class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                                <button type="button" class="danger-circle-btn remove-faq d-none origin-remove"><i class="icon-trash-alt"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 mt-3">
                    <div class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                        <button type="button" class="btn btn-success partner-add">Add New </button>
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            @include('partials.form.modal-btn-submit', [ 'title' => 'Save','addRoute'=>''])
        </div>
    </form>
</div>

<script src="{{ asset('global_assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('custom_assets/backend/js/custom-file-upload-script.js') }}" defer></script>

<script>

    jQuery(document).ready(function () {

        $(document).on('change', '.game-cover-uploader :file', function () {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $(document).on('fileselect', '.game-cover-uploader :file', function (event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }

        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#gameCoverPreview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);

            }
        }

        $(document).on('change', '#gameCoverName', function () {
            //$("#gameCoverName").change(function(){
            readURL(this);
        });
    });

    // PARTNERS DUPLICATE
    $(document).on('click', ".partner-add", function () {
        duplicatePartner();
    });

    function duplicatePartner() {
        var newId = $('.original-news-div').length ++;

        //var duplicable = $(this).parent().parent().parent();
        var duplicable = $('.original-partner');
        var container = duplicable.parent();
        var clone = duplicable.clone().removeClass("original-partner");

        clone.find("span").remove();

        clone.find(".origin-remove").removeClass("d-none");
        clone.find(".question-add").addClass("d-none");

        clone.find(".details-text-area").val('');
        clone.find(".details-text-area").attr("id", 'details.'+newId);
        //clone.find(".images").attr("id", 'images.'+newId);
        clone.find(".image-area").addClass("d-none")

        clone.find("input:text").each(function () {
            $(this).val('');
        });
        clone.find("textarea:text").each(function () {
            $(this).val('');
        });
        clone.find("input:file").each(function () {
            $(this).val(null);
        });
        clone.find("select").each(function () {
            //console.log(newId);
            $(this).attr("id", newId);
            $(this).val('');
        });

        $(container).append(clone);
        $("select").select2();
        $(".form-control-select2").select2();
    }

    $(document).on('click', ".remove-faq", function () {
        //$(this).parent().parent().parent().parent().remove();
        $(this).parent().parent().remove();
    });

    // Description and image validation
    $(document).on('keyup', ".details-text-area", function () {
        if($(this).val() !== ''){
            $(this).parent().parent().parent().find(".image-area").removeClass("d-none")
        }else{
            $(this).parent().parent().parent().find(".image-area").addClass("d-none")
        }
    });

</script>
