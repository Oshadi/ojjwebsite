<div class="modal-header bg-">
    <h5 class="modal-title fw-600">Edit News</h5>
    <button type="button" class="close access-multiple-clear" data-dismiss="modal">&times;</button>
</div>

<form class="steps-basic form-submit"
      action="{{  route($baseRouteName.'.update', $getData->id) }}" data-fouc=""
      id="steps-uid-0"
      method="post"
      novalidate="novalidate" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
    <input type="hidden" value="{{$getData->id}}" name="id">
    <div class="modal-body">

        <div class="row mt-3">

            <div class="col-sm-12 collapse show in" id="companyDetails">
                <div class="row">
                    <div class="col-sm-6 form-group">
                        @include('partials.form.select', ['name' => 'category_id',  'title' => 'News Category', 'placeholder' => 'select News Category', 'required'=>'true','values'=>$newsCategories,'value'=>$getData->category_id])
                        @include('partials.form.input', ['name' => 'title', 'type' => 'text', 'title' => 'News Title', 'required'=>'true','value'=>$getData->title])

                        <div id="preview-image" class="col-md-12">
                            <div class="form-group game-cover-upload-content">
                                <label><b>Upload Main Image</b></label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <span class="game-cover-uploader">
                                            Main Image
                                            <input type="file" data-target="gameCoverPreview" class="gameCoverName"
                                                   name="main_image" value="{{ $getData->main_image }}">
                                        </span>
                                    </span>
                                    <input type="text" class="form-control game-cover-upload-name" readonly>
                                </div>
                                <img id='gameCoverPreview' class="gameCoverPreview" src="{{ $getData->image_path }}"/>
                                <span class="validation-invalid-label  main_image-error" role="alert"></span>
                                {{-- <small class="form-text text-muted">The cover image is used whenever BoomGames wants to link to your project from another part of the site. Required (Minimum *** x ***, Recommended *** x ***)</small> --}}
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-6 form-group">
                        @include('partials.form.input', ['name' => 'added_date', 'type' => 'date', 'title' => 'Added Date', 'required'=>'true','value'=>$getData->added_date])
                        @include('partials.form.textarea', ['name' => 'description', 'type' => 'test', 'rows' => '5', 'title' => 'Description', 'required'=>'true','value'=>$getData->description])

                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="custom-hr"></div>
            </div>

            <div class="col-sm-12">

{{--                @if( isset($getData->newsImages) && $getData->newsImages->count() > 0)--}}
                    <div class="row">

                        @foreach( $getData->newsImages as $k=> $newsImage )

                            <div class="col-md-12 mt-3 duplicate-container @if($k == 0) original-partner-X @endif">
                                <div class="row duplicater original-news-div">
                                    <div class="col-sm-10 bg-color pt-2 border-4">
                                        <div class="row">
                                            <div class="col-sm-8 d-flex align-items-end">
                                                @include('partials.form.textarea', ['name' => 'details[]', 'type' => 'text', 'title' => 'Description', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>$newsImage->description])
                                            </div>

                                            <input type="hidden" name="image_id[]" value="{{ $newsImage->id }}">
                                            <div class="col-sm-4 align-items-end">
                                                <div id="preview-image" class="col-md-12">
                                                    <div class="form-group game-cover-upload-content">
                                                        <label><b>Upload Sub Image</b></label>
                                                        <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <span class="game-cover-uploader"> Sub Image
                                                                <input type="file" data-target="gameCoverPreview-{{$k}}" class="gameCoverName"
                                                                       name="images[]">
                                                            </span>
                                                        </span>
                                                            <input type="text" class="form-control game-cover-upload-name" readonly>
                                                        </div>
                                                        <img id='gameCoverPreview-{{$k}}' class="gameCoverPreview" src="{{ $newsImage->image_path }}"/>
                                                        <span class="validation-invalid-label  images-error" role="alert"></span>
                                                        {{-- <small class="form-text text-muted">The cover image is used whenever BoomGames wants to link to your project from another part of the site. Required (Minimum *** x ***, Recommended *** x ***)</small> --}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                                        <button type="button"
                                                class="danger-circle-btn remove-faq origin-remove "> {{--@if($k == 0) d-none @endif--}}
                                            <i class="icon-trash-alt"></i></button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
{{--                @else--}}

                    <div class="row">
                        <div class="col-md-12 mt-3 duplicate-container original-partner">
                            <div class="row duplicater original-news-div">
                                <div class="col-sm-10 bg-color pt-2 border-4">
                                    <div class="row">
                                        <div class="col-sm-8 d-flex align-items-end">
                                            @include('partials.form.textarea', ['name' => 'details[]', 'type' => 'text', 'title' => 'Description', 'class' => 'details-text-area', 'required' => '', 'rows' => '5', 'value'=>old('description')])
                                        </div>

                                        <div class="col-sm-4 align-items-end">
                                            <div class="form-group d-none image-area">
                                                <label for="logo">Add Image Here</label>
                                                <input type="file" class="form-control-file" id="images" name="images[]" accept="image/png, image/jpeg" >
                                                <span class="validation-invalid-label  images-error" role="alert"></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                                    <button type="button" class="danger-circle-btn remove-faq d-none origin-remove"><i
                                            class="icon-trash-alt"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
{{--                @endif--}}

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mt-3">
                <div class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                    <button type="button" class="btn btn-success partner-add">Add New </button>
                </div>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        @include('partials.form.modal-btn-submit', [ 'title' => 'Update','addRoute'=>''])
    </div>
</form>

<script src="{{ asset('global_assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('custom_assets/backend/js/custom-file-upload-script.js') }}"
        defer></script>

<script>

    window.partnerDetailsCount = {{ (isset($getData->newsImages)) ? $getData->newsImages->count() : 1-1}};

    jQuery(document).ready(function () {

        $(document).on('change', '.game-cover-uploader :file', function () {
            var input = $(this),
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [label]);
        });

        $(document).on('fileselect', '.game-cover-uploader :file', function (event, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = label;

            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var target = input.getAttribute("data-target");

                reader.onload = function (e) {
                    //$('#gameCoverPreview').attr('src', e.target.result);
                    $('#' + target).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '.gameCoverName', function () {
            //$("#gameCoverName").change(function(){
            readURL(this);
        });
    });

    // PARTNERS DUPLICATE
    $(document).on('click', ".partner-add", function () {
        duplicatePartner();
    });

    function duplicatePartner() {
        var newId = $('.original-news-div').length ++;

        //var duplicable = $(this).parent().parent().parent();
        var duplicable = $('.original-partner');
        var container = duplicable.parent();
        var clone = duplicable.clone().removeClass("original-partner");

        clone.find("span").remove();

        clone.find(".origin-remove").removeClass("d-none");
        clone.find(".question-add").addClass("d-none");

        clone.find(".details-text-area").val('');
        clone.find(".details-text-area").attr("id", 'details.' + newId);
        //clone.find(".images").attr("id", 'images.'+newId);
        clone.find(".image-area").addClass("d-none")

        clone.find("input:text").each(function () {
            $(this).val('');
        });
        clone.find("input:file").each(function () {
            $(this).val('');
        });

        clone.find("textarea:text").each(function () {
            $(this).val('');
        });

        clone.find("select").each(function () {
            $(this).attr("id", newId);
            $(this).val('');
        });

        $(container).append(clone);
        $("select").select2();
        $(".form-control-select2").select2();

    }

    $(document).on('click', ".remove-faq", function () {
        //$(this).parent().parent().parent().parent().remove();
        $(this).parent().parent().remove();
    });

    $(document).on('click', ".access-multiple-clear", function () {
        location.reload();
    });

    // Description and image validation
    $(document).on('keyup', ".details-text-area", function () {
        if($(this).val() !== ''){
            $(this).parent().parent().parent().find(".image-area").removeClass("d-none")
        }else{
            $(this).parent().parent().parent().find(".image-area").addClass("d-none")
        }
    });

</script>
