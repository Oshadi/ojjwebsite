 <div class="modal-content">
    <div class="modal-header bg-">
        <h5 class="modal-title fw-600">Add New News Category</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <form class="steps-basic form-submit"
          action="{{  route($baseRouteName.'.store') }}" data-fouc="" role="application"
          id="steps-uid-0"
          method="post"
          novalidate="novalidate" enctype="multipart/form-data">
        {{ method_field('POST') }}
        @csrf
        <div class="modal-body">

            <div class="row mt-3">

                <div class="col-sm-12 collapse show in" id="companyDetails">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            @include('partials.form.input', ['name' => 'name', 'type' => 'text', 'title' => 'Category Name', 'required'=>'true','value'=>old('name')])
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="custom-hr"></div>
                </div>
            </div>


        </div>
        <div class="modal-footer">
            @include('partials.form.modal-btn-submit', [ 'title' => 'Save','addRoute'=>''])
        </div>
    </form>
</div>

<script>

    jQuery(document).ready(function () {


    });

</script>
