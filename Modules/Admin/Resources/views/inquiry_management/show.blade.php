<div class="modal-content">
    <div class="modal-header bg-">
        <h5 class="modal-title fw-600">Inquiry Details</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
        {{ method_field('PATCH') }}
        <input type="hidden" value="{{$inquiryDetails->id}}" name="id">
        @csrf
        <div class="modal-body">

            <div class="row mt-3">

                <div class="col-sm-12 collapse show in">
                    <div class="row">
                        <div class="col-sm-12 form-group">
                            @include('partials.form.input', ['name' => 'name', 'type' => 'text', 'title' => 'Name', 'placeholder' => '', 'icon' => '','value' => $inquiryDetails->name, 'readonly' => true])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 form-group">
                            @include('partials.form.input', ['name' => 'furigana', 'type' => 'text', 'title' => 'Furigana', 'placeholder' => '', 'icon' => '','value' => $inquiryDetails->furigana, 'readonly' => true])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 form-group">
                            @include('partials.form.input', ['name' => 'company_organization_name', 'type' => 'text', 'title' => 'Company Organization Name', 'placeholder' => '', 'icon' => '','value' => $inquiryDetails->company_organization_name, 'readonly' => true])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 form-group">
                            @include('partials.form.input', ['name' => 'email', 'type' => 'text', 'title' => 'E-mail', 'placeholder' => '', 'icon' => '','value' => $inquiryDetails->email, 'readonly' => true])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 form-group">
                            @include('partials.form.input', ['name' => 'telephone', 'type' => 'text', 'title' => 'Telephone', 'placeholder' => '', 'icon' => '','value' => $inquiryDetails->telephone, 'readonly' => true])
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 form-group">
                            <label>
                                <b>Inquiry Type</b>
                            </label>
                            <select type="text"
                                    data-placeholder="Inquiry Type"
                                    data-select2-id="inquiry_type_id" class="form-control readonly form-control-select2  industry-is-invalid select-clear acc-type-class"
                                    name="inquiry_type_id" id="inquiry_type_id" disabled>
                                <option value="" disabled selected>Inquiry Type</option>
                                @foreach($inquiryTypes as $list)
                                    <option value="{{$list->id}}" {{($list->id == $inquiryDetails->inquiry_type_id ) ? "selected" : ""}}>{{$list->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 form-group">
                            @include('partials.form.textarea', ['name' => 'contents_of_inquiry', 'type' => 'text', 'title' => 'Contents Of Inquiry', 'placeholder' => '', 'icon' => '','value' => $inquiryDetails->contents_of_inquiry, 'readonly' => true])
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="custom-hr"></div>
                </div>
            </div>

        </div>
</div>
<script>

    jQuery(document).ready(function () {


    });

</script>
