<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @php
        header("Access-Control-Allow-Origin: *");
    @endphp
    <title>{{ config('app.name', 'Laravel') }} | Admin Portal</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('global_assets/assets/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

<!-- Custom Stylesheets -->
    <link href="{{ asset('custom_assets/backend/css/custom-backend.css')}}" rel="stylesheet" type="text/css">
    <link href="{{ asset('custom_assets/backend/css/custom-backend-responsive.css')}}" rel="stylesheet" type="text/css">
    <!--/ Custom Stylesheets -->

{{--    <link href="{{ asset('global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">--}}
{{--    <link href="{{ asset('global_assets/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">--}}
{{--    <link href="{{ asset('global_assets/assets/css/bootstrap_limitless.min.css') }}" rel="stylesheet" type="text/css">--}}
{{--    <link href="{{ asset('global_assets/assets/css/layout.min.css') }}" rel="stylesheet" type="text/css">--}}
{{--    <link href="{{ asset('global_assets/assets/css/components.min.css') }}" rel="stylesheet" type="text/css">--}}








{{--    <link href="{{ asset('global_assets/assets/css/components.min.css') }}" rel="stylesheet" type="text/css">--}}
    <!-- Core JS files -->
    <script src="{{ asset('global_assets/js/main/jquery.min.js')}}"></script>
    <script src="{{ asset('global_assets/js/main/bootstrap.bundle.min.js')}}" defer></script>
    <script src="{{ asset('global_assets/js/plugins/loaders/blockui.min.js') }}" defer></script>
    <script src="{{ asset('global_assets/js/plugins/forms/wizards/steps.min.js') }}"></script>

    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="{{ asset('global_assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script src="{{ asset('global_assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script src="{{ asset('global_assets/js/plugins/pickers/daterangepicker.js')}}"></script>
    <script src="{{ asset('global_assets/js/plugins/visualization/c3/c3.min.js')}}"></script>

    <script src="{{ asset('global_assets/assets/js/app.js')}}"></script>
    <script src="{{ asset('global_assets/js/demo_charts/c3/c3_lines_areas.js')}}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/dashboard.js')}}"></script>
    <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/streamgraph.js')}}"></script>
{{--    <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/sparklines.js')}}"></script>--}}
{{--    <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/lines.js')}}"></script>--}}
{{--    <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/areas.js')}}"></script>--}}
{{--    <script src="{{ asset('global_assets/js/demo_charts/pages/dashboard/light/donuts.js')}}"></script>--}}
    <script src="{{ asset('global_assets/js/plugins/extensions/cookie.js') }}" defer></script>
    <script src="{{ asset('global_assets/js/plugins/ui/moment/moment.min.js') }}" defer></script>
    <script src="{{ asset('global_assets/js/plugins/pickers/daterangepicker.js') }}" defer></script>
    <script src="{{ asset('global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>

    <script src="{{ asset('global_assets/js/plugins/extensions/jquery_ui/interactions.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/form_select2.js') }}"></script>



    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('js/backend-validation.js') }}" defer></script>
    <script type="text/javascript" src="{{ asset('js/custom.js') }}" defer></script>
    <script type="text/javascript" src="{{ asset('global_assets/demo_data/color_picker/bootstrap-clockpicker.min.js')}}"></script>
    <script type="text/javascript"
            src="{{ asset('global_assets/demo_data/color_picker/bootstrap-clockpicker.min.js')}}"></script>

    {{--    --}}{{--  Choices JS  --}}
    {{--    <script src="{{ asset('custom_assets/libraries/choices/choices.min.js') }}"></script>--}}

    <script src="{{ asset('global_assets/js/sweetalert.min.js') }}"></script>


    <script src="{{asset('global_assets/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="{{ asset('global_assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script src="{{asset('global_assets/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>
    <script src="{{ asset('global_assets/js/demo_pages/components_media.js')}}"></script>
    {{--    <!-- Custom Scripts -->--}}
    <script type="text/javascript" src="{{ asset('custom_assets/backend/js/custom-backend-script.js') }}"></script>
    {{--    <!-- Custom Scripts -->--}}

    {{--THEME JS FILES--}}
    @yield('theme_js')
    {{--ENDS THEME JS FILES--}}

    @if(isset($getData))
        <span class="get-data-holder" data-url="{{route($getData['url'])}}" data-holder="{{$getData['holder']}}"></span>
    @endif
</head>

{{--@include('admin::layouts.main-css')--}}

<body>

{{--  Pre-Loader  --}}
<div class="preloader">
    <div class="preloader-container">
        <span class="animated-preloader"></span>
    </div>
</div>

@include('admin::common.header')

<!-- Page content -->
<div class="page-content">

@include('admin::common.sidebar')

<!-- Main content -->
    <div class="content-wrapper content-wrapper-custom">

        <!-- Inner content -->
        <div class="content-inner">


        @include('admin::common.page-header')
        {{--@include('layouts.common_function')--}}

        <!-- Content area -->
            <div class="content">
                @yield('content')
            </div>
            <!-- /content area -->

            @include('admin::common.footer')

        </div>
        <!-- /inner content -->
    </div>
    <!-- /Main content -->


</div>


{{--@yield('content')--}}

{{--@include('admin::layouts.main-js')--}}
@yield('top_nav_bar')

@stack('scripts')

@yield('custom_js')

@yield('vue_js')
</body>
</html>
