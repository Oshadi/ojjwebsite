<div class="modal-content">
    <div class="modal-header bg-">
        <h5 class="modal-title fw-600">Add New User</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>
    <form class="wizard-form steps-basic wizard clearfix input-form"
          action="{{  route($baseRouteName.'.store') }}" data-fouc="" role="application"
          id="steps-uid-0"
          method="post"
          novalidate="novalidate" enctype="multipart/form-data">
        {{ method_field('POST') }}
        @csrf
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    @include('partials.form.input', ['name' => 'first_name', 'type' => 'text', 'title' => 'First Name','required'=>'true','value'=>old('first_name')])
                    @include('partials.form.input', ['name' => 'email', 'type' => 'email', 'title' => 'Email','required'=>'true','value'=>old('email')])
                    @include('partials.form.input', ['name' => 'user_name', 'type' => 'test', 'title' => 'User Name','required'=>'true','value'=>old('user_name')])
                    @include('partials.form.input', ['name' => 'password', 'type' => 'password', 'title' => 'Password','required'=>'true','value'=>''])
                </div>

                <div class="col-md-6">
                    @include('partials.form.input', ['name' => 'last_name', 'type' => 'text', 'title' => 'Last Name','required'=>'true','value'=>old('last_name')])
                    @include('partials.form.input', ['name' => 'mobile', 'type' => 'text', 'title' => 'Mobile','required'=>'true','value'=>old('mobile')])
                    @include('partials.form.select', ['name' => 'role',  'title' => 'User Role', 'required'=>true,'values'=>$roles,'value'=>old('roles'), 'className'=>''])
                    @include('partials.form.input', ['name' => 'conform_password', 'type' => 'password', 'title' => 'Conform Password','required'=>'true','value'=>''])
                </div>
            </div>
        </div>
        <div class="modal-footer">
            @include('partials.form.modal-btn-submit', [ 'title' => 'Create','addRoute'=>''])
        </div>
    </form>
</div>
