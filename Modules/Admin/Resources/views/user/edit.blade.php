<div class="modal-header bg-">
    <h5 class="modal-title fw-600">Edit User</h5>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<form class="wizard-form steps-basic wizard clearfix update-form"
      action="{{  route($baseRouteName.'.update', $user->id) }}" data-fouc="" role="application"
      id="steps-uid-0"
      method="post"
      novalidate="novalidate" enctype="multipart/form-data">
    {{ method_field('PATCH') }}
    @csrf
    <input type="hidden" value="{{ $user->id }}" name="id">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                @include('partials.form.input', ['name' => 'first_name', 'type' => 'text', 'title' => 'First Name','required'=>'true','value'=>$user->first_name ])
                @include('partials.form.input', ['name' => 'email', 'type' => 'email', 'title' => 'Email','required'=>'true','value'=>$user->email])
                @include('partials.form.input', ['name' => 'user_name', 'type' => 'test', 'title' => 'User Name','required'=>'true','value'=>$user->user_name])
            </div>


            <div class="col-md-6">
                @include('partials.form.input', ['name' => 'last_name', 'type' => 'text', 'title' => 'Last Name','required'=>'true','value'=>$user->last_name])
                @include('partials.form.input', ['name' => 'mobile', 'type' => 'text', 'title' => 'Mobile','required'=>'true','value'=>$user->mobile])
                @include('partials.form.select', ['name' => 'role',  'title' => 'User Role', 'required'=>true,'values'=>$roles,'value'=>$user->roles()->pluck('id')->first(), 'className'=>''])
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="checkbox checkbox-circle checkbox-info peers ai-c mB-15">
                    <input type="checkbox" id="passwordUpdate" name="passwordUpdate" class="peer">
                    <label for="update-password" class=" peers peer-greed js-sb ai-c">
                        <span class="peer peer-greed">Update Password</span>
                    </label>
                </div>
            </div>
            <div class="col-md-6 password-div">
                @include('partials.form.input', ['name' => 'password', 'type' => 'password', 'title' => 'Password','required'=>'true','class'=>'password-inputs'])
            </div>
            <div class="col-md-6 password-div">
                @include('partials.form.input', ['name' => 'conform_password', 'type' => 'password', 'title' => 'Conform Password','required'=>'true','class'=>'password-inputs'])
            </div>
        </div>
    </div>
    <div class="modal-footer">
        @include('partials.form.modal-btn-submit', [ 'title' => 'Update','addRoute'=>''])
    </div>
</form>

<script>
    $(document).ready(function () {
        $(".password-div").hide();
        $(".password-inputs").attr('disabled', true);

        $(document).on('change', '#passwordUpdate', function () {

            if ($(this).is(':checked')) {
                $(".password-div").show();
                $(".password-inputs").attr('disabled', false);
            } else {
                $(".password-div").hide();
                $(".password-inputs").attr('disabled', true);
            }
        })

    });
</script>
