<!-- Dashboard Card -->
<div class="card">
    <div class="card-body">
        <div class="row">

            <div class="col-sm-6 orders-card mb-3">
                <div class="orders-card-inner border-1-solid">
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center align-items-center bg-color">
                            <i class="order-card-icon icon-gift"></i>
                        </div>
                        <div class="col-8 py-2">
                            <h4 class="order-card-text">Active Products</h4>
                            <h2 class="f-28 fw-600 m-0 gray-text"> {{ $totalProducts ?? '0' }}</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 orders-card mb-3">
                <div class="orders-card-inner border-1-solid">
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center align-items-center bg-color">
                            <i class="order-card-icon icon-list2"></i>
                        </div>
                        <div class="col-8 py-2">
                            <h4 class="order-card-text">Active News Category</h4>
                            <h2 class="f-28 fw-600 m-0 gray-text"> {{ $totalNewsCategory ?? '0' }}</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 orders-card mb-3">
                <div class="orders-card-inner border-1-solid">
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center align-items-center bg-color">
                            <i class="order-card-icon icon-newspaper"></i>
                        </div>
                        <div class="col-8 py-2">
                            <h4 class="order-card-text">Active News</h4>
                            <h2 class="f-28 fw-600 m-0 gray-text"> {{ $totalNews ?? '0' }}</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 orders-card mb-3">
                <div class="orders-card-inner border-1-solid">
                    <div class="row">
                        <div class="col-4 d-flex justify-content-center align-items-center bg-color">
                            <i class="order-card-icon icon-question4"></i>
                        </div>
                        <div class="col-8 py-2">
                            <h4 class="order-card-text">Total Inquires</h4>
                            <h2 class="f-28 fw-600 m-0 gray-text"> {{ $totalInquires ?? '0' }}</h2>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


