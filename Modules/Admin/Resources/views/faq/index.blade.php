@extends('admin::layouts.master')

@section('content')

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">

                <ul class="nav nav-pills">
                    <li class="nav-item"><a href="#main-question" class="nav-link active" data-toggle="tab">Frequently Asked Questions</a></li>
                    <li class="nav-item"><a href="#sub-question" class="nav-link" data-toggle="tab">Inquiries Questions</a></li>
                </ul>

                <div class="tab-content">

                    <div class="tab-pane fade show active" id="main-question">
                        <div class="col-sm-12">
                            <form class="steps-basic form-submit"
                                  action="{{  route($baseRoute.'.update-faq-main') }}"
                                  id="steps-uid-0"
                                  method="post"
                                  novalidate="novalidate" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                @csrf

                                <h3> Frequently Asked Questions </h3>

                                @if( isset($mainFaqs) && $mainFaqs->count() > 0)
                                    <div class="row">

                                        @foreach( $mainFaqs as $k=> $qus )

                                            <div class="col-md-12 mt-3 duplicate-container @if($k == 0) original @endif">
                                                <div class="row duplicater main-question">
                                                    <div class="col-sm-10 bg-color pt-2 border-4">
                                                        <div class="row">
                                                            <div class="col-sm-6 d-flex align-items-end">
                                                                @include('partials.form.textarea', ['name' => 'question[]', 'id' => 'question.'.$k, 'type' => 'text', 'title' => 'Question', 'class' => 'text-area question', 'required' => 'true', 'rows' => '3', 'value'=>$qus->question])
                                                            </div>

                                                            <div class="col-sm-6 d-flex align-items-end">
                                                                @include('partials.form.textarea', ['name' => 'answer[]', 'id' => 'answer.'.$k, 'type' => 'text', 'title' => 'Answer', 'class' => 'text-area answer', 'required' => 'true', 'rows' => '3', 'value'=>$qus->answer])
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div
                                                        class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                                                        <button type="button"
                                                                class="danger-circle-btn remove-main-faq origin-remove @if($k == 0) d-none @endif">
                                                            <i class="icon-trash-alt"></i></button>
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @else

                                    <div class="row">
                                        <div class="col-md-12 mt-3 duplicate-container original">
                                            <div class="row duplicater main-question">
                                                <div class="col-sm-10 bg-color pt-2 border-4">
                                                    <div class="row">
                                                        <div class="col-sm-6 d-flex align-items-end">
                                                            @include('partials.form.textarea', ['name' => 'question[]', 'type' => 'text', 'title' => 'Question', 'required' => 'true', 'rows' => '3', 'value'=>old('header')])
                                                        </div>

                                                        <div class="col-sm-6 d-flex align-items-end">
                                                            @include('partials.form.textarea', ['name' => 'answer[]', 'type' => 'text', 'title' => 'Answer',  'required' => 'true', 'rows' => '3', 'value'=>old('description')])
                                                        </div>
                                                    </div>
                                                </div>


                                                <div
                                                    class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                                                    <button type="button"
                                                            class="danger-circle-btn remove-main-faq d-none origin-remove"><i
                                                            class="icon-trash-alt"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <div class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                                            <button type="button" class="btn btn-success question-add">Add New Question </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-12 d-flex justify-content-end align-items-center mt-4">
                                        <div class="btn-group m-1">
                                            <button type="button" class="btn btn-primary form-submit-button"> Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="tab-pane fade " id="sub-question">
                        <div class="col-sm-12">
                            <form class="steps-basic form-submit"
                                  action="{{  route($baseRoute.'.update-faq-sub') }}"
                                  id="steps-uid-0"
                                  method="post"
                                  novalidate="novalidate" enctype="multipart/form-data">
                                {{ method_field('POST') }}
                                @csrf

                                <h3> Inquiries Questions </h3>

                                @if( isset($subFaqs) && $subFaqs->count() > 0)
                                    <div class="row">

                                        @foreach( $subFaqs as $k=> $qus )

                                            <div class="col-md-12 mt-3 duplicate-container @if($k == 0) original-sub-question @endif">
                                                <div class="row duplicater sub-question-div">
                                                    <div class="col-sm-10 bg-color pt-2 border-4">
                                                        <div class="row">
                                                            <div class="col-sm-12 d-flex align-items-end">
                                                                @include('partials.form.input', ['name' => 'question[]', 'id' => 'question.'.$k, 'type' => 'text', 'title' => 'Question', 'class' => 'sub-question', 'required' => 'true', 'value'=>$qus->question])
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div
                                                        class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                                                        <button type="button"
                                                                class="danger-circle-btn remove-sub origin-remove @if($k == 0) d-none @endif">
                                                            <i class="icon-trash-alt"></i></button>
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @else

                                    <div class="row">
                                        <div class="col-md-12 mt-3 duplicate-container original-sub-question">
                                            <div class="row duplicater sub-question-div">
                                                <div class="col-sm-10 bg-color pt-2 border-4">
                                                    <div class="row">
                                                        <div class="col-sm-12 d-flex align-items-end">
                                                            @include('partials.form.input', ['name' => 'question[]', 'type' => 'text', 'title' => 'Question', 'class' => 'sub-question', 'required' => 'true', 'value'=>old('header')])
                                                        </div>
                                                    </div>
                                                </div>

                                                <div
                                                    class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                                                    <button type="button"
                                                            class="danger-circle-btn remove-sub d-none origin-remove"><i
                                                            class="icon-trash-alt"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <div class="col-sm-1 d-flex justify-content-center align-items-center mt-2 mt-sm-0">
                                            <button type="button" class="btn btn-success question-add-sub">Add New Question </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-12 d-flex justify-content-end align-items-center mt-4">
                                        <div class="btn-group m-1">
                                            <button type="button" class="btn btn-primary form-submit-button"> Update</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection

<script src="{{ asset('global_assets/js/main/jquery.min.js')}}"></script>
<script src="{{ asset('global_assets/js/plugins/uploaders/fileinput/fileinput.min.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('custom_assets/backend/js/custom-file-upload-script.js') }}"
        defer></script>


<script>
    window.shiftTypeCounter = 1;
    window.partnerDetailsCount = {{ (isset($ourPartner->partners)) ? $ourPartner->partners->count() : 1-1}};

    $(document).on('click', ".question-add", function () {
        duplicate();
    });

    // ADD MAIN QUESTION
    function duplicate() {
        //var duplicable = $(this).parent().parent().parent();
        var duplicable = $('.original');
        var container = duplicable.parent();
        var clone = duplicable.clone().removeClass("original");

        var newId = $('.main-question').length ++;

        //clone.find("span").remove();
        clone.find(".origin-remove").removeClass("d-none");
        clone.find(".question-add").addClass("d-none");

        clone.find(".text-area").val('');
        clone.find(".question").removeClass("question.0-error").addClass( 'question.' + newId + '-error');
        clone.find(".answer").removeClass("answer.0-error").addClass( 'answer.' + newId + '-error');

        clone.find(".question").attr("id", 'question.' + newId);
        clone.find(".answer").attr("id", 'answer.' + newId);

        clone.find("input:text").each(function () {
            $(this).val('');
        });
        clone.find("textarea:text").each(function () {
            $(this).val('');
        });

        $(container).append(clone);

    }

    // REMOVE MAIN QUESTION
    $(document).on('click', ".remove-main-faq", function () {
        $(this).parent().parent().remove();

        $("span.question").each(function (i, obj) {
            $(this).closest('.question').removeAttr("class").addClass( 'validation-invalid-label text-area question question.' + i + '-error');
        });

        $("span.answer").each(function (i, obj) {
            $(this).closest('.answer').removeAttr("class").addClass( 'validation-invalid-label text-area answer answer.' + i + '-error');
        });
    });

    // SUB QUESTION DUPLICATE
    $(document).on('click', ".question-add-sub", function () {
        duplicateSubQuestion();
    });

    // ADD NEW SUB QUESTION
    function duplicateSubQuestion() {
        var newId = $('.sub-question-div').length ++;

        //var duplicable = $(this).parent().parent().parent();
        var duplicable = $('.original-sub-question');
        var container = duplicable.parent();
        var clone = duplicable.clone().removeClass("original-sub-question");

        //clone.find("span").remove();

        clone.find(".origin-remove").removeClass("d-none");
        clone.find(".question-add").addClass("d-none");

        clone.find(".sub-question").removeClass("question.0-error").addClass( 'question.' + newId + '-error');
        clone.find("input:text").each(function () {
            $(this).val('');
        });
        clone.find("textarea:text").each(function () {
            $(this).val('');
        });

        clone.find("select").each(function () {
            $(this).attr("id", newId);
            $(this).val('');
        });

        $(container).append(clone);
        $("select").select2();
        $(".form-control-select2").select2();

    }

    // REMOVE SUB QUESTION
    $(document).on('click', ".remove-sub", function () {
        $(this).parent().parent().remove();

        $("span.sub-question").each(function (i, obj) {
            $(this).closest('.sub-question').removeAttr("class").addClass( 'validation-invalid-label sub-question question.' + i + '-error');
        });
    });


</script>
