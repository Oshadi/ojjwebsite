<link href="{{asset('css/imageuploadify.min.css')}}" rel="stylesheet">
<link href="{{ asset('template_assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">


<!------ Include the above in your HEAD tag ---------->
<style>
    .clear-both{clear:both;}


    #multiple-file-preview{border-top: 1px solid rgba(0, 0, 0, 0.11); margin-top: 10px; padding: 10px;}

    #sortable {
        list-style-type: none;
        margin: 0;
        padding: 0;
        min-height: 300px;
    }

    #sortable li {
        margin: 3px 3px 3px 0;
        float: left;
        width: 100px;
        text-align: center;
        position: relative;
        background-color: #FFFFFF;
    }

    #sortable li, #sortable li img
    {
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }

    #sortable li div.order-number{
        position: absolute;
        top: 2px;
        right: 2px;
        width: 15px;
        heigth: 15px;
        background-color: #2B91E3;
        color: #FFFFFF;
        font-size: 12px;
        -webkit-border-radius: 20px;
        -moz-border-radius: 20px;
        border-radius: 20px;
    }
    .remove {
        display: block;
        background: #444;
        border: 1px solid darkred;
        color: white;
        text-align: center;
        cursor: pointer;
    }
    .remove:hover {
        background: white;
        color: darkred;
    }
</style>

<div class="modal fade modal-holder-xl show" role="dialog" aria-labelledby="" style="padding-right: 17px; display: block;" aria-modal="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content"><div class="modal-header bg-primary">
                <h6 class="modal-title">Add New Product</h6>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <form class="wizard-form steps-validation wizard clearfix form-submit" accept-charset="UTF-8"
                  action="{{  route($baseRouteName.'.store') }}" data-fouc=""
                  id="steps-uid-0"
                  method="post"
                  novalidate="novalidate" enctype="multipart/form-data" role="application">
                {{ method_field('POST') }}
                @csrf
                {{--            <form method="POST" action="https://kcinc.jp/admin/vehicle-management/vehicles" class="wizard-form wizard clearfix" role="application" id="steps-uid-0">--}}

                <h6>Step 1</h6>
                <fieldset>
                    <div class="row">
                        <div class="col-sm-4 form-group">

                            <div class="form-group">
                                <label>Product Name: <span class="text-danger">*</span></label>
                                <input type="text" name="name" id="name" class="form-control " placeholder="product Name" required>
                                <label id="name-error" class="validation-invalid-label" for="name"></label>
                            </div>
                        </div>

                        <div class="col-sm-4 form-group">
                            <div class="form-group">
                                <label for="logo">Main Image  <span class="text-danger">*</span></label>

                                {{--                                            <input type="file" accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple>--}}

                                <input type="file" class="form-control-file required " id="main_image" name="main_image" accept="image/png, image/jpeg" required >
                                <span class="form-text text-muted" role="alert">Accepted SIZE (Width - Height) px:800 - 600</span>
                            </div>

                        </div>
                        <div class="col-sm-4 form-group">
                            <div class="form-group">
                                <label for="logo">Thumbnail Image  <span class="text-danger">*</span></label>

                                {{--                                            <input type="file" accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" multiple>--}}

                                <input type="file" class="form-control-file required " id="thumbnail_image" name="thumbnail_image" accept="image/png, image/jpeg" required >
                                <span class="form-text text-muted" role="alert">Accepted SIZE (Width - Height) px:800 - 600</span>
                            </div>

                        </div>
                        <div class="col-sm-12 form-group">
                            <label for="inputEmail4" class="form-label mb-0 mr-5">Add Image<span class="text-danger">*</span></label>
                            <input type="file" id="images" name="bootImages[]" multiple="multiple"/>
                            <div id="multiple-file-preview">
                                <ul id="sortable">
                                    <div class="clear-both">

                                    </div>
                                </ul>
                                <span class="form-text text-muted">Accepted SIZE (Width - Height) px:1920 - 500</span>
{{--<input type="text" id="array_id" name="array_id[]">--}}
                            </div>
                        </div>



{{--                            <div class="form-group">--}}

{{--                                <input name="bootImages[]" id="bootImages" type="file" class="file-input" multiple="multiple" data-show-upload="false">--}}
{{--                            </div>--}}
{{--                            <div id="multiple-file-preview">--}}
{{--                                <ul id="sortable">--}}
{{--                                    <div class="clear-both"></div>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
                            {{--                                <div class="d-flex align-center">--}}
                            {{--                                    <label for="inputEmail4" class="form-label mb-0 mr-5">Add Image<sup>*</sup></label>--}}
                            {{--                                    <a href="#" type="button" class="transparent-circle-btn custom-tooltip" data-toggle="tooltip" data-placement="right" title="" data-original-title="Upload MCQ Question Paper."><i class="fa fa-info"></i></a>--}}
                            {{--                                </div>--}}
                            {{--                                <div class="d-flex align-items-center hdtuto control-group lst increment mt-10">--}}
                            {{--                                    <input type="file"  name="bootImages[]" class="myfrm" required>--}}
                            {{--                                    <button type="button" class="success-circle-btn  docUploadCloneBtnNew"><i class="icon-plus3"></i></button>--}}



                            {{--                                </div>--}}
                            {{--                                <div class="clone hide">--}}
                            {{--                                    <div class="d-flex align-items-center hdtuto lst control-group mt-10">--}}
                            {{--                                        <input type="file" name="bootImages[]" class="myfrm">--}}
                            {{--                                        <button class="danger-circle-btn remove-faq docUploadDeleteBtn" type="button"><i class="icon-trash"></i></button>--}}
                            {{--                                    </div>--}}
                            {{--                                </div>--}}



                </fieldset>

                <h6>Step 2</h6>
                <fieldset>

                    <div class="row">
                        <div class="col-md-12 mt-3 duplicate-container original-partner">
                            <h3>（Table 1）</h3>
                            <div class="row duplicater">
                                <div class="col-sm-12  pt-2 border-4">
                                    <div class="row">

                                        <div class="col-sm-6 d-flex align-items-end">
                                            @include('partials.form.input', ['name' =>'table_1_name','id'=>"table_1_name" ,'type' => 'text', 'title' => 'Table 1 Name', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('table_1_name')])
                                        </div>

                                        <div class="col-sm-6 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_1_capacity','id'=>"table_1_capacity" ,'type' => 'text', 'title' => '定員数', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">人</div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_1_tube_diameter','id'=>'table_1_tube_diameter', 'type' => 'text', 'title' => 'チューブ径', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_1_full_length', 'id'=>'table_1_full_length', 'type' => 'text', 'title' => '全長', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_1_weight','id'=>'table_1_weight', 'type' => 'text', 'title' => '重量 ', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">kg</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_1_full_width','id'=>'table_1_full_width', 'type' => 'text', 'title' => '全幅 ', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_1_transom_high','id'=>'table_1_transom_high', 'type' => 'text', 'title' => 'トランサム高', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_1_inner_width','id'=>'table_1_inner_width', 'type' => 'text', 'title' => '内幅', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 align-items-end">
                                            <div class="form-group">
                                                <label for="logo">Add Boat Length Image<span class="text-danger">*</span></label>
                                                <input type="file" class="form-control-file" id="boat_length_image" name="boat_length_image" accept="image/png, image/jpeg" >
                                                <span class="form-text text-muted">Accepted SIZE (Width - Height) px:800 - 600px</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                </fieldset>
                <h6>Step 3</h6>
                <fieldset>

                    <div class="row">
                        <div class="col-md-12 mt-3 duplicate-container original-partner">
                            <h3>（Table 2）</h3>
                            <div class="row duplicater">
                                <div class="col-sm-12 bg-color pt-2 border-4">
                                    <div class="row">

                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_name','id'=>'table_2_name' ,'type' => 'text', 'title' => 'Table 2 Name', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                        </div>

                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_ful_length','id'=>'table_2_ful_length' ,'type' => 'text', 'title' => '全長', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_full_width','id'=>'table_2_full_width', 'type' => 'text', 'title' => '全幅 ', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_overall_height','id'=>'table_2_overall_height', 'type' => 'text', 'title' => '全高', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_weight',  'id'=>'table_2_weight','type' => 'text', 'title' => '重量', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">Kg</div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_transom_high','id'=>'table_2_transom_high', 'type' => 'text', 'title' => 'トランサム高', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_maximum_output','id'=>'table_2_maximum_output', 'type' => 'text', 'title' => '最大出力', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">kW(PS)/r/min</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_recommended_rotation_when_fully_open','id'=>'table_2_recommended_rotation_when_fully_open', 'type' => 'text', 'title' => '全開時推奨回転', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">r/min</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_trolling_speed','id'=>'table_2_trolling_speed', 'type' => 'text', 'title' => 'トローリング回転数', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">r/min</div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_engine_type','id'=>'table_2_engine_type' ,'type' => 'text', 'title' => 'エンジンタイプ', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_cylinder_array','id'=>'table_2_cylinder_array', 'type' => 'text', 'title' => 'シリンダー配列', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_boa', 'id'=>'table_2_boa','type' => 'text', 'title' => 'ボア', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_stroke', 'id'=>'table_2_stroke','type' => 'text', 'title' => 'ストローク', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">mm</div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_total_displacement','id'=>'table_2_total_displacement' , 'type' => 'text', 'title' => '総排気量', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">cm³</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_compression_ratio','id'=>'table_2_compression_ratio', 'type' => 'text', 'title' => '圧縮比 ', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">:1</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_spark_plug', 'id'=>'table_2_spark_plug','type' => 'text', 'title' => 'スパークプラグ', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">NGK</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_ignition_system','id'=>'table_2_ignition_system', 'type' => 'text', 'title' => 'イグニッションシステム', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>

                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_fuel_supply_method','id'=>'table_2_fuel_supply_method', 'type' => 'text', 'title' => ' 燃料供給方式', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_exhaust_system','id'=>'table_2_exhaust_system', 'type' => 'text', 'title' => '排気方式', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_lubrication_method','id'=>'table_2_lubrication_method', 'type' => 'text', 'title' => '潤滑方式', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_starter','id'=>'table_2_starter', 'type' => 'text', 'title' => '始動裝置', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>

                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_chalk_system','id'=>'table_2_chalk_system', 'type' => 'text', 'title' => 'チョークシステム', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_throttle_control','id'=>'table_2_throttle_control', 'type' => 'text', 'title' => 'スロットルコントロール', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_thermostat_valve_opening_temperature','id'=>'table_2_thermostat_valve_opening_temperature', 'type' => 'text', 'title' => 'サーモスタット開弁温度', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">°C</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_fuel_used','id'=>'table_2_fuel_used' , 'type' => 'text', 'title' => '使用燃料', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>

                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_engine_oil','id'=>'table_2_engine_oil', 'type' => 'text', 'title' => 'エンジンオイル', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_engine_oil_grade',  'id'=>'table_2_engine_oil_grade','type' => 'text', 'title' => 'エンジンオイルグレード', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_engine_oil_viscosity', 'id'=>'table_2_engine_oil_viscosity','type' => 'text', 'title' => 'エンジンオイル粘度', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_engine_oil_specified_amount','id'=>'table_2_engine_oil_specified_amount', 'type' => 'text', 'title' => 'エンジンオイル規定量 （オイル交換のみ）', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">L</div>
                                            </div>
                                        </div>

                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_specified_amount_of_engine_oil','id'=>'table_2_specified_amount_of_engine_oil', 'type' => 'text', 'title' => 'エンジンオイル規定量 （オイルフィルター交換時', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                <div class="input-group-text">L</div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_jet_pump_grease','id'=>'table_2_jet_pump_grease', 'type' => 'text', 'title' => 'JETポンプグリス', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_clutch_type','id'=>'table_2_clutch_type', 'type' => 'text', 'title' => 'クラッチ形式', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_clutch_operation_method','id'=>'table_2_clutch_operation_method', 'type' => 'text', 'title' => 'クラッチ操作方式', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>

                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_gear_ratio','id'=>'table_2_gear_ratio', 'type' => 'text', 'title' => 'ギヤ比', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>
                                        <div class="col-sm-4 d-flex align-items-end">
                                            @include('partials.form.input', ['name' => 'table_2_jet_impeller_rotation_direction','id'=>'table_2_jet_impeller_rotation_direction', 'type' => 'text', 'title' => 'JETインペラ回転方向', 'class' => 'details-text-area', 'required' => 'true', 'rows' => '5', 'value'=>old('description')])
                                            <div style="margin-bottom: 20px;">
                                                {{--                                                <div class="input-group-text">mm</div>--}}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </fieldset>
                <h6>Step 4</h6>
                <fieldset>

                    <div class="row">
                        <div class="col-sm-4 align-items-end">
                            <div class="form-group">
                                <label for="logo">Add Graph Image<span class="text-danger">*</span></label>
                                <input type="file" class="form-control-file" id="graph_Image" name="graph_Image" accept="image/png, image/jpeg" >
                                <span class="form-text text-muted" role="alert">Accepted SIZE (Width - Height) px:600 - 1280</span>
                            </div>
                        </div>
                        <div class="col-sm-4 align-items-end">
                            <div class="form-group">
                                <label for="logo">Add Table Image<span class="text-danger">*</span></label>
                                <input type="file" class="form-control-file" id="table_Image" name="table_Image" accept="image/png, image/jpeg" >
                                <span class="form-text text-muted" role="alert">Accepted SIZE (Width - Height) px:600 - 1280</span>
                            </div>
                        </div>

                    </div>


                </fieldset>


            </form>

        </div>
    </div>
</div>
<script src="{{asset('global_assets/js/plugins/uploaders/fileinput/fileinput.min.js')}}"></script>



