<!-- Main sidebar -->
<div class="sidebar sidebar-dark sidebar-main sidebar-expand-lg">

    <!-- Sidebar content -->
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-section sidebar-user my-1">
            <div class="sidebar-section-body pt-1 pr-1">
                <div class="mb-1 d-flex justify-content-end align-self-center">
                    <button type="button"
                            class="btn m-w-unset p-0 text-white border-transparent btn-icon btn-sm sidebar-mobile-main-toggle d-lg-none">
                        <i class="icon-cross2"></i>
                    </button>
                </div>
                <div class="media">
                    <a href="#" class="mr-3">
                        <img src="{{ \Illuminate\Support\Facades\Auth::user()->profile_image ?? '' }}"
                             class="rounded-circle" alt="">
                    </a>

                    <div class="media-body">
                        <div class="font-weight-semibold"> {{ \Illuminate\Support\Facades\Auth::user()->display_name  }}</div>
                        <div class="font-size-sm line-height-sm opacity-50">
                            {{ @(\Illuminate\Support\Facades\Auth::user()->roles->pluck('name')[0]) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-section">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <!-- Main -->
                <li class="nav-item-header">
                    <div class="text-uppercase font-size-xs line-height-xs">Main Menu</div>
                    <i class="icon-menu" title="Main"></i></li>
                <li class="nav-item">
                    <a href="{{ route('admin.dashboard') }}" class="nav-link {{ Request::is('admin/dashboard') ? 'active' : '' }}"><i class="icon-home4"></i> <span>Dashboard</span></a>
                </li>



                <li class="nav-item">
                    <a href="{{ route('admin.product.index') }}" class="nav-link"><i class="icon-color-sampler"></i> <span>Product Management</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.news-category.index') }}" class="nav-link"><i class="icon-color-sampler"></i> <span>News Category Management</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.news.index') }}" class="nav-link"><i class="icon-color-sampler"></i> <span>News Management</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.faq.index') }}" class="nav-link"><i class="icon-color-sampler"></i> <span>Q & A Management</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.inquiry-management.index') }}" class="nav-link"><i class="icon-color-sampler"></i> <span>Inquiry Management</span></a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('admin.web-content.index') }}" class="nav-link"><i class="icon-color-sampler"></i> <span>Contact Management</span></a>
                </li>




                {{--@php( $active = $activeR = false)

                <li class="nav-item nav-item-submenu  {{ $active ? 'nav-item-open' : '' }}">
                    <a href="javascript:void(0);" class="nav-link "><i class="icon-people"></i> <span>User Management</span></a>

                    <ul class="nav nav-group-sub {{ $active ? 'd-block' : '' }}">

                        @can('View Roles')
                            <li class="nav-item"><a class="nav-link {{ Request::is('admin/roles') ? 'active' : '' }}" href="{{ route('admin.roles.index') }}">Roles</a></li>
                        @endcan
                        @can('View Permissions')
                            <li class="nav-item"><a class="nav-link {{ Request::is('admin/permissions') ? 'active' : '' }}" href="{{ route('admin.permissions.index') }}">Permissions</a></li>
                        @endcan
                        @can('View Users')
                            <li class="nav-item"><a class="nav-link {{ Request::is('admin/users') ? 'active' : '' }}" href="{{ route('admin.users.index') }}">Users</a></li>
                        @endcan
                        @can('View Action Log')
                            <li class="nav-item"><a class="nav-link {{ Request::is('admin/action-log') ? 'active' : '' }}" href="{{ route('admin.permissions.index') }}">Action Logs</a></li>
                        @endcan

                    </ul>
                </li>--}}

            </ul>
        </div>
        <!-- /main navigation -->

    </div>
    <!-- /sidebar content -->

</div>
<!-- /main sidebar -->
