<!-- Footer -->
<div class="navbar navbar-expand-lg navbar-light">
    <div class="text-center d-lg-none w-100">
        <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse"
                data-target="#navbar-footer">
            <i class="icon-unfold mr-2"></i>
            Footer
        </button>
    </div>

    <div class="navbar-collapse collapse text-center justify-content-center" id="navbar-footer">
        <span class="navbar-text">
            &copy; 2022 <a href="{{ url('/') }}" target="_blank"> {{ config('app.name', 'Laravel') }} </a> by <a href="http://zincat.net" target="_blank">Zincat Technology</a>
        </span>
    </div>
</div>
<!-- /footer -->
