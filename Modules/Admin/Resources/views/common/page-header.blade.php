
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex py-2">
            <h4><i class="{{isset($icon)?$icon:"icon-quill4"}}"></i><span
                        class="ml-2 font-weight-semibold">{{ isset($title)?$title:'' }}</span></h4>
        </div>

        <div class="header-elements py-0 py-sm-0 py-md-2 py-lg-2 py-xl-2">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="{{route('admin.dashboard')}}" class="breadcrumb-item"><i class="icon-home2 mr-2"></i>
                        Home</a>
                    <a class="breadcrumb-item active">{{ isset($title)?$title:'' }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page header -->
