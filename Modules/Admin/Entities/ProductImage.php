<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $guarded = [];
    protected $appends = ['image_path'];

    public function product() {
        return $this->hasOne(Product::class);
    }

    public function getImagePathAttribute($value)
    {
        $value = $this->file_path;

        if (empty($value)) {
            return asset('template_assets/assets/images/home/default.webp');
        } else {
            return asset('/storage/product_images/' . $value);
        }
    }
}
