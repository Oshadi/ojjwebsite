<?php

namespace Modules\Admin\Entities;

use App\NewsImage;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $guarded = [];
    protected $appends = ['image_path'];

    public function ProductImages() {
        return $this->hasMany(ProductImage::class);
    }
    public function ProductMeasurement() {
        return $this->belongsTo(ProductMeasurements::class,'id','product_id');
    }
    public function ProductDetails() {
        return $this->belongsTo(ProductDetails::class,'id','product_id');
    }

    public function getImagePathAttribute($value)
    {
        $value = $this->main_image;
        if (empty($value)) {
            return asset('template_assets/assets/images/home/default.webp');
        } else {
            return asset('/storage/main_product_images/' . $value);

        }
    }

    public function getThumbnailImageAttribute($value)
    {
//        $value = $this->thumbnail_image;

        if (empty($value)) {
            return asset('custom_assets/images/default.png');
        } else {
            return asset('/storage/thumbnailImages/' . $value);
        }
    }
}
