<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductDetails extends Model
{

    protected $guarded = [];
    protected $appends = ['image_path'];


    public function product() {
        return $this->hasOne(Product::class);
    }

    public function getImagePathAttribute($value)
    {
        $value = $this->graph_Image;

        if (empty($value)) {
            return asset('template_assets/assets/images/home/default.webp');
        } else {
            return asset('/storage/product_images_graph/' . $value);
        }
    }

    public function getTableImageAttribute($value)
    {
//        $value = $this->table_Image;

        if (empty($value)) {
            return asset('custom_assets/images/default.png');
        } else {
            return asset('/storage/product_images_table/' . $value);
        }
    }

}
