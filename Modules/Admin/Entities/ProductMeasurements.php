<?php

namespace Modules\Admin\Entities;

use App\News;
use Illuminate\Database\Eloquent\Model;

class ProductMeasurements extends Model
{

    protected $guarded = [];
    protected $appends = ['image_path'];

    public function product() {
        return $this->belongsTo(Product::class);
    }

    public function getImagePathAttribute($value)
    {
        $value = $this->boat_length_image;

        if (empty($value)) {
            return asset('template_assets/assets/images/home/default.webp');
        } else {
            return asset('/storage/product_images_boat/' . $value);
        }
    }

}
