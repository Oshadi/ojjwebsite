<?php

namespace Modules\Admin\Services;

use App\Constants\Actions;
use App\Faq;
use App\Services\MainService;
use Modules\Admin\Repositories\FaqRepository;

class FaqService extends MainService
{

    /*
     * Created By :oshadhi
     * Created At : 2022-07-31
     */

    public function __construct(FaqRepository $faqRepository)
    {
        $this->repository = $faqRepository;
    }


    /**
     * @param $request
     * @return mixed
     * @throws \Exception
     */
    public function update($request)
    {
        try {
            // DELETE ALL PREVIOUS FAQ
            Faq::where('type', $request->mode)->delete();

            // ALL MAIN QUESTION STORE
            foreach ($request->question as $k => $question) {

                $data['type'] = $request->mode;
                $data['question'] = $question;
                if(isset($request->answer[$k])) $data['answer'] = $request->answer[$k];

                $results = $this->repository->create($data);
            }

            return $results;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function delete($id)
    {

    }

    public function get($editId)
    {
        return $this->repository->show($editId);
    }

    /**
     * @param $type
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model[]
     * @throws \Exception
     */
    public function getAllQuestion($type)
    {
        return $this->repository->getAllActiveQuestion($type);
    }

}
