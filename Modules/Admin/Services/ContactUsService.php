<?php

namespace Modules\Admin\Services;

use App\Constants\Actions;
use App\Repositories\UserRepository;
use App\Services\MainService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Admin\Repositories\ContactUsRepository;
use Modules\Web\Entities\ContactUsDetail;





class ContactUsService extends MainService
{
    private $repository;

    private $contactUsRepository;

    public function __construct(ContactUsRepository $contactUsRepository)
    {

        $this->ContactUsRepository = $contactUsRepository;
    }





    /**
     * Created by : Kelum
     * Created at :
     * Summary: CONTACT US UPDATE
     *
     * @param Request $request
     * @throws \Exception
     */
    public function updateContactUs(Request $request)
    {

        try {
            $this->ContactUsRepository->updateContact($request);
          return getFormattedResponce(true, "redirect", 'Contact Us Updated Successfully', [], "web-content", "");

        } catch (\Exception $e) {
            Log::error($e);
            return getFormattedResponce(false, "failed", $e, [], "", "");
        }
    }
}
