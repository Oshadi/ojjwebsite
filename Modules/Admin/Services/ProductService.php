<?php

namespace Modules\Admin\Services;

use App\NewsImage;
use App\Services\MainService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Entities\Product;
use Modules\Admin\Entities\ProductDetails;
use Modules\Admin\Entities\ProductImage;
use Modules\Admin\Entities\ProductMeasurements;
use Modules\Admin\Repositories\ProductDetailsRepository;
use Modules\Admin\Repositories\ProductImageRepository;
use Modules\Admin\Repositories\ProductMeasurementRepository;
use Modules\Admin\Repositories\ProductRepository;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Exception;

class ProductService extends MainService
{
    protected $productRepository;
    protected $productImageRepository;
    protected $productMeasurementRepository;
    protected $productDetailsRepository;

    public function __construct(ProductRepository $productRepository,ProductImageRepository $productImageRepository,ProductMeasurementRepository $productMeasurementRepository,ProductDetailsRepository $productDetailsRepository)
    {
        $this->productRepository = $productRepository;
        $this->productImageRepository = $productImageRepository;
        $this->productMeasurementRepository = $productMeasurementRepository;
        $this->productDetailsRepository=$productDetailsRepository;
    }

    public function getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection)
    {
        // * customize start
        $model = Product::query();
        // * customize end

        if (!empty($search)) {
            $model = $this->generateWhereLikeQuery($model, $columns, $search);
        }
        if (!empty($orderBy)) {
            $model->orderBy($orderBy, $orderDirection);
        }

        // get the filtered row count before limiting the results
        $rows = $model->get();
        $count = count($rows);

        // limit the results for pagination
        $model->offset($offset)->limit($limit);
        $rows = $model->get();

        $data = [];
        foreach ($rows as $k => $row) {
            $common_index_actions = [
                'row' => $row,
                'route' => "product",
                'title' => "product",
                'module' => "admin"
            ];

            $temp = [
                // * customize start
                $k + 1,
                $row->name,
                $row->created_at->format('Y-m-d'),
                ($row->status == 1) ? '<span class="badge badge-success">Active</span>'  : '<span class="badge badge-danger">Inactive</span>',
                //$row->status,
                // * customize end

                generateActionButtons('permission', $common_index_actions, ['view' => false, 'edit' => true, 'delete' => false, "status" => true])
            ];
            array_push($data, $temp);
        }

        $out['data'] = $data;
        $out['recordsFiltered'] = $count; // count of records after applying search filters
        $out['recordsTotal'] = Product::count(); // count of all the records in the database table

        return $out;
    }

    public function store(Request $request)
    {
        dd($request->all());
//        dd($request->bootImages[0]->getClientOriginalName());
        try {
            $newsResults="";
            $picture = $request->file('main_image');
            if ($picture) {
                $data['main_image'] = $this->imageUploader($picture, 'main');
            }

            $thumbnail = $request->file('thumbnail_image');
            if ($picture) {
                $data['thumbnail_image'] = $this->imageUploaderThumbnail($thumbnail, 'thumbnail');
            }


            $data['name'] = $request->name;
            $data['added_date'] = date('yy-mm-dd');
            $data['status'] = 1;


            $newsResults = $this->productRepository->create($data);


            foreach ($request->array_name as $d => $details_new) {

                array_filter(
                    $request->bootImages,
                    function ($e) use (&$details_new,$newsResults) {
                       if ($e->getClientOriginalName() == $details_new){

                               $dataImg['file_path'] = $this->imageUploaderSub($e, 'other');
                             $dataImg['product_id'] = $newsResults->id;
                           $dataImg['status'] = 1;

                           $this->productImageRepository->create($dataImg);


                       }
                    }
                );





//                foreach ($request->bootImages as $k => $details) {
//
//                    if ($details) {
//                        $picture = $request->bootImages[$k]->getClientOriginalName();
//                        $order_picture=$request->array_name[$d];
//                        if ($picture==$order_picture) {
//                            $dataImg['file_path'] = $this->imageUploaderSub($picture, 'other');
//                        }
//
//                        $dataImg['product_id'] = $newsResults->id;
//                        $dataImg['status'] = 1;
//
//                        $this->productImageRepository->create($dataImg);
//                    }
//                }
            }

            if ($request->table_1_capacity) {

                $picture = $request->boat_length_image;


                if ($picture) {
                    $measurement['boat_length_image'] = $this->imageUploaderBoat($picture, 'other');
                }
                $measurement['table_1_name'] = $request->table_1_name;
                $measurement['product_id'] = $newsResults->id;
                $measurement['capacity'] = $request->table_1_capacity;
                $measurement['tube_diameter'] = $request->table_1_tube_diameter;
                $measurement['full_length'] = $request->table_1_full_length;
                $measurement['weight'] = $request->table_1_weight;
                $measurement['full_width'] = $request->table_1_full_width;
                $measurement['transom_high'] = $request->table_1_transom_high;
                $measurement['inner_width'] = $request->table_1_inner_width;
//                    $dataImg['description'] = $details;

                $this->productMeasurementRepository->create($measurement);
            }

            if ($request->table_2_ful_length) {

                $picture = $request->graph_Image;


                if ($picture) {
                    $details_product['graph_Image'] = $this->imageUploaderGraph($picture, 'other');
                }

                $picture_table = $request->table_Image;


                if ($picture_table) {
                    $details_product['table_Image'] = $this->imageUploaderTable($picture_table, 'other');
                }

                $details_product['product_id'] = $newsResults->id;
                $details_product['table_2_name'] = $request->table_2_name;
                $details_product['ful_length'] = $request->table_2_ful_length;
                $details_product['total_displacement'] = $request->table_2_total_displacement;
                $details_product['fuel_used'] = $request->table_2_fuel_used;
                $details_product['full_width'] = $request->table_2_full_width;
                $details_product['compression_ratio'] = $request->table_2_compression_ratio;
                $details_product['engine_oil'] = $request->table_2_engine_oil;
                $details_product['overall_height'] = $request->table_2_overall_height;
                $details_product['spark_plug'] = $request->table_2_spark_plug;
                $details_product['engine_oil_grade'] = $request->table_2_engine_oil_grade;
                $details_product['weight'] = $request->table_2_weight;
                $details_product['ignition_system'] = $request->table_2_ignition_system;
                $details_product['engine_oil_viscosity'] = $request->table_2_engine_oil_viscosity;
                $details_product['transom_high'] = $request->table_2_transom_high;
                $details_product['fuel_supply_method'] = $request->table_2_fuel_supply_method;
                $details_product['engine_oil_specified_amount'] = $request->table_2_engine_oil_specified_amount;
                $details_product['maximum_output'] = $request->table_2_maximum_output;
                $details_product['exhaust_system'] = $request->table_2_exhaust_system;
                $details_product['specified_amount_of_engine_oil'] = $request->table_2_specified_amount_of_engine_oil;
                $details_product['recommended_rotation_when_fully_open'] = $request->table_2_recommended_rotation_when_fully_open;
                $details_product['lubrication_method'] = $request->table_2_lubrication_method;
                $details_product['jet_pump_grease'] = $request->table_2_jet_pump_grease;
                $details_product['trolling_speed'] = $request->table_2_trolling_speed;
                $details_product['starter'] = $request->table_2_starter;
                $details_product['clutch_type'] = $request->table_2_clutch_type;
                $details_product['engine_type'] = $request->table_2_engine_type;
                $details_product['chalk_system'] = $request->table_2_chalk_system;
                $details_product['clutch_operation_method'] = $request->table_2_clutch_operation_method;
                $details_product['cylinder_array'] = $request->table_2_cylinder_array;
                $details_product['throttle_control'] = $request->table_2_throttle_control;
                $details_product['gear_ratio'] = $request->table_2_gear_ratio;
                $details_product['boa'] = $request->table_2_boa;
                $details_product['thermostat_valve_opening_temperature'] = $request->table_2_thermostat_valve_opening_temperature;
                $details_product['stroke'] = $request->table_2_stroke;
                $details_product['jet_impeller_rotation_direction']=$request->table_2_jet_impeller_rotation_direction;
                $this->productDetailsRepository->create($details_product);
            }



            return $newsResults;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function update($request, $id){
        $picture = $request->file('main_image');

        if ($picture) {
            $data['main_image'] = $this->imageUploader($picture, 'main');
        }

        $thumbnail_image = $request->file('thumbnail_image');
        if ($thumbnail_image) {
            $data['thumbnail_image'] = $this->imageUploaderThumbnail($thumbnail_image, 'thumbnail');
        }

        $data['name'] = $request->name;
        $data['added_date'] = date('yy-mm-dd');
        $data['status'] = 1;


        $newsResults = $this->productRepository->update($data, $id);


//        ProductImage::where('product_id', $id)->update(['status' => 0]);
        if (isset($request->bootImages)) {
            foreach ($request->bootImages as $k => $details) {

                if (isset($details)) {


                    if ($details) {
                        $picture = $request->bootImages[$k];

                        if ($picture) {
                            $dataImg['file_path'] = $this->imageUploaderSub($picture, 'other');
                        }


                        $dataImg['product_id'] = $id;
                        $dataImg['status'] = 1;

                        $this->productImageRepository->create($dataImg);


                    }
                }
            }
        }

        $ms_id=ProductMeasurements::where('product_id', $id)->first();
        $product_mes_id=$ms_id->id;
        if ($request->table_1_capacity) {

            $picture = $request->boat_length_image;


            if ($picture) {
                $measurement['boat_length_image'] = $this->imageUploaderBoat($picture, 'other');
            }

            $measurement['product_id'] = $id;
            $measurement['table_1_name'] = $request->table_1_name;
            $measurement['capacity'] = $request->table_1_capacity;
            $measurement['tube_diameter'] = $request->table_1_tube_diameter;
            $measurement['full_length'] = $request->table_1_full_length;
            $measurement['weight'] = $request->table_1_weight;
            $measurement['full_width'] = $request->table_1_full_width;
            $measurement['transom_high'] = $request->table_1_transom_high;
            $measurement['inner_width'] = $request->table_1_inner_width;
//                    $dataImg['description'] = $details;



            $newsResults_mes = $this->productMeasurementRepository->update($measurement,$product_mes_id);


        }


        $pr_id=ProductDetails::where('product_id', $id)->first();
        $product_des_id=$pr_id->id;
        if ($request->table_2_ful_length) {

            $picture = $request->graph_Image;


            if ($picture) {
                $details_product['graph_Image'] = $this->imageUploaderGraph($picture, 'other');
            }

            $picture_table = $request->table_Image;


            if ($picture_table) {
                $details_product['table_Image'] = $this->imageUploaderTable($picture_table, 'other');
            }

            $details_product['product_id'] = $id;
            $details_product['table_2_name'] = $request->table_2_name;
            $details_product['ful_length'] = $request->table_2_ful_length;
            $details_product['total_displacement'] = $request->table_2_total_displacement;
            $details_product['fuel_used'] = $request->table_2_fuel_used;
            $details_product['full_width'] = $request->table_2_full_width;
            $details_product['compression_ratio'] = $request->table_2_compression_ratio;
            $details_product['engine_oil'] = $request->table_2_engine_oil;
            $details_product['overall_height'] = $request->table_2_overall_height;
            $details_product['spark_plug'] = $request->table_2_spark_plug;
            $details_product['engine_oil_grade'] = $request->table_2_engine_oil_grade;
            $details_product['weight'] = $request->table_2_weight;
            $details_product['ignition_system'] = $request->table_2_ignition_system;
            $details_product['engine_oil_viscosity'] = $request->table_2_engine_oil_viscosity;
            $details_product['transom_high'] = $request->table_2_transom_high;
            $details_product['fuel_supply_method'] = $request->table_2_fuel_supply_method;
            $details_product['engine_oil_specified_amount'] = $request->table_2_engine_oil_specified_amount;
            $details_product['maximum_output'] = $request->table_2_maximum_output;
            $details_product['exhaust_system'] = $request->table_2_exhaust_system;
            $details_product['specified_amount_of_engine_oil'] = $request->table_2_specified_amount_of_engine_oil;
            $details_product['recommended_rotation_when_fully_open'] = $request->table_2_recommended_rotation_when_fully_open;
            $details_product['lubrication_method'] = $request->table_2_lubrication_method;
            $details_product['jet_pump_grease'] = $request->table_2_jet_pump_grease;
            $details_product['trolling_speed'] = $request->table_2_trolling_speed;
            $details_product['starter'] = $request->table_2_starter;
            $details_product['clutch_type'] = $request->table_2_clutch_type;
            $details_product['engine_type'] = $request->table_2_engine_type;
            $details_product['chalk_system'] = $request->table_2_chalk_system;
            $details_product['clutch_operation_method'] = $request->table_2_clutch_operation_method;
            $details_product['cylinder_array'] = $request->table_2_cylinder_array;
            $details_product['throttle_control'] = $request->table_2_throttle_control;
            $details_product['gear_ratio'] = $request->table_2_gear_ratio;
            $details_product['boa'] = $request->table_2_boa;
            $details_product['thermostat_valve_opening_temperature'] = $request->table_2_thermostat_valve_opening_temperature;
            $details_product['stroke'] = $request->table_2_stroke;
            $details_product['jet_impeller_rotation_direction']=$request->table_2_jet_impeller_rotation_direction;
            $this->productDetailsRepository->update($details_product,$product_des_id);
        }


        return $newsResults;





    }

    public function imageUploader($picture, $slug)
    {
        $fileName = Storage::disk('local')->putFile('/public/main_product_images', $picture);
        return basename($fileName);
    }

    public function imageUploaderThumbnail($thumbnail_image,$slug){
        $fileName = Storage::disk('local')->putFile('/public/thumbnailImages', $thumbnail_image);
        return basename($fileName);
    }


    public function imageUploaderSub($picture, $slug){


        $fileName = Storage::disk('local')->putFile('/public/product_images', $picture);
        return basename($fileName);

//        $fileName = time().'-'.$slug.'.'.$picture->getClientOriginalExtension();
////        $fileName = time().'-'.$slug.'.'.$picture->getClientOriginalExtension();
//        $destinationPath = storage_path('app/public/product_images');
//        $picture->move($destinationPath, $fileName);
//
//        return $fileName;
    }

    public function imageUploaderBoat($picture, $slug){
        $fileName = Storage::disk('local')->putFile('/public/product_images_boat', $picture);
        return basename($fileName);
    }

    public function imageUploaderGraph($picture, $slug){
        $fileName = Storage::disk('local')->putFile('/public/product_images_graph', $picture);
        return basename($fileName);
    }

    public function imageUploaderTable($picture, $slug){
        $fileName = Storage::disk('local')->putFile('/public/product_images_table', $picture);
        return basename($fileName);

    }

    public function getActiveAll(){
        return $this->productRepository->getActiveProductList();
    }

    public function firstProduct(){
        return $this->productRepository->firstProduct();
    }

    public function getProductDetails($id){
        return $this->productRepository->getProductDetails($id);
    }

    public function get($editId)
    {

        return $this->productRepository->show($editId);

    }

    public function deleteImage($request)
    {
        try {

            $id = $request->key;
//            dd($id);
//
          ProductImage::findOrFail($id)->delete();


        } catch (Exception $e) {
            throw $e;
        }
    }



}
