<?php

namespace Modules\Admin\Services;

use App\Constants\Actions;
use App\News;
use App\NewsCategory;
use App\Services\MainService;
use Modules\Admin\Entities\Product;
use Modules\Web\Entities\Inquiry;

class DashboardService extends MainService
{

    /**
     * Created By : oshadhi
     * Created At : 2022-07-30
     */

    /**
     * DashboardService constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function getTotalProducts()
    {
        $query = Product::whereStatus(1);

        return $query->count();
    }

    /**
     * @return mixed
     */
    public function getTotalNewsCategory()
    {
        $query = NewsCategory::whereStatus(1);

        return $query->count();
    }

    /**
     * @return mixed
     */
    public function getTotalNews()
    {
        $query = News::whereStatus(1);

        return $query->count();
    }

    /**
     * @return mixed
     */
    public function getTotalInquires()
    {
        $query = Inquiry::whereStatus(1);

        return $query->count();
    }

}
