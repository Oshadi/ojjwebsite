<?php
namespace Modules\Admin\Services;

use App\Constants\Actions;
use App\Repositories\UserRepository;
use App\Services\MainService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\Admin\Repositories\PermissionRepository;
use Modules\Admin\Repositories\RoleRepository;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionService extends MainService
{
    private $repository;

    public function __construct(PermissionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection)
    {
        // * customize start
        $model = Permission::select(DB::raw('permissions.id, permissions.name, GROUP_CONCAT(roles.name) as roles'))
            ->leftJoin('role_has_permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
            ->leftJoin('roles', 'roles.id', '=', 'role_has_permissions.role_id')
            ->groupBy('permissions.id');
        // * customize end

        if (!empty($search)) {
            $model = $this->generateWhereLikeQuery($model, $columns, $search);
        }
        if (!empty($orderBy)) {
            $model->orderBy($orderBy, $orderDirection);
        }

        // get the filtered row count before limiting the results
        $rows = $model->get();
        $count = count($rows);

        // limit the results for pagination
        $model->offset($offset)->limit($limit);
        $rows = $model->get();

        $data = [];
        foreach ($rows as $row) {
            $common_index_actions = [
                'row' => $row,
                'route' => "permissions",
                'title' => "Permission",
                'module' => "admin"
            ];

            $temp = [
                // * customize start
                $row->id,
                $row->name,
                $row->roles()->pluck('name')->implode(", "),
                // * customize end

                generateActionButtons('permission', $common_index_actions, ['view' => false, 'edit' => checkHasPermission(Actions::EDIT_PERMISSIONS), 'delete' => false, "status" => false])
            ];
            array_push($data, $temp);
        }

        $out['data'] = $data;
        $out['recordsFiltered'] = $count; // count of records after applying search filters
        $out['recordsTotal'] = Permission::count(); // count of all the records in the database table

        return $out;
    }

    public function getAll()
    {
        $rows = $this->repository->all();
        $returnArray = [];
        foreach ($rows as $row) {
            $temp['id'] = $row->id;
            $temp['name'] = $row->name;
            $temp['name'] = $row->name;
            $temp['roles'] = $row->roles->pluck('name')->implode(", ");
            array_push($returnArray, $temp);
        }

        return $returnArray;
    }

    public function create(Request $request)
    {
        try {
            $name = $request['name'];
            $permission = new Permission();
            $permission->name = $name;

            $roles = $request['roles'];

            $permission->save();

            if (!empty($request['roles'])) { //If one or more role is selected
                foreach ($roles as $role) {
                    $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                    $permission = Permission::where('name', '=', $name)->first(); //Match input //permission to db record
                    $r->givePermissionTo($permission);
                }
            }
            return ['status' => 'success', 'permission' => $permission];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function update($request, $id)
    {
        try {
            $permission = $this->repository->show($id);
            $input = $request->all();
            $permission->name = $input['name'];
            $permission->syncRoles($input['roles']);
            $permission->save();

            return ['status' => 'success', 'permission' => $permission];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function delete($id)
    {
        try {
            $this->repository->delete($id); //Get role with the given id
            return ['status' => 'success'];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function get($editId)
    {
        $row = $this->repository->show($editId);

        $temp['id'] = $row->id;
        $temp['name'] = $row->name;
        $temp['roles'] = $row->roles->pluck('name')->toArray();

        return $temp;
    }
}