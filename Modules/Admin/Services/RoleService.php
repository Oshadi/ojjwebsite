<?php
namespace Modules\Admin\Services;

use App\Constants\Actions;
use App\Repositories\UserRepository;
use App\Services\MainService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\Admin\Repositories\RoleRepository;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleService extends MainService
{
    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection)
    {
        // * customize start
        $model = Role::select(DB::raw('roles.id, roles.name, GROUP_CONCAT(permissions.name) as permissions'))
            ->leftJoin('role_has_permissions', 'roles.id', '=', 'role_has_permissions.role_id')
            ->leftJoin('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
            ->groupBy('roles.id', 'roles.name');
        // * customize end

        if (!empty($search)) {
            unset($columns['Permissions@no-sort@']);
            $model = $this->generateWhereLikeQuery($model, $columns, $search);
        }
        if (!empty($orderBy)) {
            $model->orderBy($orderBy, $orderDirection);
        }

        // get the filtered row count before limiting the results
        $rows = $model->get();
        $count = count($rows);

        // limit the results for pagination
        $model->offset($offset)->limit($limit);
        $rows = $model->get();

        $data = [];
        foreach ($rows as $row) {
            $common_index_actions = [
                'row' => $row,
                'route' => "roles",
                'title' => "Role",
                'module' => "admin"
            ];

            $temp = [
                // * customize start
                $row->id,
                $row->name,
                $row->permissions()->pluck('name')->toArray(),
                // * customize end

                generateActionButtons('roles', $common_index_actions, ['view' => false, 'edit' => checkHasPermission(Actions::EDIT_ROLES), 'delete' => false, "status" => false])
            ];
            array_push($data, $temp);
        }

        $out['data'] = $data;
        $out['recordsFiltered'] = count($rows); // count of records after applying search filters
        $out['recordsTotal'] = Role::count(); // count of all the records in the database table

        return $out;
    }

    public function getAll()
    {
        $roles = $this->roleRepository->all();
        $returnArray = [];
        foreach ($roles as $role) {
            $temp['id'] = $role->id;
            $temp['name'] = $role->name;
            $temp['permissions'] = str_replace(array('[',']','"'),'', $role->permissions()->pluck('name'));
            array_push($returnArray, $temp);
        }

        return $returnArray;
    }

    public function createRole(Request $request)
    {
        try {
            $name = $request['name'];
            $role = new Role();
            $role->name = $name;

            $permissions = $request['permissions'];

            $role->save();
            //Looping thru selected permissions
            foreach ($permissions as $permission) {
                $p = Permission::where('id', '=', $permission)->firstOrFail();
                //Fetch the newly created role and assign permission
                $role = Role::where('name', '=', $name)->first();
                $role->givePermissionTo($p);
            }
            return ['status' => 'success', 'role' => $role];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function updateRole($request, $id)
    {
        try {
            $role = $this->roleRepository->show($id); //Get role with the given id
            $input = $request->except(['permissions']);
            $permissions = $request['permissions'];
            $role->fill($input)->save();

            $p_all = Permission::all();//Get all permissions

            foreach ($p_all as $p) {
                $role->revokePermissionTo($p); //Remove all permissions associated with role
            }

            foreach ($permissions as $permission) {
                $p = Permission::where('id', '=', $permission)->firstOrFail(); //Get corresponding form //permission in db
                $role->givePermissionTo($p);  //Assign permission to role
            }

            return ['status' => 'success', 'role' => $role];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteRole($id)
    {
        try {
            $this->roleRepository->delete($id); //Get role with the given id
            return ['status' => 'success'];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getRole($editId)
    {
        $role = $this->roleRepository->show($editId);

        $temp['id'] = $role->id;
        $temp['name'] = $role->name;
        $temp['permissions'] = $role->permissions()->pluck('name')->toArray();

        return $temp;
    }
}