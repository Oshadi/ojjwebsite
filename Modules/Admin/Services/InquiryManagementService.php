<?php

namespace Modules\Admin\Services;

use App\Constants\Actions;
use App\DeliveryCompany;
use App\DeliveryCompanyBankDetail;
use App\DeliveryCompanyPerson;
use App\NewsCategory;
use App\Services\MainService;
use Exception;
use Illuminate\Http\Request;
use Modules\Admin\Repositories\DeliveryCompanyRepository;
use Modules\Admin\Repositories\InquiryManagementRepository;
use Modules\Web\Entities\Inquiry;

class InquiryManagementService extends MainService
{
    private $deliveryCompanyRepository;

    public function __construct(InquiryManagementRepository $inquiryManagementRepository)
    {
        $this->repository = $inquiryManagementRepository;
    }

    public function getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection)
    {
        // * customize start
        $model = Inquiry::query();
        // * customize end

        if (!empty($search)) {
            $model = $this->generateWhereLikeQuery($model, $columns, $search);
        }
        if (!empty($orderBy)) {
            $model->orderBy($orderBy, $orderDirection = 'DESC');
        }

        // get the filtered row count before limiting the results
        $rows = $model->get();
        $count = count($rows);

        // limit the results for pagination
        $model->offset($offset)->limit($limit);
        $rows = $model->get();

        $data = [];
        foreach ($rows as $k => $row) {
            $common_index_actions = [
                'row' => $row,
                'route' => "inquiry-management",
                'title' => "inquiry-management",
                'module' => "admin"
            ];

            $temp = [
                // * customize start
                $k + 1,
                $row->name,
                $row->furigana,
                $row->company_organization_name,
                $row->email,
                //($row->status == 1) ? '<span class="badge badge-success">Active</span>'  : '<span class="badge badge-danger">Inactive</span>',
                //$row->status,
                // * customize end

                generateActionButtons('permission', $common_index_actions, ['view' => true, 'edit' => false, 'delete' => false, "status" => false])
            ];
            array_push($data, $temp);
        }

        $out['data'] = $data;
        $out['recordsFiltered'] = $count; // count of records after applying search filters
        $out['recordsTotal'] = Inquiry::count(); // count of all the records in the database table

        return $out;
    }


    /**
     *
     * Created by : oshadhi
     * Created at : 2022.07.28
     * Summary: get specific inquiry by ID
     *
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function specificInquiryById($id)
    {
        try {
            return $this->repository->getSpecificInquiryById($id);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     *
     * Created at: oshadhi
     * Created by: 2022.07.28
     * Summary: Filter account category data
     *
     * @param $input
     * @return mixed
     * @throws Exception
     */
    public function getAll($input)
    {
        return $this->repository->getAll($input);
    }
}
