<?php

namespace Modules\Admin\Services;

use App\Constants\Actions;
use App\Services\MainService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Modules\Admin\Repositories\PermissionRepository;
use Modules\Admin\Repositories\RoleRepository;
use Modules\Admin\Repositories\UserRepository;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserService extends MainService
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection)
    {
        // * customize start
        $model = User::role(['super admin', 'admin']);
        // * customize end

        if (!empty($search)) {
            $model = $this->generateWhereLikeQuery($model, $columns, $search);
        }
        if (!empty($orderBy)) {
            $model->orderBy($orderBy, $orderDirection);
        }

        // get the filtered row count before limiting the results
        $rows = $model->get();
        $count = count($rows);

        // limit the results for pagination
        $model->offset($offset)->limit($limit);
        $rows = $model->get();

        $data = [];
        foreach ($rows as $row) {
            $common_index_actions = [
                'row' => $row,
                'route' => "users",
                'title' => "users",
                'module' => "admin"
            ];

            $temp = [
                // * customize start
                $row->id,
                $row->display_name,
                $row->email,
                $row->mobile,
                $row->roles()->pluck('name')->implode(", "),
                // * customize end

                generateActionButtons('permission', $common_index_actions, ['view' => false, 'edit' => checkHasPermission(Actions::EDIT_PERMISSIONS), 'delete' => false, "status" => false])
            ];
            array_push($data, $temp);
        }

        $out['data'] = $data;
        $out['recordsFiltered'] = $count; // count of records after applying search filters
        $out['recordsTotal'] = User::role(['super admin', 'admin'])->count(); // count of all the records in the database table

        return $out;
    }

    public function create(Request $request)
    {
        try {

            $input = $request->only(['first_name', 'last_name', 'email', 'mobile', 'user_name']);
            $input['password'] = Hash::make($request->input('password'));
            $user = $this->repository->create($input);

            //dd( $user );

            $role = $request['role'];//Retrieving the roles field
            if (!empty($request['role'])) { //If one or more role is selected
                //foreach ($roles as $role) {
                $role_r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record
                $user->assignRole($role_r); //Assigning role to user
                //}
            }
            return ['status' => 'success', 'user' => $user];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function update($request, $id)
    {
        try {
            $user = $this->repository->show($id); //Get user specified by id

            $input = $request->only(['first_name', 'last_name', 'email', 'mobile', 'user_name']);
            if ($request->passwordUpdate)
                $input['password'] = Hash::make($request->password);

            $user->fill($input)->save();

            $role = $request['role']; //Retreive all roles
            if (isset($role)) {
                $user->roles()->sync($role);  //If one or more role is selected associate user to roles
            } else {
                $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
            }

            return ['status' => 'success', 'user' => $user];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function deleteXX($id)
    {
        try {
            $this->repository->delete($id); //Get role with the given id
            return ['status' => 'success'];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function get($editId)
    {
        return $this->repository->show($editId);
    }

    /**
     * USER LOGIN STATUS SAVE
     */
    public function logLogin()
    {
        $user = auth()->user();
        //$user->api_token = Str::random(60);
        $user->last_login_at = date('Y-m-d H:i:s');
        $user->last_login_ip = request()->ip();
        $user->save();
    }

    /**
     * @param $request
     * @return mixed
     * @throws \Exception
     *
     * MY PROFILE UPDATE
     * Updated By : oshadhi
     * Updated At : 2022-04-28
     */
    public function myProfileUpdate($request)
    {
        try {
            $user = User::find($request->id);
            $user->first_name = $request->firstName;
            $user->last_name = $request->lastName;
            $user->mobile = $request->mobile;

            if ($request->passwordUpdate)
                $user->password = Hash::make($request->password);
            $user->save();

            //dd($request->businessLogo);

            // BUSINESS LOGO UPDATE
            $filename = null;
            if ($request->hasFile('businessLogo')) {

                $uploads = $request->file('businessLogo');
                $filename = time() . '.' . $uploads->getClientOriginalExtension();

                $destinationPath = storage_path('app/public/business_logo');
                $uploads->move($destinationPath, $filename);
            }

            // USER PROFILE UPDATE
            $profileImage = null;
            if ($request->hasFile('profileImage')) {

                $uploads = $request->file('profileImage');
                $profileImage = time() . '.' . $uploads->getClientOriginalExtension();

                $destinationPath = storage_path('app/public/profile_image');
                $uploads->move($destinationPath, $profileImage);
            }

            $userProfile = $user->userProfile;
            $userProfile->address1 = $request->address1;
            $userProfile->address2 = $request->address2;
            $userProfile->address3 = $request->address3;
            $userProfile->district_id = $request->district;
            $userProfile->city_id = $request->city;

            if ($filename) $userProfile->business_logo = $filename;
            if ($profileImage) $userProfile->profile_image = $profileImage;
            $userProfile->business_name = $request->businessName;
            $userProfile->business_nature = $request->businessNature;
            $userProfile->save();

            return $user;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param null $roles
     * @return mixed
     *
     * GET ACTIVE USERS
     * Updated By : oshadhi
     * Updated At : 2022-05-05
     */
    public function getActiveAll($roles = null)
    {
        if ($roles)
            return User::role($roles)->where('status', 1)->get();
        else
            return User::where('status', 1)->get();
        //return $this->repository->all()->where('status',1);
    }
}
