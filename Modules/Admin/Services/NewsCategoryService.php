<?php

namespace Modules\Admin\Services;

use App\Constants\Actions;
use App\NewsCategory;
use App\Services\MainService;
use Illuminate\Http\Request;
use Modules\Admin\Repositories\NewsCategoryRepository;

class NewsCategoryService extends MainService
{
    /*
     * Created By :oshadhi
     * Created At : 2022-07-31
     */

    /**
     * NewsCategoryService constructor.
     * @param  NewsCategoryRepository  $newsCategoryRepository
     */
    public function __construct(NewsCategoryRepository $newsCategoryRepository)
    {
        $this->repository = $newsCategoryRepository;
    }

    public function getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection)
    {
        // * customize start
        $model = NewsCategory::query();
        // * customize end

        if (!empty($search)) {
            $model = $this->generateWhereLikeQuery($model, $columns, $search);
        }
        if (!empty($orderBy)) {
            $model->orderBy($orderBy, $orderDirection);
        }

        // get the filtered row count before limiting the results
        $rows = $model->get();
        $count = count($rows);

        // limit the results for pagination
        $model->offset($offset)->limit($limit);
        $rows = $model->get();

        $data = [];
        foreach ($rows as $k => $row) {
            $common_index_actions = [
                'row' => $row,
                'route' => "news-category",
                'title' => "news-category",
                'module' => "admin"
            ];

            $temp = [
                // * customize start
                $k + 1,
                $row->name,
                ($row->status == 1) ? '<span class="badge badge-success">Active</span>'  : '<span class="badge badge-danger">Inactive</span>',
                //$row->status,
                // * customize end

                generateActionButtons('permission', $common_index_actions, ['view' => false, 'edit' => true, 'delete' => false, "status" => true])
            ];
            array_push($data, $temp);
        }

        $out['data'] = $data;
        $out['recordsFiltered'] = $count; // count of records after applying search filters
        $out['recordsTotal'] = NewsCategory::count(); // count of all the records in the database table

        return $out;
    }

    /**
     * @param Request $request
     * @return DeliveryCompany
     * @throws \Exception
     *
     * STORE
     */
    public function store(Request $request)
    {
        try {

            return $this->repository->create($request->all());

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $request
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function update($request, $id)
    {
        try {

            return $this->repository->update($request->all(), $id);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function delete($id)
    {

    }

    public function get($editId)
    {
        return $this->repository->show($editId);
    }

    public function getActiveAll()
    {
        return $this->repository->all()->where('status',1);
    }
}
