<?php

namespace Modules\Admin\Services;

use App\Constants\Actions;
use App\News;
use App\NewsImage;
use App\Services\MainService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Modules\Admin\Repositories\NewsImageRepository;
use Modules\Admin\Repositories\NewsRepository;

class NewsService extends MainService
{
    /*
     * Created By :oshadhi
     * Created At : 2022-07-31
     */

    public function __construct(NewsRepository $newsRepository, NewsImageRepository $newsImageRepository)
    {
        $this->repository = $newsRepository;
        $this->newsImageRepository = $newsImageRepository;
    }

    public function getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection)
    {
        // * customize start
        $model = News::query();
        // * customize end

        if (!empty($search)) {
            $model = $this->generateWhereLikeQuery($model, $columns, $search);
        }
        if (!empty($orderBy)) {
            $model->orderBy($orderBy, $orderDirection);
        }

        // get the filtered row count before limiting the results
        $rows = $model->get();
        $count = count($rows);

        // limit the results for pagination
        $model->offset($offset)->limit($limit);
        $rows = $model->get();

        $data = [];
        foreach ($rows as $k => $row) {
            $common_index_actions = [
                'row' => $row,
                'route' => "news",
                'title' => "news",
                'module' => "admin"
            ];

            $temp = [
                // * customize start
                $row->id,
                $row->title,
                $row->newsCategory->name,
                mb_substr($row->description, 0, 100),
                $row->added_date,
                ($row->status == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>',
                // * customize end

                generateActionButtons('permission', $common_index_actions,
                    ['view' => false, 'edit' => true, 'delete' => false, "status" => true])
            ];
            array_push($data, $temp);
        }

        $out['data'] = $data;
        $out['recordsFiltered'] = $count; // count of records after applying search filters
        $out['recordsTotal'] = News::count(); // count of all the records in the database table

        return $out;
    }

    /**
     * @param  Request  $request
     * @return mixed
     * @throws \Exception
     */
    public function store(Request $request)
    {
        try {

            $picture = $request->file('main_image');
            if ($picture) {
                $data['main_image'] = $this->imageUploader($picture);
            }

            $data['category_id'] = $request->category_id;
            $data['title'] = $request->title;
            $data['description'] = $request->description;
            $data['added_date'] = $request->added_date;

            $newsResults = $this->repository->create($data);

            foreach ($request->details as $k => $details) {

                if ($details) {

                    if (isset($request->images[$k])) {
                        $picture = $request->images[$k];
                        $dataImg['image'] = $this->imageUploader($picture);
                    }
                    $dataImg['news_id'] = $newsResults->id;
                    $dataImg['description'] = $details;

                    $this->newsImageRepository->create($dataImg);
                }
            }

            return $newsResults;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $request
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function update($request, $id)
    {
        try {

            $picture = $request->file('main_image');
            if ($picture) {
                $data['main_image'] = $this->imageUploader($picture);
            }

            $data['category_id'] = $request->category_id;
            $data['title'] = $request->title;
            $data['description'] = $request->description;
            $data['added_date'] = $request->added_date;

            $newsResults = $this->repository->update($data, $id);

            NewsImage::where('news_id', $id)->update(['status' => 0]);

            foreach ($request->details as $k => $details) {

                if (isset($details)) {

                    $filename = null;
                    if ($request->hasFile('images')) {
                        if (isset($request->images[$k])) {

                            $picture = $request->images[$k];
                            $filename = $this->imageUploader($picture);
                        }
                    }

                    if (isset($request->image_id[$k])) {
                        $image = NewsImage::updateOrCreate(
                            [
                                'id' => $request->image_id[$k],
                                'news_id' => $id,
                            ],
                            [
                                'news_id' => $id,
                                'description' => $request->details[$k],
                                'status' => 1,
                            ]
                        );

                        if ($filename) {
                            $image->image = $filename;
                            $image->save();
                        }
                    } else {

                        $dataImg['image'] = $filename;
                        $dataImg['news_id'] = $id;
                        $dataImg['description'] = $request->details[$k];

                        $this->newsImageRepository->create($dataImg);
                    }
                }
            }

            return $newsResults;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function get($editId)
    {
        return $this->repository->show($editId);
    }

    /**
     * NEWS RELATED IMAGE UPLOADER
     * @param $picture
     * @return string
     */
    public function imageUploader($picture)
    {
        $fileName = Storage::disk('local')->putFile('/public/news_images', $picture);
        return basename($fileName);
    }

    /**
     * @param  null  $limit
     */
    public function getActiveAll($limit = null)
    {
        return $this->repository->getLatestNews($limit);
    }

    /**
     * please edit very carefully
     * using below methods
     *
     * Home page - latest 3 news
     * News page - main latest news
     * News page - right side news cards
     * @param  null  $categorySlug
     * @param  null  $newsSlug
     */
    public function getLatestNews($categorySlug = null, $newsSlug = null, $currantPageNo = null)
    {
        $taken = 10;
        $skip = 0;

        if ($currantPageNo > 1) {
            $taken = 10;
            $skip = ($currantPageNo-1) * 10;
        }

        $getData = $this->repository->getLatestNewsFilter($categorySlug, $newsSlug)
            ->skip($skip)
            ->take($taken);

        if ($newsSlug && $newsSlug != 'all') {
            $getData = $getData->where('slug', $newsSlug);
        }

        return $getData;
    }

    /**
     * @param  null  $categorySlug
     * @param  null  $newsSlug
     * @param  null  $currantPageNo
     * @return array
     */
    public function getCardFilterPageNews($categorySlug = null, $newsSlug = null, $currantPageNo = null)
    {
        $allNewsData = $this->repository->getLatestNewsFilter($categorySlug, $newsSlug);
        $allCount = $allNewsData->count();
        $noOfPage = (int) ceil($allCount / 10);

        $data['currentPage'] = $currantPageNo ?? 1;
        $data['noOfPage'] = ($noOfPage == 0) ? 1 : $noOfPage;
        $data['allCount'] = $allCount;
        $data['categorySlug'] = $categorySlug ?? 'all';
        $data['newsSlug'] = $newsSlug ?? 'all';

        //dd($data);
        return $data;
    }


}
