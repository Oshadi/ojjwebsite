<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubFaqRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question.*' => 'required|string',
            //'answer.*' => 'required|string',
        ];
    }

    public function messages()
    {
        return [
            'question.*.required' => 'The question field is required.',
            'answer.*.required' => 'The answer field is required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
