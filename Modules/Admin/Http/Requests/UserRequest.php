<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile' => 'required|string|min:10|max:10',
            'email' => 'required|email|unique:users,email,' . $this->id . ',id',
            'user_name' => 'required|unique:users,user_name,' . $this->id . ',id',
            'role' => 'required',
            'password' => 'required_if:passwordUpdate,on|string|min:6|same:conform_password',
            'conform_password' => 'required_if:passwordUpdate,on|string|min:6',
        ];
    }

    public function messages()
    {
        return [];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
