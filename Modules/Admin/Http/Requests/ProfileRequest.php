<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'name' => 'required|max:120|unique:permissions,name,' . $this->id . ',id',
            'firstName' => 'required|alpha',
            'lastName' => 'required|alpha',
            'mobile' => 'required',
            'email' => 'required|email:rfc,dns',
            'address1' => 'required',
            'address2' => 'required',
            'district' => 'required',
            'city' => 'required',
            'password'=>'required_if:passwordUpdate,on|min:6',
            'conformPass'=>'required_if:passwordUpdate,on|min:6|same:password',

            'businessName'=>'required_if:user_mode,2',
            'businessNature'=>'required_if:user_mode,2',
            'businessLogo'=>'required_if:user_mode,2|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function messages()
    {
        return [
            'businessName.required_if'=>'The business name field is required when user mode is Business Account.',
            'businessNature.required_if'=>'The business nature field is required when user mode is Business Account',
            'businessLogo.required_if'=>'The business logo field is required when user mode is Business Account',

            'password.required_if'=>'The password field is required.',
            'conformPass.required_if'=>'The conform pass field is required.',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
