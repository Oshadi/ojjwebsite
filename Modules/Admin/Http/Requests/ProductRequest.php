<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

use Illuminate\Http\Exceptions\HttpResponseException;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize():bool
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'main_image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'thumbnail_image'=> 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'table_1_name'=>'required',
            'table_1_capacity'=>'required',
            'table_1_tube_diameter'=>'required',
            'table_1_full_length'=>'required',
            'table_1_weight'=>'required',
            'table_1_full_width'=>'required',
            'table_1_transom_high'=>'required',
            'table_1_inner_width'=>'required',
            'table_2_name'=>'required',
            'table_2_ful_length'=>'required',
            'table_2_full_width'=>'required',
            'table_2_overall_height'=>'required',
            'table_2_weight'=>'required',
            'table_2_transom_high'=>'required',
            'table_2_maximum_output'=>'required',
            'table_2_recommended_rotation_when_fully_open'=>'required',
            'table_2_trolling_speed'=>'required',
            'table_2_engine_type'=>'required',
            'table_2_cylinder_array'=>'required',
            'table_2_boa'=>'required',
            'table_2_stroke'=>'required',
            'table_2_total_displacement'=>'required',
            'table_2_compression_ratio'=>'required',
            'table_2_spark_plug'=> 'required',
            'table_2_ignition_system'=>'required',
            'table_2_fuel_supply_method'=>'required',
            'table_2_exhaust_system'=>'required',
            'table_2_lubrication_method'=>'required',
            'table_2_starter'=>'required',
            'table_2_chalk_system'=>'required',
            'table_2_throttle_control'=>'required',
            'table_2_thermostat_valve_opening_temperature'=>'required',
            'table_2_fuel_used'=>'required',
            'table_2_engine_oil'=>'required',
            'table_2_engine_oil_grade'=>'required',
            'table_2_engine_oil_viscosity'=>'required',
            'table_2_engine_oil_specified_amount'=>'required',
            'table_2_specified_amount_of_engine_oil'=>'required',
            'table_2_jet_pump_grease'=>'required',
            'table_2_clutch_type'=>'required',
            'table_2_clutch_operation_method'=>'required',
            'table_2_gear_ratio'=>'required',
            'table_2_jet_impeller_rotation_direction'=>'required',


        ];
    }

    public function messages():array
    {
        return [
            'name.required' => 'This field is required',
            'main_image.required' => 'This field is required',
        ];
    }




    /**
     * Handle a failed validation attempt.
     *
     * @param  Validator  $validator
     * @return void
     *
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
                'popupType' => 'validation'
            ])
        );
    }
}
