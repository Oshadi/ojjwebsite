<?php

namespace Modules\Admin\Http\Controllers;

use App\BusinessNature;
use App\Constants\Actions;
use App\District;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

//Importing laravel-permission models
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Admin\Http\Requests\PermissionRequest;
use Modules\Admin\Http\Requests\ProfileRequest;

//Enables us to output flash messaging
use Modules\Admin\Services\UserService;
use Session;
use Spatie\Permission\Models\Role;

class ProfileController extends Controller
{
    protected $userService;
    private $baseRouteName = "admin.profile";


    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
//     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        checkPermissionRedirect(Actions::VIEW_PERMISSIONS);
//
        $data['title'] = "My Profile";
        $data['page_name'] = "My Profile";
        $data['icon'] = "icon-user-plus";
        $data['baseRoute'] = $this->baseRouteName;

        $data['user'] = Auth::user();
        $data['districts'] = District::all();
        $data['businessTypes'] = BusinessNature::all();

        return view('admin::profile.index', $data);

    }

    public function getData(Request $request)
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PermissionRequest $request
     */
    public function store(PermissionRequest $request)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {

    }

    /**
     * MY PROFILE UPDATE
     * Updated By : oshadhi
     * Updated At : 2022-04-28
     *
     * @param ProfileRequest $request
     */
    public function update(ProfileRequest $request)
    {
        try {
            DB::beginTransaction();
            //checkPermissionRedirect(Actions::EDIT_PERMISSIONS);

            $result = $this->userService->myProfileUpdate($request);

            DB::commit();
            logAction('', $result->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "redirect", 'User Profile updated successfully', [], "profile", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('ProfileController -- update() '.$ex);;
            return getFormattedResponce(false, "failed", 'User Profile updated failed', [$ex->getMessage()], "", "");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id) {
        try {
            $this->checkPermissionRedirect(Actions::DELETE_ROLES);

            $result = $this->service->deleteRole($id);

            if ($result['status'] == 'success') {
                $this->logAction(Actions::DELETE_ROLES, $id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");

                $outPutArray = array('status' => 'success', 'message' => 'Role deleted successfully', 'data' => '', 'redirect' => '', 'notifyType' => 'message');
                return $outPutArray;
            } else {
                $outPutArray = array('status' => 'error', 'message' => 'Something went wrong', 'data' => '', 'redirect' => '', 'notifyType' => 'message');
                return $outPutArray;
            }
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }*/

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
//        return view('auth::show');
    }
}
