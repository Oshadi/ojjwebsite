<?php

namespace Modules\Admin\Http\Controllers;

use App\Constants\Actions;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

//Importing laravel-permission models
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Modules\Admin\Http\Requests\PermissionRequest;
use Modules\Admin\Http\Requests\RoleRequest;
use Modules\Admin\Services\PermissionService;
use Modules\Admin\Services\RoleService;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    protected $permissionService;
    private $baseRouteName = "admin.permissions";

    /** @var array The names and relevent property name of columns in the table */
    private $columns = array(
        ['title' => 'ID', 'name' => 'id', "searchable" => true, 'width' => "10%", "visible" => true],
        ['title' => 'Permission', 'name' => 'name', "searchable" => true, 'width' => "30%", "visible" => "true"],
        ['title' => 'Role', 'name' => 'name', "searchable" => true, 'width' => "30%", "visible" => "true"],
        ['title' => 'Action', 'name' => 'action', "sortable" => false, 'width' => "20%", "searchable" => true, "className" => "text-center"],
    );

    public function __construct(PermissionService $permissionService, RoleService $roleService)
    {
        $this->service = $permissionService;
        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        checkPermissionRedirect(Actions::VIEW_PERMISSIONS);

        $data['title'] = "User Permissions";
        $data['page_name'] = "Permission";
        $data['icon'] = "icon-pencil7";
        $data['addRoute'] = $this->baseRouteName . ".create";
        $data['editRoute'] = $this->baseRouteName . ".edit";
        $data['form_type'] = "modal";
        $data['addPermission'] = checkHasPermission(Actions::CREATE_PERMISSIONS);
        //$data['export'] =  checkHasPermission(Actions::EXPORT_ROLES);
        $data['dataTableRoute'] = $this->baseRouteName . ".get-data";
        $data['export_route'] = "role/export";
        $columns = getTableColumns("role", auth()->user()->id);
        if ($columns == false) {
            $data['columns'] = json_encode($this->columns);
        } else {
            $data['columns'] = $columns;
        }

        return view('common.table-holder', $data);

    }

    public function getData(Request $request)
    {
        try {

            $input = $request->all();

            $offset = $input['start'];
            $limit = $input['length'];
            $search = $input['search']['value'];
            $columns = $this->columns;

            $orderBy = '';
            $orderDirection = '';
            if (isset($input['order'])) {
                $orderBy = getOrderByColumn($columns, $input['order'][0]['column']);
                $orderDirection = $input['order'][0]['dir'];
            }

            $data = $this->service->getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection);

            $data['draw'] = $input['draw'];

            return $data;
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        checkPermissionRedirect(Actions::CREATE_PERMISSIONS);

        $data['roles'] = Role::all();
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::permissions.create', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PermissionRequest $request
     */
    public function store(PermissionRequest $request)
    {
        try {
            DB::beginTransaction();
            checkPermissionRedirect(Actions::CREATE_PERMISSIONS);

            $result = $this->service->create($request);

            DB::commit();
            logAction(Actions::CREATE_PERMISSIONS, $result['permission']->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'Permission create successfully', [], "", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return getFormattedResponce(false, "failed", 'Permission updated failed', $ex->getMessage(), "", "");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        checkPermissionRedirect(Actions::EDIT_PERMISSIONS);

        $data['roles'] = Role::all();
        $data['permission'] = $this->service->get($id); //Get permissions with specified id
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::permissions.edit', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PermissionRequest $request
     * @param $id
     */
    public function update(PermissionRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            checkPermissionRedirect(Actions::EDIT_PERMISSIONS);

            $result = $this->service->update($request, $id);

            DB::commit();
            logAction(Actions::EDIT_PERMISSIONS, $result['permission']->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'Permission updated successfully', [], "", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return getFormattedResponce(false, "failed", 'Permission updated failed', [], "", "");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id) {
        try {
            $this->checkPermissionRedirect(Actions::DELETE_ROLES);

            $result = $this->service->deleteRole($id);

            if ($result['status'] == 'success') {
                $this->logAction(Actions::DELETE_ROLES, $id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");

                $outPutArray = array('status' => 'success', 'message' => 'Role deleted successfully', 'data' => '', 'redirect' => '', 'notifyType' => 'message');
                return $outPutArray;
            } else {
                $outPutArray = array('status' => 'error', 'message' => 'Something went wrong', 'data' => '', 'redirect' => '', 'notifyType' => 'message');
                return $outPutArray;
            }
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }*/

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
//        return view('auth::show');
    }
}