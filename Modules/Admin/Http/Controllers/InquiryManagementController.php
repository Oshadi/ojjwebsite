<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Admin\Services\InquiryManagementService;
use Illuminate\Support\Facades\View;
use Modules\Web\Entities\InquiryType;
use Exception;
use Session;

class InquiryManagementController extends Controller
{
    protected $inquiryType;
    protected $service;

    private $baseRouteName = "admin.inquiry-management";

    /** @var array The names and relevent property name of columns in the table */
    private $columns = array(
        ['title' => '#', 'name' => 'id', "searchable" => false, "visible" => true, "className" => "text-center"],
        ['title' => 'Name', 'name' => 'name', "searchable" => true, "visible" => true],
        ['title' => 'Furigana', 'name' => 'furigana', "searchable" => true, "visible" => true],
        ['title' => 'Company Name', 'name' => 'company_organization_name', "searchable" => true, "visible" => true],
        ['title' => 'Email', 'name' => 'email', "searchable" => true, "visible" => true],
        ['title' => 'Action', 'name' => 'action', "sortable" => false, "searchable" => true, "className" => "text-left"],
    );

    public function __construct(InquiryManagementService $inquiryManagementService, InquiryType $inquiryType)
    {
        $this->service = $inquiryManagementService;
        $this->inquiryType = $inquiryType;
    }

    /**
     * Display a listing of the resource.
     * Created by : oshadhi
     * Created at : 2022.07.28
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "Inquiry Management";
        $data['page_name'] = "Inquiry Management";
        $data['icon'] = "icon-question4";
        $data['form_type'] = "modal";
        $data['addPermission'] = false;
        $data['dataTableRoute'] = $this->baseRouteName . ".get-data";
        $data['export_route'] = "role/export";
        $columns = getTableColumns("role", auth()->user()->id);

        if ($columns == false) {
            $data['columns'] = json_encode($this->columns);
        }
        else {
            $data['columns'] = $columns;
        }

        return view('common.table-holder', $data);

    }

    /**
     *
     * Created by : oshadhi
     * Created at : 2022.07.28
     *
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function getData(Request $request)
    {
        try {
            $input = $request->All();

            $offset = $input['start'];
            $limit = $input['length'];
            $search = $input['search']['value'];
            $columns = $this->columns;
            $orderBy = '';
            $orderDirection = '';
            if (isset($input['order'])) {
                $orderBy = getOrderByColumn($columns, $input['order'][0]['column']);
                $orderDirection = $input['order'][0]['dir'];
            }
            $data = $this->service->getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection);
            $data['draw'] = $input['draw'];

            return $data;

        }
        catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }

    public function show($id)
    {
        $inquiryDetails = $this->service->specificInquiryById($id);
        $inquiryTypes = $this->inquiryType->all();

        $view = View::make('admin::inquiry_management.show')
            ->withInquiryDetails($inquiryDetails)
            ->withInquiryTypes($inquiryTypes)
            ->render();

        return array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
    }
}
