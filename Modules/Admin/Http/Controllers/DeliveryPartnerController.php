<?php

namespace Modules\Admin\Http\Controllers;

use App\Bank;
use App\Constants\Actions;
use App\DeliveryCompany;
use App\District;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller;

//Importing laravel-permission models
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Modules\Admin\Http\Requests\DeliveryPartnerRequest;
use Modules\Admin\Http\Requests\PermissionRequest;
use Modules\Admin\Services\DeliveryPartnerService;

//Enables us to output flash messaging
use Session;
use Spatie\Permission\Models\Role;

class DeliveryPartnerController extends Controller
{
    protected $deliveryPartnerService;
    private $baseRouteName = "admin.delivery-partner";

    /** @var array The names and relevent property name of columns in the table */
    private $columns = array(
        ['title' => '#', 'name' => 'id', "searchable" => false, "visible" => true, "className" => "text-center"],
        ['title' => 'Company Name', 'name' => 'name', "searchable" => true, "visible" => true],
        ['title' => 'Company Address', 'name' => 'address', "searchable" => true, "visible" => "true"],
        ['title' => 'BR NO', 'name' => 'br_number', "searchable" => true, "visible" => "true"],
        ['title' => 'Contact No', 'name' => 'mobile', "searchable" => true, "visible" => "true"],
        ['title' => 'Email', 'name' => 'email', "searchable" => true, "visible" => "true"],
        ['title' => 'API', 'name' => '', "searchable" => true, "visible" => "true", "className" => "text-center"],
        //['title' => 'Status', 'name' => 'status', "searchable" => true, "visible" => "true"],
        ['title' => 'Action', 'name' => 'action', "sortable" => false, "searchable" => true, "className" => "text-center"],
    );

    public function __construct(DeliveryPartnerService $deliveryPartnerService)
    {
        $this->service = $deliveryPartnerService;
        //$this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //checkPermissionRedirect(Actions::VIEW_PARTNERS);

        $data['title'] = "Delivery Partners";
        $data['page_name'] = "Delivery Partners";
        $data['icon'] = "icon-truck";
        $data['addRoute'] = $this->baseRouteName . ".create";
        $data['editRoute'] = $this->baseRouteName . ".edit";
        $data['form_type'] = "modal";
        $data['addPermission'] = true;
        //$data['export'] =  checkHasPermission(Actions::EXPORT_ROLES);
        $data['dataTableRoute'] = $this->baseRouteName . ".get-data";
        $data['export_route'] = "role/export";
        $columns = getTableColumns("role", auth()->user()->id);
        if ($columns == false) {
            $data['columns'] = json_encode($this->columns);
        } else {
            $data['columns'] = $columns;
        }

        return view('common.table-holder', $data);

    }

    public function getData(Request $request)
    {
        try {

            $input = $request->all();

            $offset = $input['start'];
            $limit = $input['length'];
            $search = $input['search']['value'];
            $columns = $this->columns;

            $orderBy = '';
            $orderDirection = '';
            if (isset($input['order'])) {
                $orderBy = getOrderByColumn($columns, $input['order'][0]['column']);
                $orderDirection = $input['order'][0]['dir'];
            }

            $data = $this->service->getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection);

            $data['draw'] = $input['draw'];

            return $data;
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        checkPermissionRedirect(Actions::CREATE_PARTNERS);

        $data['districts'] = District::all();
        $data['banks'] = Bank::whereStatus(1)->orderBy('name', 'asc')->get();
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::delivery_partners.create', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DeliveryPartnerRequest $request
     */
    public function store(DeliveryPartnerRequest $request)
    {
        try {
            DB::beginTransaction();
            checkPermissionRedirect(Actions::CREATE_PARTNERS);

            $result = $this->service->store($request);

            DB::commit();
            logAction(Actions::CREATE_PARTNERS, $result->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'Delivery Partner create successfully', [], "", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('DeliveryPartnerController - store() ' . $ex);
            return getFormattedResponce(false, "failed", 'Delivery Partner create failed', $ex->getMessage(), "", "");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        checkPermissionRedirect(Actions::EDIT_PARTNERS);

        $data['partner'] = $this->service->get($id); //Get permissions with specified id
        $data['districts'] = District::all();
        $data['banks'] = Bank::whereStatus(1)->orderBy('name', 'asc')->get();
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::delivery_partners.edit', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * Update the specified resource in storage.
     * @param DeliveryPartnerRequest $request
     * @param $id
     */
    public function update(DeliveryPartnerRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            checkPermissionRedirect(Actions::EDIT_PARTNERS);

            $result = $this->service->update($request, $id);

            DB::commit();
            logAction(Actions::EDIT_PARTNERS, $result->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'Delivery Partner updated successfully', [], "", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('DeliveryPartnerController - update() ' . $ex);
            return getFormattedResponce(false, "failed", 'Delivery Partner updated failed', $ex->getMessage(), "", "");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id) {
        try {
            $this->checkPermissionRedirect(Actions::DELETE_ROLES);

            $result = $this->service->deleteRole($id);

            if ($result['status'] == 'success') {
                $this->logAction(Actions::DELETE_ROLES, $id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");

                $outPutArray = array('status' => 'success', 'message' => 'Role deleted successfully', 'data' => '', 'redirect' => '', 'notifyType' => 'message');
                return $outPutArray;
            } else {
                $outPutArray = array('status' => 'error', 'message' => 'Something went wrong', 'data' => '', 'redirect' => '', 'notifyType' => 'message');
                return $outPutArray;
            }
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }*/

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        checkPermissionRedirect(Actions::VIEW_PARTNERS);

        $data['partner'] = $this->service->get($id); //Get permissions with specified id
        $data['districts'] = District::all();
        $data['banks'] = Bank::whereStatus(1)->orderBy('name', 'asc')->get();
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::delivery_partners.show', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;

    }

    /**
     * @param $activeId
     */
    public function active($activeId)
    {
        active(DeliveryCompany::class, $activeId);
        logAction(Actions::EDIT_PARTNERS, $activeId, json_encode(\Illuminate\Support\Facades\Request::all()), '', "");
        return getFormattedResponce(true, "success", 'Activated Successfully.', [], "", "");
    }

    /**
     * @param $inactiveId
     */
    public function inactive($inactiveId)
    {
        inactive(DeliveryCompany::class, $inactiveId);
        logAction(Actions::EDIT_PARTNERS, $inactiveId, json_encode(\Illuminate\Support\Facades\Request::all()), '', "");
        return getFormattedResponce(true, "success", 'Inactivated Successfully.', [], "", "");
    }
}
