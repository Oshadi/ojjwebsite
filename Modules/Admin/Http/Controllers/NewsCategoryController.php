<?php

namespace Modules\Admin\Http\Controllers;

use App\NewsCategory;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Importing laravel-permission models
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

//Enables us to output flash messaging
use Modules\Admin\Http\Requests\NewsCategoryRequest;
use Modules\Admin\Services\NewsCategoryService;

class NewsCategoryController extends Controller
{
    protected $deliveryPartnerService;
    private $baseRouteName = "admin.news-category";

    /** @var array The names and relevent property name of columns in the table */
    private $columns = array(
        ['title' => '#', 'name' => 'id', "searchable" => false, "visible" => true, "className" => "text-center"],
        ['title' => 'Category Name', 'name' => 'name', "searchable" => true, "visible" => true],
        ['title' => 'Status', 'name' => 'status', "searchable" => true, "visible" => "true"],
        ['title' => 'Action', 'name' => 'action', "sortable" => false, "searchable" => true, "className" => "text-center"],
    );

    public function __construct(NewsCategoryService $newsCategoryService)
    {
        $this->service = $newsCategoryService;
    }

    /**
     *
     */
    public function index()
    {
        $data['title'] = "News Category Management";
        $data['page_name'] = "News Category Management";
        $data['icon'] = "icon-tree7";
        $data['addRoute'] = $this->baseRouteName . ".create";
        $data['editRoute'] = $this->baseRouteName . ".edit";
        $data['form_type'] = "modal";
        $data['addPermission'] = true;
        //$data['export'] =  checkHasPermission(Actions::EXPORT_ROLES);
        $data['dataTableRoute'] = $this->baseRouteName . ".get-data";
        $data['export_route'] = "role/export";
        $columns = getTableColumns("role", auth()->user()->id);
        if ($columns == false) {
            $data['columns'] = json_encode($this->columns);
        } else {
            $data['columns'] = $columns;
        }

        return view('common.table-holder', $data);
    }

    /**
     * @param  Request  $request
     * @return array
     */
    public function getData(Request $request)
    {
        try {

            $input = $request->all();

            $offset = $input['start'];
            $limit = $input['length'];
            $search = $input['search']['value'];
            $columns = $this->columns;

            $orderBy = '';
            $orderDirection = '';
            if (isset($input['order'])) {
                $orderBy = getOrderByColumn($columns, $input['order'][0]['column']);
                $orderDirection = $input['order'][0]['dir'];
            }

            $data = $this->service->getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection);

            $data['draw'] = $input['draw'];

            return $data;
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::news_category.create', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * Store a newly created resource in storage.
     * @param  NewsCategoryRequest  $request
     * @return array|void
     */
    public function store(NewsCategoryRequest $request)
    {
        try {
            DB::beginTransaction();

            $result = $this->service->store($request);

            DB::commit();
            logAction('Add New News Category', $result->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'New create successfully', [], "", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('NewsCategoryController - store() ' . $ex);
            return getFormattedResponce(false, "failed", 'New create failed', $ex->getMessage(), "", "");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $data['getData'] = $this->service->get($id); //Get permissions with specified id
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::news_category.edit', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * @param  NewsCategoryRequest  $request
     * @param $id
     * @return array|void
     */
    public function update(NewsCategoryRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            //checkPermissionRedirect(Actions::EDIT_PARTNERS);

            $result = $this->service->update($request, $id);

            DB::commit();
            logAction('Edit News Category', $id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'Updated successfully', [], "", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('NewsCategoryController - update() ' . $ex);
            return getFormattedResponce(false, "failed", 'Updated failed', $ex->getMessage(), "", "");
        }
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {


    }

    /**
     * @param $activeId
     * @return array|void
     */
    public function active($activeId)
    {
        active(NewsCategory::class, $activeId);
        logAction('Edit', $activeId, json_encode(\Illuminate\Support\Facades\Request::all()), '', "");
        return getFormattedResponce(true, "success", 'Activated Successfully.', [], "", "");
    }

    /**
     * @param $inactiveId
     * @return array|void
     */
    public function inactive($inactiveId)
    {
        inactive(NewsCategory::class, $inactiveId);
        logAction('Edit', $inactiveId, json_encode(\Illuminate\Support\Facades\Request::all()), '', "");
        return getFormattedResponce(true, "success", 'Inactivated Successfully.', [], "", "");
    }
}
