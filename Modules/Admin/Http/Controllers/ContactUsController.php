<?php

namespace Modules\Admin\Http\Controllers;

use App\Constants\Actions;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller;

//Importing laravel-permission models
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Modules\Admin\Services\ContactUsService;
use Modules\Web\Entities\ContactUsDetail;

//Enables us to output flash messaging
use Session;
use Spatie\Permission\Models\Role;

class ContactUsController extends Controller
{

    private $baseRouteName = "admin.web-content";


    public function __construct(ContactUsService $contactUsService)
    {
        $this->service = $contactUsService;

    }

    /**
     * Display a listing of the resource.
     *
     * //     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        checkPermissionRedirect(Actions::WEB_CONTENT_MANAGEMENT);

        $data['title'] = "Contact Management";
        $data['page_name'] = "Contact Management";
        $data['icon'] = "icon-users";
        $data['addRoute'] = $this->baseRouteName . ".create";
        $data['editRoute'] = $this->baseRouteName . ".edit";
        $data['baseRoute'] = $this->baseRouteName;
        $data['form_type'] = "modal";
        $data['dataTableRoute'] = $this->baseRouteName . ".get-data";


      $data['contactUs'] = ContactUsDetail::whereStatus(1)->first();

        return view('admin::web_content.index', $data);

    }
    /**
     * Created by : oshadhi
     * Created at :
     * Summary: CONTACT US UPDATE
     *
     * @param Request $request
     */
    public function updateContactUs(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'address_line_1' => 'required',
            'address_line_2' => 'required',
            'email' => 'required',
            'business_content' => 'required',
            'contact' => 'required',

        ]);

        return $this->service->updateContactUs($request);
    }
}
