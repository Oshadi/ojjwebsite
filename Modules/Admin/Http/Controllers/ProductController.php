<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Modules\Admin\Entities\Product;
use Modules\Admin\Http\Requests\ProductRequest;
use Modules\Admin\Services\ProductService;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\JsonResponse;
use http\Exception;
class ProductController extends Controller
{
    protected $productService;
    private $baseRouteName = "admin.product";

    /** @var array The names and relevent property name of columns in the table */
    private $columns = array(
        ['title' => '#', 'name' => 'id', "searchable" => false, "visible" => true, "className" => "text-center"],
        ['title' => 'Product Name', 'name' => 'product_name', "searchable" => true, "visible" => true],
        ['title' => 'Date', 'name' => 'date', "searchable" => true, "visible" => true],
        ['title' => 'Status', 'name' => 'status', "searchable" => true, "visible" => "true"],
        ['title' => 'Action', 'name' => 'action', "sortable" => false, "searchable" => true, "className" => "text-center"],
    );


    public function __construct(ProductService $productService)
    {
        try {
                $this->productService = $productService;
        } catch (\Exception $ex) {

            Log::error("ProductController (construct) : " . $ex);
            return getFormattedResponce(false, "failed", 'Updated failed', $ex->getMessage(), "", "");
        }

    }

    public function index()
    {
        $data['title'] = "Product Management";
        $data['page_name'] = "Product Management";
        $data['icon'] = "icon-gift";
        $data['addRoute'] = $this->baseRouteName . ".create";
        $data['editRoute'] = $this->baseRouteName . ".edit";
        $data['form_type'] = "modal";
        $data['modalSize'] = "modal-lg";
        $data['addPermission'] = true;
        //$data['export'] =  checkHasPermission(Actions::EXPORT_ROLES);
        $data['dataTableRoute'] = $this->baseRouteName . ".get-data";
        $data['export_route'] = "role/export";
        $columns = getTableColumns("role", auth()->user()->id);
        if ($columns == false) {
            $data['columns'] = json_encode($this->columns);
        } else {
            $data['columns'] = $columns;
        }

        return view('common.table-holder3', $data);

    }

    public function getData(Request $request)
    {
        try {

            $input = $request->all();

            $offset = $input['start'];
            $limit = $input['length'];
            $search = $input['search']['value'];
            $columns = $this->columns;

            $orderBy = '';
            $orderDirection = '';
            if (isset($input['order'])) {
                $orderBy = getOrderByColumn($columns, $input['order'][0]['column']);
                $orderDirection = $input['order'][0]['dir'];
            }

            $data = $this->productService->getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection);

            $data['draw'] = $input['draw'];

            return $data;
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }

    public function create()
    {
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::product.create', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;



    }

    public function store(ProductRequest $request)
    {

        try {
            DB::beginTransaction();

            $result = $this->productService->store($request);


            DB::commit();

            logAction('Add Product ', $result->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");


            return getFormattedResponce(true, "success", 'New create successfully', [], "", "");





        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('ProductController - store() ' . $ex);
            return getFormattedResponce(false, "success", 'New create success', $ex->getMessage(), "", "");
        }
    }

    public function edit(Request $request, $id)
    {

        $data['getData'] = $this->productService->get($id); //Get permissions with specified id
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::product.edit', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    public function update(ProductRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $result = $this->productService->update($request, $id);

            DB::commit();
            logAction('Edit Product', $id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'Updated successfully', [], route('admin.news.index'), "");


        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('NewsController - update() ' . $ex);
            return getFormattedResponce(false, "failed", 'Updated failed', $ex->getMessage(), "", "");
        }
    }

    public function deleteImage(Request $request)
    {
        try {

           $this->productService->deleteImage($request);
            return Response::json(['status' => 'success', 'data' => 'Status changed successfully']);



        } catch (Exception $e) {
            return Response::json(['status' => 'error', 'data' => $e->getMessage()]);
        }
    }

    public function active($activeId){
        active(Product::class, $activeId);
        logAction('Edit', $activeId, json_encode(\Illuminate\Support\Facades\Request::all()), '', "");
        return getFormattedResponce(true, "success", 'Activated Successfully.', [], "", "");

    }

    public function inactive($inactiveId)
    {
        inactive(Product::class, $inactiveId);
        logAction('Edit', $inactiveId, json_encode(\Illuminate\Support\Facades\Request::all()), '', "");
        return getFormattedResponce(true, "success", 'Inactivated Successfully.', [], "", "");
    }


}
