<?php

namespace Modules\Admin\Http\Controllers;

use App\Constants\Actions;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;

//Importing laravel-permission models
use Modules\Admin\Services\DashboardService;

class DashboardController extends Controller
{
    protected $service;

    public function __construct(DashboardService $dashboardService)
    {
        $this->service = $dashboardService;
    }

    /**
     * DASHBOARD
     */
    public function index()
    {
        $data['title'] = "Dashboard";
        $data['page_name'] = "Dashboard";
        $data['icon'] = "icon-home2";

        $data['totalProducts'] = $this->service->getTotalProducts();
        $data['totalNewsCategory'] = $this->service->getTotalNewsCategory();
        $data['totalNews'] = $this->service->getTotalNews();
        $data['totalInquires'] =$this->service->getTotalInquires();

        Artisan::call('cache:clear');

        return view('admin::dashboard.admin-home', $data);

    }

}
