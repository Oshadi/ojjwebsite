<?php

namespace Modules\Admin\Http\Controllers;

use App\Constants\Actions;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

//Importing laravel-permission models
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Modules\Admin\Http\Requests\PermissionRequest;
use Modules\Admin\Http\Requests\RoleRequest;
use Modules\Admin\Http\Requests\UserRequest;
use Modules\Admin\Services\PermissionService;
use Modules\Admin\Services\RoleService;
use Modules\Admin\Services\UserService;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    protected $service;
    private $baseRouteName = "admin.users";

    /** @var array The names and relevent property name of columns in the table */
    private $columns = array(
        ['title' => 'ID', 'name' => 'id', "searchable" => true, 'width' => "10%", "visible" => true],
        ['title' => 'Name', 'name' => 'display_name', "searchable" => true, 'width' => "30%", "visible" => "true"],
        ['title' => 'Email', 'name' => 'email', "searchable" => true, 'width' => "30%", "visible" => "true"],
        ['title' => 'Mobile', 'name' => 'mobile', "searchable" => true, 'width' => "30%", "visible" => "true"],
        ['title' => 'Role', 'name' => 'email', "searchable" => true, 'width' => "30%", "visible" => "true"],
        ['title' => 'Action', 'name' => 'action', "sortable" => false, 'width' => "20%", "searchable" => true, "className" => "text-center"],
    );

    public function __construct(UserService $userService)
    {
        $this->service = $userService;
        //$this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        checkPermissionRedirect(Actions::VIEW_USER);

        $data['title'] = "Users";
        $data['page_name'] = "Users";
        $data['icon'] = "icon-people";
        $data['addRoute'] = $this->baseRouteName . ".create";
        $data['editRoute'] = $this->baseRouteName . ".edit";
        $data['form_type'] = "modal";
        $data['modalSize'] = "modal-md";
        $data['addPermission'] = checkHasPermission(Actions::CREATE_USER);
        //$data['export'] =  checkHasPermission(Actions::EXPORT_ROLES);
        $data['dataTableRoute'] = $this->baseRouteName . ".get-data";
        $data['export_route'] = "role/export";
        $columns = getTableColumns("role", auth()->user()->id);
        if ($columns == false) {
            $data['columns'] = json_encode($this->columns);
        } else {
            $data['columns'] = $columns;
        }

        return view('common.table-holder', $data);

    }

    public function getData(Request $request)
    {
        try {

            $input = $request->all();

            $offset = $input['start'];
            $limit = $input['length'];
            $search = $input['search']['value'];
            $columns = $this->columns;

            $orderBy = '';
            $orderDirection = '';
            if (isset($input['order'])) {
                $orderBy = getOrderByColumn($columns, $input['order'][0]['column']);
                $orderDirection = $input['order'][0]['dir'];
            }

            $data = $this->service->getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection);

            $data['draw'] = $input['draw'];

            return $data;
        } catch (\Exception $ex) {
            Log::error('UserController -- getData() '.$ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        checkPermissionRedirect(Actions::CREATE_USER);

        $data['roles'] = Role::all()->except(3);
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::user.create', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return array|void
     */
    public function store(UserRequest $request)
    {
        try {
            DB::beginTransaction();
            checkPermissionRedirect(Actions::CREATE_USER);

            $result = $this->service->create($request);

            DB::commit();
            logAction(Actions::CREATE_USER, $result['user']->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'User create successfully', [], "", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('UserController -- store() '.$ex);
            return getFormattedResponce(false, "failed", 'User updated failed', $ex->getMessage(), "", "");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        checkPermissionRedirect(Actions::EDIT_USER);

        $data['roles'] = Role::all()->except(3);
        $data['user'] = $this->service->get($id); //Get permissions with specified id
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::user.edit', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PermissionRequest $request
     * @param $id
     */
    public function update(UserRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            checkPermissionRedirect(Actions::EDIT_USER);

            $result = $this->service->update($request, $id);

            DB::commit();
            logAction(Actions::EDIT_USER, $result['user']->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'User updated successfully', [], "", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('UserController -- update() '.$ex);
            return getFormattedResponce(false, "failed", 'User updated failed ' . $ex->getMessage(), $ex->getMessage(), "", "");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id) {
        try {
            $this->checkPermissionRedirect(Actions::DELETE_ROLES);

            $result = $this->service->deleteRole($id);

            if ($result['status'] == 'success') {
                $this->logAction(Actions::DELETE_ROLES, $id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");

                $outPutArray = array('status' => 'success', 'message' => 'Role deleted successfully', 'data' => '', 'redirect' => '', 'notifyType' => 'message');
                return $outPutArray;
            } else {
                $outPutArray = array('status' => 'error', 'message' => 'Something went wrong', 'data' => '', 'redirect' => '', 'notifyType' => 'message');
                return $outPutArray;
            }
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }*/

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
//        return view('auth::show');
    }
}
