<?php

namespace Modules\Admin\Http\Controllers;

use App\Constants\Actions;
use Illuminate\Http\Request;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

//Importing laravel-permission models
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Modules\Admin\Http\Requests\RoleRequest;
use Modules\Admin\Services\RoleService;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;

class RoleController extends Controller
{
    protected $roleService;
    private $baseRouteName = "admin.roles";

    /** @var array The names and relevent property name of columns in the table */
    private $columns = array(
        ['title' => 'ID', 'name' => 'id', "searchable" => true, 'width' => "10%", "visible" => true],
        ['title' => 'Role', 'name' => 'name', "searchable" => true, 'width' => "30%", "visible" => "true"],
        ['title' => 'Permission', 'name' => 'name', "searchable" => true, 'width' => "30%", "visible" => "true"],
        ['title' => 'Action', 'name' => 'action', "sortable" => false, 'width' => "20%", "searchable" => true, "className" => "text-center"],
    );

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        checkPermissionRedirect(Actions::VIEW_ROLES);

        $data['title'] = "User Role";
        $data['page_name'] = "role";
        $data['icon'] = "icon-users";
        $data['addRoute'] = $this->baseRouteName . ".create";
        $data['editRoute'] = "roles.edit";
        $data['form_type'] = "modal";
        $data['addPermission'] = checkHasPermission(Actions::CREATE_ROLES);
        //$data['export'] =  checkHasPermission(Actions::EXPORT_ROLES);
        $data['dataTableRoute'] = $this->baseRouteName . ".get-data";
        $data['export_route'] = "role/export";
        $columns = getTableColumns("role", auth()->user()->id);
        if ($columns == false) {
            $data['columns'] = json_encode($this->columns);
        } else {
            $data['columns'] = $columns;
        }

        return view('common.table-holder', $data);

    }

    public function getData(Request $request)
    {
        try {

            $input = $request->all();

            $offset = $input['start'];
            $limit = $input['length'];
            $search = $input['search']['value'];
            $columns = $this->columns;

            $orderBy = '';
            $orderDirection = '';
            if (isset($input['order'])) {
                $orderBy = getOrderByColumn($columns, $input['order'][0]['column']);
                $orderDirection = $input['order'][0]['dir'];
            }

            $data = $this->roleService->getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection);

            $data['draw'] = $input['draw'];

            return $data;
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        checkPermissionRedirect(Actions::CREATE_ROLES);

        //Get all roles and pass it to the view
        $data['permissions'] = Permission::get();
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::roles.create', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleRequest $request
     */
    public function store(RoleRequest $request)
    {
        try {
            DB::beginTransaction();
            checkPermissionRedirect(Actions::CREATE_ROLES);

            $result = $this->roleService->createRole($request);

            DB::commit();
            logAction(Actions::CREATE_ROLES, $result['role']->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'Role create successfully', [], "", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return getFormattedResponce(false, "failed", 'Role updated failed', $ex->getMessage(), "", "");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        checkPermissionRedirect(Actions::EDIT_ROLES);

        $data['role'] = $this->roleService->getRole($id); //Get role with specified id
        $data['permissions'] = Permission::get(); //Get all permissions
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::roles.edit', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleRequest $request
     * @param $id
     * @return array
     */
    public function update(RoleRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            checkPermissionRedirect(Actions::EDIT_ROLES);

            $result = $this->roleService->updateRole($request, $id);

            DB::commit();
            logAction(Actions::EDIT_ROLES, $result['role']->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "success", 'Role updated successfully', [], "", "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error($ex);
            return getFormattedResponce(false, "failed", 'Role updated failed', [], "", "");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id) {
        try {
            $this->checkPermissionRedirect(Actions::DELETE_ROLES);

            $result = $this->roleService->deleteRole($id);

            if ($result['status'] == 'success') {
                $this->logAction(Actions::DELETE_ROLES, $id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");

                $outPutArray = array('status' => 'success', 'message' => 'Role deleted successfully', 'data' => '', 'redirect' => '', 'notifyType' => 'message');
                return $outPutArray;
            } else {
                $outPutArray = array('status' => 'error', 'message' => 'Something went wrong', 'data' => '', 'redirect' => '', 'notifyType' => 'message');
                return $outPutArray;
            }
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }*/

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
//        return view('auth::show');
    }
}
