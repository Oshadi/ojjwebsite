<?php

namespace Modules\Admin\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Importing laravel-permission models
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

//Enables us to output flash messaging
use Modules\Admin\Http\Requests\NewsRequest;
use Modules\Admin\Services\NewsCategoryService;
use Modules\Admin\Services\NewsService;

class NewsController extends Controller
{
    protected $deliveryPartnerService;
    private $baseRouteName = "admin.news";

    /** @var array The names and relevent property name of columns in the table */
    private $columns = array(
        ['title' => '#', 'name' => 'id', "searchable" => false, "visible" => true, "className" => "text-center"],
        ['title' => 'News Title', 'name' => 'title', "searchable" => true, "visible" => true],
        ['title' => 'News Category', 'name' => '', "searchable" => false, "visible" => true],
        ['title' => 'News Description', 'name' => 'Description', "searchable" => true, "visible" => true],
        ['title' => 'News Added Date', 'name' => 'added_date', "searchable" => true, "visible" => true],
        ['title' => 'Status', 'name' => 'status', "searchable" => true, "visible" => "true"],
        ['title' => 'Action', 'name' => 'action', "sortable" => false, "searchable" => true, "className" => "text-center"],
    );

    public function __construct(NewsService $newsService, NewsCategoryService $newsCategoryService)
    {
        $this->newsService = $newsService;
        $this->categoryService = $newsCategoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "News Management";
        $data['page_name'] = "News Management";
        $data['icon'] = "icon-newspaper";
        $data['addRoute'] = $this->baseRouteName . ".create";
        $data['editRoute'] = $this->baseRouteName . ".edit";
        $data['form_type'] = "modal";
        $data['modalSize'] = "modal-lg";
        $data['addPermission'] = true;
        $data['dataTableRoute'] = $this->baseRouteName . ".get-data";
        $data['export_route'] = "role/export";
        $columns = getTableColumns("role", auth()->user()->id);
        if ($columns == false) {
            $data['columns'] = json_encode($this->columns);
        } else {
            $data['columns'] = $columns;
        }

        return view('common.table-holder', $data);

    }

    public function getData(Request $request)
    {
        try {

            $input = $request->all();

            $offset = $input['start'];
            $limit = $input['length'];
            $search = $input['search']['value'];
            $columns = $this->columns;

            $orderBy = '';
            $orderDirection = '';
            if (isset($input['order'])) {
                $orderBy = getOrderByColumn($columns, $input['order'][0]['column']);
                $orderDirection = $input['order'][0]['dir'];
            }

            $data = $this->newsService->getPaginated($columns, $offset, $limit, $search, $orderBy, $orderDirection);

            $data['draw'] = $input['draw'];

            return $data;
        } catch (\Exception $ex) {
            Log::error($ex);
            $outPutArray = array('status' => 'error', 'message' => $ex->getMessage(), 'data' => '', 'redirect' => '', 'notifyType' => 'message');
            return $outPutArray;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */
    public function create()
    {
        $data['baseRouteName'] = $this->baseRouteName;
        $data['newsCategories'] = $this->categoryService->getActiveAll();

        $view = View::make('admin::news.create', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * Store a newly created resource in storage.
     * @param  NewsRequest  $request
     * @return array|void
     */
    public function store(NewsRequest $request)
    {
        try {
            DB::beginTransaction();

            $result = $this->newsService->store($request);

            DB::commit();
            logAction('Add News ', $result->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "redirect", 'New create successfully', [], route('admin.news.index'), "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('NewsController - store() ' . $ex);
            return getFormattedResponce(false, "failed", 'New create failed', $ex->getMessage(), "", "");
        }
    }

    /**
     * @param  Request  $request
     * @param $id
     * @return array
     */
    public function edit(Request $request, $id)
    {
        $data['getData'] = $this->newsService->get($id); //Get permissions with specified id
        $data['newsCategories'] = $this->categoryService->getActiveAll();
        $data['baseRouteName'] = $this->baseRouteName;

        $view = View::make('admin::news.edit', $data)->render();
        $outPutArray = array('status' => 'success', 'message' => '', 'data' => $view, 'redirect' => '', 'notifyType' => 'modal');
        return $outPutArray;
    }

    /**
     * @param  NewsRequest  $request
     * @param $id
     * @return array|void
     */
    public function update(NewsRequest $request, $id)
    {
        try {
            DB::beginTransaction();

            $result = $this->newsService->update($request, $id);

            DB::commit();
            logAction('Edit News', $id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "redirect", 'Updated successfully', [], route('admin.news.index'), "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('NewsController - update() ' . $ex);
            return getFormattedResponce(false, "failed", 'Updated failed', $ex->getMessage(), "", "");
        }
    }


    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {


    }

    /**
     * @param $activeId
     * @return array|void
     */
    public function active($activeId)
    {
        active(News::class, $activeId);
        logAction('Active News', $activeId, json_encode(\Illuminate\Support\Facades\Request::all()), '', "");
        return getFormattedResponce(true, "success", 'Activated Successfully.', [], "", "");
    }

    /**
     * @param $inactiveId
     * @return array|void
     */
    public function inactive($inactiveId)
    {
        inactive(News::class, $inactiveId);
        logAction('Inactive News', $inactiveId, json_encode(\Illuminate\Support\Facades\Request::all()), '', "");
        return getFormattedResponce(true, "success", 'Inactivated Successfully.', [], "", "");
    }
}
