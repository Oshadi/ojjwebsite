<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Importing laravel-permission models
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;


//Enables us to output flash messaging
use Modules\Admin\Http\Requests\MainFaqRequest;
use Modules\Admin\Http\Requests\NewsRequest;
use Modules\Admin\Http\Requests\SubFaqRequest;
use Modules\Admin\Services\FaqService;

class FaqController extends Controller
{
    protected $deliveryPartnerService;
    private $baseRouteName = "admin.faq";


    public function __construct(FaqService $faqService)
    {
        $this->faqService = $faqService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $data['title'] = "Q & A Management";
        $data['page_name'] = "Q & A Management";
        $data['icon'] = "icon-question7";
        $data['form_type'] = "modal";
        $data['modalSize'] = "modal-lg";
        $data['addPermission'] = true;
        $data['dataTableRoute'] = $this->baseRouteName . ".get-data";
        $data['baseRoute'] = $this->baseRouteName;

        $data['mainFaqs'] = $this->faqService->getAllQuestion("MAIN");
        $data['subFaqs'] = $this->faqService->getAllQuestion("SUB");

        return view('admin::faq.index', $data);

    }

    /**
     * @param  MainFaqRequest  $request
     * @return array|void
     */
    public function updateMainFaq(MainFaqRequest $request)
    {
        try {
            DB::beginTransaction();
            $request['mode'] = "MAIN";
            $result = $this->faqService->update($request);

            DB::commit();
            logAction('Frequently Asked Questions Update ', $result->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "redirect", 'Frequently Asked Questions Update successfully', [], route('admin.faq.index'), "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('FaqController - updateMainFaq() ' . $ex);
            return getFormattedResponce(false, "failed", 'Questions Update failed', $ex->getMessage(), "", "");
        }
    }

    /**
     * @param  SubFaqRequest  $request
     * @return array|void
     */
    public function updateSubFaq(SubFaqRequest $request)
    {
        try {
            DB::beginTransaction();
            $request['mode'] = "SUB";
            $result = $this->faqService->update($request);

            DB::commit();
            logAction('Inquiries Questions Update ', $result->id, json_encode(\Illuminate\Support\Facades\Request::all()), json_encode($result), "");
            return getFormattedResponce(true, "redirect", 'Inquiries Questions Update successfully', [], route('admin.faq.index'), "");

        } catch (\Exception $ex) {
            DB::rollBack();
            Log::error('FaqController - updateSubFaq() ' . $ex);
            return getFormattedResponce(false, "failed", 'Questions Update failed', $ex->getMessage(), "", "");
        }
    }
}
