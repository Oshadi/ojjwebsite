<?php

namespace Modules\Admin\Repositories;

use App\Repositories\Repository;
use Modules\Web\Entities\Inquiry;

class InquiryManagementRepository extends Repository
{
    protected $inquiry;

    public function __construct(Inquiry $inquiry)
    {
        // the model instance can be accessed with "$this->model" variable
        parent::__construct($inquiry);
        $this->inquiry = $inquiry;
    }

    /**
     *
     * Created by : oshadhi
     * Created at : 2022.07.28
     * Summary: get specific inquiry using ID
     *
     * @param $id
     * @return mixed
     */
    public function getSpecificInquiryById($id)
    {
        return $this->inquiry->find($id);
    }
}
