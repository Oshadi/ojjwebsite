<?php

namespace Modules\Admin\Repositories;

use App\Repositories\Repository;
use App\User;
use Spatie\Permission\Models\Permission;

class UserRepository extends Repository
{
    public function __construct(User $user)
    {
        // the model instance can be accessed with "$this->model" variable
        parent::__construct($user);
    }


}
