<?php

namespace Modules\Admin\Repositories;

use App\NewsCategory;
use App\Repositories\Repository;

class NewsCategoryRepository extends Repository
{
    public function __construct(NewsCategory $newsCategory)
    {
        // the model instance can be accessed with "$this->model" variable
        parent::__construct($newsCategory);
    }


}
