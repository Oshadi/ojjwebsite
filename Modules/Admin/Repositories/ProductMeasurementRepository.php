<?php

namespace Modules\Admin\Repositories;

use App\Repositories\Repository;
use Modules\Admin\Entities\ProductMeasurements;

class ProductMeasurementRepository extends Repository
{
    public function __construct(ProductMeasurements $productMeasurements)
    {
        // the model instance can be accessed with "$this->model" variable
        parent::__construct($productMeasurements);
    }

}
