<?php

namespace Modules\Admin\Repositories;

use App\Repositories\Repository;
use Modules\Admin\Entities\ProductDetails;


class ProductDetailsRepository extends Repository
{
    public function __construct(ProductDetails $productDetails)
    {
        // the model instance can be accessed with "$this->model" variable
        parent::__construct($productDetails);
    }

}
