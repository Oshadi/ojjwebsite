<?php

namespace Modules\Admin\Repositories;

use App\NewsImage;
use App\Repositories\Repository;

class NewsImageRepository extends Repository
{
    public function __construct(NewsImage $newsImage)
    {
        // the model instance can be accessed with "$this->model" variable
        parent::__construct($newsImage);
    }


}
