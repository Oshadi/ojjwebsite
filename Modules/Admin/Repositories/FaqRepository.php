<?php

namespace Modules\Admin\Repositories;

use App\Faq;
use App\Repositories\Repository;

class FaqRepository extends Repository
{
    public function __construct(Faq $faq)
    {
        // the model instance can be accessed with "$this->model" variable
        parent::__construct($faq);
    }


    /**
     * @param $type
     * @return mixed
     */
    public function getAllActiveQuestion($type)
    {
        return $this->model
            ->where('status', 1)
            ->where('type', $type)
            ->get();
    }
}
