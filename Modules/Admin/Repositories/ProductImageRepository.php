<?php

namespace Modules\Admin\Repositories;

use App\Repositories\Repository;
use Modules\Admin\Entities\ProductImage;

class ProductImageRepository extends Repository
{
    public function __construct(ProductImage $productImage)
    {
        // the model instance can be accessed with "$this->model" variable
        parent::__construct($productImage);
    }

}
