<?php

namespace Modules\Admin\Repositories;

use App\News;
use App\Repositories\Repository;

class NewsRepository extends Repository
{
    public function __construct(News $news)
    {
        // the model instance can be accessed with "$this->model" variable
        parent::__construct($news);
    }

    /**
     * @param $limit
     * @return mixed
     */
    public function getLatestNews($limit)
    {
        $queryData = $this->model->where('status', 1)
            ->latest('added_date')
            ->latest('id')
            ->limit($limit)
            ->get();

        return $queryData;
    }

    /**
     * @param $limit
     * @param $categorySlug
     * @param $newsSlug
     * @return mixed
     */
    public function getLatestNewsFilter($categorySlug, $newsSlug)
    {
        $queryData = $this->model;

        /*if ($newsSlug && $newsSlug != 'all') {
            $queryData = $queryData->where('slug', $newsSlug);
        }*/

        if ($categorySlug && $categorySlug != 'all') {
            $queryData = $queryData->whereHas('newsCategory', function ($q) use ($categorySlug) {
                $q->where('slug', $categorySlug);
            });
        }

        $queryData = $queryData->latest('added_date')
            ->latest('id')
            ->where('status', 1)
            ->whereHas('newsCategory', function ($q) {
                $q->where('status', 1);
            })
            ->get();

        return $queryData;
    }


}
