<?php

namespace Modules\Admin\Repositories;

use App\Repositories\Repository;
use Modules\Web\Entities\ContactUsDetail;

class ContactUsRepository extends Repository
{
    protected $contactUsDetail;
    public function __construct(ContactUsDetail $contactUsDetail )
    {
        // the model instance can be accessed with "$this->model" variable
        $this->contactUsDetail = $contactUsDetail;
    }

    /**
     *
     * Created by : oshadhi
     * Created at :
     * Summary: update record data in DB
     *
     * @param $request
     * @return mixed
     * @throws \Exception
     */
    public function updateContact($request)
    {

        try {

            $updateData = $this->contactUsDetail->where('id',$request->id)->first();
            $updateData->name = $request->name;
            $updateData->address_line_1 = $request->address_line_1;
            $updateData->address_line_2 = $request->address_line_2;
            $updateData->email = $request->email;
            $updateData->contact = $request->contact;
            $updateData->fax = $request->fax;
            $updateData->business_content = $request->business_content;
            $updateData->facebook = $request->facebook;
            $updateData->instagram = $request->instagram;
            $updateData->status = 1;

  //          $updateData->updated_by = auth()->user()->id;
            $updateData->save();

            return $updateData;
        } catch (\Exception $e) {
            throw $e;
        }
    }

}
