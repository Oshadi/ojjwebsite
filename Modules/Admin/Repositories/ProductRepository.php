<?php

namespace Modules\Admin\Repositories;

use App\Repositories\Repository;
use Modules\Admin\Entities\Product;

class ProductRepository extends Repository
{
    public function __construct(Product $product)
    {
        // the model instance can be accessed with "$this->model" variable
        parent::__construct($product);
    }

    public function getActiveProductList()
    {
        $queryData = $this->model->where('status', 1)
            ->orderBy('id')
            ->get();
        return $queryData;
    }

    public function firstProduct(){
        $queryData = $this->model->where('status', 1)
            ->orderBy('id')
            ->first();
        return $queryData;
    }

    public function getProductDetails($id){
        $query=$this->model->where('id',$id)->first();

        return $query;
    }
}
