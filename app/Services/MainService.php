<?php

namespace App\Services;

use Modules\Admin\Traits\TableViewHelperTrait;

class MainService
{
    use TableViewHelperTrait;

    public function __construct()
    {
    }

    function generateWhereLikeQuery($model, $columns, $search)
    {
        $i = 0;
        foreach ($columns as $key => $column) {

            if ($column['searchable'] == false || $column['name'] == "action") {
                continue;
            }
            if ($i == 0) {
                $model->having($column['name'], 'LIKE', '%'.$search.'%');
            } else {
                $model->orHaving($column['name'], 'LIKE', '%'.$search.'%');
            }
            $i++;
        }

        return $model;
    }
}
