<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;

    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'name', 'email', 'password',
    ];*/

    protected $guarded = [];

    protected $appends = ['display_name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getDisplayNameAttribute()
    {
        return $this->first_name . ' '. $this->last_name;
    }

    public function getProfileImageAttribute($value)
    {
        if (empty($value)) {
            return asset('custom_assets/images/profile.jpg');
        } else {
            return asset('/storage/profile_image/' . $value);
            //return storage_path('app/public/profile_image/' . $value);
        }
    }

}
