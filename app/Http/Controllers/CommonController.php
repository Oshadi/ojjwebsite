<?php

namespace App\Http\Controllers;

use App\BankBranch;
use App\City;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return string
     * LOAD DISTRICT WISE CITY
     * Created At : 2022-04-05
     * Created By : oshadhi
     */
    public function getCities(Request $request)
    {
        $cities = City::where('district_id', $request->id)->orderBy('name_en', 'asc')->get();

        $tmp = '';
        $tmp .= "<option value=''> -- Select City -- </option>";
        foreach ($cities as $citie) {
            if($citie->id  == $request->saveValue){
                $tmp .= '<option value="' . $citie->id . '" selected>' . $citie->name_en . ' | ' . $citie->postcode . '</option> ';
            }else{
                $tmp .= '<option value="' . $citie->id . '" >' . $citie->name_en . ' | ' . $citie->postcode . '</option> ';
            }
        }
        return $tmp;
    }

    /**
     * @param Request $request
     * @return string
     * LOAD BANK WISE BANK BRANCHES
     * Created At : 2022-04-05
     * Created By : oshadhi
     */
    public function getBankBranches(Request $request)
    {
        $branches = BankBranch::where('bank_id', $request->id)->orderBy('name', 'asc')->get();

        $tmp = '';
        $tmp .= "<option value=''> -- Select Branch  -- </option>";
        foreach ($branches as $branch) {
            if($branch->id  == $request->saveValue){
                $tmp .= '<option value="' . $branch->id . '" selected>' . $branch->name . ' | ' . $branch->code . '</option> ';
            }else{
                $tmp .= '<option value="' . $branch->id . '" >' . $branch->name . ' | ' . $branch->code . '</option> ';
            }
        }
        return $tmp;
    }
}
