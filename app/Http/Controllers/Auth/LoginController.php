<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout', 'userLogout');
    }

    /*
     * LOGIN METHOD OVERRIDE
     */
    public function login(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password, 'status' => 1], $request->remember)) {

            // LOG THE USER LOGIN DETAILS
            $user = auth()->user();
            //$user->api_token = Str::random(60);
            $user->last_login_at = date('Y-m-d H:i:s');
            $user->last_login_ip = request()->ip();
            $user->save();

            return $request->wantsJson()
                ? new JsonResponse([], 204)
                : redirect()->intended($this->redirectPath());

        }

        throw ValidationException::withMessages([
            'email' => [trans('auth.failed')],
        ]);

        /*return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withMessage('message', 'Some message');*/
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function userLogout()
    {
        $user = Auth()->user();

        if ($user) {
            $user->api_token = null; // clear api token
            $user->save();
        }
        Auth::guard('web')->logout();
        return redirect()->route('login');
    }
}
