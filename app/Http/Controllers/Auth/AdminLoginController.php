<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;


class AdminLoginController extends Controller
{
    public function __construct()
    {
//        $this->middleware('guest:admin', ['except' =>['logout']]);
        $this->middleware('guest:web')->except('logout');
    }

    public function loginForm()
    {
        return view('auth.admin-login');
    }

    /**
     * ADMIN LOGIN
     * create at : oshadhi
     * create by : 2022-03-23
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {

            // LOG THE USER LOGIN DETAILS
            $user = auth()->user();
            //$user->api_token = Str::random(60);
            $user->last_login_at = date('Y-m-d H:i:s');
            $user->last_login_ip = request()->ip();
            $user->save();

            return redirect()->intended(route('admin.dashboard'));
        }

        throw ValidationException::withMessages([
            'email' => [trans('auth.failed')],
        ]);

        /*return redirect()->back()
            ->withInput($request->only('email', 'remember'))
            ->withMessage('message', 'Some message');*/
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect('/admin/login');
    }
}
