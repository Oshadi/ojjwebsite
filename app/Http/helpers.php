<?php

use App\User;
use App\OrderNotification;
use App\ActionLog;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

use Spatie\Permission\Exceptions\PermissionDoesNotExist;

/**
 * Created At : 17/8/2020
 * Created By : Nilaksha
 * Summary : Handles logging the errors
 *
 * @param Exception $ex
 * @return void
 */
function logErrors(Exception $ex)
{
    Illuminate\Support\Facades\Log::error($ex);
}

/**
 * Created At : 12/8/2020
 * Created By : Nilaksha
 * Summary : forms a return array for responces
 *         : pass custom data fields to the data parameter  as array
 *
 * @param [bool] $success
 * @param [string] $status
 * @param [array] $message
 * @param [array] $data
 * @param [string] $redirect
 * @param [string] $notifytype
 * @return void
 */
function getFormattedResponce(bool $success, string $status, string $message, $data, string $redirect, string $notifytype)
{
    return array(
        'success' => $success,
        'status' => $status,
        'message' => $message,
        'data' => $data,
        'redirect' => $redirect,
        'notifyType' => $notifytype
    );
}

function resourceNames($name)
{
    return [
        'names' => [
            'store' => $name . '.store', // Post : used to pass data to backend and store
            'index' => $name . '.index', // Base page
            'create' => $name . '.create', // get: used to show the create page
            'update' => $name . '.update', // post : used to pass data to back-end to store update data
            'show' => $name . '.show', // get:  show a record for a given ID
            'destroy' => $name . '.destroy',  // Delete a record for a given ID
            'edit' => $name . '.edit', // get : used to show the edit page for a record
        ]
    ];
}


/**
 * Insert User Actions to Log Table
 *
 * For every action defined in permissions, an action log records should be
 * created except for GET (data retrieval) functions. But any action that modifies system data should
 * be logged along with the permission.
 *
 * @param $action String Name of the permission
 * @param $subject String ID of the item being modified or created
 * @param $parameters String All of input parameters
 * @param $response String Response data or any results of the action
 * @param $comments String Any other data that should be saved
 *
 * @access public
 * @author Prasith Fernando
 */
function logAction($action, $subject, $parameters, $response, $comments)
{
    $log = new ActionLog();

    $log->user = auth()->user()->display_name;
    $log->ip = Request::ip();
    $log->action = $action;
    $log->subject = $subject;
    $log->parameters = $parameters;
    $log->response = $response;
    $log->comments = $comments;

    $log->save();
}

function resourceNamesSalaryList($name)
{
    return [
        'names' => [
            'store' => $name . '.store', // Post : used to pass data to backend and store
            'index' => $name . '.index', // Base page
            'create' => $name . '.create', // get: used to show the create page
            'update' => $name . '.update', // post : used to pass data to back-end to store update data
            'show' => $name . '.show', // get:  show a record for a given ID
            'destroy' => $name . '.destroy',  // Delete a record for a given ID
            'edit' => $name . '.edit', // get : used to show the edit page for a record
            'showPrintTemplates' => $name . '.showPrintTemplates', // get : used to show the edit page for a record
        ]
    ];
}

function getObjectByStatus($modelType, $typeOfStatus)
{
    $obj = $modelType::where('status', $typeOfStatus)->get();
    return $obj;
}


/**
 * @param $modelType
 * @param $type
 */
function active($modelType, $type)
{
    if (is_object($type)) {
        $modelType::where('id', $type->id)->first()->update(['status' => 1]);
    } else {
        $modelType::where('id', $type)->first()->update(['status' => 1]);
    }
}

/**
 * @param $modelType
 * @param $type
 */
function inactive($modelType, $type)
{
    if (is_object($type)) {

        $modelType::where('id', $type->id)->first()->update(['status' => 0]);
    } else {
        $modelType::where('id', $type)->first()->update(['status' => 0]);
    }
}

/**
 * summary : generate table action buttons
 * @param $route
 * @param $data
 * @param $permissions
 * @param string $view
 * @return string
 */
function generateActionButtons($route, $data, $permissions, $view = 'partials/common_index_action')
{
    return View::make($view, ["data" => $data, 'route' => $route, 'permissions' => $permissions])->render();
}

/**
 * Created By : oshadhi
 * Created At : 2022-07-25
 * ORDER RELATED NOTIFICATION STORE HELPER FUNCTION
 * @param $orderId
 * @param $title
 * @param $remarks
 */
function logOrderNotification($orderId, $title, $remarks = null)
{
    $log = new OrderNotification();

    $log->order_id = $orderId;
    $log->title = $title;
    $log->remarks = $remarks;
    $log->created_by = auth()->user()->id;

    $log->save();
}


function getLoanStatusLabel($status)
{
    $allStatus = Config::get('constants.loan_status');
    if ($status == 1) {
        $status = '<span class="badge badge-success">' . $allStatus[$status] . '</span>';
    } elseif ($status == 2) {
        $status = '<span class="badge badge-danger">' . $allStatus[$status] . '</span>';
    } elseif ($status == 3) {
        $status = '<span class="badge badge-warning">' . $allStatus[$status] . '</span>';
    } else {
        $status = '<span class="badge badge-secondary">' . $allStatus[$status] . '</span>';

    }
    return $status;
}

/**
 * summary : get table user wise columns
 * @param $tableName
 * @param $userId
 * @return bool|string|string[]
 */
function getTableColumns($tableName, $userId)
{
    /*$columns = ColumnCustomizer::where('table_name', $tableName)->where("created_by", $userId)->get();
    if (count($columns) == 0) {
        return false;
    } else {
        $strTrueReplace = str_replace("true", true, $columns[0]->columns);
        return str_replace("false", false, $strTrueReplace);
    }*/
    return false;
}

function getOrderByColumn($columns, $orderColumn)
{
    $keys = array_keys($columns);
    return $columns[$keys[($orderColumn)]]['name'];
}

/**
 * Summary : check company value is exits.
 * @return bool
 */
function checkCompanyExits()
{
    $company = Company::where("status", "1")->first();
    if (!$company) {
        Session::flash('error', 'Please complete your company profile!');
        return true;
    }
    return false;
}

/**
 * Summary : Employee personal detail checked;
 * @param $name
 * @return int
 */
function checkEmployeePersonalDetail($name)
{
    $detail = EmployeePersonalDetail::where("status", "1")->where("name", $name)->first();
    if ($detail) {
        return 1;
    }
    return 0;
}

/**
 * created at : 01/12/2020
 * created by : Dulan Chandrakumara
 * Summary : commonly use permission.
 *           use this line : ' checkHasPermission(Actions::CREATE_BANK_BRANCH);'
 *           Actions file have a all permission name list.you can add all permission variables.
 * @param $action
 * @return bool
 */
function checkHasPermission($action)
{
    try {
        if (!Auth::user()->hasPermissionTo($action, 'web')) {
            return false;
        }
        return true;
    } catch (PermissionDoesNotExist $e) {
        return false;
    }
}


function generateLoanId($id)
{
    $invID = Config::get('constants.loan_prefix') . str_pad($id, 4, '0', STR_PAD_LEFT);
    return $invID;
}

function checkPermissionRedirect($action)
{
    try {
        if (!Auth::user()->hasPermissionTo($action, 'web')) {
            $redirect = '';
            if (!Request::ajax()) {
                Redirect::route('permission-denied')->send();
            }
            $outPutArray = array('status' => 'error', 'message' => 'You do not have permission to perform this action: "' . $action . '"', 'data' => route('permission-denied'), 'redirect' => $redirect, 'notifyType' => 'message');
            response()->json($outPutArray)->send();
            exit();
        }
    } catch (PermissionDoesNotExist $e) {
        $redirect = '';
        if (!Request::ajax()) {
            Redirect::route('permission-denied')->send();
        }
        $outPutArray = array('status' => 'error', 'message' => 'You do not have permission to perform this action: "' . $action . '"', 'data' => route('permission-denied'), 'redirect' => $redirect, 'notifyType' => 'message');
        response()->json($outPutArray)->send();
        exit();
    }
}

/**
 * created at : 20/1/2020
 * created by : Nilaksha
 * Summary : Returns custom fields for a given section id
 *
 * @param [type] $sectionId
 * @return void
 */
function getCustomFieldsBySectionId($sectionId)
{
    try {
        $customFieldService = new CustomFieldService(new CustomFieldRepository(new CustomField()));
        return $customFieldService->getFieldsBySectionId($sectionId);
    } catch (Exception $ex) {
        Log::error($er);
        return [];
    }
}


/**
 * created at : 17/12/2020
 * created by : Nilaksha
 * Summary : Returns hidden elements
 *
 * @return void
 */
function currentSelFormFields()
{
    return '<input type="hidden" name="selectelement" id="selectelement" class="selectelement" value="">';
}


/**
 * Created At : 2/9/2020
 * Created By : Nilaksha
 * Summary : Created a unique GUID
 *           https://stackoverflow.com/questions/21671179/how-to-generate-a-new-guid
 *
 * @return void
 */
function GUID()
{
    if (function_exists('com_create_guid') === true) {
        return trim(com_create_guid(), '{}');
    }
    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}


function generateCode($document_type)
{

    $code_generation = \Modules\System\Entities\CodeGeneration::whereHas('documentType', function ($q) use ($document_type) {
        $q->where('key', '=', $document_type);
    })->whereStatus(1)->first();

    if ($code_generation) {
        $generated_code = $code_generation->generateNextDocumentCode();
        $code_generation->increment_code = $code_generation->increment_code + 1;
        $code_generation->save();
    } else {
        $generated_code = null;
    }

    return $generated_code;
}

function previewCode($document_type)
{

    $code_generation = \Modules\System\Entities\CodeGeneration::whereHas('documentType', function ($q) use ($document_type) {
        $q->where('key', '=', $document_type);
    })->whereStatus(1)->first();

    if ($code_generation) {
        $generated_code = $code_generation->generateNextDocumentCode();
//        $code_generation->increment_code = $code_generation->increment_code + 1;
//        $code_generation->save();
    } else {
        $generated_code = null;
    }

    return $generated_code;
}

/**
 *
 * Created by : Gayan
 * Created at: 2021.04.16
 * Summary : generate account category Code
 *
 * @param $accType
 * @return string
 */
function previewAccountCategoryCode($accType)
{
    $accountCategory = \Modules\Accounting\Entities\FinanceAccountCategory::where('finance_account_type_id', $accType)
        ->orderBy('id', 'desc')
        ->first();
    if ($accountCategory) {
        $code = (int)$accountCategory->code + 1;
    } else {
        $lastSelectedId = 1;
        $code = $accType . str_pad($lastSelectedId, 2, '0', STR_PAD_LEFT);
    }

    return $code;
}

/**
 *
 * Created by : Gayan
 * Created at: 2021.04.20
 * Summary : generate control account Code
 *
 * @param $accCategoryId
 * @return string
 */
function previewControlAccountCode($accCategoryId)
{
    $accountCategory = \Modules\Accounting\Entities\FinanceAccountCategory::where('id', $accCategoryId)
        ->orderBy('id', 'desc')
        ->first();

    $controlAccount = \Modules\Accounting\Entities\FinanceControlAccount::where('finance_account_category_id', $accCategoryId)
        ->orderBy('id', 'desc')
        ->first();
    if ($controlAccount) {
        $lastSelectedId = (int)$controlAccount->id + 1;
    } else {
        $lastSelectedId = 1;
    }

    $code = $accountCategory->code . str_pad($lastSelectedId, 2, '0', STR_PAD_LEFT);

    return $code;
}

/**
 *
 * Created by : Gayan
 * Created at: 2021.04.27
 * Summary : generate ledger account Code
 *
 * @param $accCategoryId
 * @param $controlAccId
 * @return string
 */
function previewLedgerAccountCode($accCategoryId, $controlAccId)
{
    $accountCategory = \Modules\Accounting\Entities\FinanceAccountCategory::where('id', $accCategoryId)
        ->orderBy('id', 'desc')
        ->first();

    $controlAccount = \Modules\Accounting\Entities\FinanceControlAccount::where('id', $controlAccId)
        ->orderBy('id', 'desc')
        ->first();

    if ($controlAccId != 0) {
        $ledgerAccount = \Modules\Accounting\Entities\FinanceLedgerAccount::where('finance_account_category_id', $accCategoryId)
            ->where('finance_control_account_id', $controlAccId)
            ->orderBy('id', 'desc')
            ->first();

        if ($ledgerAccount) {
            $lastSelectedId = (int)$ledgerAccount->id + 1;
        } else {
            $lastSelectedId = 1;
        }

        $code = $controlAccount->code . str_pad($lastSelectedId, 3, '0', STR_PAD_LEFT);
    } else {
        $ledgerAccount = \Modules\Accounting\Entities\FinanceLedgerAccount::where('finance_account_category_id', $accCategoryId)
            ->orderBy('id', 'desc')
            ->first();

        if ($ledgerAccount) {
            $lastSelectedId = (int)$ledgerAccount->id + 1;
        } else {
            $lastSelectedId = 1;
        }

        $code = $accountCategory->code . '00' . str_pad($lastSelectedId, 3, '0', STR_PAD_LEFT);
    }

    return $code;
}

/**
 *
 * Created by : Gayan
 * Created at: 2021.05.07
 * Summary : generate petty cash category Code
 *
 * @param $categoryId
 * @return string
 */
function previewPettyCashCategoryCode()
{
    $pettyCashCategory = \Modules\Accounting\Entities\FinancePettyCashCategory::orderBy('id', 'desc')
        ->first();

    if ($pettyCashCategory) {
        $lastSelectedId = (int)$pettyCashCategory->id + 1;
    } else {
        $lastSelectedId = 1;
    }

    $code = str_pad($lastSelectedId, 2, '0', STR_PAD_LEFT);

    return $code;
}

/**
 * Created by : Dulan
 * Created at: 1/21/2021
 * Summary : check and store document Authentication
 * @param $empId
 * @param $transactionTypeId ,$id
 */
function storeAuthentication($transactionTypeId, $id)
{
    $emp = Employee::where('profile_registries_id', auth()->user()->profile_registry_id)->where('status', 1)->first();

    if ($emp) {
        $countValue = 0;
        $authorization_array = json_decode($emp['authorization_array'], true);
        if (count($authorization_array) > 0) {
            $authorizationMasterId = AuthenticationMaster::create([
                "status" => 0,
                "transaction_id" => $id,
                "created_by" => auth()->user()->id,
                "transaction_type_id" => $transactionTypeId
            ]);
            $level = 1;
            for ($i = 0; $i < count($authorization_array); $i++) {
                if ($authorization_array[$i]['transaction'] == $transactionTypeId) {
                    $checkedByAr = $authorization_array[$i]['checked-by'];
                    $approvedByAr = $authorization_array[$i]['approved-by'];
                    for ($j = 0; $j < count($checkedByAr); $j++) {
                        for ($k = 0; $k < count($checkedByAr[$j]); $k++) {
                            $countValue++;
                            AuthenticationPerson::create([
                                "status" => 0,
                                "authentication_master_id" => $authorizationMasterId->id,
                                "profile_registry_id" => $checkedByAr[$j][$k],
                                "level" => $level,
                                "type" => "CHECK",
                                "created_by" => auth()->user()->id,
                            ]);
                        }
                        $level++;
                    }
                    for ($j = 0; $j < count($approvedByAr); $j++) {
                        for ($k = 0; $k < count($approvedByAr[$j]); $k++) {
                            $countValue++;
                            AuthenticationPerson::create([
                                "status" => 0,
                                "authentication_master_id" => $authorizationMasterId->id,
                                "profile_registry_id" => $approvedByAr[$j][$k],
                                "level" => $level,
                                "type" => "APPROVE",
                                "created_by" => auth()->user()->id,
                            ]);
                        }
                        $level++;
                    }
                }
            }
//            dd($countValue);
            if ($countValue > 0) {
                return true;
            } else {
                $authorizationMasterId->update(["status" => 1]);
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function checkUserDocumentApproveStatus($transactionTypeId, $transactionId)
{
    $id = auth()->user()->profile_registry_id;
    $data = DB::select(DB::raw("SELECT
    am.transaction_id, am.transaction_type_id,am.status as master_status, a.* FROM
     authentication_persons a
        JOIN
     authentication_masters am ON a.authentication_master_id = am.id
WHERE
    am.transaction_id = " . $transactionId . " AND am.transaction_type_id = " . $transactionTypeId . "  order by level asc"));
//dd($data);
    $level = @$data[0]->level;
    $oldStatus = @$data[0]->status;
    for ($i = 0; $i < count($data); $i++) {
        if ($level != $data[$i]->level && $data[$i]->status == 0 && $oldStatus == 0) {
            return 0;
        }
        if ($level == $data[$i]->level && $data[$i]->status == 2) {
            return 0;
        }
        if ($level != $data[$i]->level && $data[$i]->status == 0 && $oldStatus == 1) {
            return 1;
        }
        if ($level == $data[$i]->level && $data[$i]->profile_registry_id == $id && $data[$i]->status == 0) {
            return 1;
        }

        if ($data[$i]->profile_registry_id == $id && $data[$i]->status == 0) {
            return 1;
        }
        $oldStatus = $data[$i]->status;
        $level = $data[$i]->level;
    }
//    return 1;
}


function getUserDocumentApproveStatusId($transactionTypeId, $transactionId, $statusVal)
{
    $id = auth()->user()->profile_registry_id;
    $data = DB::select(DB::raw("SELECT
    am.transaction_id, am.transaction_type_id,am.status as master_status,a.*, a.id as auth_id_person FROM
     authentication_persons a
        JOIN
     authentication_masters am ON a.authentication_master_id = am.id
WHERE
    am.transaction_id = " . $transactionId . " AND am.transaction_type_id = " . $transactionTypeId . " and a.status='" . $statusVal . "' order by level asc"));

    $level = @$data[0]->level;
    for ($i = 0; $i < count($data); $i++) {
        if ($level !== $data[$i]->level) {
              return 0;
        }
        if ($level == $data[$i]->level && $data[$i]->profile_registry_id === $id) {
            return $data[$i]->auth_id_person;
        }
        if ($data[$i]->profile_registry_id == $id && $data[$i]->status == 0) {
             return $data[$i]->auth_id_person;
        }
        $level = $data[$i]->level;
    }
    return 0;
}


function checkUserApproveDocument($transactionTypeId, $transactionId)
{
    $id = auth()->user()->profile_registry_id;
    $data = DB::select(DB::raw("SELECT
    am.transaction_id, am.transaction_type_id,am.status as master_status, a.*,a.id as auth_id_person FROM
     authentication_persons a
        JOIN
     authentication_masters am ON a.authentication_master_id = am.id
WHERE
    am.transaction_id = " . $transactionId . " AND am.transaction_type_id = " . $transactionTypeId . " and a.status!=0 order by level asc"));

    $level = @$data[0]->level;
    for ($i = 0; $i < count($data); $i++) {
        $level = $data[$i]->level;
        if ($level != $data[$i]->level) {
            return 0;
        }
        if ($level == $data[$i]->level && $data[$i]->profile_registry_id == $id) {
            return $data[$i]->status;
        }
        if ($data[$i]->profile_registry_id == $id) {
            return $data[$i]->status;
        }
    }
    return 0;
}

function getDocumentStatus($transactionTypeId, $transactionId)
{
    $data = AuthenticationMaster::where("transaction_id", $transactionId)->where("transaction_type_id", $transactionTypeId)->first();

    if ($data) {
        return $data->status;
    }
}

function getDocumentStatusCheck($transactionTypeId, $transactionId)
{
    $data = AuthenticationMaster::where("transaction_id", $transactionId)->where("transaction_type_id", $transactionTypeId)->first();
    if ($data) {
        foreach ($data->authPersons as $authPerson) {
            if ($authPerson->status != 0)
                return false;
        }
        return true;
    } else {
        return true;

    }
}

function getDocumentRejectReason($transactionTypeId, $transactionId)
{
    $data = DB::select(DB::raw("SELECT
    am.transaction_id, am.transaction_type_id,am.status as master_status, a.*,a.id as auth_id_person FROM
     authentication_persons a
        JOIN
     authentication_masters am ON a.authentication_master_id = am.id
WHERE
    am.transaction_id = " . $transactionId . " AND am.transaction_type_id = " . $transactionTypeId . " and a.status=2 order by level asc "));


    if ($data) {
        return $data[0]->remark;
    } else {
        return "";
    }
}

function getCreateUserName($id)
{
    $user = User::where("id", $id)->first();
    return $user->name;
//    return $data->status;
}

function sendMail($mailClass, $to, $cc = [], $bcc = [], $subject = null, $data = [])
{
//    dd($to);
    \Illuminate\Support\Facades\Mail::to($to)
        ->cc($cc)
        ->bcc($bcc)
        ->send(new $mailClass($data, $subject));
}


function flatten($array)
{
    $result = [];
    foreach ($array as $item) {
        if (is_array($item)) {
            $result[] = array_filter($item, function ($array) {
                return !is_array($array);
            });
            $result = array_merge($result, flatten($item));
        }
    }
    return array_filter($result);
}


function numberTowords($num = false)
{
    $num = str_replace(array(',', ' '), '', trim($num));
    if (!$num) {
        return false;
    }
    $num = (int)$num;
    $words = array();
    $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
        'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
    );
    $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
    $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
        'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
        'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
    );
    $num_length = strlen($num);
    $levels = (int)(($num_length + 2) / 3);
    $max_length = $levels * 3;
    $num = substr('00' . $num, -$max_length);
    $num_levels = str_split($num, 3);
    for ($i = 0; $i < count($num_levels); $i++) {
        $levels--;
        $hundreds = (int)($num_levels[$i] / 100);
        $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
        $tens = (int)($num_levels[$i] % 100);
        $singles = '';
        if ($tens < 20) {
            $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
        } else {
            $tens = (int)($tens / 10);
            $tens = ' ' . $list2[$tens] . ' ';
            $singles = (int)($num_levels[$i] % 10);
            $singles = ' ' . $list1[$singles] . ' ';
        }
        $words[] = $hundreds . $tens . $singles . (($levels && ( int )($num_levels[$i])) ? ' ' . $list3[$levels] . ' ' : '');
    } //end for loop
    $commas = count($words);
    if ($commas > 1) {
        $commas = $commas - 1;
    }
    return implode(' ', $words);
}

function calDiscount($discount, $total, $type)
{

    $discount_amount = 0;

    if ($type == 1 && $discount > 0) {
        $discount_amount = $discount;
    } elseif ($type == 2 && $discount > 0) {
        $discount_amount = ($total * $discount) / 100;
    } else {
        $discount_amount = 0;
    }


    return $discount_amount;
}

function calTax($tax, $total, $type)
{

    $tax_amount = 0;

    if ($type == 1 && $tax > 0) {
        $tax_amount = $tax;
    } elseif ($type == 2 && $tax > 0) {
        $tax_amount = ($total * $tax) / 100;
    } else {
        $tax_amount = 0;
    }


    return $tax_amount;
}

function generateNumericOTP()
{
    $n = 6;

    $generator = "1357902468";

    $result = "";

    for ($i = 1; $i <= $n; $i++) {
        $result .= substr($generator, (rand() % (strlen($generator))), 1);
    }

    // Return result
    return $result;
}


function hitUserOTP($user, $otp)
{


    $existUserOTP = \Modules\Membership\Entities\UserOTP::where('user_id', $user)->update(array('status' => 0));
    $userOTP = new \Modules\Membership\Entities\UserOTP();
    $userOTP->user_id = $user;
    $userOTP->key = $otp;
    $userOTP->status = 1;
    $expire_at = date('Y-m-d H:i:s', strtotime(Date('Y-m-d H:i:s') . ' + 3 minute'));
    $userOTP->expire_at = $expire_at;
    $userOTP->save();

    return $userOTP;

}

function verifyUserOTP($user, $enteredOTP)
{

    try {

        $existUserOTP = \Modules\Membership\Entities\UserOTP::where('user_id', $user)->where('status', 1)->first();

        if ($existUserOTP) {
            if ($existUserOTP->used == 0) {
                if ($existUserOTP->key == $enteredOTP) {

                    if ($existUserOTP->expire_at > date('Y-m-d H:i:s')) {
                        $webUser = \Modules\Membership\Entities\WebsiteUser::where('reference_no', $user)->first();
                        if ($webUser) {
                            $webUser->is_verified = 1;
                            $webUser->verified_at = date('Y-m-d H:i:s');
                            $webUser->save();
                        }

                        $existUserOTP->used = 1;
                        $existUserOTP->save();
                        return $existUserOTP;
                    } else {
                        throw new \App\Exceptions\BaseException(2004);
                    }
                } else {
                    throw new \App\Exceptions\BaseException(2003);
                }
            } else {
                throw new \App\Exceptions\BaseException(2006);
            }


        } else {
            throw new \App\Exceptions\BaseException(2002);
        }

    } catch (\Exception $ex) {
        throw $ex;
    }


}

/**
 * BASIC SALARY WISE ETF & EPF CAL
 * @param $basic
 * @return array
 */
function calEtfEpf($basic, $allowanceArray, $deductionArray)
{
    // GET ETF ALLOW SALARY COMPONENT
    $salaryComponents = \Modules\System\Entities\SalaryComponent::select('id')->where('apply_on_epf_etf', 1)->get();
    $componentArrary = [];
    foreach ($salaryComponents as $k=>$salary){
        array_push($componentArrary, $salary->id);
    }

    $allowanceTotal = 0;
    foreach ($allowanceArray as $allowance){
        if( in_array($allowance['component_id'], $componentArrary) ){
            $allowanceTotal += $allowance['value'];
        }
    }

    $deductionTotal = 0;
    foreach ($deductionArray as $k=>$deduction){
        if( in_array($deduction['component_id'], $componentArrary) ){
            $deductionTotal += $deduction['value'];
        }
    }

    $netBasic = ($basic + $allowanceTotal) - $deductionTotal;

    $epf = number_format((float)((8 / 100) * $netBasic), 2,'.','');
    $epf_company = number_format((float)((12 / 100) * $netBasic), 2,'.','');
    $etf = number_format((float)((3 / 100) * $netBasic), 2,'.','');

    //dd($basic, $allowanceTotal, $deductionTotal , $netBasic, $epf, $epf_company, $etf);
    return [
        'netBasic' => $netBasic,
        'epf' => $epf,
        'epf_company' => $epf_company,
        'etf' => $etf,
    ];
}



function checkBookingAvailability($request){

    $item_id = $request->item_id;

    $item = [];
    $totalItemDetails = [];


    // request coming from backend
    if(isset($request->item)){
        foreach ($request->item as $key=>$each_item){

            if(isset($each_item['item_time_from']) && isset($each_item['item_time_to'])){
                $item[] = $each_item['item_name'];
                $totalItemDetails[$key]['id'] = $each_item['item_name'];
                $totalItemDetails[$key]['event_name'] = $request->event_name;
                $totalItemDetails[$key]['start_time'] = $each_item['item_time_from'];
                $totalItemDetails[$key]['end_time'] = $each_item['item_time_to'];
                $totalItemDetails[$key]['start_date'] = $request->event_date_from;
                $totalItemDetails[$key]['end_date'] = $request->event_date_to;
                $totalItemDetails[$key]['days_status_name'] = $request->days_status_name;
                $totalItemDetails[$key]['repeat_every'] = $request->repeat_every;
            }

        }


        // request coming from front end
    }elseif (isset($request->item_id)){

        $item[] = $request->item_id;

        $totalItemDetails[0]['id'] = $request->item_id;
        $totalItemDetails[0]['event_name'] = $request->event_name;
        $totalItemDetails[0]['start_time'] = $request->time_from;
        $totalItemDetails[0]['end_time'] = $request->time_to;
        $totalItemDetails[0]['start_date'] = $request->event_date_from;
        $totalItemDetails[0]['end_date'] = $request->event_date_to;
        $totalItemDetails[0]['days_status_name'] = $request->days_status_name;
        $totalItemDetails[0]['repeat_every'] = $request->repeat_every;
        $totalItemDetails[0]['days_status_1'] = $request->days_status_1;

    }




    if($request->recall){

        $invoice_no = $request->recall;
        $events = \Modules\Membership\Entities\InvoiceItem::whereNotNull('id')->whereIn('item_id', $item)->whereHas('item', function($q){
            $q->where(function ($query) {
                $query->where('type', '=', 1)
                    ->orWhere('type', '=', 4);
            });
        })->whereHas('invoice', function($q) use ($invoice_no){
            $q->where('is_draft',0);
            $q->where('id','!=',$invoice_no);
            $q->where('type','=',4);
        });


    }elseif ($request->draft_no && $request->is_draft){

        $invoice_no = $request->draft_no;

        $events = \Modules\Membership\Entities\InvoiceItem::whereNotNull('id')->whereIn('item_id', $item)->whereHas('item', function($q){
            $q->where(function ($query) {
                $query->where('type', '=', 1)
                    ->orWhere('type', '=', 4);
            });
        })->whereHas('invoice', function($q) use ($invoice_no){
            $q->where('is_draft',0);
            $q->where('id','!=',$invoice_no);
            $q->where('type','=',4);
        });
    }else{
        $events = \Modules\Membership\Entities\InvoiceItem::whereNotNull('id')->whereIn('item_id', $item)->whereHas('item', function($q){
            $q->where(function ($query) {
                $query->where('type', '=', 1)
                    ->orWhere('type', '=', 4);
            });
        })->whereHas('invoice', function($q){
            $q->where('is_draft',0);
            $q->where('type','=',4);
        });
    }






    $weekDays = null;
    $day_status = null;
    if ($request->days_status_name==1){
        $status_type="daily";
        $freq = 1;
        $weekDays = null;
    }
    elseif ($request->days_status_name==2){
        $status_type="weekly";

        $date = date('Y-m-d');
        $freq = 1;
        $day = Carbon::parse($date)->format('l');

        $weekDays = null;




    }
    elseif ($request->days_status_name==3){
        $status_type="annually";
        $date = explode("-", $request->event_date_from);

        $day = $date[2];
        $month = $date[1];
        $year = $date[0];
        $freq = 1;

        $date = date('Y-m-d');

        $day = Carbon::parse($date)->format('l');
        $weekDays = null;

    }
    elseif ($request->days_status_name==4){
        $status_type="week_days";
        $freq = 1;
        $date = explode("-", $request->event_date_from);

        $day = $date[2];
        $month = $date[1];
        $year = $date[0];


        $date = date('Y-m-d');

        $day = Carbon::parse($date)->format('l');
        $arr =['Monday','Tuesday','Wednesday','Thursday','Friday'];

        $serializedArr = json_encode($arr);
        $weekDays = $serializedArr;





    }
    elseif ($request->days_status_name==5){
        $status_type="custom";
        if($request->days_status_1==1) {
            $day_status="daily";
            $freq = $request->repeat_every;
        }else if ($request->days_status_1==2)
        {
            $freq = $request->repeat_every;

            $day_status="weekly";
            $arr=[];
            if($request->day_s=="on"){
                $s="Sunday";
                array_push ( $arr,$s ) ;
//                        $arr = array($s);
            }
            if($request->day_m=="on"){
                $m="Monday";
                array_push ( $arr,$m ) ;
            }
            if($request->day_t=="on"){
                $t="Tuesday";
                array_push ( $arr,$t ) ;
            }
            if($request->day_w=="on"){
                $w="Wednesday";
                array_push ( $arr,$w ) ;
            }
            if($request->day_tu=="on"){
                $tu="Thursday";
                array_push ( $arr,$tu ) ;
            }
            if($request->day_f=="on"){
                $f="Friday";
                array_push ( $arr,$f ) ;
            }
            if($request->day_se=="on"){
                $se="Saturday";
                array_push ( $arr,$se ) ;
            }
            $serializedArr = json_encode($arr);
            $weekDays = $serializedArr;
        }


    }
    else{
        $status_type="one_day";
        $freq = 1;
    }




    $new_event_title = '';
    $new_event_id = 0;
    $new_event_start_date = $request->event_date_from;
    $new_event_end_date = $request->event_date_to;
    $new_event_time_from = $request->time_from;
    $new_event_time_to = $request->time_to;
    $new_event_freq_term = $status_type;
    $new_event_freq = $freq;
    $status = 'pending';
    $new_event_weekly_days = $weekDays;
    $new_event_custom_status = $day_status;

    $new_items = [];

    //create multiple entries for repeating events
    //count days from start to end and repeat


    foreach ($totalItemDetails as $itemDetail){

        if ($new_event_freq_term == 'one_day') {

            foreach (getOneDayTasks($itemDetail['event_name'],$itemDetail['id'],$itemDetail['start_date'],$itemDetail['end_date'],$itemDetail['start_time'],$itemDetail['end_time'],$status,$new_event_freq,$new_event_weekly_days,$new_event_custom_status) as $s) {
                array_push($new_items, $s);
            }
        }
        if ($new_event_freq_term == 'daily') {

            foreach (getDailyTasks($itemDetail['event_name'],$itemDetail['id'],$itemDetail['start_date'],$itemDetail['end_date'],$itemDetail['start_time'],$itemDetail['end_time'],$status,$new_event_freq,$new_event_weekly_days,$new_event_custom_status) as $s) {
                array_push($new_items, $s);
            }
        }

        if ($new_event_freq_term == 'weekly') {
            foreach (getWeeklyTasks($itemDetail['event_name'],$itemDetail['id'],$itemDetail['start_date'],$itemDetail['end_date'],$itemDetail['start_time'],$itemDetail['end_time'],$status,$new_event_freq,$new_event_weekly_days,$new_event_custom_status) as $s) {
                array_push($new_items, $s);
            }
        }

        if ($new_event_freq_term == 'week_days') {
            foreach (getEveryWeeklyTasks($itemDetail['event_name'],$itemDetail['id'],$itemDetail['start_date'],$itemDetail['end_date'],$itemDetail['start_time'],$itemDetail['end_time'],$status,$new_event_freq,$new_event_weekly_days,$new_event_custom_status) as $s) {
                array_push($new_items, $s);
            }
        }
        if ($new_event_freq_term == 'annually') {
            foreach (getYearlyTasks($itemDetail['event_name'],$itemDetail['id'],$itemDetail['start_date'],$itemDetail['end_date'],$itemDetail['start_time'],$itemDetail['end_time'],$status,$new_event_freq,$new_event_weekly_days,$new_event_custom_status) as $s) {
                array_push($new_items, $s);
            }
        }
        if ($new_event_freq_term == 'custom') {
            if ($new_event_custom_status == 'daily') {
                foreach (getCustomDailyTasks($itemDetail['event_name'],$itemDetail['id'],$itemDetail['start_date'],$itemDetail['end_date'],$itemDetail['start_time'],$itemDetail['end_time'],$status,$new_event_freq,$new_event_weekly_days,$new_event_custom_status) as $s) {
                    array_push($new_items, $s);
                }
            }
        }
        if ($new_event_freq_term == 'custom') {

            if ($new_event_custom_status == 'weekly') {

                foreach (getCustomWeeklyTasks($itemDetail['event_name'],$itemDetail['id'],$itemDetail['start_date'],$itemDetail['end_date'],$itemDetail['start_time'],$itemDetail['end_time'],$status,$new_event_freq,$new_event_weekly_days,$new_event_custom_status) as $s) {
                    array_push($new_items, $s);
                }
            }
        }
    }




    $event_items = $events->get();



    $items = array();
    foreach ($event_items as $event_item) {


        if ($event_item->invoice->eventBooking->repeats == 1) {
            //create multiple entries for repeating events
            //count days from start to end and repeat
            if ($event_item->invoice->eventBooking->freq_term == 'one_day') {
                foreach (getOneDayTasks($event_item->invoice->eventBooking->title,$event_item->item_id,$event_item->invoice->eventBooking->start_date,$event_item->invoice->eventBooking->end_date,$event_item->start_time,$event_item->end_time,$event_item->invoice->eventBooking->status,$event_item->invoice->eventBooking->freq,$event_item->invoice->eventBooking->weekly_days,$event_item->invoice->eventBooking->custom_status) as $s) {
                    array_push($items, $s);
                }
            }
            if ($event_item->invoice->eventBooking->freq_term == 'daily') {

                foreach (getDailyTasks($event_item->invoice->eventBooking->title,$event_item->item_id,$event_item->invoice->eventBooking->start_date,$event_item->invoice->eventBooking->end_date,$event_item->start_time,$event_item->end_time,$event_item->invoice->eventBooking->status,$event_item->invoice->eventBooking->freq,$event_item->invoice->eventBooking->weekly_days,$event_item->invoice->eventBooking->custom_status) as $s) {
                    array_push($items, $s);
                }
            }

            if ($event_item->invoice->eventBooking->freq_term == 'weekly') {
                foreach (getWeeklyTasks($event_item->invoice->eventBooking->title,$event_item->item_id,$event_item->invoice->eventBooking->start_date,$event_item->invoice->eventBooking->end_date,$event_item->start_time,$event_item->end_time,$event_item->invoice->eventBooking->status,$event_item->invoice->eventBooking->freq,$event_item->invoice->eventBooking->weekly_days,$event_item->invoice->eventBooking->custom_status) as $s) {
                    array_push($items, $s);
                }
            }

            if ($event_item->invoice->eventBooking->freq_term == 'week_days') {
                foreach (getEveryWeeklyTasks($event_item->invoice->eventBooking->title,$event_item->item_id,$event_item->invoice->eventBooking->start_date,$event_item->invoice->eventBooking->end_date,$event_item->start_time,$event_item->end_time,$event_item->invoice->eventBooking->status,$event_item->invoice->eventBooking->freq,$event_item->invoice->eventBooking->weekly_days,$event_item->invoice->eventBooking->custom_status) as $s) {
                    array_push($items, $s);
                }
            }
            if ($event_item->invoice->eventBooking->freq_term == 'annually') {
                foreach (getYearlyTasks($event_item->invoice->eventBooking->title,$event_item->item_id,$event_item->invoice->eventBooking->start_date,$event_item->invoice->eventBooking->end_date,$event_item->start_time,$event_item->end_time,$event_item->invoice->eventBooking->status,$event_item->invoice->eventBooking->freq,$event_item->invoice->eventBooking->weekly_days,$event_item->invoice->eventBooking->custom_status) as $s) {
                    array_push($items, $s);
                }
            }
            if ($event_item->invoice->eventBooking->freq_term == 'custom') {
                if ($event_item->invoice->eventBooking->custom_status == 'daily') {
                    foreach (getCustomDailyTasks($event_item->invoice->eventBooking->title,$event_item->item_id,$event_item->invoice->eventBooking->start_date,$event_item->invoice->eventBooking->end_date,$event_item->start_time,$event_item->end_time,$event_item->invoice->eventBooking->status,$event_item->invoice->eventBooking->freq,$event_item->invoice->eventBooking->weekly_days,$event_item->invoice->eventBooking->custom_status) as $s) {
                        array_push($items, $s);
                    }
                }
            }
            if ($event_item->invoice->eventBooking->freq_term == 'custom') {
                if ($event_item->invoice->eventBooking->custom_status == 'weekly') {
                    foreach (getCustomWeeklyTasks($event_item->invoice->eventBooking->title,$event_item->item_id,$event_item->invoice->eventBooking->start_date,$event_item->invoice->eventBooking->end_date,$event_item->start_time,$event_item->end_time,$event_item->invoice->eventBooking->status,$event_item->invoice->eventBooking->freq,$event_item->invoice->eventBooking->weekly_days,$event_item->invoice->eventBooking->custom_status) as $s) {
                        array_push($items, $s);
                    }
                }
            }

        }
    }

//        dd($items,$new_items);
    $data = [];
    $booking_status = true;
    $msg = '';
    $list = '';
    foreach ($new_items as $new_item){
        foreach ($items as $item){




            if (($item['start'] <= $new_item['end']) && ($item['end'] >= $new_item['end']) || (($item['start'] <= $new_item['start']) && ($new_item['start'] <= $item['end'])) || (($item['start'] >= $new_item['start']) && ($new_item['end'] >= $item['end']))) {



                $data[]= Date('Y-m-d',strtotime($item['start'])).' ('.Date('H:i:s',strtotime($item['start'])).'-'.Date('H:i:s',strtotime($item['end'])).')- Not Available';
                $booking_status = false;
            }


        }
    }

    $returnData['status'] = $booking_status;
    $returnData['booked_list'] = $data;
    return $returnData;
}

function getOneDayTasks($title,$id,$start_date,$end_date,$start_time,$end_time,$status,$freq,$weekly_days,$custom_status)
{
    try{

        $end = Carbon::parse($end_date.' '.$end_time);
        $start = Carbon::parse($start_date.' '.$start_time);

        $days = $end->diffInDays($start);

        $events = array();
        $date = $start;
        for ($i = 1; $i <= $days + 1; $i++) {
            if ($status == 'completed')
                continue;

//                $events[] = $this->getEvent($event,$date,$date);

            $date = Carbon::parse($date)->format('Y-m-d');
            $events[] = getEvent($title,$id,Carbon::parse($date.' '.$start_time),Carbon::parse($date.' '.$end_time));
            $date = Carbon::parse($date)->addDays($freq);



        }
        return $events;
    }catch (Exception $ex){
        throw $ex;
    }
}
function getDailyTasks($title,$id,$start_date,$end_date,$start_time,$end_time,$status,$freq,$weekly_days,$custom_status)
{
    try{

        $end = Carbon::parse($end_date);
        $start = Carbon::parse($start_date);



        $days = $end->diffInDays($start);


        $events = array();
        $date = $start;
        for ($i = 1; $i <= $days + 1; $i++) {
            if ($status == 'completed')
                continue;
            $date = Carbon::parse($date)->format('Y-m-d');
            $events[] = getEvent($title,$id,Carbon::parse($date.' '.$start_time),Carbon::parse($date.' '.$end_time));
            $date = Carbon::parse($date)->addDays($freq);

        }
        return $events;
    }catch (Exception $ex){
        throw $ex;
    }
}
function getCustomDailyTasks($title,$id,$start_date,$end_date,$start_time,$end_time,$status,$freq,$weekly_days,$custom_status)
{
    try{
        $end = Carbon::parse($end_date.' '.$end_time);
        $start = Carbon::parse($start_date.' '.$start_time);

        $days = $end->diffInDays($start);

        $rep= $freq;
        $repeat_days=$days+1;
        $days_rep=$repeat_days/$rep;

        $events = array();
        $date = $start;
        for ($i = 1; $i <= $days_rep; $i++) {
            if ($status == 'completed')
                continue;
            $date = Carbon::parse($date)->format('Y-m-d');
            $events[] = getEvent($title,$id,Carbon::parse($date.' '.$start_time),Carbon::parse($date.' '.$end_time));
//                $events[] = $this->getEvent($event,$date,$date);
            $date = Carbon::parse($date)->addDays($freq);

        }

        return $events;
    }catch (Exception $ex){
        throw $ex;
    }
}

function getCustomWeeklyTasks($title,$id,$start_date,$end_date,$start_time,$end_time,$status,$freq,$weekly_days,$custom_status)
{
    try{

        $end = Carbon::parse($end_date.' '.$end_time);
        $start = Carbon::parse($start_date.' '.$start_time);

        $weeks = $end->diffInWeeks($start);

        $events = array();
        $date = $start;
        for ($i = 1; $i <= $weeks + 1; $i++) {
            //skip completed.
            if ($status == 'completed')
                continue;
            $weekDays = $weekly_days;
            $end_code=json_decode($weekDays);
//            dd($end_code);
            foreach($end_code as $day){
                $date =  $start->next($day);
                $date = Carbon::parse($date)->format('Y-m-d');
                $events[] = getEvent($title,$id,Carbon::parse($date.' '.$start_time),Carbon::parse($date.' '.$end_time));
//                    $events[] = $this->getEvent($event,$date,$date);
            }

//            $events[] = $this->getEvent($event,$date,$date);
            $date = Carbon::parse($date)->addWeeks($freq);

        }
        return $events;
    }catch (Exception $ex){
        throw $ex;
    }

}



function getEvent($title,$id,$start,$end){

    try{
        return array(
            'id' => (int)$id,
            'title' => $title,
            'start' => $start->format('Y-m-d H:i:s'),
            'end' => $end->format('Y-m-d H:i:s'),
            'allDay'=>false,

        );
    }catch (Exception $ex){
        throw $ex;
    }

}
function getWeeklyTasks($title,$id,$start_date,$end_date,$start_time,$end_time,$status,$freq,$weekly_days,$custom_status)
{

    try{
        $end = Carbon::parse($end_date);
        $start = Carbon::parse($start_date);

        $weeks = $end->diffInWeeks($start);

        $events = array();
        $date = $start;
        for ($i = 1; $i <= $weeks + 1; $i++) {
            //skip completed.
            if ($status == 'completed')
                continue;
            $weekDays = $weekly_days;

            $date =  $start->next($weekDays);
            $date = Carbon::parse($date)->format('Y-m-d');
            $events[] = getEvent($title,$id,Carbon::parse($date.' '.$start_time),Carbon::parse($date.' '.$end_time));
            $date = Carbon::parse($date)->addWeeks($freq);

        }
        return $events;
    }catch (Exception $ex){
        throw $ex;
    }

}

function getYearlyTasks($title,$id,$start_date,$end_date,$start_time,$end_time,$status,$freq,$weekly_days,$custom_status)
{
    try{
        $end = Carbon::parse($end_date);
        $start = Carbon::parse($start_date);

        $years = $end->diffInYears($start);

        $events = array();
        $date = $start;
        //daily tasks
        for ($i = 1; $i <= $years + 1; $i++) {
            //skip completed.
            if ($status == 'completed')
                continue;

            $date = Carbon::parse($date)->format('Y-m-d');
            $events[] = getEvent($title,$id,Carbon::parse($date.' '.$start_time),Carbon::parse($date.' '.$end_time));
            $date = Carbon::parse($date)->addYears($freq);

        }
        return $events;
    }catch (Exception $ex){
        throw $ex;
    }
}
function getEveryWeeklyTasks($title,$id,$start_date,$end_date,$start_time,$end_time,$status,$freq,$weekly_days,$custom_status)
{

    try {
        $end = Carbon::parse($end_date);
        $start = Carbon::parse($start_date);

        $weeks = $end->diffInWeeks($start);

        $events = array();
        $date = $start;
        for ($i = 1; $i <= $weeks; $i++) {
            //skip completed.
            if ($status == 'completed')
                continue;
//            $weekDays = $event_item->invoice->eventBooking->weekly_days;

            $weekDays = $weekly_days;

            $end_code = json_decode($weekDays);

            foreach ($end_code as $day) {
                $date = $start->next($day);
                $date = Carbon::parse($date)->format('Y-m-d');
                $events[] = getEvent($title,$id, Carbon::parse($date . ' ' . $start_time), Carbon::parse($date . ' ' . $end_time));
            }
            $date = Carbon::parse($date)->addWeeks($freq);

        }
        return $events;
    } catch (Exception $ex) {
        throw $ex;
    }
}

/**
 *
 * Created by: Gayan
 * Created at: 2021.07.14
 * Summary: generate API secret key for a machine
 *
 * @param $api_key
 * @param $posMachineId
 * @return \App\SystemAuth
 */
function createApiToken($api_key, $posMachineId)
{
    $encryptedValue = encrypt($api_key);

    $systemAuth = new \App\SystemAuth();
    $systemAuth->pos_machine_id = $posMachineId;
    $systemAuth->secret_key = $encryptedValue;
    $systemAuth->expired_at = Carbon::now()->addMinutes(Config::get('constants.pos_session_timeout'));
    $systemAuth->status = 1;
    $systemAuth->save();

    return $systemAuth;

}
