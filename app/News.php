<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasSlug;

    protected $table = 'news';
    protected $guarded = [];
    protected $appends = ['image_path'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug')
            ->usingLanguage('jp')
            ->slugsShouldBeNoLongerThan(20);
    }

    public function newsCategory() {
        return $this->belongsTo(NewsCategory::class,'category_id','id');
    }

    public function newsImages() {
        return $this->hasMany(NewsImage::class)->where('status', 1);
    }

    public function getImagePathAttribute($value)
    {
        $value = $this->main_image;
        if (empty($value)) {
            return asset('template_assets/assets/images/home/default.webp');
        } else {
            return asset('/storage/news_images/' . $value);
        }
    }
}
