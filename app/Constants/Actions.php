<?php
namespace App\Constants;


/**
 * Actions
 *
 * Stores the permission names to reduce the duplications
 * to make it easy to change permission names later without modifying everywhere
 * Add the permissions as constants inside the Actions class
 * This class has been registered as an Alias and can be easily used as
 *      Actions::CONST_NAME
 *
 * @package App\Constants
 * @author  Prasith Fernando
 */
class Actions {

}
