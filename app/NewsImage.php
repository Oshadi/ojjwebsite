<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsImage extends Model
{
    protected $guarded = [];
    protected $appends = ['image_path'];

    public function news() {
        return $this->hasOne(News::class);
    }

    public function getImagePathAttribute($value)
    {
        $value = $this->image;

        if (empty($value)) {
            return asset('template_assets/assets/images/home/default.webp');
        } else {
            return asset('/storage/news_images/' . $value);
        }
    }
}
