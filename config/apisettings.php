<?php
return [

    'koombiyoUrl' => env('KOOMBIYO_URL',''),
    'koombiyoApiKey' => env('KOOMBIYO_API_KEY',''),

    'prontoUrl' => env('PRONTO_URL',''),
    'prontoUsername' => env('PRONTO_USERNAME',''),
    'prontoPassword' => env('PRONTO_PASSWORD',''),
    'prontoAuth' => env('PRONTO_AUTH',''),

    'connectCoUrl' => env('CONNECTCO_URL',''),
    'connectCoToken' => env('CONNECTCO_TOKEN',''),
];
