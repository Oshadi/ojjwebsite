/*!
  * Created by : Charith Gamage
  * Date : 2022/04/01
  */



/******************
 Initialize Component
 *******************/

$(document).ready(function () {
    initializeComponent();
    imageLoard();


});


function imageLoard(){


    $('#images').change(function (e) {
        var imageids_arr = [];
        var data=[];

        var files = e.target.files;

        for (var i = 0; i <= files.length; i++) {

            // when i == files.length reorder and break
            if (i == files.length) {
                // need timeout to reorder beacuse prepend is not done
                setTimeout(function () {
                    reorderImages();
                }, 100);
                break;
            }

            var file = files[i];
            var reader = new FileReader();

            reader.onload = (function (file) {
                console.log(file.name);
                return function (event) {
                    data=[file.name];
                    $('input:hidden[name=array_id]').val(data);
                    $('#sortable').prepend('<li class="ui-state-default" data-order=0 data-id="' + file.lastModified + '"><input type="hidden" name="array_name[]" value="'+file.name+'"><img class="img-thumb-wrapper" src="' + event.target.result + '" style="width:100%;" /> <div class="order-number">0</div><span class="remove" >Remove Image</span></li>');
                    $(".remove").click(function(){

                        $(this).parent(".ui-state-default").empty();
                    });
                };
            })(file);

            reader.readAsDataURL(file);

            $(".remove").click(function(){

                $(this).parent(".ui-state-default").empty();
            });
        }// end for;

    });




    $('#sortable').sortable();
    $('#sortable').disableSelection();

    //sortable events

    $('#sortable').on('sortbeforestop', function (event) {

        reorderImages();

    });




    function reorderImages() {
        // get the items
        var images = $('#sortable li');
        console.log(images);
        var i = 0, nrOrder = 0;
        for (i; i < images.length; i++) {

            var image = $(images[i]);


            if (image.hasClass('ui-state-default') && !image.hasClass('ui-sortable-placeholder')) {
                image.attr('data-order', nrOrder);
                var orderNumberBox = image.find('.order-number');
                orderNumberBox.html(nrOrder + 1);
                nrOrder++;
            }// end if;

        }// end for;
    }



}



function initializeComponent() {
    // Initialize Wizard
    $('.wizard-form').steps({
        headerTag: 'h6',
        bodyTag: 'fieldset',
        transitionEffect: 'fade',
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            previous: 'Previous',
            next: 'Next',
            finish: 'Submit Form'
        },
        onFinished: function (event, currentIndex) {

            var block = $(this).closest('.modal-content');

            $(block).block({
                message: '<span class="font-weight-semibold"><i class="icon-spinner4 spinner mr-2"></i>&nbsp; Saving data</span>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: '10px 15px',
                    color: '#fff',
                    width: 'auto',
                    '-webkit-border-radius': 3,
                    '-moz-border-radius': 3,
                    backgroundColor: '#333'
                }
            });

            var url = $(this).attr('action');

            var form = $(this)[0];
            var data = new FormData(form);

            let ajaxResponse = ajaxRequest(url, 'POST', data, true);

            ajaxResponse.done(function (response) {
                console.log(response);
                $(block).unblock();
                if (response.status === 'success'){
                    $('.modal').modal('hide');
                    swal({
                        title: "Success!",
                        text: response.message,
                        type: "success",
                        timer: 5000
                    });
                    // popups(response.status, response.data, response.popupType);
                    // $('#dataTable').DataTable().ajax.reload();
                 document. location. reload();

            }
                else {
                    swal({
                        title: "Error!",
                        text: response.message,
                        type: "error",
                        timer: 5000
                    });
                    // popups(response.status, response.data, response.popupType);
                }
            });
        }
    });

    // Initialize Uniform Check
    $('.form-check-input-styled').uniform();

    // Initialize Uniform Upload
    $('.form-input-styled').uniform({
        fileButtonClass: 'action btn bg-blue'
    });

    // Initialize Fileinput

    // Modal template
    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
        '  <div class="modal-content">\n' +
        '    <div class="modal-header align-items-center">\n' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
        '    </div>\n' +
        '    <div class="modal-body">\n' +
        '      <div class="floating-buttons btn-group"></div>\n' +
        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>\n';
    // Buttons inside zoom modal
    var previewZoomButtonClasses = {
        toggleheader: 'btn btn-light btn-icon btn-header-toggle btn-sm',
        fullscreen: 'btn btn-light btn-icon btn-sm',
        borderless: 'btn btn-light btn-icon btn-sm',
        close: 'btn btn-light btn-icon btn-sm'
    };
    // Icons inside zoom modal classes
    var previewZoomButtonIcons = {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross2 font-size-base"></i>'
    };
    // File actions
    var fileActionSettings = {
        zoomClass: '',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        removeClass: '',
        removeErrorClass: 'text-danger',
        removeIcon: '<i class="icon-bin"></i>',
        indicatorNew: '<i class="icon-file-plus text-success"></i>',
        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
    };
    // Uploaded Files
    var images = $('.file-input').data("images");
    console.log(images);
    var details = $('.file-input').data("details");

    $('.file-input').fileinput({
        browseLabel: 'Browse',
        browseIcon: '<i class="icon-file-plus mr-2"></i>',
        uploadIcon: '<i class="icon-file-upload2 mr-2"></i>',
        removeIcon: '<i class="icon-cross2 font-size-base mr-2"></i>',
        layoutTemplates: {
            icon: '<i class="icon-file-check"></i>',
            modal: modalTemplate
        },
        initialPreview: images,
        initialPreviewConfig: details,
        initialPreviewAsData: true,
        overwriteInitial: false,
        allowedFileExtensions: ["jpeg", "jpg", "png"],
        maxFileSize: 10240,
        initialCaption: "No file selected",
        sortableIndex:1,
        previewZoomButtonClasses: previewZoomButtonClasses,
        previewZoomButtonIcons: previewZoomButtonIcons,
        fileActionSettings: fileActionSettings,

    });


}



function ajaxRequest(url, method, data, hasFileUpload) {
    var processData = true;
    var contentType = "application/x-www-form-urlencoded; charset=UTF-8";
    if (hasFileUpload !== undefined) {
        processData = false;
        contentType = false;
    }

    return $.ajax({
        'url': url,
        'type': method,
        'data': data,
        'processData': processData,
        'contentType': contentType,
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
            }
        }
    });
}

/******************
 AJAX Request
 *******************/

function ajaxRequest(url, method, data, hasFileUpload) {
    var processData = true;
    var contentType = "application/x-www-form-urlencoded; charset=UTF-8";
    if (hasFileUpload !== undefined) {
        processData = false;
        contentType = false;
    }

    return $.ajax({
        'url': url,
        'type': method,
        'data': data,
        'processData': processData,
        'contentType': contentType,
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {
                xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
            }
        }
    });
}

/******************
 Popups
 *******************/






