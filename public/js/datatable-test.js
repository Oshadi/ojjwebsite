function initDatatable(route, columns, customClass = '') {
    //normal select2 dropdown without search bar
    // $('.select').select2({});
    // Basic datatable

    // Adding this 2 line to solve this problems
    $(".dataTables_scrollHeadInner").css({"width":"100%"});
    $(".table ").css({"width":"100%"});
    // End Line

    let table = $('.datatable-basic').DataTable(
        {
            "processing": true,
            "stateSave": true,
            "serverSide": true,
            "dom": 'r<"datatable-header"fBl>t<"datatable-footer"ip>',
            "buttons": {
                dom: {
                    button: {
                        className: 'btn btn-mini btn-default'
                    },
                },
                buttons: [
                    {
                        //extend: 'colvis',
                        //text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                        //className: 'btn bg-blue btn-icon custom-column',
                        // columnText: function (dt, idx, title) {
                        //     return title;
                        // },
                    }
                ]
            },
           
            
            orderCellsTop: true,
            fixedHeader: true,

            // ajax: {
            //     "url": route,
            // },

            "ajax": {
                dataType: "JSON",
                type: "GET",
                url: route,
                data: function (d) {
                    //d.form =  $('.input-form').serializeArray();
                    d.form =  $('.filter-form').serializeObject();
                },
                // data: $('.input-form').serialize(),
                async: true,
                "dataSrc": function (json) {
                    $(".invalid-feedback").text("");
                    if (json.status == "warning") {
                        swal('warning', json.message);
                        return [];
                    } else if (json.status == "error") {
                        swal('warning', json.message);
                        return [];
                    } else {
                        return json.data;
                    }
                }
            },
            "columns": columns,

            "drawCallback": function (settings) {
                function formSubmit(e, form) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#FF7043",
                        confirmButtonText: "Yes"
                    }, function (isConfirm) {
                        if (isConfirm) {
                            form.submit();
                        }
                    });
                }
            }
        }
    );




    let name = $('#name');
    $('#filter').on('click', function (e) {
        e.preventDefault();
        table.column(1).search(name.val()).draw();

    });

    $('#clear').on('click', function (e) {
        e.preventDefault();
        name.val(null).change();

        table.column(1).search(name.val()).draw();
    });

    function formSubmit(e, form) {
        swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#ff7043",
            confirmButtonText: "Yes"
        }, function (isConfirm) {
            if (isConfirm) {
                form.submit();
            }
        });
    }

}

$.fn.serializeObject = function(){
    var obj = {};

    $.each( this.serializeArray(), function(i,o){
        var n = o.name, v = o.value;

        obj[n] = obj[n] === undefined ? v
            : $.isArray( obj[n] ) ? obj[n].concat( v )
                : [ obj[n], v ];
    });

    return obj;
};