$(document).ready(function () {

    // $('.invalid-feedback').hide();

    // $('input').change(function() {
    //     alert('ddddd')
    //     $(this).closest('.invalid-feedback').hide();
    //
    // });

    $('input').on('input',function(e){

        //console.log($(this).parents('.form-group').find('.validation-invalid-label'));
        $(this).parents('.form-group').find('.validation-invalid-label').hide();
    });

    $('input').on('input',function(e){

        //console.log($(this).parents('.input-group').find('.validation-invalid-label'));
        $(this).parents('.input-group').find('.validation-invalid-label').hide();
    });

    $('select').change(function() {
        $(this).parents('.form-group').find('.validation-invalid-label').hide();
    });

    $('textarea').change(function() {
        $(this).parents('.form-group').find('.validation-invalid-label').hide();
    });

    $("form").keypress(function (e) {
        if (e.which == 13) {
            return false;
        }
    });
    //Adding New Data
    /**
     * Created by : Dulan
     * Created at : 2020/08/17
     * summary : Create and new redirection check variable
     * @type {boolean}
     */
    let createAndNew = false;
    $('.create-new-form-submit-button').on('click', function (e) {
        createAndNew = true;
        /**
         * Created by : Dulan
         * Created at : 2020/08/17
         * summary : Form submit button clicked
         * @type {boolean}
         */
        $('.form-submit-button').click();
    });
    // $('.increment-form-submit-button').on('click', function (e) {
    //     e.preventDefault();
    //     var increment_status = $(this).attr("data-status");
    //
    //     var form = $(this).closest('form');
    //
    //     $('#increment_status').val(increment_status);
    //     $(".validation-invalid-label").hide();
    //     $(".validation-invalid-label").text("");
    //     $(".form-control").removeClass("is-invalid-custom");
    //     var url = $(this).closest('form').attr('action');
    //     var data = '';
    //     var hasFileUpload = undefined;
    //     if ($('.form-submit').attr('enctype') == 'multipart/form-data') {
    //         hasFileUpload = true;
    //         var form = $(this).closest('form')[0];
    //         data = new FormData(form);
    //     } else {
    //         data = $(this).closest('form').serialize();
    //     }
    //     var post = ajax(url, data, 'POST', hasFileUpload);
    //
    //     post.done(function (response) {
    //
    //         responseHandler(response);
    //     }).fail(function (jqXHR, textStatus, errorThrown) {
    //
    //         if (jqXHR.responseJSON)
    //             notifications('validation', jqXHR.responseJSON.errors, form);
    //     });
    // });

    $('.form-submit').on('submit', function (e) {
        e.preventDefault();
        var form = $(this).closest('form');
        var formObject = this;


        var selectResource = $('#selectelement').val();

        $(".validation-invalid-label").hide();
        $(".validation-invalid-label").text("");
        $(".form-control").removeClass("is-invalid-custom");

        var url = $(this).attr('action');

        var data = '';
        var hasFileUpload = undefined;
        if ($(this).attr('enctype') == 'multipart/form-data') {
            hasFileUpload = true;
            data = new FormData(this);
        }else{
            data = $(this).serialize();
        }

        if ($(this).attr('data-pre-execute') && $(this).attr('enctype') == 'multipart/form-data') {
            var funcName = $(this).attr('data-pre-execute');
            var variableName = $(this).attr('data-pre-execute-variable');
            data.append(variableName, window[funcName]());
        }

        var post = ajax(url, data, 'POST', hasFileUpload);

        post.done(function (response) {

            if ($('.datatable-basic').length > 0) {
                $('.datatable-basic').DataTable().ajax.reload();
            }
            if ($('#is_draft').length > 0) {
                $('#is_draft').val(0);
            }

            $('.invalid-feedback').hide();

            responseHandler(response, formObject);
            if (selectResource) {
                loadSelectElement(response, selectResource);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            $('.invalid-feedback').hide();
            if ($('#is_draft').length > 0) {
                $('#is_draft').val(0);
            }

            console.log($('.invalid-feedback'));
            if (jqXHR.responseJSON)
                notifications('validation', jqXHR.responseJSON.errors, form);
        });
    });


    $('.form-submit-button').on('click', function (e) {

        $(".preloader").addClass('d-block');

        e.preventDefault();
        var form = $(this).closest('form');
        var selectResource = $('#selectelement').val();
        var formObject = $(this).closest('form')[0];

        $(".validation-invalid-label").hide();
        $(".validation-invalid-label").text("");
        $(".form-control").removeClass("is-invalid-custom");
        var url = $(this).closest('form').attr('action');

        var data = '';
        var hasFileUpload = undefined;
        if ($('.form-submit').attr('enctype') == 'multipart/form-data') {
            hasFileUpload = true;
            data = new FormData(formObject);
        } else {
            data = $(this).closest('form').serialize();
        }

        var post = ajax(url, data, 'POST', hasFileUpload);

        post.done(function (response) {

            responseHandler(response, formObject);

            if (selectResource) {
                loadSelectElement(response, selectResource);
            }
            // $('#calendar').fullCalendar('removeEvents');
            $('.preloader').removeClass('d-block');

        }).fail(function (jqXHR, textStatus, errorThrown) {

            if (jqXHR.responseJSON)
                notifications('validation', jqXHR.responseJSON.errors, form);
            $('.preloader').removeClass('d-block');
        });
    });

    function loadSelectElement(responce, selectResource) {
        var options = '';

        console.log('Select Source : ' + selectResource);

        responce.data.forEach(element => {
            options += '<option selected="" id="' + element['id'] + '" value="' + element['id'] + '">' + element['name'] + '</option>';
        });

        $('#' + selectResource).html(options);

        $('#' + selectResource).select2("destroy");
        $('#' + selectResource).select2();
    }

    function responseHandler(response, formObject = null) {

        if (response.success == true) {

            if (formObject && $(formObject).attr('data-reset-form') == true) {
                $(formObject).trigger("reset");
            }

            if (formObject && $(formObject).attr('data-post-execute')) {
                var funcName = $(formObject).attr('data-post-execute');
                window[funcName]();
            }

            /**
             * comment by : dulan
             * created at : 2020/08/12
             * summary : Save success after asking to redirect list view or add new data
             */
            if (response.redirect !== "") {

                if (createAndNew == true) {
                    var href = $('.create-new-button').attr('href');
                    window.location.href = href;
                }else if(response.status == 'redirect'){
                    successWithRedirectMsg(response.message, response.redirect);

                } else {

                    window.location = response.redirect;
                }
            } else {
                if (createAndNew == true) {

                    // var href = $('.create-new-button').attr('href');
                    // window.location.href = href;
                    if ($(formObject).attr('data-hide-on-success') === undefined || $(this).attr('data-hide-on-success') == 'true') {
                        $("#create-modal-success-color").modal('hide');
                    }
                    successMsg(response.message, response.data, response.redirect);
                    $("#create-modal-open").click();


                    if($("#create-modal-open-1").length != 0) {
                        $("#create-modal-open-1").click();
                    }
                    if($("#create-modal-open-2").length != 0) {
                        $("#create-modal-open-2").click();
                    }
                } else {
                    if ($(formObject).attr('data-hide-on-success') === undefined || $(this).attr('data-hide-on-success') == 'true') {

                        $("#create-modal-success-color").modal('hide');
                        $("#open-modal-success-color").modal('hide');
                    }
                    successMsg(response.message, response.data, response.redirect);
                }
            }

            if (formObject && $(formObject).attr('data-reload-on-success')) {
                location.reload();
            }

        } else if (response.success == false) {
            warningMsg(response);
        } else if (response.status == 'redirect') {
            window.location = response.path;
        } else if (response.status == 'Success-Edit') {
            successEditMsg(response.msg);
        } else if (response.status == 'Info') {
            infoMsg(response.msg);
        } else {
            warningMsg(response);
        }
    }

//displaying error messages bottom of inputs
    function notifications(notifyType, data, form) {

        if (notifyType === 'validation') {

            $.each(data, function (index, value) {
                console.log('index ' + index)
                index = index.replace(/\./g, '\\.');
                var selector = '.' + index + "-error";
                var fieldBorder = '.' + index + "-is-invalid";
                if (Array.isArray(value) && value[0] != '') {
                    errorText = "";
                    $.each(value, function (index, value) {
                        if (index != 0) {
                            errorText += "<br>"
                        }
                        errorText += value;
                    });

                    $(form).find(fieldBorder).addClass('is-invalid-custom');
                    $(form).find(selector).show();
                    $(form).find(selector).html(errorText);

                    var words = index.split(".");

                    if (words.length == 4) {
                        var newClassName = '.' + words[0] + '.' + words[1] + '.' + words[2] + '-error';
                        $(form).find(newClassName).show();
                        $(form).find(newClassName).html(errorText);
                    }
                }
            });
        }
    }

    function successMsg(msg, details, redirecting = '') {

        if ($('#data-tables').length > 0) {
            $('#data-tables').DataTable().ajax.reload();
        }

        swal({
            title: "Success!",
            text: msg,
            type: "success",
            timer: 5000
        });

        // window.location = redirecting;
    }

    /**
     * SUCCESS WITH REDIRECT
     * @param msg
     * @param redirecting
     */
    function successWithRedirectMsg(msg, redirecting = '') {
        swal({
            title: "Success!",
            text: msg,
            type: "success",
            timer: 5000
        }).then(function() {
            window.location = redirecting;
        });
    }

    function successEditMsg(msg, details, redirecting = '') {
        if (createAndNew == true) {
            var href = $('.create-new-button').attr('href');
            window.location.href = href;
        } else {
            successMsg(response.msg, response.details, response.redirect);
        }
        // swal({
        //     title: msg,
        //     text: details,
        //     icon: "success",
        //     button: "Close",
        // })
        //     .then((willDelete) => {
        //         $(".modal").modal('hide');
        //         $('.modal').on('hidden.bs.modal', function (e) {
        //             $(this)
        //                 .find("input,textarea,select")
        //                 .val('')
        //                 .end()
        //                 .find("input[type=checkbox], input[type=radio]")
        //                 .prop("checked", "")
        //                 .end();
        //         });
        //         window.location.reload();
        //     });
    }

    function warningMsg(res) {
        swal({
            title: res.status, //"Warning!",
            text: res.message,
            type: "warning",
            timer: 5000,
            icon:"warning",
        });
        // swal({
        //     title: msg,
        //     icon: "error",
        //     button: "Close",
        // })
        //     .then((willDelete) => {
        //         $(".modal").modal('hide');
        //         $('.modal').on('hidden.bs.modal', function (e) {
        //             $(this)
        //                 .find("input,textarea,select")
        //                 .val('')
        //                 .end()
        //                 .find("input[type=checkbox], input[type=radio]")
        //                 .prop("checked", "")
        //                 .end();
        //         });
        //     });
    }

    function infoMsg(msg) {
        swal({
            title: msg,
            icon: "info",
            button: "Close",
        })
            .then((willDelete) => {
                $(".modal").modal('hide');
                $('.modal').on('hidden.bs.modal', function (e) {
                    $(this)
                        .find("input,textarea,select")
                        .val('')
                        .end()
                        .find("input[type=checkbox], input[type=radio]")
                        .prop("checked", "")
                        .end();
                });
            });
    }

    function ajax(url, data, method, hasFileUpload) {
        var processData = true;
        var contentType = "application/x-www-form-urlencoded; charset=UTF-8";
        if (hasFileUpload !== undefined) {
            processData = false;
            contentType = false;
        }

        return $.ajax({
            'dataType': 'json',
            'type': method,
            'url': url,
            'data': data,
            'processData': processData,
            'contentType': contentType,
            beforeSend: function (xhr, type) {

                $(".btn").prop('disabled', true);
                $(".btn").addClass('disabled');

                if (!type.crossDomain) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                }
            },
            complete: function () {
                $(".btn").prop('disabled', false);
                $(".btn").removeClass('disabled');
            }
        });
    }


//on every modal close remove all validation error msgs
    $('.modal-header .close').on('click', function (e) {
// $('.modal .fade').on('hide.bs.modal', function(){
        $(".invalid-feedback-span").text("");
        $(".invalid-feedback").text("");
    });

    // select2 initial & append selected close
    $('.form-control-select2').select2();
    $('.form-control-select2').select2({ allowClear: true });

    // clock initial
    $('.clockpicker-24h').clockpicker({
        autoclose: true,
        twelvehour: false,
        placement: 'bottom',
        align: 'left',
        donetext: 'Done',
    });

    // form reset select2 initial
    $(document).on("click", "button[type='reset']", function () {
        console.log(123);
        //$('.loadType').attr('disabled', false);
        //$('.loadType').select2('data', {});
        //$('.loadType').select2({ allowClear: true });
       // $('.form-control-select2').each(function () {
            //alert()
            //$(this).val('').select2('data', {});
           // $(".form-control-select2").val(false);
       // });
    });
})


function deleteModal(id, el) {
    var route = $(el).attr("data-name");
    var page_refresh_attribute = $(el).attr("data-page-refresh");

    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this record!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'DELETE',
                    url: route,
                    data: {id: id},
                    success: function (data) {
                        if (typeof page_refresh_attribute !== "undefined" && page_refresh_attribute == true) {
                            location.reload();
                        }
                        $('.datatable-basic').DataTable().ajax.reload();

                        // table.draw();
                        swal("Record is deleted successfully.", {
                            icon: "success",
                            timer: 5000
                        }).then(function () {
                            location.reload();
                        });
                    },

                    error: function (data) {
                        swal("Something went wrong.", {
                            icon: "warning",
                        });
                    }
                });

            } else {

            }
        });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


}

function approveTransaction(id, el, approval) {
    var route = $(el).attr("data-route");

    if (approval == 1) {
        var approvalDescription = "Once approved, you will not be able to reverse this action!";
    } else {
        var approvalDescription = "Once rejected, you will not be able to reverse this action!";
    }

    swal({
        title: "Are you sure?",
        text: approvalDescription,
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then((willApprove) => {
            if (willApprove) {
                $.ajax({
                    type: 'POST',
                    url: route,
                    data: {
                        id: id,
                        approval: approval,
                    },
                    success: function (data) {
                        swal(data.msg, {
                            icon: "success",
                            timer: 5000
                        }).then(function () {
                            location.reload();
                        });
                    },

                    error: function (data) {
                        swal("Something went wrong.", {
                            icon: "warning",
                        });
                    }
                });

            } else {

            }
        });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


}




