// Header Scroll
$(document).ready(function(){
    $(window).scroll(function(){
        // if($(window).scrollTop() >= $(window).height()){
        if ($(this).scrollTop() > 100) {
            $(".header").addClass('header-scroll');
            $("a[href='#top']").removeClass('d-none');
        }
        else{
            $(".header").removeClass('header-scroll');
            $("a[href='#top']").addClass('d-none');
        }
    });
});
// Header Scroll

//Scroll To Top
$("a[href='#top']").click(function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
});
//Scroll To Top

//Product Information Slider
$(document).ready(function(){
    $('.main-product-view-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        speed:500,
        lazyLoad: 'ondemand',
        asNavFor: '.product-list-slider'
    });
    $('.product-list-slider').slick({
        slidesToShow: 9,
        slidesToScroll: 1,
        asNavFor: '.main-product-view-slider',
        dots: false,
        arrows: true,
        // lazyLoad: 'ondemand',
        autoPlay: false,
        centerMode: false,
        focusOnSelect: true,
        speed:500,
        infinite: false,

        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    infinite: false,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: false,
                }
            }
        ]
    });
});
//Product Information Slider

//Preloader
$(window).on("load", function () {
    setTimeout(function () {
        $('#preloader').fadeToggle()
    }, 500);
});

//Home News Card
$(document).ready(function() {
    $(".news-card").hover(function(){
        $(".news-card-inner", this).addClass('news-card-visible');
    }, function(){
        $(".news-card-inner", this).removeClass('news-card-visible');
    });
});

//Product Scroll
$(document).ready(function () {
    $('#productLink').on('click', function (){
        $("html, body").animate({
            scrollTop: $("#productInfo").offset().top
        }, 1000);
        return false;
    })
})

