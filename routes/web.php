<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::post('logout', 'Auth\LoginController@userLogout')->name('user-logout');

//Route::get('/home', 'HomeController@index')->name('home');

// Common Route
Route::get('/get-cities', 'CommonController@getCities')->name('get-cities');
Route::get('/get-bank-branches', 'CommonController@getBankBranches')->name('get-bank-branches');

Route::prefix('admin')->group(function(){

    Route::get('/','Auth\AdminLoginController@loginForm');
    Route::get('/login','Auth\AdminLoginController@loginForm')->name('admin.login');
    Route::post('/login-submit','Auth\AdminLoginController@login')->name('admin.login.submit');
    //Route::get('/', 'AdminController@index')->name('admin.dashboard');

    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

});

Route::middleware('auth')->group(function () {

    Route::get('/home', '\Modules\Admin\Http\Controllers\DashboardController@index')->name('home');

    Route::get('/permission-denied', function () {
        return view('no-permission');
    })->name('permission-denied');
});
